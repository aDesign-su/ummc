import { HttpClient, HttpEventType, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { cardObj } from './content/budget/card-obj/card-obj';
import { Equipments, WorkPackages, analogBudgets } from './content/references/analogy/analogy';
import { acts, ApiTreeData, ContractsCreates, factDetails, invoices, items, listContracts, payments, paymentsFact, prepay_invoices } from './content/contract/register-contracts/reg-contracts';
import {
  Package,
  ProjectFilter,
  DirectionFilter,
  ObjectFilter,
  SystemFilter,
  WorkFilter,
  EstimateFilter,
  Wbs,
  ElementFilter, PackageElements, DeleteEntity
} from './content/budget/wbs/wbs';
import { CurrencyExchange, Wacc, DiscountRate, Parameters, ParameterValue } from './content/references/macro/parameter';
import { Estimated, analog, getLevel1, getProject, workPackage } from './content/budget/estimated/estimated';
import { Project } from './content/poject/params-project/params';
import { Mastering, PlanDeviationData } from './content/reporting/mastering/mastering';
import { MasteringCause } from './content/reporting/mastering-cause/mastering-cause';
import { BudgetObject } from './content/budget/registry/registry'
import { NodeObject, SvzObject } from './content/budget/svz-documents/svz/svz'
import {
  SvzDocumentObject,
  SvzDocumentTableObject
} from './content/budget/svz-documents/svz-documents'
import { GenerateGpcs, Gpcs, GpcsForm, GpcsPrice } from './content/gpcs/gpcs/gpcs'
import { Gpfcs, GpfcsPrice } from './content/gpcs/gpcfs/gpfcs'
import {
  AddGpcsDocument, EditGpcsDocument,
  GpcsDocuments
} from './content/gpcs/gpcs-documents/gpcs-documents'
import { GpcsComponent } from './content/gpcs/gpcs/gpcs.component'
import { Ratio } from './content/references/ratio/ratio';
import { Employee, TeamHandbook } from './content/references/project-team-handbook/ProjectTeamHandbook';
import { Commons, ObjectBudget, RegisterBudget } from './content/budget/project-budget/registerBudget';
import { Role } from './content/references/role-reference/RoleReference';
import { Plan } from './content/financing/plan/plan'
// import {Fact} from './content/financing/fact/fact'
import { OperatingCostItemActual, OperatingCostItemPlan } from './content/budget/operating_costs/OperatingСosts';
import { FinancingFact } from './content/financing/fact/financing-fact'
import { RiskActual, RiskPlan } from './content/budget/risk/Risk';
import { ProjectRegister } from './content/project-register/project-register';
import { ProjectPortfolio } from './content/project-portfolio/project-portfolio';
import { Category } from './content/references/category/category';
import { IntangibleAssets } from './content/references/intangible-assets/intangible-assets'
import { RegisterEstimates, RegisterEstimatesGSFX } from './content/estimates/register-estimates/register-estimates';
import { Reserve, ReserveData, ReserveManagement } from './content/budget/reserve-management/reserve-management';
import { CommonCostsCirectory } from './content/references/general-project-costs/common-costs-cirectory';
import { getGkr } from './content/gpcs/gcr.plan/gkr';
import { getPpo } from './content/gpcs/ppo/ppo';
import { Source } from './content/references/source-value/source';
import { FactorAnalysis } from './content/reporting/factor-analysis/factor-analysis';
import { detailFactorAnalysis } from './content/reporting/factor-analysis/waterfall-chart-factor-analysis/waterfall-chart';
import { Item, SeparationSheetsItems } from './content/contracting/separation-sheet/separation-sheet-item/separation-sheet-item';
import { Total, getTotal } from './content/budget/total-project-costs/total';
import { NoDataRowOutlet } from '@angular/cdk/table';
import { Unit } from './content/references/directory-of-units-of-measurement/units';
import { GRANDEstimate } from './content/estimates/new-estimates/estimate';


@Injectable({
  providedIn: 'root'
})
export class ApicontentService {
  private httpOptions: any;
  private httpFile: any;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access'),
      })
    };
    this.httpFile = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem('access'),
      })
    };
  }

  baseAuthUrl = 'https://darkdes-django-4s0ap.tw1.ru/api/v1'  // new
  // baseAuthUrl = 'http://darkdes-django-z38bs.tw1.ru/api/v1'  // internet
  // baseAuthUrl = 'https://asbudget.ugmk.com/api/api/v1'  // local

  baseUrl = 'https://darkdes-django-4s0ap.tw1.ru'; // new
  // baseUrl = 'http://darkdes-django-z38bs.tw1.ru'; // internet
  // baseUrl = 'https://asbudget.ugmk.com/api'; // local



  // Сметы(новый модуль)-Вид работ
  getDataEstimateWorkTypesItem(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/estimate/${key}/work_types/`);
  }

  // ГРАНД-сметы//подробнее

  getDataEstimateMoreItem(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/estimate/get_gsfx/${key}`);
  }

  // ГРАНД-сметы//Загрузка
  uploadFilesGSFXEstimate(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);

    const url = `${this.baseUrl}/estimate/upload_gsfx/`;

    return this.http.post(url, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      map((event: any) => {
        if (event.type === HttpEventType.UploadProgress) {
          const progress = Math.round(100 * event.loaded / event.total);
          return { type: 'progress', value: progress };
        } else if (event.type === HttpEventType.Response) {
          return { type: 'complete', value: event.body };
        } else {
          return { type: 'unknown', value: null };
        }
      })
    );
  }
  //Реестр Смет Обновить
  updateEstimates(id, data) {
    const url = `${this.baseUrl}/estimate/${id}/`;
    return this.http.put(url, data, this.httpOptions);
  }
  // Основание сметы (селект)
  getPostEstimateReason(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/reason/`);
  }
  // Уровень сметы (селект)
  getPostEstimateLevel(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/level/`);
  }
  // Статусы сметы (селект)
  getPostEstimateStatus(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/status/`);
  }
  // Тип сметы (селект)
  getPostEstimateType(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/type/`);
  }
  costsEstimateInclude(id: number) {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/${id}/costs/include/`);
  }
  getGsfxData(id: number) {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/get_gsfx/${id}/`);
  }
  // Основание сметы
  getLevelEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/level/`);
  }
  // Основание сметы
  getReasonEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/reason/`);
  }
  // Пользователи сметы
  getUserEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/user/`);
  }
  // Код сметы
  getCodeEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/code/`);
  }

  // Тип сметы 
  getTypeEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/type/`);
  }

  // Статус сметы
  getStatusEstimate(): Observable<NodeObject[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimate/status/`);
  }


  // ГРАНД-Смета// Получить данные
  getDataGSFXEstimate() {
    return this.http.get<GRANDEstimate[]>(`${this.baseUrl}/estimate/get_gsfx/`);
  }
  addDocumentGsfx(data: any) {
    const url = `${this.baseUrl}/estimate/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    )
  }
  addEstimate(data: any) {
    const url = `${this.baseUrl}/estimate/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    )
  }

  // Справочник Едениц Измерения //Добавить еденицу измерения=
  createUnit(data) {
    const url = `${this.baseUrl}/references/unit_of_measure_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  // Справочник Едениц Измерения //Обновить еденицу измерения=
  updateUnit(id: number, UpdatedData) {
    const url = `${this.baseUrl}/references/unit_of_measure/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  // Справочник Едениц Измерения //Удалить еденицу измерения
  delUnits(id) {
    const url = `${this.baseUrl}/references/unit_of_measure/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  // Справочник Едениц Измерения //Получить данные
  getDataUnits() {
    return this.http.get<Unit[]>(`${this.baseUrl}/references/unit_of_measure/`);
  }

  //Остаток по договорам //Получить Контракты по id

  getBalanceUnderContractDetail(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contracts/remains/${key}`);
  }



  //Остаток по договорам 
  getBalanceUnderContracts(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contracts/remains/`);
  }




  //СДР по контрактам//Получить данные(по конкретному проекту)
  getSdrToContractsItem(id): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contracts/tree_wbs/${id}/`);
  }

  //СДР по контрактам/Получть СДР
  getSDRContracts(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contracts/tree_wbs/`);
  }


  //Primavera
  getDataSource(): Observable<any[]> {
    let projectId = localStorage.getItem('project_id');
    return this.http.get<any[]>(`${this.baseUrl}/primavera/get_wbs_primavera/?project_id=${projectId}`)
  }

  //Статусы проекта //Улаоить
  delStatusProject(id) {
    const url = `${this.baseUrl}/references/status/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Справочник ресурсов // Список категорий
  getTypeResourceList() {
    return this.http.get<any>(`${this.baseUrl}/references/resource_type/`);
  }
  //Статусы проекта //Обновтиь
  editStatusProject(id: number, UpdatedData) {
    const url = `${this.baseUrl}/references/status/${id}/`;
    return this.http.put(url, UpdatedData);
  }



  // /Единичные раценки//Ресурсы
  getResourceUnitPrices(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/unit_rates/unit_rate_resource/`);
  }


  // /Единичные раценки//Ресурсы// Удаление
  deleResourceUnitPrices(id) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_resource/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  // /Единичные раценки//Ресурсы/// Добавиь 
  addResourceUnitPrices(data) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_resource_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  // /Единичные раценки//Ресурсы/// Изменить
  updateResourceUnitPrices(id: number, UpdatedData) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_resource/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }

  //Единичные раценки. Общая таблица
  getFullUnitRate(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/unit_rates/unit_rates/`);
  }


  //Единичные раценки
  getUnitRate(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/unit_rates/unit_rate/`);
  }

  //Единичные раценки // Добавиь 
  addUnitPrices(data) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  //Единичные раценки // Удаление
  deleUnitPrices(id) {
    const url = `${this.baseUrl}/unit_rates/unit_rate/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Единичные раценки // Изменить единичную расценку
  updateUnitPrices(id: number, UpdatedData) {
    const url = `${this.baseUrl}/unit_rates/unit_rate/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  //Единичные раценки // Список каитегорий
  getCategoryUnitPrices() {
    return this.http.get<any>(`${this.baseUrl}/unit_rates/unit_rate_category/`);
  }
  //Единичные раценки // Список каитегорий//Добавить
  addCategoryUnitPrices(data) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_category_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  //Единичные раценки // Список каитегорий//Изменить
  updateCategoryUnitPrices(id: number, UpdatedData) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_category/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  //Единичные раценки // Список каитегорий//Удалить
  deleCategoryUnitPrices(id) {
    const url = `${this.baseUrl}/unit_rates/unit_rate_category/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }




  //Справочник ресурсов // Список каитегорий //Удалить каитегорию
  deleResourceDirectoryСategory(id) {
    const url = `${this.baseUrl}/references/resource_category/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Справочник ресурсов // Список типов/Удалить тип
  deleResourceDirectoryType(id) {
    const url = `${this.baseUrl}/references/resource_type/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Справочник ресурсов // Список категорий
  getCategoryList() {
    return this.http.get<any>(`${this.baseUrl}/references/resource_category/`);
  }

  //Справочник ресурсов // Обновить тип ресурса
  updateResourceDirectoryСategory(id: number, UpdatedData) {
    const url = `${this.baseUrl}/references/resource_category/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }

  //Справочник ресурсов // Обновить тип ресурса
  updateResourceDirectoryType(id: number, UpdatedData) {
    const url = `${this.baseUrl}/references/resource_type/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }

  //Справочник ресурсов // Обновить ресурс
  updateResourceDirectory(id: number, UpdatedData) {
    const url = `${this.baseUrl}/references/resource/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  //Справочник ресурсов // Удалить ресурс
  deleResourceDirectory(id) {
    const url = `${this.baseUrl}/references/resource/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }


  //Справочник ресурсов // Добавиь категорию ресурса
  addResourceDirectoryСategory(data) {
    const url = `${this.baseUrl}/references/resource_category_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }


  //Справочник ресурсов // Добавиь тип ресурса
  addResourceDirectoryType(data) {
    const url = `${this.baseUrl}/references/resource_type_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  //Справочник ресурсов // Добавиь ресурс
  addResourceDirectory(data) {
    const url = `${this.baseUrl}/references/resource_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }


  //Справочник ресурсов
  getResourceDirectory(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/references/resource/`);
  }


  // Еденицы Измерений
  getUnitsOfMeasurement(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/references/unit_of_measure/`);
  }


  //Статусы проекта //Добавит статус
  addStatusProject(data) {
    const url = `${this.baseUrl}/references/status_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  //Статусы проекта
  getStatusProjectDataSheets(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/references/status/`);
  }
  //Статусы проекта //Удалить документ
  delPortfolioReportDocument(id) {
    const url = `${this.baseUrl}/portfolio_report/documents/${id}/`;
    return this.http.delete(url, this.httpFile);
  }
  //Статусы проекта //Обновтиь документ
  editPortfolioReportDocument(id: number, UpdatedData) {
    const url = `${this.baseUrl}/portfolio_report/documents/${id}/`;
    return this.http.put(url, UpdatedData);
  }

  //Статусы проекта //Добавит документ
  addPortfolioReportDocument(data) {
    const url = `${this.baseUrl}/portfolio_report/documents_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  // Отчет по портфелю//Изменить проект
  editPortfolioReportProject(id: number, UpdatedData) {
    const url = `${this.baseUrl}/portfolio_report/portfolio_report/${id}/`;
    return this.http.put(url, UpdatedData);
  }

  // Отчет по портфелю//Добавить проект
  addPortfolioReportProject(data) {
    const url = `${this.baseUrl}/portfolio_report/portfolio_report_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  // Отчет по портфелю//Получить данные(по конкретному проекту)
  getPortfolioReportItemData(id): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/portfolio_report/portfolio_report_id/${id}/`);
  }

  // Отчет по портфелю//Получить данные
  getPortfolioReportData(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/portfolio_report/portfolio_report/`);
  }





  // id_contract_package-id конрактного пакета из Реестра КП
  //id_object- id объетка
  getWBSContractPackagesDetail(id_contract_package, id_object): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contract_packages/registry/${id_contract_package}/node/${id_object}/`);
  }

  //Реестр КП /Удалить контрактный пакет
  delContractPackagesItem(id) {
    const url = `${this.baseUrl}/contract_packages/registry/package/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  // Контрактные пакеты // Реддактирование стоимости тендера
  editContractPackagesItemTender(id: number, UpdatedData) {
    const url = `${this.baseUrl}/contract_packages/registry/package/${id}/`;
    return this.http.put(url, UpdatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }

  // Контрактные пакеты // Добавиь
  addContractPackagesItem(id_contract_package, data) {
    const url = `${this.baseUrl}/contract_packages/registry/${id_contract_package}/package/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }


  // Контрактные пакеты//Получить один КП по id
  // key=0 - id из реестра контрактных пакетов
  getContractPackagesDetail(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contract_packages/registry/${key}/package/`);
  }


  // Изменить типовой контрактный пакет
  editContractPackages(id: number, UpdatedData) {
    const url = `${this.baseUrl}/contract_packages/registry/put/${id}/`;
    return this.http.put(url, UpdatedData);
  }

  //Реестр КП // Добавиь контрактных пакетов
  addContractPackages(data) {
    const url = `${this.baseUrl}/contract_packages/registry/0/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  //Реестр КП /Удалить контрактный пакет
  delContractPackages(id) {
    const url = `${this.baseUrl}/contract_packages/registry/put/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  //Реестр КП // Контрактные пакеты//Получить пакеты
  // key=0 - получить все пакеты
  // key=num - получить пакеты по конкретнопу проекту
  getContractPackages(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contract_packages/registry/${key}/`);
  }





  // Изменить типовой контрактный пакет
  editDirectoryContractPackages(id: number, UpdatedData) {
    const url = `${this.baseUrl}/contract_packages/directory/put/${id}/`;
    return this.http.put(url, UpdatedData);
  }

  //Справочник контрактных пакето/Удалить типовой контрактный пакет
  delDirectoryContractPackages(id) {
    const url = `${this.baseUrl}/contract_packages/directory/put/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  // Справочник типовых контрактных пакетов
  addDirectoryContractPackages(data) {
    const url = `${this.baseUrl}/contract_packages/directory/0/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  // Справочник контрактных пакетов//Получить пакеты
  // key=0 - получить все пакеты
  // key=num - получить пакеты по конкретнопу проекту
  getDirectoryContractPackages(key: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/contract_packages/directory/${key}/`);
  }


  // Изменить Предмет Разделительной ведомости
  editSeparationSheetItem(id: number, UpdatedData: Item) {
    const url = `${this.baseUrl}/separation_sheets/update/item/${id}/`;
    return this.http.put(url, UpdatedData);
  }

  //Удалить предемеит из Разделительной ведомоти
  delSeparationSheetItem(id) {
    const url = `${this.baseUrl}/separation_sheets/update/item/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  //Выгрузка в Excel Разделительна ведомость
  downloadSeparationSheetItemFile(id): any {
    const url = `${this.baseUrl}/separation_sheets/download/${id}/excel`
    return this.http.get(url, { responseType: 'blob' });
  }
  // Разделительная ведомость//План в факт
  updateSeparationSheets(id, updatedData) {
    const url = `${this.baseUrl}/separation_sheets/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  // Разделительная ведомость//Получить План
  getSeparationSheetsPlan(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/separation_sheets/plan/`);
  }
  // Разделительная ведомость//Получить Факт
  getSeparationSheetsFact(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/separation_sheets/fact/`);
  }
  // Разделительная ведомость//Удалить по id
  deliteSeparationSheet(id) {
    const url = `${this.baseUrl}/separation_sheets/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  updateSeparationSheet(id, updatedData) {
    const url = `${this.baseUrl}/separation_sheets/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }

  // Разделительная ведомость//Получить  Детали
  getSeparationSheetsDetail(id): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/separation_sheets/${id}/detail/`);
  }

  // Разделительная ведомость//Cоздать разделительную ведомость
  addSeparationSheet(data) {
    const url = `${this.baseUrl}/separation_sheets/create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }
  // Разделительная ведомость//Cоздать материал оборудование принадлежащее разделительной ведомости
  addSeparationSheetItem(data) {
    const url = `${this.baseUrl}/separation_sheets/create/item/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }






  // Получить источник стоимости
  getBudgetSource(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/budget/get_budget_source/`);
  }

  //Факторный анализ //Удалить Влияние на текущий бюджет проекта
  deliteInfluence(id) {
    const url = `${this.baseUrl}/financial_records/factor/influence/update/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Факторный анализ //Обновить  Влияние на текущий бюджет проекта
  updateInfluence(id, updatedData) {
    const url = `${this.baseUrl}/financial_records/factor/influence/update/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }


  //Факторный анализ //Добавить Влияние на текущий бюджет проекта
  addInfluence(data) {
    const url = `${this.baseUrl}/financial_records/factor/create/influence`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  //Факторный анализ //Удалить
  deliteFactorAnalysis(id) {
    const url = `${this.baseUrl}/financial_records/factor/update/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Факторный анализ //Обновить факторный анализ
  updateFactorAnalysi(id, updatedData) {
    const url = `${this.baseUrl}/financial_records/factor/update/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }

  //Факторный анализ //Добавить факторный нализ
  addFactorAnalysis(data) {
    const url = `${this.baseUrl}/financial_records/factor/create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => { return response; })
    );
  }

  //Факторный анализ получить данные
  getFactorAnalysis() {
    return this.http.get<FactorAnalysis[]>(`${this.baseUrl}/financial_records/factor/`);
  }
  //Факторный анализ получить данные(Для графика по id)
  getFactorAnalysisChart(id) {
    return this.http.get<detailFactorAnalysis>(`${this.baseUrl}/financial_records/factor/${id}/`);
  }

  // Управление резервами //строки конкретного реестра резерва
  getReserve(id): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/reserve/reserve/${id}/`);
  }

  // Управление резервами //строки конкретного реестра резерва//Удаление
  deliteReserve(id) {
    const url = `${this.baseUrl}/reserve/reserve_update/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  // Управление резервами //строки конкретного реестра резерва/Обновление резерва
  updateReserve(id, data) {
    const url = `${this.baseUrl}/reserve/reserve_update/${id}/`;
    return this.http.put(url, data, this.httpOptions);
  }
  // Добавить транзакию к конкретной строчке бюжета 
  addTransactionReserve(formData) {
    const url = `${this.baseUrl}/reserve/reserve_transaction/`;
    return this.http.post(url, formData);
  }
  // Управление резервами //строки конкретного реестра резерва/Создание новгого резерва
  addReserve(formData) {
    const url = `${this.baseUrl}/reserve/reserve_create/`;
    return this.http.post(url, formData);
  }

  //Управление резервами получить данные
  getReserveManagement() {
    return this.http.get<ReserveData[]>(`${this.baseUrl}/reserve/registered_reserve/`);
  }
  //Управление резервами Обновить
  updateReserveManagement(id, data) {
    const url = `${this.baseUrl}/reserve/registered_reserve_update/${id}/`;
    return this.http.put(url, data, this.httpOptions);
  }
  //Управление резервами Удалить
  delReserveManagement(id) {
    const url = `${this.baseUrl}/reserve/registered_reserve_update/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }

  //Управление резервами Генирация бюджета
  generateReserveManagement(formData) {
    const url = `${this.baseUrl}/reserve/generate_reserve/`;
    return this.http.post(url, formData);
  }
  //Управление резервами Добавить
  addReserveManagement(formData) {
    const url = `${this.baseUrl}/reserve/registered_reserve_create/`;
    return this.http.post(url, formData);
  }



  //Реестр Смет Обновить
  updateRegisterEstimates(id, data) {
    const url = `${this.baseUrl}/estimate/estimate_cost_all_fields/${id}/`;
    return this.http.put(url, data, this.httpOptions);
  }
  //Реестр Смет Добавить
  addRegisterEstimates(formData) {
    const url = `${this.baseUrl}/estimate/estimate_cost_create/`;
    return this.http.post(url, formData);
  }
  uploadRegisterEstimatesGSFX(formData) {
    const url = `${this.baseUrl}/estimate/upload_gsfx/`;
    return this.http.post(url, formData);
  }


  //Реестр Смет получить данные
  getRegisterEstimates() {
    return this.http.get<RegisterEstimates[]>(`${this.baseUrl}/estimate/register/`);
  }
  getRegisterEstimatesGSFX() {
    return this.http.get<RegisterEstimatesGSFX[]>(`${this.baseUrl}/estimate/get_gsfx/`);
  }
  getRegisterEstimatesIsBufer() {
    return this.http.get<RegisterEstimates[]>(`${this.baseUrl}/estimate/register/?is_buffer=true`);
  }
  updateRegisterEstimatesIsBufer(id, data) {
    const url = `${this.baseUrl}/estimate/${id}/`;
    return this.http.put(url, data);
  }


  //Портфель проектов получить данные
  getProjectsPortfolio() {
    return this.http.get<ProjectPortfolio[]>(`${this.baseUrl}/portfolio/get_portfolio/`);
  }
  //Портфель проектов Создать портфель
  addProjectsPortfolio(data) {
    const url = `${this.baseUrl}/portfolio/portfolio/`;
    return this.http.post(url, data, this.httpOptions).pipe(map((response: any) => response)
    )
  }
  updateProjectsPortfolio(id, data) {
    const url = `${this.baseUrl}/portfolio/portfolio/${id}/`;
    return this.http.put(url, data, this.httpOptions);
  }

  //Реестр проектов
  delProjectsPortfolio(id) {
    const url = `${this.baseUrl}/portfolio/portfolio/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  // ///////////////////////////////////////////////////////////////////////////// //
  //Реестр проектов получить проекты
  getProjects() {
    return this.http.get<ProjectRegister[]>(`${this.baseUrl}/get_projects/`);
  }

  addProject(project: ProjectRegister) {
    const url = `${this.baseUrl}/create/projects/`;
    return this.http.post(url, project);
  }

  editProject(project: ProjectRegister) {
    const url = `${this.baseUrl}/update/projects/${project.id}/`;
    return this.http.put(url, project);
  }

  removeProject(id: number) {
    const url = `${this.baseUrl}/delete/projects/${id}/`;
    return this.http.delete(url);
  }

  //Риски,Факт Получить данные
  getDataRiskActual() {
    return this.http.get<RiskActual[]>(`${this.baseUrl}/risks/budget_actual/`);
  }

  //Риски,План Получить данные
  getDataRiskPlan() {
    return this.http.get<RiskPlan[]>(`${this.baseUrl}/risks/budget_plan/`);
  }
  //Риски ,План Добавть
  addRiskPlan(data) {
    const url = `${this.baseUrl}/risks/create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  //Риски ,План Обновить
  updateRiskPlan(id, updatedData) {
    const url = `${this.baseUrl}/risks/budget_plan/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  //Риски,Удаляет одновременно план и факт
  delRisk(id) {
    const url = `${this.baseUrl}/risks/budget_plan/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  updateRiskActual(id, updatedData) {
    const url = `${this.baseUrl}/risks/budget_actual/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }




  //Общепроектные затраты,План Получить данные
  getDataOperatingCostsActual() {
    return this.http.get<OperatingCostItemActual[]>(`${this.baseUrl}/operating_costs/get_operating_costs_actual/`);
  }
  //Общепроектные затраты,Факт Обновить
  updateOperatingCostsActual(id, updatedData) {
    const url = `${this.baseUrl}/operating_costs/update_operating_costs_actual/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  //Общепроектные затраты,План-удалить
  delOperatingCostPlan(id) {
    const url = `${this.baseUrl}/operating_costs/update_delete_operating_costs_plan/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  //Общепроектные затраты,План Обновить
  updateOperatingCostsPlan(id, updatedData) {
    const url = `${this.baseUrl}/operating_costs/update_delete_operating_costs_plan/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  //Общепроектные затраты,План Добавть
  addOperatingCostsPlan(data) {
    const url = `${this.baseUrl}/operating_costs/create_operating_costs_plan/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  //Общепроектные затраты,План Получить данные
  getDataOperatingCostsPlan() {
    return this.http.get<OperatingCostItemPlan[]>(`${this.baseUrl}/operating_costs/get_operating_costs_plan/`);
  }

  //Общепроектные затраты get
  getCommonCosts() {
    return this.http.get<Total[]>(`${this.baseUrl}/wbs/get_common_node_tree/`);
  }
  getGeneralProjectBudget(id: number) {
    return this.http.get<Total[]>(`${this.baseUrl}/budget/get_general_project/${id}/`);
  }
  delGeneralProjectBudget(id: number) {
    const url = `${this.baseUrl}/budget/delete_general_project/${id}/`;
    return this.http.delete(url);
  }
  updGeneralProjectBudget(id: number, UpdatedAsset: Total) {
    const url = `${this.baseUrl}/budget/update_general_project/${id}/`;
    return this.http.post(url, UpdatedAsset).pipe(
      map((response: any) => response)
    )
  }
  // updGeneralProjectBudget(id: number, UpdatedAsset: Total) {
  //   const url = `${this.baseUrl}/budget/update_general_project/${id}/`;
  //   return this.http.put(url, UpdatedAsset);
  // }
  addFilterNodeFlat(AddedSvz: SvzObject) {
    const url = `${this.baseUrl}/wbs/get_general_project_node_flat/`;
    return this.http.post(url, AddedSvz).pipe(
      map((response: any) => response)
    )
  }
  createGeneralProjectCosts(data: CommonCostsCirectory) {
    const url = `${this.baseUrl}/budget/create_general_project/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editCommonCostsDirectory(id: number, UpdatedAsset: CommonCostsCirectory) {
    const url = `${this.baseUrl}/wbs/update_common_costs_directory/${id}/`;
    return this.http.put(url, UpdatedAsset);
  }

  // Справочник General project costs
  getGeneralProjectCosts() {
    return this.http.get<CommonCostsCirectory[]>(`${this.baseUrl}/wbs/get_common_costs_directory/`);
  }
  getGeneralProjectCostsId(data: CommonCostsCirectory) {
    return this.http.get<CommonCostsCirectory[]>(`${this.baseUrl}/wbs/get_common_costs_directory/${data}/`);
  }
  addGeneralProjectCosts(data: CommonCostsCirectory) {
    const url = `${this.baseUrl}/wbs/create_common_costs_directory/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editGeneralProjectCosts(UpdatedAsset: CommonCostsCirectory) {
    const url = `${this.baseUrl}/wbs/update_common_costs_directory/${UpdatedAsset.id}/`;
    return this.http.put(url, UpdatedAsset);
  }
  delGeneralProjectCosts(id: number) {
    const url = `${this.baseUrl}/wbs/delete_common_costs_directory/${id}/`;
    return this.http.delete(url);
  }

  // Справочник Currency
  getCurrencyReference() {
    return this.http.get<Category[]>(`${this.baseUrl}/references/currency/`);
  }
  addCurrency(data: Category) {
    const url = `${this.baseUrl}/references/currency/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editCurrency(editCurrency: number, updatedData: any): Observable<Category[]> {
    const url = `${this.baseUrl}/references/currency/${editCurrency}`;
    return this.http.put<Category[]>(url, updatedData);
  }
  delCurrency(delCurrency: Category) {
    const url = `${this.baseUrl}/references/currency/${delCurrency}`;
    return this.http.delete(url);
  }
  // Справочник Region
  getRegionReference() {
    return this.http.get<Category[]>(`${this.baseUrl}/references/region/`);
  }
  addRegion(data: Category) {
    const url = `${this.baseUrl}/references/region/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editRegion(editRegion: number, updatedData: any): Observable<Category[]> {
    const url = `${this.baseUrl}/references/region/${editRegion}/`;
    return this.http.put<Category[]>(url, updatedData);
  }
  delRegion(delRegion: Category) {
    const url = `${this.baseUrl}/references/region/${delRegion}/`;
    return this.http.delete(url);
  }
  // Справочник Оборудования
  getEquipmentReference() {
    return this.http.get<Category[]>(`${this.baseUrl}/references/analog_equipment_type/`);
  }
  addEquip(data: Category) {
    const url = `${this.baseUrl}/references/analog_equipment_type/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editEquip(editEquip: number, updatedData: any): Observable<Category[]> {
    const url = `${this.baseUrl}/references/analog_equipment_type/${editEquip}`;
    return this.http.put<Category[]>(url, updatedData);
  }
  delEquip(delEquip: Category) {
    const url = `${this.baseUrl}/references/analog_equipment_type/${delEquip}`;
    return this.http.delete(url);
  }
  // Справочник Объекта
  getTypeReference() {
    return this.http.get<Category[]>(`${this.baseUrl}/references/analog_budget_type/`);
  }
  addType(data: Category) {
    const url = `${this.baseUrl}/references/analog_budget_type/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editType(editType: number, updatedData: any): Observable<Category[]> {
    const url = `${this.baseUrl}/references/analog_budget_type/${editType}`;
    return this.http.put<Category[]>(url, updatedData);
  }
  delType(delEquip: Category) {
    const url = `${this.baseUrl}/references/analog_budget_type/${delEquip}`;
    return this.http.delete(url);
  }
  // Справочник Пакетов работ
  getPackageReference() {
    return this.http.get<Category[]>(`${this.baseUrl}/references/analog_package_type`);
  }
  addPack(data: Category) {
    const url = `${this.baseUrl}/references/analog_package_type`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  editPackage(editPackage: number, updatedData: any): Observable<Category[]> {
    const url = `${this.baseUrl}/references/analog_package_type/${editPackage}`;
    return this.http.put<Category[]>(url, updatedData);
  }
  delPackage(delEquip: Category) {
    const url = `${this.baseUrl}/references/analog_package_type/${delEquip}`;
    return this.http.delete(url);
  }



  // Нематериальные активы
  getIntangibleAssets() {
    const url = `${this.baseUrl}/wbs/get_common_intangible_assets/`;
    return this.http.get(url);
  }

  addIntangibleAsset(AddedAsset: IntangibleAssets) {
    const url = `${this.baseUrl}/wbs/create_common_intangible_assets/`;
    return this.http.post(url, AddedAsset);
  }

  updateIntangibleAsset(UpdatedAsset: IntangibleAssets) {
    const url = `${this.baseUrl}/wbs/update_common_intangible_assets/${UpdatedAsset.id}/`;
    return this.http.put(url, UpdatedAsset);
  }

  deleteIntangibleAsset(id: number) {
    const url = `${this.baseUrl}/wbs/delete_common_intangible_assets/${id}/`;
    return this.http.delete(url);
  }

  // Справочник ролей
  getRoleReference() {
    return this.http.get<Role[]>(`${this.baseUrl}/role_reference/roles/`);
  }

  // Справочник проектной команды
  getTeamHandbook() {
    return this.http.get<TeamHandbook[]>(`${this.baseUrl}/project_team/employees/`);
  }
  // Справочник проектной команды// Добавть
  AddEmployee(data: Employee) {
    const url = `${this.baseUrl}/project_team/create/employees/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  // Справочник проектной команды// Удалить
  DelEmployee(id) {
    const url = `${this.baseUrl}/project_team/delete/employees/${id}`;
    return this.http.delete(url, this.httpOptions);
  }
  // Справочник проектной команды// Обновить
  UpdateEmployee(EmployeeId: number, updatedData: any) {
    const url = `${this.baseUrl}/project_team/update/employees/${EmployeeId}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }

  //Освоение Факт
  getUtilizationFact(year: number = 0): Observable<FinancingFact[]> {
    const url = `${this.baseUrl}/utilization/fact/${year}/`;
    return this.http.get<FinancingFact[]>(url);
  }

  updateUtilizationFact(): Observable<any> {
    const url = `${this.baseUrl}/utilization/fact/update/`;
    return this.http.get<any>(url);
  }

  //Освоение План
  getUtilizationPlan(year: number = 0): Observable<Plan[]> {
    const url = `${this.baseUrl}/utilization/plan/${year}/`;
    return this.http.get<Plan[]>(url);
  }

  updateUtilizationPlan(): Observable<any> {
    const url = `${this.baseUrl}/utilization/plan/update/`;
    return this.http.get<any>(url);
  }

  getUtilizationModes(id: number) {
    const url = `${this.baseUrl}/utilization/plan/${id}/update_mode/`;
    return this.http.get<any>(url);
  }

  updateUtilizationModes(Mode: any, id: number) {
    const url = `${this.baseUrl}/utilization/plan/${id}/update_mode/`;
    return this.http.post(url, Mode);
  }


  //ГПКС
  getGpcs(): Observable<Gpcs[]> {
    return this.http.get<Gpcs[]>(`${this.baseUrl}/budget/get_gpcs/`)
  }

  getGpcsFiltered(Filter: Object): Observable<Gpcs[]> {
    const url = `${this.baseUrl}/budget/get_gpcs/`;
    return this.http.post(url, Filter).pipe(
      map((response: any) => response));
  }

  getGpcsPrice(ReceivedPrice: object) {
    const url = `${this.baseUrl}/budget/get_gpcs_price/`;
    return this.http.post(url, ReceivedPrice).pipe(
      map((response: any) => response));
  }

  addGpcs(AddedElement: Gpcs) {
    const url = `${this.baseUrl}/budget/create_gpcs/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }

  updateGpcs(UpdatedGpcs: Gpcs) {
    const url = `${this.baseUrl}/budget/update_gpcs/${UpdatedGpcs.id}/`;
    return this.http.put(url, UpdatedGpcs).pipe(
      map((response: any) => response));
  }

  deleteGpcs(DeletedGpcs: number) {
    const url = `${this.baseUrl}/budget/delete_gpcs/${DeletedGpcs}/`;
    return this.http.delete(url).pipe(
      map((response: any) => response));
  }

  addGpcsPriceObject(AddedObject: GpcsPrice) {
    const url = `${this.baseUrl}/budget/create_gpcs_price/`;
    return this.http.post(url, AddedObject).pipe(
      map((response: any) => response));
  }

  updateGpcsPriceObject(UpdatedObject: GpcsPrice) {
    const url = `${this.baseUrl}/budget/update_gpcs_price/${UpdatedObject.id}/`;
    return this.http.put(url, UpdatedObject).pipe(
      map((response: any) => response));
  }

  deleteGpcsPriceObject(DeletedObject: number) {
    const url = `${this.baseUrl}/budget/delete_gpcs_price/${DeletedObject}/`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response));
  }

  // deleteBudgetObject(DeletedBudget: number): Observable<any> {
  //   const url = `${this.baseUrl}/budget/delete_budget_package/${DeletedBudget}`;
  //   return this.http.delete<any>(url).pipe(
  //     map((response: any) => response)
  //   )
  // }


  //ГПФКС
  getGpfcs(): Observable<Gpfcs[]> {
    return this.http.get<Gpfcs[]>(`${this.baseUrl}/budget/get_gpfcs/`)
  }

  getGpfcsFiltered(Filter: Object): Observable<Gpfcs[]> {
    const url = `${this.baseUrl}/budget/get_gpfcs/`;
    return this.http.post(url, Filter).pipe(
      map((response: any) => response));
  }

  getGpfcsPrice(ReceivedPrice: object) {
    const url = `${this.baseUrl}/budget/get_gpfcs_price/`;
    return this.http.post(url, ReceivedPrice).pipe(
      map((response: any) => response));
  }

  addGpfcs(AddedElement: Gpfcs) {
    const url = `${this.baseUrl}/budget/create_gpfcs/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }

  updateGpfcs(UpdatedGpfcs: Gpfcs) {
    const url = `${this.baseUrl}/budget/update_gpfcs/${UpdatedGpfcs.id}/`;
    return this.http.put(url, UpdatedGpfcs).pipe(
      map((response: any) => response));
  }

  deleteGpfcs(DeletedGpfcs: number) {
    const url = `${this.baseUrl}/budget/delete_gpfcs/${DeletedGpfcs}/`;
    return this.http.delete(url).pipe(
      map((response: any) => response));
  }

  addGpfcsPriceObject(AddedObject: GpfcsPrice) {
    const url = `${this.baseUrl}/budget/create_gpfcs_price/`;
    return this.http.post(url, AddedObject).pipe(
      map((response: any) => response));
  }

  updateGpfcsPriceObject(UpdatedObject: GpfcsPrice) {
    const url = `${this.baseUrl}/budget/update_gpfcs_price/${UpdatedObject.id}/`;
    return this.http.put(url, UpdatedObject).pipe(
      map((response: any) => response));
  }

  deleteGpfcsPriceObject(DeletedObject: number) {
    const url = `${this.baseUrl}/budget/delete_gpfcs_price/${DeletedObject}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response));
  }


  //ГПКС/ГПКФС документ
  getGpcsDocuments(): Observable<GpcsDocuments[]> {
    return this.http.get<GpcsDocuments[]>(`${this.baseUrl}/budget/get_gpcs_document/`)
  }

  getGenerateGpcs(GenerateGpcs: GenerateGpcs) {
    return this.http.post(`${this.baseUrl}/budget/get_generate_gpcs/`, GenerateGpcs)
  }

  getGpcsForm(GpcsDocument: number) {
    return this.http.get(`${this.baseUrl}/budget/get_gpcs_form1/${GpcsDocument}/`)
  }

  getGpfcsDocuments(): Observable<GpcsDocuments[]> {
    return this.http.get<GpcsDocuments[]>(`${this.baseUrl}/budget/get_gpfcs_document/`)
  }

  addGpcsDocument(AddedElement: AddGpcsDocument) {
    const url = `${this.baseUrl}/budget/create_gpcs_document/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }

  addGpfcsDocument(AddedElement: AddGpcsDocument) {
    const url = `${this.baseUrl}/budget/create_gpfcs_document/`;
    return this.http.post(url, AddedElement).pipe(
      map((response: any) => response));
  }

  editGpcsDocument(UpdatedElement: EditGpcsDocument) {
    const url = `${this.baseUrl}/budget/update_gpcs_document/${UpdatedElement.id}/`;
    return this.http.put(url, UpdatedElement).pipe(
      map((response: any) => response));
  }

  editGpcsForm(UpdatedElement: GpcsForm) {
    const url = `${this.baseUrl}/budget/update_gpcs_form1/${UpdatedElement.id}/`;
    return this.http.put(url, UpdatedElement).pipe(
      map((response: any) => response));
  }

  editGpfcsDocument(UpdatedElement: EditGpcsDocument) {
    const url = `${this.baseUrl}/budget/update_gpfcs_document/${UpdatedElement.id}/`;
    return this.http.put(url, UpdatedElement).pipe(
      map((response: any) => response));
  }

  deleteGpcsDocument(DeletedElement: number) {
    const url = `${this.baseUrl}/budget/delete_gpcs_document/${DeletedElement}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response)
    )
  }


  deleteGpfcsDocument(DeletedElement: number) {
    const url = `${this.baseUrl}/budget/delete_gpfcs_document/${DeletedElement}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response)
    )
  }

  //Освоение Отклонение по исполнению плана Удалить Причину отклонения
  deleteMasteringCause(id) {
    const url = `${this.baseUrl}/financial_records/delete/financing/${id}`;
    return this.http.delete(url, this.httpOptions);
  }

  //Освоение Отклонение по исполнению планаДобавить Причину отклонения
  editMasteringCause(MasteringCause, id) {
    const url = `${this.baseUrl}/financial_records/update/financing/${id}`;
    return this.http.put(url, MasteringCause, this.httpOptions);
  }

  //Освоение Отклонение по исполнению плана Добавить Причину отклонения
  addMasteringCause(MasteringCause) {
    const url = `${this.baseUrl}/financial_records/create/financing/`;
    console.log(url)
    console.log(MasteringCause)
    return this.http.post(url, MasteringCause).pipe(
      map((response: any) => response.body)
    );
  }


  downloadFile(): any {
    const url = `${this.baseUrl}/financial_records/generate-excel/`
    return this.http.get(url, { responseType: 'blob' });
  }

  //Освоение Отклонение по исполнению плана
  getDataPlanDeviation(first_date, second_date) {
    return this.http.get<PlanDeviationData[]>(`${this.baseUrl}/financial_records/month_records/compare/?first_date=${first_date}&second_date=${second_date}`);
  }
  //Освоение Причины отклонения
  getDataMasteringCause(startDate?: Date, endDate?: Date) {
    let url = `${this.baseUrl}/financial_records/financing/`;
    const params = [];
    if (startDate) {
      params.push(`start_date=${startDate.toISOString().split('T')[0]}`);
    }
    if (endDate) {
      params.push(`end_date=${endDate.toISOString().split('T')[0]}`);
    }
    if (params.length) {
      url += `?${params.join('&')}`;
    }

    console.log(url)
    return this.http.get<MasteringCause[]>(url);
  }

  //Освоение
  getDataMastering(date) {
    return this.http.get<Mastering[]>(`${this.baseUrl}/financial_records/month_records/${date}/`);
  }

  // Параметры проекта
  getDataParamProj() {
    return this.http.get<Project[]>(`${this.baseUrl}/project_parameters/key_project_value/`);
  }
  // Добавление проекта
  addKeyProjectList(Project: Project): Observable<Project[]> {
    const url = `${this.baseUrl}/project_parameters/create_project_key_value/`;
    return this.http.post(url, Project).pipe(
      map((response: any) => response.body)
    );
  }
  // Вывод проекта
  getKeyProjectList(ProjectId: number): Observable<Project[]> {
    const url = `${this.baseUrl}/project_parameters/create_project_key_value/${ProjectId}/update/`;
    return this.http.get<Project[]>(url);
  }
  // Обновление проекта
  updateKeyProjectList(ProjectId: number, updatedData: any): Observable<any> {
    const url = `${this.baseUrl}/project_parameters/keyprojectvalue/${ProjectId}/update/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }

  // Обновление параметра проекта
  updatePortfolioProject(PortfolioId: number, updatedData: any): Observable<any> {
    const url = `${this.baseUrl}/project_parameters/project/${PortfolioId}/update/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  // Удаление проекта
  deleteKeyProjectList(ProjectId: number): Observable<any> {
    const url = `${this.baseUrl}/project_parameters/keyprojectvalue/${ProjectId}/delete/`;
    return this.http.delete(url, this.httpOptions);
  }


  // График проекта
  getCharts() {
    return this.http.get<Project[]>(`${this.baseUrl}/wbs/get_node_flat/`);
  }


  // Карточка объекта => добавление / вывод / обновление / файл
  cradObj(card: cardObj) {
    return this.http.post(`${this.baseUrl}/objects/`, card, this.httpOptions)
  }
  getData(): Observable<cardObj[]> {
    let projectId = localStorage.getItem('project_id');
    return this.http.get<cardObj[]>(`${this.baseUrl}/budget/get_card_object/?project_id=${projectId}`)
  }
  addCreateObj(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/2/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  getDataMore(id: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/budget/get_card_object/${id}/`)
  }
  updateData(id: number, updatedData: any): Observable<any> {
    const url = `${this.baseUrl}/objects/${id}/`;
    return this.http.put(url, updatedData, this.httpOptions);
  }
  deleteData(id: number): Observable<any> {
    const url = `${this.baseUrl}/objects/${id}/`;
    return this.http.delete(url, this.httpOptions);
  }
  uploadFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);

    return this.http.post<any>(`${this.baseUrl}/objects/loadfile/`, formData, this.httpFile);
  }



  //Реестро договоров одним списком
  getContractList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/contracts/get_contracts/`)
  }
  // Реестр договоров => вывод / обновление / добавление
  getWorkReg(): Observable<any> {
    return this.http.get(`${this.baseUrl}/contracts/`)
  }
  СontractsList(id: number): Observable<listContracts[]> {
    const url = `${this.baseUrl}/contracts/${id}/`;
    return this.http.get<listContracts[]>(url);
  }
  updСontractsList(id: number, updatedData: any): Observable<listContracts[]> {
    const url = `${this.baseUrl}/contracts/${id}/`;
    return this.http.put<listContracts[]>(url, updatedData);
  }
  // Добавление договора
  addContractsCreate(ContractsCreate: ContractsCreates, ContractsData: ContractsCreates): Observable<ContractsCreates[]> {
    const url = `${this.baseUrl}/contracts/create/`;
    return this.http.post(url, ContractsCreate, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  // Добавить WBS ОБЩИЙ
  addWbsContracts(id: number, updatedData: any) {
    const url = `${this.baseUrl}/contracts/${id}/add_wbs/`;
    return this.http.post(url, updatedData, this.httpOptions)
  }

  // Получение Код контрагента
  getContractsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/contracts/create/`)
  }
  addContractsList(contract_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/`;
    return this.http.post(url, fact, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  // Таблица Платежи факт
  getPaymentsFact(facttId: number): Observable<paymentsFact[]> {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/`;
    return this.http.get<paymentsFact[]>(url);
  }
  // Для вывода в дерево
  getWBSTree(Id: number) {
    const url = `${this.baseUrl}/contracts/${Id}/add_wbs/`;
    return this.http.get(url);
  }

  // Таблица WBS
  getWBSList(Id: number) {
    const url = `${this.baseUrl}/contracts/${Id}/wbs/`;
    return this.http.get(url);
  }
  // Таблица WBS(удаление по id)
  delWBS(Id: number) {
    const url = `${this.baseUrl}/contracts/wbs/${Id}/`;
    return this.http.delete(url);
  }
  // Изменить стоймость WBS
  editWBSamount(Id: number, updatedData: number) {
    const url = `${this.baseUrl}/contracts/wbs/${Id}/`;
    return this.http.put(url, { amount: updatedData }, this.httpOptions);
  }
  // Таблица Платежи факт (удаление по id)
  delPaymentsFact(contract_id: number, payment_id: number): Observable<paymentsFact[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/`;
    return this.http.delete<paymentsFact[]>(url);
  }
  // Формирование требования на оплату
  addPaymentsFactDocx(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/create_docx/`;
    return this.http.post<factDetails[]>(url, fact); // Просто верните response без изменений
  }

  // Формирование счетов-фактур
  addPaymentsFactInvoices(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactInvoices(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<invoices[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/${itemId}/`;
    return this.http.put<invoices[]>(url, updatedData);
  }
  delPaymentsFactInvoices(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/invoices/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
  // Формирование счетов на предоплату
  addPaymentsFactPrepay(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactPrepay(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<prepay_invoices[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/${itemId}/`;
    return this.http.put<prepay_invoices[]>(url, updatedData);
  }
  delPaymentsFactPrepay(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/prepay_invoices/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
  // Формирование пунктов плана
  addPaymentsFactItems(contract_id: number, payment_id: number, fact: factDetails): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/`;
    return this.http.post(url, fact).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsFactItems(contract_id: number, payment_id: number, itemId: number, updatedData: any): Observable<items[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/${itemId}/`;
    return this.http.put<items[]>(url, updatedData);
  }
  delPaymentsFactItems(contract_id: number, payment_id: number, itemId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/plan_items/${itemId}/`;
    return this.http.delete<factDetails[]>(url);
  }
  // Получение подробной инофрмации в таблице Платежи факт
  getPaymentsFactDetails(facttId: number, paymentId: number): Observable<factDetails[]> {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/`;
    return this.http.get<factDetails[]>(url);
  }
  // Получение подробной инофрмации в таблице Платежи факт(СДР)
  getPaymentsFactWBS(facttId: number, paymentId: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/wbs/`;
    return this.http.get(url);
  }
  // Изменить цену в таблице Платежи факт(СДР)
  editWBSFactAmount(facttId: number, paymentId: number, Id_WBS: number, updatedData: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.put(url, { amount: updatedData }, this.httpOptions);
  }

  // Удалить WBS в таблице Плтаежи Факт(СДР)
  deliteWBSFact(facttId: number, paymentId: number, Id_WBS: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.delete(url);
  }

  // Удалить WBS в таблице Плтаежи План(СДР)
  deliteWBSPlan(facttId: number, paymentId: number, Id_WBS: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_plan/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.delete(url);
  }

  // Удалить WBS в таблице Акт(СДР)
  deliteWBSAct(facttId: number, paymentId: number, Id_WBS: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/acts/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.delete(url);
  }


  // Получение подробной инофрмации в таблице Платежи факт(СДР)
  getPaymentsPlanWBS(facttId: number, paymentId: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_plan/${paymentId}/wbs/`;
    return this.http.get(url);
  }

  // Получение подробной инофрмации в таблице Акты(СДР)
  getPaymentsActWBS(facttId: number, paymentId: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/acts/${paymentId}/wbs/`;
    return this.http.get(url);
  }
  // Изменить Цену СДР Платежи План
  editWBSPlanAmount(facttId: number, paymentId: number, Id_WBS: number, updatedData: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/payments_fact/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.put(url, { amount: updatedData }, this.httpOptions);
  }
  // Изменить Цену СДР Акты
  editWBSActAmount(facttId: number, paymentId: number, Id_WBS: number, updatedData: number) {
    const url = `${this.baseUrl}/contracts/${facttId}/acts/${paymentId}/wbs/${Id_WBS}/`;
    return this.http.put(url, { amount: updatedData }, this.httpOptions);
  }

  // Добавить новый WBS в любой из платежей
  addWbsPayments(facttId: number, paymentId: number, typePay: string, updatedData: any) {
    const url = `${this.baseUrl}/contracts/${facttId}/${typePay}/${paymentId}/add_wbs/`;
    return this.http.post(url, updatedData, this.httpOptions)
  }

  editPaymentsFactDetails(contract_id: number, payment_id: number, updatedData: any): Observable<items[]> {
    const url = `${this.baseUrl}/contracts/${contract_id}/payments_fact/${payment_id}/`;
    return this.http.put<items[]>(url, updatedData);
  }
  // Таблица Платежи план
  getPaymentsList(contractId: number): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/`;
    return this.http.get<payments[]>(url);
  }
  addPaymentsList(contractId: number, payment: payments): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/`;
    return this.http.post(url, payment, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  editPaymentsList(contractId: number, paymentId: number, updatedData: any): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/${paymentId}/`;
    return this.http.put<payments[]>(url, updatedData);
  }
  delPaymentsList(contractId: number, paymentId: number): Observable<payments[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/payments_plan/${paymentId}/`;
    return this.http.delete<payments[]>(url);
  }
  // Таблица Акты
  getActsList(actsId: number): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${actsId}/acts/`;
    return this.http.get<acts[]>(url);
  }
  addActsList(contractId: number, acts: acts): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/`;
    return this.http.post(url, acts, this.httpOptions).pipe(
      map((response: any) => response.body)
    );
  }
  editActsList(contractId: number, actsId: number, updatedData: any): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/${actsId}/`;
    return this.http.put<acts[]>(url, updatedData);
  }
  delActsList(contractId: number, actsId: number): Observable<acts[]> {
    const url = `${this.baseUrl}/contracts/${contractId}/acts/${actsId}/`;
    return this.http.delete<acts[]>(url);
  }
  //Получить дерево СДР
  addSDR() {
    let projectId = localStorage.getItem('project_id');
    const url = `${this.baseUrl}/contracts/node/?project_id=${projectId}`;
    return this.http.get<ApiTreeData[]>(url);
  }

  // Источник стоимости => вывод / добавление / редактирование / удаление
  getSource(): Observable<Source[]> {
    return this.http.get<Source[]>(`${this.baseUrl}/budget/get_budget_source/`)
  }
  addSource(id: Source): Observable<Source[]> {
    const url = `${this.baseUrl}/budget/create_budget_source/`;
    return this.http.post(url, id).pipe(
      map((response: any) => response.body)
    );
  }
  editSource(id: number, updatedData: any): Observable<Source[]> {
    const url = `${this.baseUrl}/budget/update_budget_source/${id}/`;
    return this.http.put<Source[]>(url, updatedData);
  }
  delSource(id: number): Observable<Source[]> {
    const url = `${this.baseUrl}/budget/delete_budget_source/${id}/`;
    return this.http.delete<Source[]>(url);
  }


  // Ставка => вывод / добавление / редактирование / удаление
  getDifficultyFactor(): Observable<Ratio[]> {
    return this.http.get<Ratio[]>(`${this.baseUrl}/references/complexity_factor/`)
  }
  addDifficultyFactor(Ratios: Ratio): Observable<Ratio[]> {
    const url = `${this.baseUrl}/references/complexity_factor_create/`;
    return this.http.post(url, Ratios).pipe(
      map((response: any) => response.body)
    );
  }
  editDifficultyFactor(Ratios: number, updatedData: any): Observable<Ratio[]> {
    const url = `${this.baseUrl}/references/complexity_factor_update/${Ratios}/`;
    return this.http.put<Ratio[]>(url, updatedData);
  }
  delDifficultyFactor(Ratios: number): Observable<Ratio[]> {
    const url = `${this.baseUrl}/references/complexity_factor_update/${Ratios}/`;
    return this.http.delete<Ratio[]>(url);
  }

  // Объекты аналоги => добавление
  // getAnalogBudget() : Observable<analogBudgets[]> {
  //   return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/analog_budget/`).pipe(
  //     map((data) => {
  //       return data;
  //     }),
  //     catchError((error) => {
  //       return throwError(error)
  //     })
  //   )
  // }
  getAnalogBudget(): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/analog_budget/`)
  }
  getAnalogBudgetId(id: number): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/analog_budget/${id}/`)
  }
  getAnalogId(): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/analog_budget_package/`)
  }
  getEquipId(): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${this.baseUrl}/analog_budget/analog_budget_equipment/`)
  }
  // getAnalogId(id: number): Observable<analogBudgets[]> {
  //   const url = `${this.baseUrl}/estimated/analog_budget/${id}/`;
  //   return this.http.get<analogBudgets[]>(url);
  // }

  getEstimatedBudgetId(id: number): Observable<getLevel1[]> {
    return this.http.get<getLevel1[]>(`${this.baseUrl}/budget_estimated/estimated_budget/${id}/`)
  }
  getAnalogList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/analog_budget_type/`)
  }
  getAnalogPackageList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/analog_package_type`)
  }
  // Селект Коэффициента
  getComplexityList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/complexity_factor/`)
  }
  // Селект ед.измерения
  getUnitList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/unit_of_measure/`)
  }
  // Селект Analog Type Equipment
  getAnalogTypeList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/analog_equipment_type/`)
  }
  // Селект Currency
  getCurrencyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/currency/`)
  }
  // Селект Analog Package
  getAnalogTypePackageList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/analog_package_type`)
  }
  // Селект Region
  getRegionList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/region/`)
  }
  // Селект Направления
  getDirectionList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/budget_estimated/estimated_budget_direction_create/`)
  }
  // Селект unit of measure
  getUnitOfMeasureList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/references/unit_of_measure/`)
  }
  addDirection(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_direction_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editDirection(id: number, updatedData: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_direction_update/${id}/`;
    return this.http.put<any[]>(url, updatedData);
  }
  delDirection(id: number): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_direction_update/${id}/`;
    return this.http.delete<any[]>(url);
  }
  addDirectionAllReferences(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/get_analog_budget_all_data_from_references/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addDirectionReferences(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/get_analog_budget_from_references/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addObject(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_analog_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editObject(id: number, updatedData: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_analog_update/${id}/`;
    return this.http.put<any[]>(url, updatedData);
  }
  delObject(id: number): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_analog_update/${id}/`;
    return this.http.delete<any[]>(url);
  }
  addPackageReferences(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/get_analog_package_from_references/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addPackage(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_package_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editPackages(id: number, updatedData: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_package_update/${id}/`;
    return this.http.put<any[]>(url, updatedData);
  }
  delPackages(id: number): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_package_update/${id}/`;
    return this.http.delete<any[]>(url);
  }
  addEquipentReferences(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/get_analog_equipment_from_references/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addEquipent(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_equipment_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editEquipent(id: number, updatedData: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_equipment_update/${id}/`;
    return this.http.put<any[]>(url, updatedData);
  }
  delEquipent(id: number): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_equipment_update/${id}/`;
    return this.http.delete<any[]>(url);
  }
  addFactor(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/object_complexity_factor_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editFactor(id: number, updatedData: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/object_complexity_factor_update/${id}/`;
    return this.http.put<any[]>(url, updatedData);
  }
  delFactor(id: number): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/object_complexity_factor_update/${id}/`;
    return this.http.delete<any[]>(url);
  }
  addReferencesFactor(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/budget_estimated/get_complexity_factor_from_references/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addToWbs(analogBudget: any): Observable<any[]> {
    const url = `${this.baseUrl}/estimated/to_wbs/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addAnalogBudget(analogBudget: analogBudgets): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  addEquipment(Equipment: Equipments): Observable<Equipments[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_equipment_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  addEquipment1(Equipment: Equipments): Observable<Equipments[]> {
    const url = `${this.baseUrl}/estimated/estimated_equipment_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  editEquipment1(AnalogBudgetId: number, updatedData: any): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_equipment_update/${AnalogBudgetId}/`;
    return this.http.put<analogBudgets[]>(url, updatedData);
  }
  delEquipment1(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_equipment_update/${analogBudgets}/`;
    return this.http.delete<analogBudgets[]>(url);
  }

  editAnalogBudget(AnalogBudgetId: number, updatedData: any): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_update/${AnalogBudgetId}/`;
    return this.http.put<analogBudgets[]>(url, updatedData);
  }
  delAnalogBudget(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_update/${analogBudgets}`;
    return this.http.delete<analogBudgets[]>(url);
  }
  addWorkPackage(WorkPackage: WorkPackages): Observable<WorkPackages[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_package_create/`;
    return this.http.post(url, WorkPackage).pipe(
      map((response: any) => response.body)
    );
  }
  editWorkPackage1(AnalogBudgetId: number, updatedData: any): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_package_update/${AnalogBudgetId}/`;
    return this.http.put<analogBudgets[]>(url, updatedData);
  }
  delWorkPackage1(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${this.baseUrl}/analog_budget/analog_budget_package_update/${analogBudgets}/`;
    return this.http.delete<analogBudgets[]>(url);
  }


  // ГКР => вывод / добавление / редактирование / удаление
  getGkr(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/gkr/gkr_all/`)
  }
  addGkr(Equipment: getGkr): Observable<getGkr[]> {
    const url = `${this.baseUrl}/gkr/gkr_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  updGkr(id: number, updatedData: any): Observable<getGkr[]> {
    const url = `${this.baseUrl}/gkr/gkr_update/${id}/`;
    return this.http.put<getGkr[]>(url, updatedData);
  }
  delGkr(id: number): Observable<getGkr[]> {
    const url = `${this.baseUrl}/gkr/gkr_update/${id}/`;
    return this.http.delete<getGkr[]>(url);
  }
  addTotalGkr(Equipment: getGkr): Observable<number> {
    const url = `${this.baseUrl}/gkr/gkr_total_cost/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.total_cost)
    );
  }


  // ППО => вывод / добавление / редактирование / удаление
  getPpo(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/ppo/ppo_all/`)
  }
  addPpo(Equipment: getPpo): Observable<getGkr[]> {
    const url = `${this.baseUrl}/ppo/ppo_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  updPpo(id: number, updatedData: any): Observable<getPpo[]> {
    const url = `${this.baseUrl}/ppo/ppo_update/${id}/`;
    return this.http.put<getPpo[]>(url, updatedData);
  }
  delPpo(id: number): Observable<getPpo[]> {
    const url = `${this.baseUrl}/ppo/ppo_update/${id}/`;
    return this.http.delete<getPpo[]>(url);
  }


  // НМА => вывод / добавление / редактирование / удаление
  getIntangibleAssetsBudget(id: number): Observable<getTotal[]> {
    const url = `${this.baseUrl}/budget/get_intangible_assets/${id}/`;
    return this.http.get<getTotal[]>(url);
  }
  getNmaFilter(data: SvzObject) {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_node_flat/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    )
  }
  addIntangibleAssetsBudget(data: CommonCostsCirectory) {
    const url = `${this.baseUrl}/budget/create_intangible_assets/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response));
  }
  delIntangibleAssetsBudget(id: number) {
    const url = `${this.baseUrl}/budget/delete_intangible_assets/${id}/`;
    return this.http.delete(url);
  }
  updIntangibleAssetsBudget(id: number, UpdatedAsset: Total) {
    const url = `${this.baseUrl}/budget/update_intangible_assets/${id}/`;
    return this.http.post(url, UpdatedAsset).pipe(
      map((response: any) => response)
    )
  }
  getInflation(id: number): Observable<getTotal[]> {
    const url = `${this.baseUrl}/budget/get_parameters_budget/${id}/`;
    return this.http.get<getTotal[]>(url);
  }




  addEquipment2(Equipment: Equipments): Observable<Equipments[]> {
    const url = `${this.baseUrl}/estimated/estimated_equipment_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  getСomparison(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/estimated/comparison/`)
  }


  updateRateLinkBudgetList(id, data) {
    const url = `${this.baseUrl}/budget/link/general/${id}/update/`;
    return this.http.put(url, data, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );;
  }
  // Удалить связанный объект:
  delElementLinkBudgetList(id: number) {
    const url = `${this.baseUrl}/budget/link/general/${id}/delete/`;
    return this.http.delete(url);
  }
  updInta
  // Получить связанные объекты:
  getLinkBudgetList(id): Observable<any[]> {
    const url = `${this.baseUrl}/budget/link/general/${id}/list/`;
    return this.http.get<RegisterBudget[]>(url);
  }
  // Связать
  LinkCommonCosts(id, data) {
    const url = `${this.baseUrl}/budget/link/general/${id}/create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    )
  }

  // /budget/get_common_budget_flat/2/
  getCommonBudget(id, level) {
    const url = `${this.baseUrl}/budget/get_common_budget_flat/${id}/`;
    return this.http.post(url, level).pipe(
      map((response: any) => response)
    )
  }
  // Бюджет проекта => История
  getHistoryObjectBudget(id:number): Observable<History[]> {
    const url = `${this.baseUrl}/budget/histoty_common_budget/${id}/`;
    return this.http.get<History[]>(url);
  }
  // Бюджет проекта => вывод / добавление / редактирование / удаление
  getTypical(): Observable<RegisterBudget[]> {
    const url = `${this.baseUrl}/wbs/get_typical_node/`;
    return this.http.get<RegisterBudget[]>(url);
  }
  getRegisterBudget(): Observable<RegisterBudget[]> {
    const url = `${this.baseUrl}/budget/get_register_budget/`;
    return this.http.get<RegisterBudget[]>(url);
  }
  getRegister(id: number): Observable<RegisterBudget[]> {
    const url = `${this.baseUrl}/budget/get_general_project/${id}/`;
    return this.http.get<RegisterBudget[]>(url);
  }
  // Создать объект бюджета
  addCommonObjectBudget(Budget: ObjectBudget) {
    const url = `${this.baseUrl}/budget/create_common_budget/`;
    return this.http.post(url, Budget).pipe(
      map((response: any) => response)
    )
  }
  addBaseToCurrentBudget(Budget: ObjectBudget) {
    const url = `${this.baseUrl}/budget/base_to_current_budget/`;
    return this.http.post(url, Budget).pipe(
      map((response: any) => response)
    )
  }
  updateCommonObjectBudget(id: number, updatedData: any): Observable<ObjectBudget[]> {
    const url = `${this.baseUrl}/budget/update_common_budget/${id}/`;
    return this.http.post(url, updatedData).pipe(
      map((response: any) => response)
    )
  }
  getSourceBudget(): Observable<any[]> {
    const url = `${this.baseUrl}/budget/get_budget_source/`;
    return this.http.get<any[]>(url);
  }


  // Создать реестр бюджета
  addCreateRegisterBudget(Budget: RegisterBudget) {
    const url = `${this.baseUrl}/budget/create_register_budget/`;
    return this.http.post(url, Budget).pipe(
      map((response: any) => response)
    )
  }
  // Сравнение реестр бюджета
  compareEstimate(data: any) {
    const url = `${this.baseUrl}/budget/mathing_budget/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    )
  }
  // Обновить реестр бюджета
  updateRegisterBudget(RegisterBudgetId: number, updatedData: any): Observable<RegisterBudget[]> {
    const url = `${this.baseUrl}/budget/update_register_budget/${RegisterBudgetId}/`;
    return this.http.put<RegisterBudget[]>(url, updatedData);
  }
  // Удалить реестр бюджета
  delRegisterBudget(RegisterBudgetId: number): Observable<RegisterBudget[]> {
    const url = `${this.baseUrl}/budget/delete_register_budget/${RegisterBudgetId}/`;
    return this.http.delete<RegisterBudget[]>(url);
  }
  getObjectBudgetGeneral(ObjectBudgets: Number,link_general:boolean,transaction_reserve:boolean): Observable<ObjectBudget[]> {
    const url = `${this.baseUrl}/budget/get_common_budget/${ObjectBudgets}/?link_general=${link_general}&transaction_reserve=${transaction_reserve}`;
    return this.http.get<ObjectBudget[]>(url);
  }
  getObjectBudget(ObjectBudgets: Number): Observable<ObjectBudget[]> {
    const url = `${this.baseUrl}/budget/get_common_budget/${ObjectBudgets}/`;
    return this.http.get<ObjectBudget[]>(url);
  }
  getTotalCost(id: any): Observable<ObjectBudget[]> {
    const url = `${this.baseUrl}/budget/get_total_cost/${id}/`;
    return this.http.get<ObjectBudget[]>(url);
  }
  // Укрупнение сметных позиций => вывод / добавление / редактирование / удаление
  getConsolidationEstimates(id: any): Observable<ObjectBudget[]> {
    const url = `${this.baseUrl}/estimate/get_consolidation_estimates/`;
    let data: any = this.http.post(url, id).pipe(
      map((response: any) => response)
    );
    return data
  }

  // Оценочный бюджет => вывод / добавление / редактирование / удаление
  getEquipment(): Observable<Equipments[]> {
    const url = `${this.baseUrl}/estimated/analog_budget_equipment/`;
    return this.http.get<Equipments[]>(url);
  }
  editEquipment(EquipmentId: number, updatedData: any): Observable<Equipments[]> {
    const url = `${this.baseUrl}/estimated/estimated_equipment_update/${EquipmentId}/`;
    return this.http.put<Equipments[]>(url, updatedData);
  }
  delEquipment(EquipmentId: number): Observable<Equipments[]> {
    const url = `${this.baseUrl}/estimated/estimated_equipment_update/${EquipmentId}/`;
    return this.http.delete<Equipments[]>(url);
  }
  addEstimated(Estimated: Estimated): Observable<Estimated[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_create/`;
    return this.http.post(url, Estimated).pipe(
      map((response: any) => response.body)
    );
  }
  getworkPackage(): Observable<workPackage[]> {
    const url = `${this.baseUrl}/estimated/work_package/`;
    return this.http.get<workPackage[]>(url);
  }
  addAnalog(WorkPackages: workPackage): Observable<analog[]> {
    const url = `${this.baseUrl}/estimated/estimated_package_create/`;
    return this.http.post(url, WorkPackages).pipe(
      map((response: any) => response.body)
    );
  }
  editWorkPackage(workPackageId: number, updatedData: any): Observable<workPackage[]> {
    const url = `${this.baseUrl}/estimated/estimated_package_update/${workPackageId}/`;
    return this.http.put<workPackage[]>(url, updatedData);
  }
  delWorkPackage(workPackageId: number): Observable<workPackage[]> {
    const url = `${this.baseUrl}/estimated/estimated_package_update/${workPackageId}/`;
    return this.http.delete<workPackage[]>(url);
  }

  getwAnalog(analogId: number): Observable<analog[]> {
    const url = `${this.baseUrl}/estimated/analog_budget/${analogId}/`;
    return this.http.get<analog[]>(url);
  }
  getRatio(): Observable<Ratio[]> {
    const url = `${this.baseUrl}/estimated/rate/`;
    return this.http.get<Ratio[]>(url);
  }
  addRatio(Ratio: Ratio): Observable<Ratio[]> {
    const url = `${this.baseUrl}/estimated/rate/`;
    return this.http.post(url, Ratio).pipe(
      map((response: any) => response.body)
    );
  }
  editRatio(Ratio: number, updatedData: any): Observable<Ratio[]> {
    const url = `${this.baseUrl}/estimated/rate/${Ratio}/`;
    return this.http.put<Ratio[]>(url, updatedData);
  }
  delRatio(Ratio: number): Observable<Ratio[]> {
    const url = `${this.baseUrl}/estimated/rate/${Ratio}/`;
    return this.http.delete<Ratio[]>(url);
  }
  addBudget(WorkPackages: workPackage): Observable<analog[]> {
    const url = `${this.baseUrl}/estimated/estimated_analog_create/`;
    return this.http.post(url, WorkPackages).pipe(
      map((response: any) => response.body)
    );
  }
  editBudget(id: number, updatedData: any): Observable<analog[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_update/${id}/`;
    return this.http.put<analog[]>(url, updatedData);
  }
  delBudget(id: number): Observable<analog[]> {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_update/${id}/`;
    return this.http.delete<analog[]>(url);
  }

  getEstimated(): Observable<Estimated[]> {
    return this.http.get<Estimated[]>(`${this.baseUrl}/budget_estimated/estimated_budget/`)
  }
  getEstimatedId(EstimatedId: number): Observable<Estimated[]> {
    const url = `${this.baseUrl}/estimated/estimated_budget/${EstimatedId}/`;
    return this.http.get<Estimated[]>(url);
  }
  editEstimated(EstimatedId: number, updatedData: any): Observable<Estimated[]> {
    const url = `${this.baseUrl}/estimated/estimated_analog_update/${EstimatedId}/`;
    return this.http.put<Estimated[]>(url, updatedData);
  }
  delEstimated(EstimatedId: number): Observable<Estimated[]> {
    const url = `${this.baseUrl}/estimated/estimated_analog_update/${EstimatedId}/`;
    return this.http.delete<Estimated[]>(url);
  }

  // СДР => получение
  getCategory(): Observable<any> {
    let projectId = localStorage.getItem('project_id');
    return this.http.get<any>(`${this.baseUrl}/wbs/get_category/?project_id=${projectId}`);
  }
  getTypicalWbsCategory(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/wbs/get_typical_category/`);
  }
  getIntangibleAssetsCategory(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/wbs/get_intangible_assets_category/`);
  }
  //! Temp commented
  getWbs(): Observable<Wbs[]> {
    let projectId = localStorage.getItem('project_id');
    return this.http.get<Wbs[]>(`${this.baseUrl}/wbs/get_node/?project_id=${projectId}`);
  }
  // getWbs(): Observable<Wbs[]> {
  //   let projectId = localStorage.getItem('project_id');
  //   return this.http.get<Wbs[]>(`${this.baseUrl}/wbs/get_node/`);
  // }
  getTypicalWbs(): Observable<Wbs[]> {
    return this.http.get<Wbs[]>(`${this.baseUrl}/wbs/get_typical_node/`);
  }
  getIntangibleAssetsWbs(): Observable<Wbs[]> {
    return this.http.get<Wbs[]>(`${this.baseUrl}/wbs/get_intangible_assets_node/`);
  }

  // СДР => добавление пакетов
  addProjectFilter(ProjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addDirectionFilter(DirectionFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addObjectFilter(ObjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addSystemFilter(SystemFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addWorkFilter(WorkFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addEstimateFilter(EstimateFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }

  addTypicalProjectFilter(ProjectFilter: ProjectFilter): Observable<ProjectFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addTypicalDirectionFilter(DirectionFilter: DirectionFilter): Observable<DirectionFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addTypicalObjectFilter(ObjectFilter: ObjectFilter): Observable<ObjectFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addTypicalSystemFilter(SystemFilter: SystemFilter): Observable<SystemFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addTypicalWorkFilter(WorkFilter: WorkFilter): Observable<WorkFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addTypicalEstimateFilter(EstimateFilter: EstimateFilter): Observable<EstimateFilter[]> {
    const url = `${this.baseUrl}/wbs/create_typical_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsProjectFilter(ProjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsDirectionFilter(DirectionFilter: DirectionFilter): Observable<DirectionFilter[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsObjectFilter(ObjectFilter: ObjectFilter): Observable<ObjectFilter[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsSystemFilter(SystemFilter: SystemFilter): Observable<SystemFilter[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsWorkFilter(WorkFilter: WorkFilter): Observable<WorkFilter[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  addIntangibleAssetsEstimateFilter(EstimateFilter: EstimateFilter): Observable<EstimateFilter[]> {
    const url = `${this.baseUrl}/wbs/create_intangible_assets_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }

  // СДР => редактирование пакетов
  updateProjectPackage(ProjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/1/${ProjectFilter.id}`;
    return this.http.put(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateDirectionPackage(DirectionFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/2/`;
    return this.http.put(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateObjectPackage(ObjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/3/${ObjectFilter.id}`;
    return this.http.put(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateSystemPackage(SystemFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/4/${SystemFilter.id}`;
    return this.http.put(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateWorkPackage(WorkFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/5/${WorkFilter.id}`;
    return this.http.put(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateEstimatePackage(EstimateFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_wbs_package/6/${EstimateFilter.id}`;
    return this.http.put(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }

  updateTypicalProjectPackage(ProjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateTypicalDirectionPackage(DirectionFilter: DirectionFilter): Observable<DirectionFilter[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateTypicalObjectPackage(ObjectFilter: ObjectFilter): Observable<ObjectFilter[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateTypicalSystemPackage(SystemFilter: SystemFilter): Observable<SystemFilter[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateTypicalWorkPackage(WorkFilter: WorkFilter): Observable<WorkFilter[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateTypicalEstimatePackage(EstimateFilter: EstimateFilter): Observable<EstimateFilter[]> {
    const url = `${this.baseUrl}/wbs/update_typical_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsProjectPackage(ProjectFilter: Package): Observable<Package[]> {
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/1/`;
    return this.http.post(url, ProjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsDirectionPackage(DirectionFilter: DirectionFilter): Observable<DirectionFilter[]> {
    console.log(DirectionFilter)
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/2/`;
    return this.http.post(url, DirectionFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsObjectPackage(ObjectFilter: ObjectFilter): Observable<ObjectFilter[]> {
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/3/`;
    return this.http.post(url, ObjectFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsSystemPackage(SystemFilter: SystemFilter): Observable<SystemFilter[]> {
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/4/`;
    return this.http.post(url, SystemFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsWorkPackage(WorkFilter: WorkFilter): Observable<WorkFilter[]> {
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/5/`;
    return this.http.post(url, WorkFilter).pipe(
      map((response: any) => response.body)
    );
  }
  updateIntangibleAssetsEstimatePackage(EstimateFilter: EstimateFilter): Observable<EstimateFilter[]> {
    const url = `${this.baseUrl}/wbs/update_intangible_assets_wbs_package/6/`;
    return this.http.post(url, EstimateFilter).pipe(
      map((response: any) => response.body)
    );
  }

  // СДР => удаление пакетов
  deleteProjectPackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/1/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteDirectionPackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/2/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteObjectPackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/3/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteSystemPackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/4/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteWorkPackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/5/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteEstimatePackage(RemovePackage: DeleteEntity) {
    const url = `${this.baseUrl}/wbs/delete_wbs_package/6/${RemovePackage.id}`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }

  deleteTypicalProjectPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/1/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/1/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteTypicalDirectionPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/2/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/2/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteTypicalObjectPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/3/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/3/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteTypicalSystemPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/4/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/4/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteTypicalWorkPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/5/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/5/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteTypicalEstimatePackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/6/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_typical_wbs_package/6/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsProjectPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/1/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/1/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsDirectionPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/2/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/2/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsObjectPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/3/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/3/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsSystemPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/4/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/4/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsWorkPackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/5/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/5/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }
  deleteIntangibleAssetsEstimatePackage(RemovePackage: DeleteEntity) {
    // const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/6/${RemovePackage.id}`;
    const url = `${this.baseUrl}/wbs/delete_intangible_assets_wbs_package/6/`;
    return this.http.post(url, RemovePackage).pipe(
      map((response: any) => response));
  }

  // СДР => получение элементов
  getProjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/1/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getDirectionElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/2/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getObjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/3/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getSystemElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/4/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getWorkElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/5/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getEstimateElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_wbs_element/6/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response));
  }

  getTypicalProjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/1/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getTypicalDirectionElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/2/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getTypicalObjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/3/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getTypicalSystemElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/4/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getTypicalWorkElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/5/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getTypicalEstimateElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_typical_wbs_element/6/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response));
  }

  getIntangibleAssetsProjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/1/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getIntangibleAssetsDirectionElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/2/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getIntangibleAssetsObjectElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/3/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getIntangibleAssetsSystemElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/4/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getIntangibleAssetsWorkElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/5/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response)
    );
  }
  getIntangibleAssetsEstimateElement(ElementFilter: ElementFilter): Observable<PackageElements[]> {
    const url = `${this.baseUrl}/wbs/get_intangible_assets_wbs_package/6/`;
    return this.http.post(url, ElementFilter).pipe(
      map((response: any) => response));
  }

  // Реестр бюджета => получить пакеты бюджета
  getBudgetObjects(): Observable<BudgetObject[]> {
    return this.http.get<BudgetObject[]>(`${this.baseUrl}/budget/get_budget_package/`);
  }
  // Реестр бюджета => добавить пакет бюджета
  addBudgetObject(AddedBudget: BudgetObject) {
    const url = `${this.baseUrl}/budget/create_budget_package/`;
    return this.http.post(url, AddedBudget).pipe(
      map((response: any) => response)
    )
  }
  // Реестр бюджета => обновить пакет бюджета
  editBudgetObject(EditedBudget: BudgetObject) {
    const url = `${this.baseUrl}/budget/update_budget_package/${EditedBudget.id}/`;
    return this.http.put(url, EditedBudget).pipe(
      map((response: any) => response)
    )
  }
  // Реестр бюджета => удалить пакет бюджета
  deleteBudgetObject(DeletedBudget: number): Observable<any> {
    const url = `${this.baseUrl}/budget/delete_budget_package/${DeletedBudget}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response)
    )
  }


  uploaFileObjectBudget(formData) {
    const url = `${this.baseUrl}/budget/upload_file_gpcs/`;
    return this.http.post(url, formData);
  }
  addFilter(AddedSvz: SvzObject) {
    const url = `${this.baseUrl}/budget_estimated/estimated_budget_to_wbs/`;
    return this.http.post(url, AddedSvz).pipe(
      map((response: any) => response)
    )
  }
  getSvzWbs(): Observable<NodeObject[]> {
    return this.http.get<NodeObject[]>(`${this.baseUrl}/wbs/get_node_flat/`);
  }
  getSvzWbsByLevelFilter(Filter: Object): Observable<NodeObject[]> {
    return this.http.post<NodeObject[]>(`${this.baseUrl}/wbs/get_node_flat/`, Filter).pipe(
      map((response: any) => response)
    )
  }
  // СВЗ => получить пакеты СВЗ
  getSvzObjects(): Observable<SvzObject[]> {
    return this.http.get<SvzObject[]>(`${this.baseUrl}/budget/get_svz/`);
  }
  // СВЗ => добавить пакет СВЗ
  addSvzObject(AddedSvz: SvzObject) {
    const url = `${this.baseUrl}/budget/create_svz/`;
    return this.http.post(url, AddedSvz).pipe(
      map((response: any) => response)
    )
  }
  // СВЗ => обновить пакет СВЗ
  editSvzObject(EditedSvz: SvzObject) {
    const url = `${this.baseUrl}/budget/update_svz/${EditedSvz.id}/`;
    return this.http.put(url, EditedSvz).pipe(
      map((response: any) => response)
    )
  }
  // СВЗ => удалить пакет СВЗ
  deleteSvzObject(DeletedBudget: number) {
    const url = `${this.baseUrl}/budget/delete_svz/${DeletedBudget}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response)
    )
  }

  // Реестр СВЗ => получить пакеты СВЗ документов
  getSvzDocumentObjects(): Observable<SvzDocumentObject[]> {
    return this.http.get<SvzDocumentObject[]>(`${this.baseUrl}/budget/get_svz_document/`);
  }
  // Реестр СВЗ => добавить пакет СВЗ документа
  addSvzDocumentObject(AddedSvz: SvzDocumentObject) {
    const url = `${this.baseUrl}/budget/get_generate_svz/`;
    return this.http.post(url, AddedSvz).pipe(
      map((response: any) => response)
    )
  }
  // Реестр СВЗ => подробнее о пакете СВЗ документа
  editSvzDocumentMore(EditedSvz: SvzDocumentObject) {
    const url = `${this.baseUrl}/budget/get_svz/`;
    return this.http.post(url, EditedSvz).pipe(
      map((response: any) => response)
    )
  }
  // Реестр СВЗ => обновить пакет СВЗ документа
  editSvzDocumentObject(EditedSvz: SvzDocumentObject) {
    const url = `${this.baseUrl}/budget/update_svz_document/${EditedSvz.id}/`;
    return this.http.put(url, EditedSvz).pipe(
      map((response: any) => response)
    )
  }
  // Реестр СВЗ => удалить пакет СВЗ документа
  deleteSvzDocumentObject(DeletedBudget: number) {
    const url = `${this.baseUrl}/budget/delete_svz_document/${DeletedBudget}`;
    return this.http.delete<any>(url).pipe(
      map((response: any) => response)
    )
  }

  // Макропараметры => добавление / вывод
  getParameter(): Observable<CurrencyExchange[]> {
    return this.http.get<CurrencyExchange[]>(`${this.baseUrl}/macroparameters/parameter_value/`)
  }
  getWacc(): Observable<Wacc[]> {
    return this.http.get<Wacc[]>(`${this.baseUrl}/macroparameters/discount_rates/`)
  }
  getSelect(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/macroparameters/parameters/`)
  }
  delParam(Param: number): Observable<Ratio[]> {
    const url = `${this.baseUrl}/macroparameters/parameter/${Param}/delete/`;
    return this.http.delete<Ratio[]>(url);
  }
  delWacc(Wacc: number): Observable<Ratio[]> {
    const url = `${this.baseUrl}/macroparameters/discountrate/${Wacc}/delete/`;
    return this.http.delete<Ratio[]>(url);
  }
  updParam(Param: number, updatedData: any): Observable<Parameters[]> {
    const url = `${this.baseUrl}/macroparameters/parameter/${Param}/update/`;
    return this.http.put<Parameters[]>(url, updatedData);
  }
  updWacc(Wacc: number, updatedData: any): Observable<Parameters[]> {
    const url = `${this.baseUrl}/macroparameters/discountrate/${Wacc}/update/`;
    return this.http.put<Parameters[]>(url, updatedData);
  }
  updVal(Val: number, updatedData: any): Observable<Parameters[]> {
    const url = `${this.baseUrl}/macroparameters/parametervalue/${Val}/update/`;
    return this.http.put<Parameters[]>(url, updatedData);
  }
  addDiscountRate(DiscountRate: DiscountRate): Observable<DiscountRate[]> {
    const url = `${this.baseUrl}/macroparameters/create_discount_rate/`;
    return this.http.post(url, DiscountRate).pipe(
      map((response: any) => response.body)
    );
  }
  addParameters(Parameters: Parameters): Observable<Parameters[]> {
    const url = `${this.baseUrl}/macroparameters/create_parameter/`;
    return this.http.post(url, Parameters).pipe(
      map((response: any) => response.body)
    );
  }
  addParameterValue(ParameterValue: ParameterValue): Observable<ParameterValue[]> {
    const url = `${this.baseUrl}/macroparameters/create_parameter_value/`;
    return this.http.post(url, ParameterValue).pipe(
      map((response: any) => response.body)
    );
  }


  //Финансирование План
  getPlan(year: number = 0): Observable<Plan[]> {
    const url = `${this.baseUrl}/financing/plan/${year}/`;
    return this.http.get<Plan[]>(url);
  }

  updatePlan(): Observable<any> {
    const url = `${this.baseUrl}/financing/plan/update/`;
    return this.http.get<any>(url);
  }

  getPlanMode(id: number): Observable<any> {
    const url = `${this.baseUrl}/financing/plan/${id}/update_mode/`;
    return this.http.get<any>(url);
  }

  updatePlanMode(UpdatedPlan: any, id: number) {
    const url = `${this.baseUrl}/financing/plan/${id}/update_mode/`;
    return this.http.post(url, UpdatedPlan).pipe(
      map((response: any) => response));
  }

  //Финансирование Факт
  getFinancingFact(year: number = 0): Observable<FinancingFact[]> {
    const url = `${this.baseUrl}/financing/fact/${year}/`;
    return this.http.get<FinancingFact[]>(url);
  }

  updateFinancingFact(): Observable<any> {
    const url = `${this.baseUrl}/financing/fact/update/`;
    return this.http.get<any>(url);
  }
}
