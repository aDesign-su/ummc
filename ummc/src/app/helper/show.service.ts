import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ShowService {
  constructor(private location: Location) { }
  
  showBlock: boolean[] = [false,false,false]

  goBack() {
    this.location.back();
  }
  toggleShowBlock(index: number) {
    for (let i = 0; i < this.showBlock.length; i++) {
      this.showBlock[i] = false;
    }
    this.showBlock[index] = !this.showBlock[index];
  }
  toggleShowBlockTrue(index: number) {
    this.showBlock[index] = true
  }
  cancel(elem: any) {
    elem.editing = false;
  }
  closedAdd(index: number) {
    this.showBlock[index] = false
  }
}
