import { Component, Input, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  constructor(public theme: ThemeService) { }

  ngOnInit() {
  }

  // @Input() theme: { access?: any, error?: any, info?: any } = {};
}
