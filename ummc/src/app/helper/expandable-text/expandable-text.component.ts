import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-expandable-text',
  templateUrl: './expandable-text.component.html',
  styleUrls: ['./expandable-text.component.css']
})
export class ExpandableTextComponent implements OnInit {
  @Input() text: string;
  displayText: string;

  isTruncated: boolean = false;
  isExpanded: boolean = false;

  ngOnInit(): void {
    this.isTruncated = this.text && this.text.length > 100;
    this.updateDisplayText();
  }

  updateDisplayText(): void {
    if (this.isExpanded || !this.isTruncated) {
      this.displayText = this.text;
    } else {
      this.displayText = this.text.substring(0, 100) + '...';
    }
  }

  toggleText(): void {
    this.isExpanded = !this.isExpanded;
    this.updateDisplayText();
  }
}