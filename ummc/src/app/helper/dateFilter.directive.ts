import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[matDatepicker]'
})
export class DateFilterDirective {
  constructor(private el: ElementRef) {}

  @Input() appDateMask: boolean = true;
  @HostListener('input', ['$event'])

  onInput(event: any): void {
    if (this.appDateMask) {
      const input = event.target;
      const value = input.value.replace(/\D/g, ''); // Оставляем только цифры
      if (value.length <= 8) {
        // Форматируем ввод в соответствии с маской
        input.value = this.formatDate(value);
      } else {
        // Обрезаем введенные данные до 8 символов
        input.value = value.substring(0, 8);
      }
    }
  }

  formatDate(value: string): string {
    const day = value.substring(0, 2);
    const month = value.substring(2, 4);
    const year = value.substring(4, 8);

    let formattedDate = '';
    if (day) {
      formattedDate += day;
      if (month) {
        formattedDate += `.${month}`;
        if (year) {
          formattedDate += `.${year}`;
        }
      }
    }

    return formattedDate;
  }
}

export const dateRangeValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const startDate = control.get('date_start_plan')?.value;
  const endDate = control.get('date_end_plan')?.value;

  if (startDate && endDate) {
    if (startDate > endDate) {
      control.get('date_end_plan')?.setErrors({ dateRangeError: true });
      return { dateRangeError: true };
    } else if (endDate > startDate) {
      control.get('date_start_plan')?.setErrors(null);
      control.get('date_end_plan')?.setErrors(null);
      return null;
    }
  }

  return null;
};

export const dateRangeValidatorCommon: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const startDate = control.get('date_start')?.value;
  const endDate = control.get('date_end')?.value;

  if (startDate && endDate) {
    if (startDate > endDate) {
      control.get('date_end')?.setErrors({ dateRangeError: true });
      return { dateRangeError: true };
    } else if (endDate > startDate) {
      control.get('date_start')?.setErrors(null);
      control.get('date_end')?.setErrors(null);
      return null;
    }
  }

  return null;
};