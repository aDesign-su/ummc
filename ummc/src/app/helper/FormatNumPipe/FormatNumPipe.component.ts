import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumbers'
})
export class FormatNumPipeComponent implements PipeTransform {

  transform(value: number): string {
    if (isNaN(value)) return null;

    if (value >= 1e9) {
      return (value / 1e9).toFixed(3).replace(/\.000$/, '') + ' млрд.';
    }
    if (value >= 1e6) {
      return (value / 1e6).toFixed(3).replace(/\.000$/, '') + ' млн.';
    }
    if (value >= 1e3) {
      return (value / 1e3).toFixed(3).replace(/\.000$/, '') + ' тыс.';
    }
    return value.toString();
  }
}