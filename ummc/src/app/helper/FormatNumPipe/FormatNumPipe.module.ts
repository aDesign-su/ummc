import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatNumPipeComponent } from './FormatNumPipe.component';
import { ParamsProjectComponent } from 'src/app/content/poject/params-project/params-project.component';
import { CardParamsOptionsComponent } from 'src/app/content/poject/params-project/card-params-options/card-params-options.component';
import { CloseBlockDirective } from './media/oem/CE329A41329A2F09/GitLab/ugmk/budgeting/ummc/src/app/helper/closeBlock.directive';
import { CustomNumberPipe } from '.d:/UGMK/ummcnew/ummc/src/app/helper/CustomNumber.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [		FormatNumPipeComponent,
      CloseBlockDirective,
      CustomNumberPipe
   ]
})
export class FormatNumPipeModule { }
