import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';
import { ShowService } from './show.service';

@Directive({
  selector: '[clickOutside]'
})
export class CloseBlockDirective {
  @Output() appClickOutside = new EventEmitter<void>();

  constructor(private elementRef: ElementRef) {}

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement: any): void {
    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.appClickOutside.emit();
    }
  }
}
