import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customNumber'
})
export class CustomNumberPipe implements PipeTransform {

  transform(value: number | string | null | undefined, ...args: unknown[]): string {
    if (value === null || value === undefined) {
      return '';
    }

    let numberValue: number;

    if (typeof value === 'string') {
      numberValue = parseFloat(value);
      if (isNaN(numberValue)) {
        return value;
      }
    } else {
      numberValue = value;
    }

    return numberValue.toLocaleString('fr-FR', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 3
    });
  }
}