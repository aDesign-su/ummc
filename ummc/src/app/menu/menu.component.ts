import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { ThemeService } from '../theme.service';
import { ShowService } from '../helper/show.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(public themeService: ThemeService,public show:ShowService, public theme: ThemeService, private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // Закрываем все блоки при начале навигации
        this.show.showBlock.forEach((value, index) => this.show.showBlock[index] = false);
      }
    });
  }

}
