export interface Plan {
  id: number,
  level: number,
  code_wbs: number,
  name_wbs: number,
  date_start: Date,
  date_end: Date,
  amount: number,
  smr_amount: number,
  pir_amount: number,
  oto_amount: number,
  other_amount: number,
  type_of_mode: string,
  point_of_fin: string,
  plan_fin: MonthlyPayment[]
}

export interface MonthlyPayment {
  date: Date,
  title: string,
  amount: number
}

export interface PlanFinancial {
  mode: string,
  days_for_advance: number,
  proccent_advance: number,
  days_for_postponement: number,
  proccent_postponement: number,
  days_for_delay: number
}
