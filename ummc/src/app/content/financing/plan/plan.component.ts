import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service'
import {ShowService} from '../../../helper/show.service'
import {ApicontentService} from '../../../api.content.service'
import {MonthlyPayment, Plan} from './plan'
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms'
import { CommonService } from '../../budget/project-budget/common.service';
import {FinancingFact} from '../fact/financing-fact'

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {

  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private wbsService: ApicontentService, private formBuilder: FormBuilder) {
    this.UpdatedPlan = this.formBuilder.group({
      mode: [null, [Validators.required]],
      days_for_advance: [null, [Validators.required]],
      proccent_advance: [null, [Validators.required]],
      days_for_postponement: [null, [Validators.required]],
      proccent_postponement: [null, [Validators.required]],
      days_for_delay: [null, [Validators.required]]
    })
    this.searchForm = formBuilder.group({
      searchText: ''
    });
    this.applyFilters();
  }

  ngOnInit(): void {
    this.getPlan();
  }

  UpdatedPlan!: FormGroup;

  _resetFields() {
    Object.keys(this.UpdatedPlan.controls).filter(key => key != "mode").forEach(key => {
      this.UpdatedPlan.get(key).reset();
    });
  }

  _getControlValue(name: string) {
    return this.UpdatedPlan.get(name);
  }

  _checkFieldByMode(mode: string) {
    return this.UpdatedPlan.get('mode').value == mode;
  }

  _setValidatorsByMode(name: string) {
    Object.keys(this.UpdatedPlan.controls).filter(key => key != "mode").forEach(key => {
      if (name == "Ежемесячный платёж") {
        this.UpdatedPlan.get(key).clearValidators();
        this.UpdatedPlan.get(key).setErrors(null);
        this.UpdatedPlan.get(key).updateValueAndValidity();
      } else {
        this.UpdatedPlan.get(key).setValidators(Validators.required);
        this.UpdatedPlan.get(key).updateValueAndValidity();
      }
    });
  }

  _disabledSubmitByUsedMode() {
    const mode = this.UpdatedPlan.get("mode").value;
    this._setValidatorsByMode(mode);
    switch (mode) {
      case "Аванс с отсрочкой (два платежа)":
        return !(this._getControlValue("days_for_advance").valid
          && this._getControlValue("days_for_postponement").valid
          && this._getControlValue("proccent_advance").valid
          && this._getControlValue("proccent_postponement").valid
          && this._getControlValue('proccent_postponement').value + this._getControlValue('proccent_advance').value == 100
        );
      case "Ежемесячный платёж":
        return !((
          this._getControlValue("days_for_advance").value
            && this._getControlValue("proccent_advance").value
            && !this._getControlValue("days_for_postponement").value
            && !this._getControlValue("proccent_postponement").value
          ) || (
            !this._getControlValue("days_for_advance").value
            && !this._getControlValue("proccent_advance").value
            && this._getControlValue("days_for_postponement").value
            && this._getControlValue("proccent_postponement").value
          ) || (
            this._getControlValue("days_for_advance").value
            && this._getControlValue("proccent_advance").value
            && this._getControlValue("days_for_postponement").value
            && this._getControlValue("proccent_postponement").value
          )
        );
      case "По умолчанию":
        return false;
      case "Договор":
        return false;
      default:
        return true;
    }
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  displayedColumns: string[] = ["wbs_id", "name", "date_start", "date_end", "amount", "pir", "smr", "equipment", "other", "point_of_fin", "actions"]
  years: any = [];
  filterYears: any = [];
  months: string[] = [];
  planData: Plan[] = [];
  filteredData: Plan[] = [];
  modes: string[] = [];

  searchForm!: FormGroup;

  isPlanUpdateVisible: boolean = false;
  editedPlanId!: number;

  setPlanUpdateVisible(bool: boolean) {
    this.isPlanUpdateVisible = bool;
    this.modes = [];
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  getSelection(level: number, year: number = 0) {
    let classes: Object;
    switch (level) {
      case 0:
        classes = {
          level0Selection: true,
        }
        break;
      case 1:
        classes = {
          level1Selection: true,
        }
        break;
      case 2:
        classes = {
          level2Selection: true,
        }
        break;
      case 3:
        classes = {
          level3Selection: true,
        }
        break;
      default:
        classes = {
          level4Selection: true,
        }
    }

    if (isNaN(Number(year))) {
      classes['monthsColumn'] = true;
    }

    return classes;
  }

  getPlan() {
    this.loading = true;
    this.wbsService.getPlan().subscribe(
      (data: Plan[]) => {
        console.log(data)
        this.planData = this.formatCommonData(data);
        this.changeLoading();
        this.applyFilters();
      }
    )
  }

  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    this.filteredData = this.planData.filter((plan: Plan) => {
      return this.matchesSearchText(plan, searchText)
    });
  }

  matchesSearchText(plan:Plan, searchText: string): boolean {
    return (plan.name_wbs.toString().toLowerCase() ?? '').includes(searchText);
  }

  formatCommonData(data: Plan[]) {
    this.months = [];
    this.displayedColumns = ["wbs_id", "name", "date_start", "date_end", "amount", "pir", "smr", "equipment", "other", "point_of_fin", "actions"]

    this.filterYears = data.slice(-1)[0];
    this.years = data.slice(-1)[0];

    this.years = this.years.map(item => {
      const yearNumber = item["year"]
      this.displayedColumns.push(String(yearNumber));
      return yearNumber;
    })

    return data.slice(0, -1);
  }

  getAnnualPlan(year: number) {
    this.changeLoading();
    this.wbsService.getPlan(year).subscribe(
      (data: Plan[]) => {
        console.log(data)
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length);
        this.planData = this.formatDataByYear(data, year);
        this.applyFilters();
        this.changeLoading();
      }
    )
  }

  formatDataByYear(data: Plan[], filteredYear: number) {
    this.years = data.slice(-1)[0];
    this.months = this.getMonths(data[0].plan_fin);

    this.years = this.years.map(item => {
      return String(item["year"]);
    });

    this.months.reverse().forEach(month => {
      this.years.splice(this.years.indexOf(String(filteredYear)) + 1, 0, month);
    });

    this.displayedColumns = this.displayedColumns.concat(this.years);

    return data.slice(0, -1);
  }

  getMonths(data: MonthlyPayment[]) {
    this.months = [];

    data.filter(payment => isNaN(Number(payment.title))).forEach(payment => {
      this.months.push(payment.title)
    })

    return this.months;
  }

  editPlan(id: number) {
    this.editedPlanId = id;
    this.setPlanUpdateVisible(true);
    this.modes = [];
    this.getPlanMode();
  }

  updatePlan() {
    this.wbsService.updatePlan().subscribe(
      (data: any) => {
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length - 1);
        this.getPlan();
        this.theme.info = 'Обновление успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  getPlanMode() {
    this.wbsService.getPlanMode(this.editedPlanId).subscribe(
      (data: any) => {
        console.log(data[1]);
        Object.values(data[1]).forEach(plan => {
          this.modes.push(String(plan));
        });
      }
    )
  }

  savePlan() {
    this.setPlanUpdateVisible(false);
    this.wbsService.updatePlanMode(this.UpdatedPlan.value, this.editedPlanId).subscribe(
      (data: any) => {
        this.UpdatedPlan.reset();
        this.modes = [];
        this.getPlan();
        this.theme.info = 'План обновлен успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить план. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  protected readonly isNaN = isNaN
  protected readonly Number = Number
}
