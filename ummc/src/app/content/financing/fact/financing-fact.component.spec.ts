import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancingFactComponent } from './financing-fact.component';

describe('FactComponent', () => {
  let component: FinancingFactComponent;
  let fixture: ComponentFixture<FinancingFactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancingFactComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FinancingFactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
