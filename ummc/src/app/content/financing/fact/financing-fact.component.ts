import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service'
import {ShowService} from '../../../helper/show.service'
import {ApicontentService} from '../../../api.content.service'
import {MonthlyPayment, FinancingFact} from './financing-fact'
import { CommonService } from '../../budget/project-budget/common.service';
import {FormBuilder, FormGroup} from '@angular/forms'

@Component({
  selector: 'app-fact',
  templateUrl: './financing-fact.component.html',
  styleUrls: ['./financing-fact.component.css']
})
export class FinancingFactComponent implements OnInit {

  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private wbsService: ApicontentService, private formBuilder: FormBuilder) {
    this.searchForm = formBuilder.group({
      searchText: ''
    });
    this.applyFilters();
  }

  ngOnInit(): void {
    this.getFact();
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  displayedColumns: string[] = ["wbs_id", "name", "date_start", "date_end", "amount"]
  years: any = [];
  filterYears: any = [];
  months: string[] = [];
  factData: FinancingFact[] = [];
  filteredData: FinancingFact[] = [];

  searchForm!: FormGroup;

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    this.filteredData = this.factData.filter((fact: FinancingFact) => {
      return this.matchesSearchText(fact, searchText)
    });
  }

  matchesSearchText(fact:FinancingFact, searchText: string): boolean {
    return (fact.name_wbs.toString().toLowerCase() ?? '').includes(searchText);
  }

  getSelection(level: number, year: number = 0) {
    let classes: Object;
    switch (level) {
      case 0:
        classes = {
          level0Selection: true,
        }
        break;
      case 1:
        classes = {
          level1Selection: true,
        }
        break;
      case 2:
        classes = {
          level2Selection: true,
        }
        break;
      case 3:
        classes = {
          level3Selection: true,
        }
        break;
      default:
        classes = {
          level4Selection: true,
        }
    }

    if (isNaN(Number(year))) {
      classes['monthsColumn'] = true;
    }

    return classes;
  }

  getFact() {
    this.loading = true;
    this.wbsService.getFinancingFact().subscribe(
      (data: FinancingFact[]) => {
        console.log(data)
        this.factData = this.formatCommonData(data);
        this.changeLoading();
        this.applyFilters();
      }
    )
  }

  formatCommonData(data: FinancingFact[]) {
    this.months = [];
    this.displayedColumns = ["wbs_id", "name", "date_start", "date_end", "amount"]

    this.filterYears = data.slice(-1)[0];
    this.years = data.slice(-1)[0];

    this.years = this.years.map(item => {
      const yearNumber = item["year"]
      this.displayedColumns.push(String(yearNumber));
      return yearNumber;
    })

    return data.slice(0, -1);
  }

  getAnnualFact(year: number) {
    this.changeLoading();
    this.wbsService.getFinancingFact(year).subscribe(
      (data: FinancingFact[]) => {
        console.log(data)
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length);
        this.factData = this.formatDataByYear(data, year);
        this.applyFilters();
        this.changeLoading();
      }
    )
  }

  formatDataByYear(data: FinancingFact[], filteredYear: number) {
    this.years = data.slice(-1)[0];
    this.months = this.getMonths(data[0].plan_fin);

    this.years = this.years.map(item => {
      return String(item["year"]);
    });

    this.months.reverse().forEach(month => {
      this.years.splice(this.years.indexOf(String(filteredYear)) + 1, 0, month);
    });

    this.displayedColumns = this.displayedColumns.concat(this.years);

    return data.slice(0, -1);
  }

  getMonths(data: MonthlyPayment[]) {
    this.months = [];

    data.filter(payment => isNaN(Number(payment.title))).forEach(payment => {
      this.months.push(payment.title)
    })

    return this.months;
  }

  updateFact() {
    this.wbsService.updateFinancingFact().subscribe(
      (data: any) => {
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length - 1);
        this.getFact();
        this.theme.info = 'План изменен успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить план. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  protected readonly isNaN = isNaN
  protected readonly Number = Number
}
