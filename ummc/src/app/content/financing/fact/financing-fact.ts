export interface FinancingFact {
  id: number,
  level: number,
  code_wbs: number,
  name_wbs: number,
  date_start: Date,
  date_end: Date,
  amount: number,
  plan_fin: MonthlyPayment[]
}

export interface MonthlyPayment {
  date: Date,
  title: string,
  amount: number
}
