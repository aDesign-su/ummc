/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FileUploadServiceService } from './file-upload.service.service';

describe('Service: FileUpload.service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileUploadServiceService]
    });
  });

  it('should ...', inject([FileUploadServiceService], (service: FileUploadServiceService) => {
    expect(service).toBeTruthy();
  }));
});
