import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadServiceService {
  constructor(private http: HttpClient,private api: ApicontentService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access'),
      })
    };
   }
  private httpOptions: any;

  upload(file): Observable<any> {
    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append('file', file, file.name);

    // Make http post request over api
    // with formData as req
    return this.http.post(`${this.api.baseUrl}/primavera/upload_primavera_oracle/`, formData, this.httpOptions);
  }
  postDataToPrimavera(data) {
    let treeId = localStorage.getItem('tree_id');
    let projectId = localStorage.getItem('project_id');
    const url = `${this.api.baseUrl}/primavera/wbs_to_primavera/?tree_id=${treeId}&project_id=${projectId}`;
    return this.http.post(url, data, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );
  }
  postDataToWbs(data) {
    let treeId = localStorage.getItem('tree_id');
    let projectId = localStorage.getItem('project_id');
    const url = `${this.api.baseUrl}/primavera/primavera_to_wbs/?tree_id=${treeId}&project_id=${projectId}`;
    return this.http.post(url, data, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );
  }
  postToPrimavera(data) {
    let treeId = localStorage.getItem('tree_id');
    let projectId = localStorage.getItem('project_id');
    const url = `${this.api.baseUrl}/primavera/to_primavera/?tree_id=${treeId}&project_id=${projectId}`;
    return this.http.post(url, data, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );
  }
  getDataToWbs() {
    const url = `${this.api.baseUrl}/primavera/upload_primavera_oracle/`;
    return this.http.get(url, this.httpOptions).pipe(
      map((response: any) => { return response; })
    );
  }
}
