export interface Synch {
    id: number,
    object_id: string,
    code_wbs: string,
    guid: string,
    name: string,
    start_date: string,
    end_date: string,
    start_date_node: string,
    end_date_node: string,
    parent: string,
    level: number,
    tree_id: string,
    wbs_id?: null,
    is_updated?: boolean,
    children?: Synch[]
}
