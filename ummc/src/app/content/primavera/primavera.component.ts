import { Component, OnInit } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { PackageElements, Wbs, WbsElement } from '../budget/wbs/wbs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { ShowService } from 'src/app/helper/show.service';
import { Synch } from './synch';
import { FileUploadServiceService } from './file-upload.service.service';
import { TodoItemFlatNode } from '../contract/register-contracts/reg.more/reg.more.component';
import { SelectionModel } from '@angular/cdk/collections';

@Component({ 
  selector: 'app-primavera',
  templateUrl: './primavera.component.html',
  styleUrls: ['./primavera.component.scss']
})
export class PrimaveraComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, private wbsService: ApicontentService, private formBuilder: FormBuilder,private fileUploadService: FileUploadServiceService) { 
    this.wbsToPrimavera = this.formBuilder.group({
      data: []
    });
    this.ProjectPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      parent: 0,
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.DirectionPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.ObjectPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.SystemPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.WorkPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });

    this.treeFlattener = new MatTreeFlattener(
      this._transformer,
      node => node.level,
      node => node.expandable,
      node => node.children
    );
    this.treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level,
      node => node.expandable
    );
    this.structureViewDataSource = [
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener),
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener),
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener)
    ]
  }

  ngOnInit() {
    this.getWbs();
  }

  treeControl: FlatTreeControl<ExampleFlatNode>;
  treeFlattener: MatTreeFlattener<Synch, ExampleFlatNode>;
  structureViewDataSource: MatTreeFlatDataSource<Synch, ExampleFlatNode>[];
  
  currentViewIndex: number = 0;
  selectedTableId: number = 0;

  selectedWbs: Synch;
  isFormVisible = [false, false, false, false, false];
  isUpdateVisible = [false, false, false, false, false, false];
  isElementsListVisible = false;

  wbsToPrimavera!: FormGroup;
  ProjectPackage!: FormGroup;
  DirectionPackage!: FormGroup;
  ObjectPackage!: FormGroup;
  SystemPackage!: FormGroup;
  EstimatePackage!: FormGroup;
  WorkPackage!: FormGroup;
  

  currentOpenElementsListId = -1;
  elementLevel!: number;
  elements: WbsElement[] = [];
  
  ElementFilter!: FormGroup;
  wbs: Synch[][] = [];

  getWbs(): void {
    this.wbsService.getDataSource().subscribe(
      (data: Synch[]) => {
        this.wbs[0] = data;
        this.structureViewDataSource[0].data = this.wbs[0];
        let treeId = localStorage.setItem('tree_id', this.wbs[0][0].tree_id)
        // this.tableViewDataSources[0] = this.convertToTableView(this.wbs[0], 0);
        // this.treeControlStorage[0] = this.treeControl.dataNodes;
        // this.toggleToLevel(_, 0);
        // this.loading[0] = !this.loading[0];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  private _transformer = (node: Synch, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      name: node.name,
      level: level,
      wbs_id: node.wbs_id,
      is_updated: node.is_updated,
      code_wbs: node.code_wbs,
      object_id: node.object_id,
      guid: node.guid,
      start_date: node.start_date,
      end_date: node.end_date,
      start_date_node: node.start_date_node,
      end_date_node: node.end_date_node,
      children: node.children,
      parent: node.parent,
      filtered: false
    };
  };

  hasChild(_: number, node: ExampleFlatNode): boolean {
    return node.expandable;
  }

  hasNoContent = (_: number, nodeData: any) => nodeData.item === '';
  structureViewColumns: string[] = ['name', 'wbs_id', 'guid', 'planned_start_date', 'planned_end_date', 'actual_start_date', 'actual_end_date', 'actions'];
  tablesViewColumns = {
    0: ['Проект', 'Направление', 'Объект', 'Система', 'Пакет работ или Смета'],
    1: ['Проект', 'Тип общепроектных работ', 'Пакет работ или Смета'],
    2: ['Уровень 0', 'Уровень 1', 'Уровень 2']
  }
  clue = {
    0: ['проект', 'направление', 'объект', 'систему', 'пакет работ или смету'],
    1: ['проект', 'общепроектную работу', 'пакет работ или смету'],
    2: ['уровень 0', 'уровень 1', 'уровень 2']
  }
  setFields(selectedWbs: Wbs) {
    let form = this.findForm(selectedWbs);
    Object.keys(form.controls).forEach(key => {
      form.get(key).setValue(selectedWbs[key]);
    })
    form.get('category').setValue(selectedWbs.category_id);
    console.log(form.controls)
  }
  findForm(selectedWbs: Wbs) {
    let form;
    switch (selectedWbs.level) {
      case 1:
        form = this.DirectionPackage;
        break;
      case 2:
        form = this.ObjectPackage;
        break;
      case 3:
        form = this.SystemPackage;
        break;
      case 4:
        form = this.WorkPackage;
        break;
      case 5:
        form = this.EstimatePackage;
        break;
      default:
        form = this.ProjectPackage;
    }
    return form;
  }
  toggleFormVisibility(index: number, wbs: Synch): void {
    this.selectedWbs = wbs;
    // Сначала скрыть все формы
    this.isFormVisible = [false, false, false, false, false];
    this.isUpdateVisible = [false, false, false, false, false];
    // Затем отобразить выбранную форму
    this.isFormVisible[index] = true;
    this.isElementsListVisible = false;
  }
  toggleUpdateVisibility(index, wbs: Synch): void {
    console.log(wbs)
    this.selectedWbs = wbs;
    // this.setFields(this.selectedWbs);
    // Сначала скрыть все формы
    this.isFormVisible = [false, false, false, false, false];
    this.isUpdateVisible = [false, false, false, false, false, false];
    // Затем отобразить выбранную форму
    this.isUpdateVisible[index] = true;
    this.isElementsListVisible = false;
  }
  toggleElementsListVisible(selectedWbs: Synch, index: number, packageId: number) {
    if (packageId == this.currentOpenElementsListId) {
      this.closeElementList();
    } else {
      this.elementLevel = index;
      this.elements = [];
      this.currentOpenElementsListId = packageId;
      this.isElementsListVisible = true;
      this.selectedWbs = selectedWbs;
    }

    if (this.isElementsListVisible) {
      switch (index) {
        case 0:
          this.getProjectElement(packageId);
          break;
        case 1:
          this.getDirectionElement(packageId);
          break;
        case 2:
          this.getObjectElement(packageId);
          break;
        case 3:
          this.getSystemElement(packageId);
          break;
        case 4:
          this.getWorkElement(packageId);
          break;
        case 5:
          this.getEstimateElement(packageId);
          break;
        default:
          break;
      }
    }
  }
  closeElementList() {
    this.isElementsListVisible = false;
    this.currentOpenElementsListId = -1;
    this.elementLevel = -1;
    this.elements = [];
  }
  getProjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getProjectElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalProjectElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsProjectElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getDirectionElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getDirectionElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalDirectionElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsDirectionElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getObjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getObjectElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalObjectElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsObjectElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getSystemElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getSystemElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalSystemElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsSystemElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getWorkElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getWorkElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalWorkElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsWorkElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getEstimateElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler;
    switch (this.selectedTableId) {
      case 0:
        handler = this.wbsService.getEstimateElement(ElementFilter);
        break;
      case 1:
        handler = this.wbsService.getTypicalEstimateElement(ElementFilter);
        break;
      case 2:
        handler = this.wbsService.getIntangibleAssetsEstimateElement(ElementFilter);
        break;
    }

    this.getElementsResponse(handler, packageId);
  }
  getElementsResponse(handler: any, packageId: number) {
    handler.subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  shortLink: string = '';
  loading: boolean = false;
  file: File = null;

  private activLoading() {
    this.loading = true;
  }

  private deactivLoading() {
    this.loading = false;
  }

  onChange(event) {
    this.file = event.target.files[0];
  }
  onDown() {
    this.fileUploadService.getDataToWbs().subscribe((data: any) => {
      console.log('data:', data);
      // this.deactivLoading();
      this.getWbs();
    });
  }
  onUpload() {
    if (this.file) {
      // this.loading = !this.loading;
      this.activLoading(); 
      console.log(this.file);
      this.fileUploadService.upload(this.file).subscribe((event: any) => {
        if (typeof event === 'object') {
          // Short link via api response
          this.shortLink = event.link;
          this.deactivLoading();
          // this.loading = false; // Flag variable
          this.getWbs();
        }
      });
    }
  }
  fullUpload() {
    let projectId = localStorage.getItem('project_id');
    const load = `{"project_id":${projectId}}`
    this.activLoading(); 
    this.fileUploadService.postDataToPrimavera(load).subscribe((data: any) => {
      console.log('data:', data);
      this.deactivLoading();
      this.getWbs();
    });
  }
  fullLoad() {
    let projectId = localStorage.getItem('project_id');
    const load = `{"project_id":${projectId}}`
    this.activLoading(); 
    this.fileUploadService.postDataToWbs(load).subscribe((data: any) => {
      console.log('data:', data);
      this.deactivLoading();
      this.getWbs();
    });
  }
  checklistSelection = new SelectionModel<ExampleFlatNode>(true /* multiple */);
   /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: ExampleFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.length > 0 && descendants.every(child => {
      return this.checklistSelection.isSelected(child);
    });
    return descAllSelected;
  }
    /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: ExampleFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }
    /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: ExampleFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.forEach(child => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
  }
  checkRootNodeSelection(node: any): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.length > 0 && descendants.every(child => {
      return this.checklistSelection.isSelected(child);
    });
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }
  getLevel = (node: any) => node.level;

  getParentNode(node: any): any | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }
    /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: any): void {
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }
  todoLeafItemSelectionToggle(node: any): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  selectedData: any[] = [];
  selectedAllData: any[] = [];
  
  showKnotSingle(element: any) {
    const isSelected = this.checklistSelection.isSelected(element);
    const index = this.selectedData.findIndex(item => item.id === element.id);

    if (!isSelected && index === -1) {
        let knot = element.id;
        if (knot) {
            const data = { id: element.id, level: element.level };
            this.selectedData.push(data);
            console.log(this.selectedData);
        }
    } else if (isSelected && index !== -1) {
        this.selectedData.splice(index, 1);
        console.log(this.selectedData);
    }
  }
  showKnotAll(element: any) {
    const isSelected = this.checklistSelection.isSelected(element);
    const index = this.selectedAllData.findIndex(item => item.id === element.id);

    if (!isSelected && index === -1) {
        let knot = element.id;
        if (knot) {
            const data = {
                id: element.id,
                level: element.level,
                children: element.children.map(child => ({ id: child.id, level: child.level }))
            };
            this.selectedAllData.push(data);
            console.log(this.selectedAllData);
        }
    } else if (isSelected && index !== -1) {
        this.selectedAllData.splice(index, 1);
        console.log(this.selectedAllData);
    }
  }
  toWbsPrimavera() {
    const data = this.wbsToPrimavera.value;
    this.fileUploadService.postToPrimavera(data).subscribe(
      (response: any[]) => {
        this.deactivLoading();
        // this.loading = false; // Flag variable
        this.getWbs();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
}

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  object_id: string,
  code_wbs: string,
  guid: string,
  start_date: string,
  end_date: string,
  start_date_node: string,
  end_date_node: string,
  parent: string,
  wbs_id: null
  filtered: boolean;
}