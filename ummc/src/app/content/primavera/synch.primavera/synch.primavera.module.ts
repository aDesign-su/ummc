import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SynchPrimaveraComponent } from './synch.primavera.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: 
  [
    // SynchPrimaveraComponent
  ]
})
export class SynchPrimaveraModule { }
