import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { switchMap, EMPTY } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';
import { getRegisterBudget, getTotal, Total } from '../../budget/total-project-costs/total';
import * as jmespath from 'jmespath';
import * as moment from 'moment';

@Component({
  selector: 'app-synch-primavera',
  templateUrl: './synch.primavera.component.html',
  styleUrls: ['./synch.primavera.component.css']
})
export class SynchPrimaveraComponent implements OnInit {
  constructor(private total: ApicontentService) { }

  ngOnInit() {
    this.getIsCurrent()
  }

  getIsCurrent() {
    this.total.getRegisterBudget().pipe(
      switchMap((data: getRegisterBudget[]) => {
        const row = "@.data[]";
        const response = jmespath.search(data, row);
        const currentObject = response.find(item => item.is_current);
  
        if (currentObject) {
          this.setIsCurrent = currentObject.id;
          console.log('ID текущего объекта:', this.setIsCurrent);
          return this.total.getGeneralProjectBudget(this.setIsCurrent);
        } else {
          console.log('Текущий объект не найден');
          return EMPTY; 
        }
      })
    ).subscribe((data: getTotal[]) => {
      const row = "@.data[]";
      const response = jmespath.search(data, row);
      this.dataSources.data = response;
    });
  }
  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price',  'actions'];
  dataSource = new MatTableDataSource<getTotal>([]);
  setIsCurrent: number

  private _transformer = (node: getTotal, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      code_wbs: node.code_wbs,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      date_end: node.date_end,
      date_start: node.date_start,
      other_price: node.other_price,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;
}
