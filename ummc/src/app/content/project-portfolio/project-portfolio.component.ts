import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { Column, ProjectPortfolio } from './project-portfolio';
import { ProjectRegister } from '../project-register/project-register';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonService } from '../budget/project-budget/common.service';
declare var bootstrap: any;

@Component({
  selector: 'app-project-portfolio',
  templateUrl: './project-portfolio.component.html',
  styleUrls: ['./project-portfolio.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProjectPortfolioComponent implements OnInit {
  expandedElements: any[] = [];
  dataProjectsPortfolioTable: MatTableDataSource<ProjectPortfolio> = new MatTableDataSource<ProjectPortfolio>();
  columnsToDisplay = ['name'];//Главная таблица
  columnsToDisplayWithExpand = ['expand', ...this.columnsToDisplay, 'action'];//Главная таблица/Стрелка для раскрытия 
  comparisonList: any[] = [];//Лист с данными для сравнения
  selectedColumns = new FormControl();//Колонки которые будут отображаться в таблице параметров проекта 
  displayedColumnsComparison: string[] = ['id'];  // По умолчанию, отображаем ID для выбора параметров стравнения 
  addedToComparison: { [id: number]: boolean } = {};  // Флаг для отслеживания добавленных элементов(Если true, то участвует в сравнении)
  finalList = [];//Лист с версиями прокта. Он и отображается в таблице сранения

  FormAddEditPortfolio: FormGroup;
  editMode = false;
  projects: ProjectRegister[];
  selectedPortfolio: number

  constructor(public theme: ThemeService, private fb: FormBuilder, public params: ApicontentService, public isTitle: CommonService, private dialog: MatDialog,private _snackBar: MatSnackBar) {

    this.FormAddEditPortfolio = this.fb.group({
      name: ['', Validators.required],
      projects: [[], Validators.required]
    });
  }

  ngOnInit() {
    this.getDataProjectsPortfolio()
    this.selectedColumns.setValue(this.cols.filter(col => col.display).map(col => col.field));
    this.updateDisplayedColumns();
    this.getProject()
  }

  // Полуить данные Проекты
  getDataProjectsPortfolio(): void {
    this.params.getProjectsPortfolio().subscribe(
      (data) => {
        console.log(data)
        this.dataProjectsPortfolioTable.data = data;

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  //Раскрывается табличка с табличками
  toggleExpand(element: any): void {
    const index = this.expandedElements.indexOf(element);
    if (index === -1) {
      this.expandedElements.push(element);
    } else {
      this.expandedElements.splice(index, 1);
    }
  }
  // список для сравнения:
  addToComparison(element: any) {
    const exists = this.comparisonList.some(item => item.id === element.id);
    if (exists) {
      this.comparisonList = this.comparisonList.filter(item => item.id !== element.id);
      this.addedToComparison[element.id] = false;
    } else {
      this.comparisonList.push(element);
      this.addedToComparison[element.id] = true;
    }
    console.log('Текущий список для сравнения:', this.comparisonList);
  }
  //При выборе версии проекта 
  addToFinalList() {
    this.finalList = [];
    for (const project of this.comparisonList) {
      const keyValue = project.keyprojectvalues.find(kv => kv.id === project.selectedKeyValueId);
      if (keyValue) {
        const newEntry = {
          ...project,
          keyprojectvalues: keyValue
        };
        this.finalList.push(newEntry);
      }
    }
    console.log(this.finalList)
  }
  //Удалить проект из сравнения 
  removeProject(id: number, index: number): void {
    this.addedToComparison[id] = false;
    this.comparisonList.splice(index, 1);
    this.addToFinalList();
  }
  // Параметры для сравнения 
  cols: Column[] = [
    { field: 'name', header: 'Название проекта', display: true },
    { field: 'keyprojectvalues.title', header: 'Версия проекта', display: true },
    { field: 'phase', header: 'Фаза', display: false },
    { field: 'project_manager', header: 'Менеджер проекта', display: true },
    { field: 'keyprojectvalues.budget', header: 'Бюджет проекта, млрд руб', display: true },
    { field: 'keyprojectvalues.IRR', header: 'IRR проекта(%)', display: true },
    { field: 'keyprojectvalues.WACC', header: 'WACC', display: true },
    { field: 'keyprojectvalues.NPV', header: 'NPV проекта, млн руб', display: true },
    { field: 'keyprojectvalues.priority', header: 'Приоритет', display: true },
    { field: 'keyprojectvalues.category', header: 'Категория проекта', display: true },
    { field: 'keyprojectvalues.effective_plan_date', header: 'Веха', display: true },
    { field: 'base_budget', header: 'Базовый бюджет', display: true },
    { field: 'current_budget', header: 'Текущий бюджет', display: true },
    { field: 'actual_budget', header: 'Факт освоения', display: true },
    { field: 'deviation', header: 'Отклонение', display: true },

  ];
  //Обновляем отображение колонок в таблице 
  updateDisplayedColumns() {
    this.displayedColumnsComparison = ['id', ...this.selectedColumns.value];
    console.log(this.displayedColumnsComparison)
  }

  add() {
    this.editMode = false
    this.FormAddEditPortfolio.reset()
  }
  private closeForm() {
    const offcanvasElement = document.getElementById('addPortfolio');
    const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    if (offcanvasInstance) {
      offcanvasInstance.hide();
    }
  }

  public createOrUpdateProjectsPortfolio(): void {
    if (this.FormAddEditPortfolio.valid) {
      const formData = { ...this.FormAddEditPortfolio.value };
      console.log(formData)
      if (this.editMode) {
        //         this.updateProjectsPortfolio(this.selectedOperatingCost, formData);
        this.updateProjectsPortfolio(this.selectedPortfolio, formData);
      } else {
        this.createProjectsPortfolio(formData);
      }
      this.editMode = false;
      this.selectedPortfolio = null;
    }
  }


  createProjectsPortfolio(data) {
    console.log('create:', data)
    this.params.addProjectsPortfolio(data).subscribe(
      (data:ProjectPortfolio ) => {
        console.log(data)
        this.getDataProjectsPortfolio()
        this.theme.openSnackBar(`Создан портфель ${data.name}`);
        this.closeForm()
      },
      (error: any) => {
        console.error('Error fetching data:', error);
        
      }
    );
  }
  updateProjectsPortfolio(id, data) {
    this.params.updateProjectsPortfolio(id, data).subscribe(
      (data: any) => {
        this.getDataProjectsPortfolio()
        this.theme.openSnackBar('Изменения сохранены');
        this.closeForm()
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  editPortfolio(element) {
    this.editMode = true;
    this.selectedPortfolio = element.id;
    const projectIds = element.projects.map(project => project.id);  // Извлекаем массив ID проектов
    this.FormAddEditPortfolio.setValue({
      name: element.name,
      projects: projectIds  // Присваиваем массив ID проектов
    });
  }
  delPortfolio(element) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {  // Если пользователь нажал "Да"
        this.params.delProjectsPortfolio(element.id).subscribe(
          (data: any) => {
            console.log(data);
            this.getDataProjectsPortfolio();
            this.theme.openSnackBar('Портфель удален');
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  getProject() {
    this.params.getProjects().subscribe(
      (response) => {
        console.log('Data Projects:', response);
        this.projects = response;

      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }

}
