export interface ProjectPortfolio {
    id: number;
    name:string;
    projects: Project[];
    user: number;
}

export interface Project {
    id: number;
    name: string;
    base_budget: number | null;
    current_budget: number | null;
    deviation: number | null;
    project_parameters: ProjectParameter[];
}

export interface ProjectParameter {
    id: number;
    name: string;
    activ: string;
    phase: string;
    level: string;
    project_manager: string;
    effective_plan: number;
    created: string;
    keyprojectvalues: KeyProjectValue[];
}

export interface KeyProjectValue {
    id: number;
    project: number;
    title: string;
    budget: number;
    comment_budget: string;
    IRR: number;
    comment_IRR: string;
    WACC: number;
    effective_plan_date: string;
    comment_effective_plan_date: null | string;
    production_volume: number;
    comment_production_volume: null | string;
    production_volume_year: number;
    NPV: number;
    comment_NPV: string;
    priority: string;
    comment_priority: string;
    category: string;
    comment_category: null | string;
    created: string;
}
export interface Column {
    field: string;
    header: string;
    display: boolean;
  }
  
  