import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';




@Component({
  selector: 'app-inner-table',
  templateUrl: './inner-table.component.html',
  styleUrls: ['./inner-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class InnerTableComponent implements OnInit {
  @Input() projects: any[];
  @Input() columnsToDisplay: { [header: string]: string };
  @Input() toggleExpand: (element: any) => void;
  expandedElements: any[] = [];
  DataSourse: MatTableDataSource<any> = new MatTableDataSource<any>();
  headers: string[] = [];
  keys: string[] = [];
  columnsToDisplayWithExpand = []
  addedToComparison:{ [id: number]: boolean } = {};
  @Input() addToComparison?: (element: any) => void;
  @Input() addedToComparisonFlag?: { [id: number]: boolean } = {};

  constructor(public theme: ThemeService, private fb: FormBuilder, public params: ApicontentService) { }


  ngOnInit() {
    console.log(this.projects)
    this.expandedElements = [];
    this.headers = Object.keys(this.columnsToDisplay);
    this.keys = Object.values(this.columnsToDisplay);
    this.columnsToDisplayWithExpand = ['expand', ...this.keys];
    this.DataSourse.data = this.projects;
    this.addedToComparison=this.addedToComparisonFlag
  }
  isExpanded = (element: any): boolean => {
    return this.expandedElements.indexOf(element) !== -1;
  };

  isNotExpanded = (element: any): boolean => {
    return !this.isExpanded(element);
  };

}
