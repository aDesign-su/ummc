import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service';
import { CommonService } from '../../budget/project-budget/common.service';
import { FlatNode } from './zimono';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';
import { ApicontentService } from 'src/app/api.content.service';

@Component({
  selector: 'app-zimono-project',
  templateUrl: './zimono-project.component.html',
  styleUrls: ['./zimono-project.component.css']
})
export class ZimonoProjectComponent implements OnInit {

  nameFilter: string = "";

  readonly viewStates = {
    WBS: 'wbs',
    TABLE: 'table'
  };

  readonly levelNames: string[] = [];

  private currentViewState: string;

  get getCurrentViewState() : string {
    return this.currentViewState;
  }

  set setCurrentViewState(newState : string) {
    this.currentViewState = newState;
  }

  private loadingState: boolean = true;

  get getCurrentLoadingState() : boolean {
    return this.loadingState;
  }

  set setCurrentLoadingState(newState : boolean) {
    this.loadingState = newState;
  }

  treeControlStorage: FlatNode[] = [];

  constructor(public theme: ThemeService, public isTitle: CommonService, private apiService: ApicontentService) {
    this.currentViewState = this.viewStates.WBS;
  }

  ngOnInit() {
    Promise.all([this.getData]).then(() => {
      this.setCurrentLoadingState = false;
    })
  }

  changeCurrentView(index: number) : void {
    // this.currentViewIndex = index;
    // this.currentView = this.views[index];
  }

  toggleToLevel(level: number = 0) {
    // this.treeControlStorage.forEach(element => {
    //   if (this.treeControl.isExpanded(element)) {
    //     this.treeControl.toggle(element);
    //   }
    //   if (element.level <= level && !this.treeControl.isExpanded(element)) {
    //     this.treeControl.toggle(element);
    //   }
    // })
  }

  resetFound() {
    this.nameFilter = "";
  }

  filterLeafNode(node: FlatNode): boolean {
    // if (!this.nameFilter) {
    //   return false
    // }
    // return (node.name.toLowerCase()
    //   .indexOf(this.nameFilter?.toLowerCase()) === -1 
    //   && node.code_wbs.toLowerCase()
    //   .indexOf(this.nameFilter?.toLowerCase()) === -1)
    return false;
  }

  filterLeafParentNode(node: FlatNode): boolean {
    // if (!this.nameFilter) {
    //   return false
    // }

    // for (let child of node.children) {
    //   if (child.name.toLowerCase().indexOf(this.nameFilter?.toLowerCase()) !== -1 || child.code_wbs.toLowerCase()
    //   .indexOf(this.nameFilter?.toLowerCase()) === -1) {
    //     return false;
    //   }
    // }

    // return true;
    return false;
  }

  async getCategory() : Promise<void> {
    // this.apiService
  }

  async getData() : Promise<void> {
    // this.apiService
  }

}
