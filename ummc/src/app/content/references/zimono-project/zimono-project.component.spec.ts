import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZimonoProjectComponent } from './zimono-project.component';

describe('ZimonoProjectComponent', () => {
  let component: ZimonoProjectComponent;
  let fixture: ComponentFixture<ZimonoProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZimonoProjectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZimonoProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
