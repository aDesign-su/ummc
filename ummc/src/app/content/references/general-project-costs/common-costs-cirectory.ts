export interface CommonCostsCirectory {
    id: number,
    name: string,
    type_project: string,
    type_general_project: string,
    code_work_package: string,
    work_package_code: string,
    parent: number,
    lft: number,
    rght: number,
    tree_id: number,
    level: number
}
