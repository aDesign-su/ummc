import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { CommonCostsCirectory } from './common-costs-cirectory';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-general-project-costs',
  templateUrl: './general-project-costs.component.html',
  styleUrls: ['./general-project-costs.component.css']
})
export class GeneralProjectCostsComponent implements OnInit {
  AddedAsset!: FormGroup;
  UpdatedAsset!: FormGroup;

  assets: CommonCostsCirectory[] = [];
  items: any = ''

  displayedColumns: string[] = ["id", "type_project", "type_general_project", "code_work_package", "name", "action"];

  constructor(public theme: ThemeService, public isTitle: CommonService, public show: ShowService, private formBuilder: FormBuilder, private commonCostsDirectory: ApicontentService) {
    this.AddedAsset = this.formBuilder.group({
      id: null,
      name: [null, [Validators.required]],
      parent_id: null,
      type_project: [null, [Validators.required]],
      type_general_project: [null, [Validators.required]],
      code_work_package: [null, [Validators.required]],
      lft: null,
      rght: null,
      tree_id: null,
      level: null
    });
    this.UpdatedAsset = this.formBuilder.group({
      id: null,
      name: [null, [Validators.required]],
      parent_id: null,
      type_project: [null, [Validators.required]],
      type_general_project: [null, [Validators.required]],
      code_work_package: [null, [Validators.required]],
      lft: null,
      rght: null,
      tree_id: null,
      level: null
    });
  }

  ngOnInit(): void {
    this.getAssetsData();
  }

  getAddedFormControl(name: string) {
    return this.AddedAsset.get(name);
  }

  getUpdatedFormControl(name: string) {
    return this.UpdatedAsset.get(name);
  }

  setUpdatedFields(element: CommonCostsCirectory) {
    for (let field in element) {
      this.UpdatedAsset.get(field).setValue(element[field]);
    }
  }

  getAssetsData() {
    this.commonCostsDirectory.getGeneralProjectCosts().subscribe(
      (data: CommonCostsCirectory[]) => {
        console.log(data['data']);
        this.assets = data['data'];
      }
    );
  }


  showDetails(element: CommonCostsCirectory) {
    const id = element
    if(id) {
      console.log(id);
      this.commonCostsDirectory.getGeneralProjectCostsId(id).subscribe(
        (data: CommonCostsCirectory[]) => {
          this.items = data
          console.log(data);
        }
      );
    }
  }

  addAsset() {
    this.commonCostsDirectory.addGeneralProjectCosts(this.AddedAsset.value).subscribe(
      (data: any) => {
        console.log(data);
        this.show.closedAdd(0);
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getAssetsData();
      },
      (error: any) => {
        // console.error('Failed to add data:', error);

        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  updateAsset() {
    this.commonCostsDirectory.editGeneralProjectCosts(this.UpdatedAsset.value).subscribe(
      (data: any) => {
        console.log(data);
        this.show.closedAdd(1);
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getAssetsData();
      },
      (error: any) => {
        // console.error('Failed to add data:', error);

        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  removeAsset(id: number) {
    this.commonCostsDirectory.delGeneralProjectCosts(id).subscribe(
      (data: any) => {
        console.log(data);
        this.theme.info = 'Общепроектная затрата удалена.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getAssetsData();
      }
    );
  }
}
