import _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service'
import {ShowService} from '../../../helper/show.service'
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {IntangibleAssets} from './intangible-assets'
import {ApicontentService} from '../../../api.content.service'
import {
  Category,
  DirectionFilter,
  EstimateFilter,
  ObjectFilter,
  PackageElements,
  Package,
  Wbs,
  WbsElement,
  WbsTable
} from '../../budget/wbs/wbs';
import { AuthenticationService } from 'src/app/authentication/login/login/authentication.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { CommonService } from '../../budget/project-budget/common.service';
/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: number;
  type_node: string;
  start_date: string;
  end_date: string;
  percent_date: string;
  code_wbs: string;
  category: string;
  category_id: number;
  children: Wbs[] | undefined;
  id_package: number;
  name_package: string;
  filtered: boolean;
}

@Component({
  selector: 'app-intangible-assets',
  templateUrl: './intangible-assets.component.html',
  styleUrls: ['./intangible-assets.component.css']
})
export class IntangibleAssetsComponent implements OnInit {


  constructor(public theme: ThemeService,public isTitle: CommonService, private wbsService: ApicontentService,private formBuilder: FormBuilder,){
    this.ProjectFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.DirectionFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.ObjectFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.SystemFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.WorkFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.EstimateFilters = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });

    this.ProjectPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: 0,
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.DirectionPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.ObjectPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.SystemPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.WorkPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });
    this.EstimatePackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [0, [Validators.required]],
      node_parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [0]
    });

    this.treeFlattener = new MatTreeFlattener(
      this._transformer,
      node => node.level,
      node => node.expandable,
      node => node.children
    );
    this.treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level,
      node => node.expandable
    );
    this.structureViewDataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    this.ElementFilter = this.formBuilder.group({
      name: "",
      delete_flg: "False",
      user_created: 36
    });
    this.DeleteElement = this.formBuilder.group({
      id: -1,
      name_element: ""
    })
    this.DeletePackage = this.formBuilder.group({
      id: -1,
      name_package: "",
      user_created: "36"
    })
    this.CreateElement = this.formBuilder.group({
      user_created: 36,
      delete_flg: "False",
      name: ["", [Validators.required]],
      version_id: [0, [Validators.required]],
      package: 11,
      date_create_start: null,
      date_create_end: null
    })
    this.ChangeElement = this.formBuilder.group({
      name: "",
      version_id: -1
    })
  }
  ngOnInit() {
    this.getCategory();
    this.getIntangibleAssetsWbs();
  }

  structureViewColumns: string[] = ['name', 'type_node', 'category'];
  tablesViewColumns = ['Уровень 0', 'Уровень 1', 'Уровень 2'];
  clue = ['уровень 0', 'уровень 1', 'уровень 2'];

  elements: WbsElement[] = [];
  currentChangeElement!: WbsElement;
  currentOpenElementsListId = -1;

  isElementsListVisible = false;
  isElementCreateVisible = false;
  isElementChangeVisible = false;

  selectedWbs!: Wbs;
  elementLevel!: number;
  nameFilter: string = "";
  currentFilter: number = 6;
  selectedTableId: number = 0;

  tableFilter = this.tablesViewColumns;
  tableToggle = this.tablesViewColumns;
  currentTableToggle = this.tableToggle;
  loading = true;

  views = {0: "Структурный вид", 1: "Табличный вид"}
  currentViewIndex: number = 0;
  currentView: string = this.views[this.currentViewIndex];

  wbs: Wbs[] = [];
  treeControlStorage: ExampleFlatNode[] = [];

  ProjectFilter: Package[] = []
  DirectionFilter: DirectionFilter[] = []
  ObjectFilter: ObjectFilter[] = []

  structureViewDataSource: MatTreeFlatDataSource<Wbs, ExampleFlatNode>;
  tableViewDataSources: WbsTable[] = [];

  treeControl: FlatTreeControl<ExampleFlatNode>;
  treeFlattener: MatTreeFlattener<Wbs, ExampleFlatNode>;

  categories: Category[];

  changeCurrentView(index: number) {
    this.currentViewIndex = index;
    this.currentView = this.views[index];
  }

  getUserPf: any = []
  userPf!: string;
  getPackagePf: any = []
  packagePf!: string;
  getUserDf: any = []
  userDf!: string;
  getPackageDf: any = []
  packageDf!: string;
  getUserObj: any = []
  userObj!: string;
  getPackageObj: any = []
  packageObj!: string;

  ProjectFilters!: FormGroup;
  DirectionFilters!: FormGroup;
  ObjectFilters!: FormGroup;
  SystemFilters!: FormGroup;
  EstimateFilters!: FormGroup;
  WorkFilters!: FormGroup;

  ProjectPackage!: FormGroup;
  DirectionPackage!: FormGroup;
  ObjectPackage!: FormGroup;
  SystemPackage!: FormGroup;
  EstimatePackage!: FormGroup;
  WorkPackage!: FormGroup;

  ElementFilter!: FormGroup;
  CreateElement!: FormGroup;
  DeleteElement!: FormGroup;
  DeletePackage!: FormGroup;
  ChangeElement!: FormGroup;

  structure = true;
  table = false;
  showBlock1 = false;
  createAdd = false;
  createUpdate = false;

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  get _nameElementCreate() {
    return this.CreateElement.get('name')
  }

  get _versionElementCreate() {
    return this.CreateElement.get('version_id')
  }

  create() {
    this.showBlock1 = !this.showBlock1;
  }
  closed() {
    // Cкрывает все формы
    this.isFormVisible = [false, false, false, false, false];
    this.isUpdateVisible = [false, false, false, false, false, false];
    this.createAdd = false;
    this.createUpdate = false;
  }

  clearGroup(form: FormGroup) {
      form.reset();
  }

  isFormVisible = [false, false, false, false, false];
  isUpdateVisible = [false, false, false, false, false, false];
  toggleFormVisibility(index: number, wbs: Wbs): void {
    this.selectedWbs = wbs;
    // Сначала скрыть все формы
    this.isFormVisible = [false, false, false, false, false];
    this.isUpdateVisible = [false, false, false, false, false];
    // Затем отобразить выбранную форму
    this.isFormVisible[index] = true;
    this.isElementsListVisible = false;
  }

  toggleUpdateVisibility(index, wbs: Wbs): void {
    this.selectedWbs = wbs;
    this.setFields(this.selectedWbs);
    // Сначала скрыть все формы
    this.isFormVisible = [false, false, false, false, false];
    this.isUpdateVisible = [false, false, false, false, false, false];
    // Затем отобразить выбранную форму
    this.isUpdateVisible[index] = true;
    this.isElementsListVisible = false;
  }

  setFields(selectedWbs: Wbs) {
      let form = this.findForm(selectedWbs);
      Object.keys(form.controls).forEach(key => {
          form.get(key).setValue(selectedWbs[key]);
      })
      form.get('category').setValue(selectedWbs.category_id);
  }

    findForm(selectedWbs: Wbs) {
        let form;
        switch (selectedWbs.level) {
            case 1:
                form = this.DirectionPackage;
                break;
            case 2:
                form = this.ObjectPackage;
                break;
            case 3:
                form = this.SystemPackage;
                break;
            case 4:
                form = this.WorkPackage;
                break;
            case 5:
                form = this.EstimatePackage;
                break;
            default:
                form = this.ProjectPackage;
        }
        return form;
    }

  toggleElementsListVisible(selectedWbs: Wbs, index: number, packageId: number) {
    if (packageId == this.currentOpenElementsListId) {
      this.closeElementList();
    } else {
      this.elementLevel = index;
      this.elements = [];
      this.currentOpenElementsListId = packageId;
      this.isElementsListVisible = true;
      this.selectedWbs = selectedWbs;
    }

    if (this.isElementsListVisible) {
      switch (index) {
        case 0:
          this.getProjectElement(packageId);
          break;
        case 1:
          this.getDirectionElement(packageId);
          break;
        case 2:
          this.getObjectElement(packageId);
          break;
        case 3:
          this.getSystemElement(packageId);
          break;
        case 4:
          this.getWorkElement(packageId);
          break;
        case 5:
          this.getEstimateElement(packageId);
          break;
        default:
          break;
      }
    }
  }

  closeElementList() {
    this.isElementsListVisible = false;
    this.currentOpenElementsListId = -1;
    this.elementLevel = -1;
    this.elements = [];
  }

  toggleElementCreateVisible() {
    this.isElementCreateVisible = !this.isElementCreateVisible;
  }

  toggleElementChangeVisible(element: WbsElement) {
    this.isElementChangeVisible = !this.isElementChangeVisible;
    this.currentChangeElement = element;
  }

  deletePackage(wbs: Wbs) {
    this.closed();

    switch (this.elementLevel) {
      case 0:
        this.deleteProjectPackage(wbs);
        break;
      case 1:
        this.deleteDirectionPackage(wbs);
        break;
      case 2:
        this.deleteObjectPackage(wbs);
        break;
      case 3:
        this.deleteSystemPackage(wbs);
        break;
      case 4:
        this.deleteWorkPackage(wbs);
        break;
      case 5:
        this.deleteEstimatePackage(wbs);
        break;
      default:
        break;
    }
  }

  //Получение элементов
  getProjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsProjectElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }
  getDirectionElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsDirectionElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }
  getObjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsObjectElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }
  getSystemElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsSystemElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }
  getWorkElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsWorkElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }
  getEstimateElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    let handler = this.wbsService.getIntangibleAssetsEstimateElement(ElementFilter);
    this.getElementsResponse(handler, packageId);
  }

  getElementsResponse(handler: any, packageId: number) {
    handler.subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }


  //Unused now
  // filterData(data: Wbs[]): Wbs[] {
  //   const filteredData = data.filter(element=> element.level <= this.currentFilter);
  //   const filteredData = _.cloneDeep(data.filter(element=> element.level <= this.currentFilter));
  //   filteredData.forEach(item => {
  //     if (item.children) {
  //       item.children = this.filterData(item.children);
  //     }
  //   })
  //   return filteredData;
  // }

  getCategory() {
    this.wbsService.getIntangibleAssetsCategory().subscribe(
      (data: any) => {
        this.categories = data[0]['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  getIntangibleAssetsWbs(): void {
    this.wbsService.getIntangibleAssetsWbs().subscribe(
      (data: Wbs[]) => {
        this.wbs = data;
        this.structureViewDataSource.data = this.wbs;
        this.tableViewDataSources = this.convertToTableView(this.wbs);
        this.treeControlStorage = this.treeControl.dataNodes;
        this.toggleToLevel(_);
        this.loading = !this.loading;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  toggleToLevel(level: number = 0) {
    this.treeControlStorage.forEach(element => {
      if (this.treeControl.isExpanded(element)) {
        this.treeControl.toggle(element);
      }
      if (element.level <= level && !this.treeControl.isExpanded(element)) {
        this.treeControl.toggle(element);
      }
    })
  }

  resetFound() {
    this.treeControlStorage.forEach(element => {
      if (element.filtered) {
        element.filtered = false;
      }
    })
    this.nameFilter = "";
    this.tableViewDataSources =
      this.convertToTableView(this.wbs)
  }

  performFilter() {
    this.treeControlStorage.forEach(element => {
      if (element.filtered) {
        element.filtered = false;
      }
      if (this._includes(element.name, this.nameFilter)) {
        element.filtered = true;
      }
    })
    this.tableViewDataSources =
      this.convertToTableView(this.wbs)
  }

  _includes(str, fstr) {
    return (!!fstr && fstr.length)
      ? str.includes(fstr)
      : false;
  }

  //TODO Optimize this shit
  tableRowspanInfo = {
    level0: [],
    level1: [],
    level2: [],
    level3: [],
    level4: [],
    level5: []
  };

  //TODO Optimize this shit
  convertToTableView(data: Wbs[]) {
    const convertedData: WbsTable[] = [];

    data.filter(element => element.level <= this.currentFilter).forEach((element) => {
      const rowsCount = this.getTableDataRows(element, 0);
      this.tableRowspanInfo.level0.push(rowsCount);
      const level0 = element.name;
      const coloredLevel0 = this._includes(element.name, this.nameFilter)

      let filteredElement =
        element.children?.filter(child1 => child1.level <= this.currentFilter);

      filteredElement?.forEach(child1 => {
        const level1RowsCount = this.getTableDataRows(child1, 1);
        this.tableRowspanInfo.level1.push(level1RowsCount);
        const level1 = child1.name;
        const coloredLevel1 = this._includes(child1.name, this.nameFilter)

        let filteredChild1 =
          child1.children?.filter(child2 => child2.level <= this.currentFilter);

        filteredChild1?.forEach(child2 => {
          const level2RowsCount = this.getTableDataRows(child2, 2);
          this.tableRowspanInfo.level2.push(level2RowsCount);
          const level2 = child2.name;
          const coloredLevel2 = this._includes(child2.name, this.nameFilter)

          let filteredChild2 =
            child2.children?.filter(child3 => child3.level <= this.currentFilter);

          filteredChild2?.forEach(child3 => {
            const level3RowsCount = this.getTableDataRows(child3, 3);
            this.tableRowspanInfo.level3.push(level3RowsCount);
            const level3 = child3.name;
            const coloredLevel3 = this._includes(child3.name, this.nameFilter)

            let filteredChild3 =
              child3.children?.filter(child4 => child4.level <= this.currentFilter);

            filteredChild3?.forEach(child4 => {
              const level4RowsCount = this.getTableDataRows(child4, 4);
              this.tableRowspanInfo.level4.push(level4RowsCount);
              const level4 = child4.name;
              const coloredLevel4 = this._includes(child4.name, this.nameFilter)

              let filteredChild4 =
                child4.children?.filter(child5 => child5.level <= this.currentFilter);

              filteredChild4?.forEach(child5 => {
                const level5RowsCount = this.getTableDataRows(child5, 5);

                this.tableRowspanInfo.level0.push(0);
                this.tableRowspanInfo.level1.push(0);
                this.tableRowspanInfo.level2.push(0);
                this.tableRowspanInfo.level3.push(0);
                this.tableRowspanInfo.level4.push(0);
                this.tableRowspanInfo.level5.push(level5RowsCount);

                const level5 = child5.name;
                const coloredLevel5 = this._includes(child5.name, this.nameFilter)

                for (let i = 0; i < level5RowsCount; i++) {
                  const row = {
                    level0: {
                      id: element.id,
                      name: level0,
                      package_id:
                      element.id_package,
                      wbs: element,
                      filtered: coloredLevel0
                    },
                    level1: {
                      id: element.id,
                      name: level1,
                      package_id: child1.id_package,
                      wbs: child1,
                      filtered: coloredLevel1},
                    level2: {
                      id: element.id,
                      name: level2,
                      package_id: child2.id_package,
                      wbs: child2,
                      filtered: coloredLevel2},
                    level3: {
                      id: element.id,
                      name: level3,
                      package_id: child3.id_package,
                      wbs: child3,
                      filtered: coloredLevel3},
                    level4: {
                      id: element.id,
                      name: level4,
                      package_id: child4.id_package,
                      wbs: child4,
                      filtered: coloredLevel4},
                    level5: {
                      id: element.id,
                      name: level5,
                      package_id: child5.id_package,
                      wbs: child5,
                      filtered: coloredLevel5}
                  }

                  convertedData.push(row);
                }

              })

              if (!filteredChild4 || filteredChild4.length <= 0) {
                this.tableRowspanInfo.level0.push(0);
                this.tableRowspanInfo.level1.push(0);
                this.tableRowspanInfo.level2.push(0);
                this.tableRowspanInfo.level3.push(0);
                this.tableRowspanInfo.level4.push(0);
                this.tableRowspanInfo.level5.push(1);

                const row = {
                  level0: {name: level0, package_id: element.id_package, wbs: element, filtered: coloredLevel0},
                  level1: {name: level1, package_id: child1.id_package, wbs: child1, filtered: coloredLevel1},
                  level2: {name: level2, package_id: child2.id_package, wbs: child2, filtered: coloredLevel2},
                  level3: {name: level3, package_id: child3.id_package, wbs: child3, filtered: coloredLevel3},
                  level4: {name: level4, package_id: child4.id_package, wbs: child4, filtered: coloredLevel4},
                  level5: {name: "", package_id: -1, wbs: null, filtered: false}
                }

                convertedData.push(row);
              }

              this.tableRowspanInfo.level4.pop();

            })

            if (!filteredChild3 || filteredChild3.length <= 0) {
              this.tableRowspanInfo.level0.push(0);
              this.tableRowspanInfo.level1.push(0);
              this.tableRowspanInfo.level2.push(0);
              this.tableRowspanInfo.level3.push(0);
              this.tableRowspanInfo.level5.push(1);
              this.tableRowspanInfo.level4.push(1);

              const row = {
                level0: {name: level0, package_id: element.id_package, wbs: element, filtered: coloredLevel0},
                level1: {name: level1, package_id: child1.id_package, wbs: child1, filtered: coloredLevel1},
                level2: {name: level2, package_id: child2.id_package, wbs: child2, filtered: coloredLevel2},
                level3: {name: level3, package_id: child3.id_package, wbs: child3, filtered: coloredLevel3},
                level4: {name: "", package_id: -1, wbs: null, filtered: false},
                level5: {name: "", package_id: -1, wbs: null, filtered: false}
              }

              convertedData.push(row);
            }

            this.tableRowspanInfo.level3.pop();

          })

          if (!filteredChild2 || filteredChild2.length <= 0) {
            this.tableRowspanInfo.level0.push(0);
            this.tableRowspanInfo.level1.push(0);
            this.tableRowspanInfo.level2.push(0);
            this.tableRowspanInfo.level3.push(1);
            this.tableRowspanInfo.level4.push(1);
            this.tableRowspanInfo.level5.push(1);

            const row = {
              level0: {name: level0, package_id: element.id_package, wbs: element, filtered: coloredLevel0},
              level1: {name: level1, package_id: child1.id_package, wbs: child1, filtered: coloredLevel1},
              level2: {name: level2, package_id: child2.id_package, wbs: child2, filtered: coloredLevel2},
              level3: {name: "", package_id: -1, wbs: null, filtered: false},
              level4: {name: "", package_id: -1, wbs: null, filtered: false},
              level5: {name: "", package_id: -1, wbs: null, filtered: false}
            }

            convertedData.push(row);
          }

          this.tableRowspanInfo.level2.pop();

        })

        if (!filteredChild1 || filteredChild1.length <= 0) {
          this.tableRowspanInfo.level0.push(0);
          this.tableRowspanInfo.level1.push(0);
          this.tableRowspanInfo.level2.push(1);
          this.tableRowspanInfo.level3.push(1);
          this.tableRowspanInfo.level4.push(1);
          this.tableRowspanInfo.level5.push(1);

          const row = {
            level0: {name: level0, package_id: element.id_package, wbs: element, filtered: coloredLevel0},
            level1: {name: level1, package_id: child1.id_package, wbs: child1, filtered: coloredLevel1},
            level2: {name: "", package_id: -1, wbs: null, filtered: false},
            level3: {name: "", package_id: -1, wbs: null, filtered: false},
            level4: {name: "", package_id: -1, wbs: null, filtered: false},
            level5: {name: "", package_id: -1, wbs: null, filtered: false}
          }

          convertedData.push(row);
        }

        this.tableRowspanInfo.level1.pop();

      })

      if (!filteredElement || filteredElement.length <= 0) {
        this.tableRowspanInfo.level0.push(0);
        this.tableRowspanInfo.level1.push(1);
        this.tableRowspanInfo.level2.push(1);
        this.tableRowspanInfo.level3.push(1);
        this.tableRowspanInfo.level4.push(1);
        this.tableRowspanInfo.level5.push(1);

        const row = {
          level0: {name: level0, package_id: element.id_package, wbs: element, filtered: coloredLevel0},
          level1: {name: "", package_id: -1, wbs: null, filtered: false},
          level2: {name: "", package_id: -1, wbs: null, filtered: false},
          level3: {name: "", package_id: -1, wbs: null, filtered: false},
          level4: {name: "", package_id: -1, wbs: null, filtered: false},
          level5: {name: "", package_id: -1, wbs: null, filtered: false}
        }

        convertedData.push(row);
      }

      this.tableRowspanInfo.level0.pop();

    })

    return convertedData;
  }

  getRowspan(column: string, elementIndex: number) {
    return this.tableRowspanInfo[column][elementIndex];
  }

  getTableDataRows(data: Wbs, level: number) {
    let row = (data.children && data.children.length > 0) ? 0 : 1;

    data.children?.forEach((children) =>
      row += this.getTableDataRows(children, level + 1)
    )

    return row;
  }

  setFilterData(index: number) {
    this.currentFilter = index;
    this.tableViewDataSources =
      this.convertToTableView(this.wbs);
  }

  private _transformer = (node: Wbs, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id,
      type_node: node.type_node,
      start_date: node.start_date,
      end_date: node.end_date,
      percent_date: node.percent_date,
      code_wbs: node.code_wbs,
      category: node.category,
      category_id: node.category_id,
      children: node.children,
      id_package: node.id_package,
      name_package: node.name_package,
      filtered: false
    };
  };

  hasChild(_: number, node: ExampleFlatNode): boolean {
    return node.expandable;
  }

  //Unused now
  // cutToggleToLevel(index: number) {
  //   this.currentTableToggle = [];
  //   for (let i = 0; i <= index; i++) {
  //     this.currentTableToggle[i] =  this.tablesViewColumns[this.selectedTableId][i];
  //   }
  // }

  //TODO Вынести в отдельный файл
  //Unused now
  //Добавление пакетов
  addProjectFilter() {
    this.loading = !this.loading
    const ProjectFilter = this.ProjectFilters.value;
    let handler = this.wbsService.addIntangibleAssetsProjectFilter(ProjectFilter);
    this.addPackageResponse(handler);
  }
  addDirectionFilter(form: FormGroup) {
    this.loading = !this.loading;
    const DirectionFilter = this.DirectionFilters.value;
    DirectionFilter.node_parent = this.selectedWbs.id;
    DirectionFilter.delete_flg = false;
    DirectionFilter.user_created = 36;
    this.closed();
    this.addPackageResponse(this.wbsService.addIntangibleAssetsDirectionFilter(DirectionFilter));
      this.clearGroup(form);
  }
  addObjectFilter(form: FormGroup) {
    this.loading = !this.loading
    const ObjectFilter = this.ObjectFilters.value;
    ObjectFilter.node_parent = this.selectedWbs.id;
    ObjectFilter.delete_flg = false;
    ObjectFilter.user_created = 36;
    this.closed();
    this.addPackageResponse(this.wbsService.addIntangibleAssetsObjectFilter(ObjectFilter));
      this.clearGroup(form);

  }
  addSystemFilter(form: FormGroup) {
    this.loading = !this.loading
    const SystemFilter = this.SystemFilters.value;
    SystemFilter.node_parent = this.selectedWbs.id;
    this.closed();
    this.addPackageResponse(this.wbsService.addIntangibleAssetsSystemFilter(SystemFilter));
    this.getIntangibleAssetsWbs();
      this.clearGroup(form);
  }
  addWorkFilter(form: FormGroup) {
    this.loading = !this.loading
    const WorkFilter = this.WorkFilters.value;
    WorkFilter.node_parent = this.selectedWbs.id;
    this.closed();
    this.addPackageResponse(this.wbsService.addIntangibleAssetsWorkFilter(WorkFilter));
    this.getIntangibleAssetsWbs();
      this.clearGroup(form);
  }
  addEstimateFilter(form: FormGroup) {
    this.loading = !this.loading
    const EstimateFilter = this.EstimateFilters.value;
    EstimateFilter.node_parent = this.selectedWbs.id;
    this.closed();
    this.addPackageResponse(this.wbsService.addIntangibleAssetsEstimateFilter(EstimateFilter));
    this.getIntangibleAssetsWbs();
      this.clearGroup(form);
  }

  addPackageResponse(handler: any) {
    handler.subscribe(
      (response: EstimateFilter[]) => {
        console.log('Data added successfully:', response);
        this.getIntangibleAssetsWbs();
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }

  updateProjectFilter(form: FormGroup) {
    this.loading = !this.loading
    const ProjectPackage = this.ProjectPackage.value;
      ProjectPackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsProjectPackage(ProjectPackage));
      this.clearGroup(form);

  }
  updateDirectionFilter(form: FormGroup) {
    this.loading = !this.loading
    const DirectionPackage = this.DirectionPackage.value;
      DirectionPackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsDirectionPackage(DirectionPackage));
      this.clearGroup(form);

  }
  updateObjectFilter(form: FormGroup) {
    this.loading = !this.loading
    const ObjectPackage = this.ObjectPackage.value;
      ObjectPackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsObjectPackage(ObjectPackage));
      this.clearGroup(form);

  }
  updateSystemFilter(form: FormGroup) {
    this.loading = !this.loading
    const SystemPackage = this.SystemPackage.value;
      SystemPackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsSystemPackage(SystemPackage));
      this.clearGroup(form);

  }
  updateWorkFilter(form: FormGroup) {
    this.loading = !this.loading
    const WorkPackage = this.WorkPackage.value;
      WorkPackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsWorkPackage(WorkPackage));
      this.clearGroup(form);
  }
  updateEstimateFilter(form: FormGroup) {
    this.loading = !this.loading
    const EstimatePackage = this.EstimatePackage.value;
      EstimatePackage['user_created'] = 1;
    this.closed();
    this.updatePackageResponse(this.wbsService.updateIntangibleAssetsEstimatePackage(EstimatePackage));
      this.clearGroup(form);
  }

  updatePackageResponse(handler: any) {
    handler.subscribe(
      (response: EstimateFilter[]) => {
        console.log('Data added successfully:', response);
        this.getIntangibleAssetsWbs();
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }


  //Удаление пакетов
  deleteProjectPackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsProjectPackage(DeletePackage);

    this.deletePackageResponse(handler);
  }
  deleteDirectionPackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsDirectionPackage(DeletePackage);

    this.deletePackageResponse(handler);
  }
  deleteObjectPackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsObjectPackage(DeletePackage);


    this.deletePackageResponse(handler);
  }
  deleteSystemPackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsSystemPackage(DeletePackage);


    this.deletePackageResponse(handler);
  }

  deleteWorkPackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsWorkPackage(DeletePackage);

    this.deletePackageResponse(handler);
  }
  deleteEstimatePackage(wbs: Wbs) {
    this.loading = !this.loading
    let DeletePackage = this.DeletePackage.value;
    DeletePackage.id = wbs.id;
    DeletePackage.name_package = wbs.name_package;
    let handler = this.wbsService.deleteIntangibleAssetsEstimatePackage(DeletePackage);

    this.deletePackageResponse(handler);
  }

  deletePackageResponse(handler: any) {
    this.closeElementList()
    handler.subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getIntangibleAssetsWbs();
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }
}
