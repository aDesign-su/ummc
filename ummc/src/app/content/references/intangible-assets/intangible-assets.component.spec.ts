import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntangibleAssetsComponent } from './intangible-assets.component';

describe('IntangibleAssetsComponent', () => {
  let component: IntangibleAssetsComponent;
  let fixture: ComponentFixture<IntangibleAssetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntangibleAssetsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IntangibleAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
