export interface IntangibleAssets {
  id: number,
  name: string,
  parent_id: number,
  type_project: string,
  type_general_project_work: string,
  code_work_package: string,
  lft: number,
  rght: number,
  tree_id: number,
  level: number
}
