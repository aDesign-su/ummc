import { Component, OnInit, ViewChild } from '@angular/core';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Category } from '../category/category';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { ApicontentService } from 'src/app/api.content.service';
import { CategoryService } from '../category/category.service';
import { MatTableDataSource } from '@angular/material/table';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class RegionsComponent implements OnInit {
  constructor(public theme: ThemeService,public isTitle: CommonService, private cat: ApicontentService, public categoryService: CategoryService,public show:ShowService,private formBuilder: FormBuilder) { 
    this.editForm = this.formBuilder.group({
      title: "",
    });
  }
  element: number
  editForm: FormGroup
  ngOnInit() { this.getDataSourceRegion() }
  
  displayedColumns: string[] = ['title', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSourceRegion.paginator = this.paginator;
  }
  dataSourceRegion = new MatTableDataSource<Category>([]);
  
  getDataSourceRegion(): void {
    this.cat.getRegionReference().subscribe(
      (data: Category[]) => {
        this.dataSourceRegion.data = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  create() {
    this.editForm.reset()
  }
  addRegion() {
    const title = this.editForm.value;

    this.cat.addRegion(title).subscribe(
      (response: Category[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSourceRegion();
      },
    );
  }

  editRegion(element: Category) {
    this.element = element.id; // Установим значение свойства element
    this.editForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  saveRegion() {
    if (this.element) {
      let editedObject = this.editForm.value;
      this.cat.editRegion(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getDataSourceRegion();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  
  delRegion(element: Category) {
    const delElement: any = element.id
    this.cat.delRegion(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Регион удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSourceRegion();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }
}
