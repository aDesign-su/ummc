import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { CommonService } from '../../budget/project-budget/common.service';
import { UnitRate, CategoryUnitRate, ResourceUnitPrices, UnitRateFullData, UnitOfMeasure } from './directory-of-unit-prices';
import { ResourceDirectoryData } from '../resource-directory/resource-directory';


declare var bootstrap: any;
@Component({
  selector: 'app-directory-of-unit-prices',
  templateUrl: './directory-of-unit-prices.component.html',
  styleUrls: ['./directory-of-unit-prices.component.css']
})
export class DirectoryOfUnitPricesComponent implements OnInit {
  editMode = false;
  FormAddEditUnitPrices: FormGroup;
  UnitPricesTableSource = this.initTable<UnitRate>();

  FormAddEditCategoryUnitRate: FormGroup;
  CategoryUnitPricesTableSource = this.initTable<CategoryUnitRate>();

  FormAddEditResourceUnitPrices: FormGroup;
  ResourceUnitPricesTableSource = this.initTable<ResourceUnitPrices>();

  FullUnitPricesTableSource = this.initTable<UnitRateFullData>();


  @ViewChild('PaginatorUnitPrices') paginatorForUnitPrices: MatPaginator;
  @ViewChild('paginatorForСategoryUnitPrices') paginatorForСategoryUnitPrices: MatPaginator;
  @ViewChild('paginatorResourceUnitPrices') paginatorForResourceUnitPrices: MatPaginator;

  // Для поиска внутри формы
  filteredSelectCategoryUnitPrices: CategoryUnitRate[]
  public SelectCategoryUnitPrices: CategoryUnitRate[]


  //Ресурс
  filteredSelectResourceUnitPrices: ResourceDirectoryData[]
  public SelectResourceUnitPrices: ResourceDirectoryData[]


  //Ресурс
  filteredSelectUnitPrices: UnitRate[]
  public SelectUnitPrices: UnitRate[]


  //Еденицы измерения
  filteredSelectUnitOfMeasure: UnitOfMeasure[]
  public SelectUnitOfMeasure: UnitOfMeasure[]


  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }

  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.FormAddEditUnitPrices = this.fb.group({
      id: [null],
      unit_rate_category: [null],
      unit_of_measure: [null],
      unit_rate_cypher: [null],
      unit_rate_title: [null],
      comments: [null],
      volume: [null],
    });

    this.FormAddEditCategoryUnitRate = this.fb.group({
      id: [null],
      unit_rate_category_code: [null],
      unit_rate_category_title: [null],
    });

    this.FormAddEditResourceUnitPrices = this.fb.group({
      id: [null],
      resource_code: "",
      h_h: [null],
      m_h: [null],
      comments: "",
      unit_rate: [null],
      resource: [null]
    });

  }

  ngOnInit() {
    this.getDataUnitPrices()
    this.getCategoryUnitPrices()
    this.getResourceUnitPrices()
    this.getResourceDirectory()
    this.getUnitsOfMeasurement()
    this.getFullDataUnitPrices()
  }

  ngAfterViewInit() {
    this.UnitPricesTableSource.paginator = this.paginatorForUnitPrices;
    this.CategoryUnitPricesTableSource.paginator = this.paginatorForСategoryUnitPrices;
    this.ResourceUnitPricesTableSource.paginator = this.paginatorForResourceUnitPrices;
  }
  add() {
    this.editMode = false

    this.FormAddEditUnitPrices.reset()
    this.FormAddEditCategoryUnitRate.reset()
    this.FormAddEditResourceUnitPrices.reset()

  }

  getFullDataUnitPrices(): void {
    this.params.getFullUnitRate().subscribe(
      (data: UnitRateFullData[]) => {
        this.FullUnitPricesTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  getDataUnitPrices(): void {
    this.params.getUnitRate().subscribe(
      (data: UnitRate[]) => {
        this.UnitPricesTableSource.data = data;
        this.SelectUnitPrices = data;
        this.filteredSelectUnitPrices = this.SelectUnitPrices.slice();
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  // Добавить Изменить  
  AddEditUnitPrices() {
    if (this.FormAddEditUnitPrices.valid) {
      const formData = { ...this.FormAddEditUnitPrices.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)
     
      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateUnitPrices(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createUnitPrices(formValueWithoutId);
      }

    }
  }


  // Cоздание
  createUnitPrices(data: UnitRate) {
    console.log('create:', data)
    this.params.addUnitPrices(data).subscribe(
      (data: UnitRate) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлена еденичная расценка `);
        this.UnitPricesTableSource.data = [...this.UnitPricesTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditUnitPrices.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }

  // Удаление разделительную ведомоCть
  deleUnitPrices(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleUnitPrices(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('РеCурC удален');
            const index = this.UnitPricesTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.UnitPricesTableSource.data.splice(index, 1);
              this.UnitPricesTableSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }
  updateUnitPrices(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateUnitPrices(id, data).subscribe(
      (response: UnitRate) => {
        console.log('Новые данные:', response);

        // Находим индекC обновленного элемента
        const updatedItemIndex = this.UnitPricesTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в маCCиве
        // if (updatedItemIndex > -1) {
        //   this.UnitPricesTableSource.data[updatedItemIndex] = response;
        // }
        // // Обновляем таблицу C новым маCCивом данных
        // this.UnitPricesTableSource.data = [...this.UnitPricesTableSource.data];

        this.theme.openSnackBar('Изменения Cохранены')
        const offcanvasElement = document.getElementById('addEditUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditUnitPrices.reset()
        this.editMode = false
        this.ngOnInit()
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  editUnitPrices(element: UnitRate) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента ед.раCцен.
    this.FormAddEditUnitPrices.setValue({
      id: element.id,
      unit_rate_category: element.unit_rate_category_id,
      unit_of_measure: element.unit_of_measure_id,
      unit_rate_cypher: element.unit_rate_cypher,
      unit_rate_title: element.unit_rate_title,
      comments: element.comments,
      volume: element.volume
    });
    this.editMode = true
    console.log(this.FormAddEditUnitPrices.value)
  }










  getCategoryUnitPrices(): void {
    this.params.getCategoryUnitPrices().subscribe(
      (data: CategoryUnitRate[]) => {
        this.CategoryUnitPricesTableSource.data = data
        this.SelectCategoryUnitPrices = data;
        this.filteredSelectCategoryUnitPrices = this.SelectCategoryUnitPrices.slice();
        console.log(data)
        console.log(this.filteredSelectCategoryUnitPrices)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

  }



  // Добавить Изменить  
  AddEditCategoryUnitPrices() {
    if (this.FormAddEditCategoryUnitRate.valid) {
      const formData = { ...this.FormAddEditCategoryUnitRate.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateCategoryUnitPrices(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createCategoryUnitPrices(formValueWithoutId);
      }

    }
  }


  deleCategoryUnitPrices(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleCategoryUnitPrices(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Категория удалена');
            const index = this.CategoryUnitPricesTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.CategoryUnitPricesTableSource.data.splice(index, 1);
              this.CategoryUnitPricesTableSource._updateChangeSubscription();
            }
            this.getCategoryUnitPrices()
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  // Cоздание
  createCategoryUnitPrices(data: UnitRate) {
    console.log('create:', data)
    this.params.addCategoryUnitPrices(data).subscribe(
      (data: CategoryUnitRate) => {
        console.log(data)
        this.theme.openSnackBar(`Категория добавлена`);
        this.CategoryUnitPricesTableSource.data = [...this.CategoryUnitPricesTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditCategoryUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditCategoryUnitRate.reset()
        this.ngOnInit()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }


  updateCategoryUnitPrices(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateCategoryUnitPrices(id, data).subscribe(
      (response: CategoryUnitRate) => {
        console.log('Новые данные:', response);

        // Находим индекс обновленного элемента
        const updatedItemIndex = this.CategoryUnitPricesTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.CategoryUnitPricesTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу C новым массивом данных
        this.CategoryUnitPricesTableSource.data = [...this.CategoryUnitPricesTableSource.data];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditCategoryUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditCategoryUnitRate.reset()
        this.editMode = false
        this.ngOnInit()

      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }
  editCategoryUnitPrices(element: any) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditCategoryUnitRate.setValue({
      id: element.id,
      unit_rate_category_title: element.unit_rate_category_title,
      unit_rate_category_code: element.unit_rate_category_code

    });
    this.editMode = true
    console.log(this.FormAddEditCategoryUnitRate.value)
  }





  getResourceUnitPrices(): void {
    this.params.getResourceUnitPrices().subscribe(
      (data: ResourceUnitPrices[]) => {
        this.ResourceUnitPricesTableSource.data = data
        console.log(data)
        console.log(this.filteredSelectResourceUnitPrices)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

  }



  // Добавить Изменить  
  AddEditResourceUnitPrices() {
    if (this.FormAddEditResourceUnitPrices.valid) {
      const formData = { ...this.FormAddEditResourceUnitPrices.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateResourceUnitPrices(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createResourceUnitPrices(formValueWithoutId);
      }

    }
  }


  deleResourceUnitPrices(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleResourceUnitPrices(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Ресурс удален');
            const index = this.ResourceUnitPricesTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.ResourceUnitPricesTableSource.data.splice(index, 1);
              this.ResourceUnitPricesTableSource._updateChangeSubscription();
            }
            this.getResourceUnitPrices()
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  // Cоздание
  createResourceUnitPrices(data: UnitRate) {
    console.log('create:', data)
    this.params.addResourceUnitPrices(data).subscribe(
      (data: ResourceUnitPrices) => {
        console.log(data)
        this.theme.openSnackBar(`Ресурс добавлен`);
        this.ResourceUnitPricesTableSource.data = [...this.ResourceUnitPricesTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditResourceUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditResourceUnitPrices.reset()
        this.ngOnInit()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }


  updateResourceUnitPrices(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateResourceUnitPrices(id, data).subscribe(
      (response: ResourceUnitPrices) => {
        console.log('Новые данные:', response);

        // Находим индекс обновленного элемента
        const updatedItemIndex = this.ResourceUnitPricesTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.ResourceUnitPricesTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу C новым массивом данных
        this.ResourceUnitPricesTableSource.data = [...this.ResourceUnitPricesTableSource.data];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditResourceUnitPrices');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditResourceUnitPrices.reset()
        this.editMode = false
        this.ngOnInit()

      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }
  editResourceUnitPrices(element: any) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditResourceUnitPrices.setValue({
      id: element.id,
      resource_code: element.resource_code,
      h_h: element.h_h,
      m_h: element.m_h,
      comments: element.comments,
      unit_rate: element.unit_rate_id,
      resource: element.resource_id

    });

    this.editMode = true
    console.log(this.FormAddEditResourceUnitPrices.value)
  }


  getResourceDirectory(): void {
    this.params.getResourceDirectory().subscribe(
      (data: ResourceDirectoryData[]) => {
        this.SelectResourceUnitPrices = data;
        this.filteredSelectResourceUnitPrices = this.SelectResourceUnitPrices.slice();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  getUnitsOfMeasurement(): void {
    this.params.getUnitsOfMeasurement().subscribe(
      (data: UnitOfMeasure[]) => {
        this.SelectUnitOfMeasure = data;
        this.filteredSelectUnitOfMeasure = this.SelectUnitOfMeasure.slice();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

}

