
export interface UnitRate {
    id: number
    unit_rate_category: string
    unit_of_measure: string
    unit_rate_cypher: string
    unit_rate_title: string
    comments: string
    volume: string
    unit_rate_category_id:number,
    unit_of_measure_id:number
}

export interface CategoryUnitRate {
    id: number
    unit_rate_category_code: string
    unit_rate_category_title: string
}

export interface ResourceUnitPrices {
    id: number
    unit_rate_id: number
    resource_code: string
    h_h: number
    m_h: number
    resource_id: number
    comments: string
    resource_title: string
    resource_type: string
}

export interface UnitRateFullData {
    id: number
    unit_rate_id: number
    resource_code: string
    h_h: number
    m_h: number
    resource_id: number
    comments: string
    unit_rate_category_cypher: string
    unit_rate_category_title: string
    unit_rate_cypher: string
    unit_rate_title: string
    unit_rate_resource_cypher: string
    resource_title: string
    resource_type: string
    unit_rate_volume: number
    unit_rate_measure: string
}

export interface UnitOfMeasure {
    id: number
    title: string
}