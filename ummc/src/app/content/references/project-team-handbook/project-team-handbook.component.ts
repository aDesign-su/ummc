import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Employee, TeamHandbook } from './ProjectTeamHandbook';
import * as jmespath from 'jmespath';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { ProjectRegister } from '../../project-register/project-register';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;

@Component({
  selector: 'app-project-team-handbook',
  templateUrl: './project-team-handbook.component.html',
  styleUrls: ['./project-team-handbook.component.css']
})
export class ProjectTeamHandbookComponent implements OnInit {
  // TeamArray: Employee[] = [];
  TeamArray: MatTableDataSource<Employee>;
  SelectPosition: string[]
  SelectCiti: string[]
  displayedColumns: string[] = ['id', 'full_name', 'full_address', 'position', 'project', 'phone', 'email', 'room_number', 'action'];
  EmployeeAddForm!: FormGroup;
  EmployeeEditForm!: FormGroup;

  public selectedPosition: string = 'Все';
  public selectedCity: string = 'Все';
  filterDictionary = new Map<string, any>();
  projects: ProjectRegister[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(public theme: ThemeService, private fb: FormBuilder, public isTitle: CommonService, public params: ApicontentService, private _snackBar: MatSnackBar) {
    this.TeamArray = new MatTableDataSource<Employee>([]);
    this.EmployeeAddForm = this.fb.group({
      full_name: "",
      position: "",
      project: "",
      phone: "",
      email: "",
      city: "",
      street: "",
      house_number: "",
      building: "",
      floor: "",
      room_number: ""
    });
    this.EmployeeEditForm = this.fb.group({
      id: "",
      full_name: "",
      position: "",
      project: "",
      phone: "",
      email: "",
      city: "",
      street: "",
      house_number: "",
      building: "",
      floor: "",
      room_number: ""
    });
  }
  ngOnInit() {
    this.getTeamHandbookData()
    this.getProject();
  }
  ngAfterViewInit() {
    this.TeamArray.paginator = this.paginator;
    this.TeamArray.sort = this.sort;
  }



  applyFilterByPosition(event: any) {
    this.filterDictionary.set('position', event.value);
    this.applyFilters();
  }

  applyFilterByCity(event: any) {
    this.filterDictionary.set('city', event.value);
    this.applyFilters();
  }

  applyFilters() {
    this.TeamArray.filterPredicate = (data: any, filter: string) => {
      let filterMap = new Map<string, any>(JSON.parse(filter));
      for (let [key, value] of filterMap) {
        if (value === "Все") continue; // Если выбрано "Все", пропускаем этот фильтр
        if (data[key] !== value) return false;
      }
      return true;
    };
    this.TeamArray.filter = JSON.stringify(Array.from(this.filterDictionary.entries()));
  }


  getTeamHandbookData(): void {
    this.params.getTeamHandbook().subscribe(
      (data: TeamHandbook[]) => {
        this.TeamArray = new MatTableDataSource(jmespath.search(data, 'employees'));
        this.SelectPosition = jmespath.search(data, 'unique_positions');
        this.SelectCiti = jmespath.search(data, 'unique_cities');
        this.SelectPosition = ['Все', ...this.SelectPosition];
        this.SelectCiti = ['Все', ...this.SelectCiti];

        // Сброс значений для двусторонней привязки
        this.selectedPosition = 'Все';
        this.selectedCity = 'Все';
        if (this.paginator && this.sort) {
          this.TeamArray.paginator = this.paginator;
          this.TeamArray.sort = this.sort;
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  AddEmployee() {
    const formValue = this.EmployeeAddForm.value;
    this.params.AddEmployee(formValue).subscribe(
      (response: Employee) => {
        console.log('Data added successfully:', response);
        this.EmployeeAddForm.reset(); // Сбросить форму

        const offcanvasElement = document.getElementById('offcanvasScrolling');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.getTeamHandbookData();
        this.theme.openSnackBar('Пользователь создан');
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        if (error.status === 400 && error.error) {
          let errorMsg = '';
          errorMsg += `${error.error['error']}`;
          this.theme.openSnackBar(errorMsg);
        }
      }
    );
  }


  delEmployee(id) {
    console.log(id)
    this.params.DelEmployee(id).subscribe(
      (response: any) => {
        console.log('Data del:', response);
        this.getTeamHandbookData();
      },
      (error: any) => {
        console.error('Failed to del data:', error);

      }
    );
  }
  editEmployeeDetails(obj) {
    console.log(obj)

    this.EmployeeEditForm.patchValue({
      id: obj.id,
      full_name: obj.full_name,
      position: obj.position,
      project: obj.project,
      phone: obj.phone,
      email: obj.email,
      city: obj.city,
      street: obj.street,
      house_number: obj.house_number,
      building: obj.building,
      floor: obj.floor,
      room_number: obj.room_number
    })
  }


  updateEmployee() {
    const formValue = this.EmployeeEditForm.value;
    console.log(formValue)
    console.log(formValue.id)
    this.params.UpdateEmployee(formValue.id, formValue).subscribe(

      (response) => {
        console.log('Data update:', response);
        this.EmployeeEditForm.reset(); // Сбросить форму
        const offcanvasElement = document.getElementById('editEmployee');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.getTeamHandbookData();
        this.theme.openSnackBar('Изменено ');
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        if (error.status === 400 && error.error) {
          let errorMsg = '';
          Object.keys(error.error).forEach(key => {
            errorMsg += `${error.error[key].join(' ')}\n`;
          });
          this.theme.openSnackBar(errorMsg);
        }
      }
    );
  }


  getProject() {
    this.params.getProjects().subscribe(
      (response) => {
        console.log('Data :', response);
        this.projects = response;
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }

}
