export interface Employee {
    id?: number;
    full_address?: string;
    full_name: string;
    position: string;
    project: number;
    project_name:string;
    phone: string;
    email: string;
    city: string;
    street: string;
    house_number: string;
    building: string;
    floor: string;
    room_number: string;
}

export interface Select {
    unique_positions: string[];
    unique_cities: string[];
}

export interface TeamHandbook {
    employees: Employee[];
    unique_positions: string[];
    unique_cities: string[];
}