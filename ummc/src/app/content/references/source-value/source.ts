export interface Source {
    id: any,
    coefficient_rate: number,
    name: string,
    percent_rate: number,
    comment: string,
    editing?: any
}
