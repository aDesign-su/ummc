/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SourceValueComponent } from './source-value.component';

describe('SourceValueComponent', () => {
  let component: SourceValueComponent;
  let fixture: ComponentFixture<SourceValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
