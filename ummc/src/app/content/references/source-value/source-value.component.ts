import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Ratio } from '../ratio/ratio';
import { Source } from './source';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-source-value',
  templateUrl: './source-value.component.html',
  styleUrls: ['./source-value.component.css']
})
export class SourceValueComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder, private source: ApicontentService) { 
    this.sourceForm = this.formBuilder.group({
      name: null,
      percent_rate: "",
      comment: ""
      });
    this.editSourceForm = this.formBuilder.group({
      name: null,
      percent_rate: "",
      comment: ""
    });
  }

  ngOnInit() {
    this.getDataSource()
  }

  editSourceForm: FormGroup;
  sourceForm!: FormGroup;

  element: any;
  displayedColumns: string[] = ['serial_number', 'name', 'coefficient_rate', 'percent_rate', 'comment', 'actions'];
  dataSource = new MatTableDataSource<Source>([]);
  
  @ViewChild('paginator') paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getDataSource(): void {
    this.source.getSource().subscribe(
      (data: Source[]) => {
        const source = data

        this.dataSource = new MatTableDataSource<Source>(source);
        this.dataSource.paginator = this.paginator;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  
  addSource() {
    const source = this.sourceForm.value;
    this.source.addSource(source).subscribe(
      (response: Source[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.getDataSource();
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editSource(element: Source) {
    this.element = element.id; // Установим значение свойства element
    this.editSourceForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  saveSource() {
    if (this.element) {
      let editedObject = this.editSourceForm.value;
      this.source.editSource(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getDataSource();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delSource(element: Source) {
    const delElement = element.id
    this.source.delSource(delElement).subscribe(
      (data: any) => {
        this.getDataSource();
        this.theme.info = 'Источник стоимости удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

}
