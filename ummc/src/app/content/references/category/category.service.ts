import { Injectable, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { Category } from './category';
import { MatTableDataSource } from '@angular/material/table';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private cat: ApicontentService) { }

  dataSourcePack = new MatTableDataSource<Category>([]);
  dataSourceType = new MatTableDataSource<Category>([]);
  dataSourceEqui = new MatTableDataSource<Category>([]);

  getDataSourcePack(): void {
    this.cat.getPackageReference().subscribe(
      (data: Category[]) => {
        this.dataSourcePack.data = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDataSourceType(): void {
    this.cat.getTypeReference().subscribe(
      (data: Category[]) => {
        this.dataSourceType.data = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDataSourceEqui(): void {
    this.cat.getEquipmentReference().subscribe(
      (data: Category[]) => {
        this.dataSourceEqui.data = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
