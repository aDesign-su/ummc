import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { CategoryService } from '../category.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';
import { ApicontentService } from 'src/app/api.content.service';
import { Category } from '../category';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-obj',
  templateUrl: './obj.component.html',
  styleUrls: ['./obj.component.css']
})

export class ObjComponent implements OnInit {
  constructor(public theme: ThemeService, private cat: ApicontentService, public categoryService: CategoryService,public show:ShowService,private formBuilder: FormBuilder) { 
    this.editForm = this.formBuilder.group({
      title: "",
    });
  }
  element: number
  editForm: FormGroup
  ngOnInit() { this.categoryService.getDataSourceType() }

  displayedColumns: string[] = ['title', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.categoryService.dataSourceType.paginator = this.paginator;
  }

  editObj(element: Category) {
    this.element = element.id; // Установим значение свойства element
    this.editForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  saveObj() {
    if (this.element) {
      let editedObject = this.editForm.value;
      this.cat.editType(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.categoryService.getDataSourceType();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  delObj(element: Category) {
    const delElement: any = element.id
    this.cat.delType(delElement).subscribe(
      (data: any) => {
        this.categoryService.getDataSourceType();
        this.theme.info = 'Категория удалена.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

}