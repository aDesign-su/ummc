import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { CategoryService } from '../category.service';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Category } from '../category';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-pack',
  templateUrl: './pack.component.html',
  styleUrls: ['./pack.component.css']
})
export class PackComponent implements OnInit {
  constructor(public theme: ThemeService, private cat: ApicontentService, public categoryService: CategoryService,public show:ShowService,private formBuilder: FormBuilder) { 
    this.editForm = this.formBuilder.group({
      title: "",
    });
  }
  element: number
  editForm: FormGroup
  ngOnInit() { this.categoryService.getDataSourcePack() }
  
  displayedColumns: string[] = ['title', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.categoryService.dataSourcePack.paginator = this.paginator;
  }

  editPack(element: Category) {
    this.element = element.id; // Установим значение свойства element
    this.editForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  savePack() {
    if (this.element) {
      let editedObject = this.editForm.value;
      this.cat.editPackage(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.categoryService.getDataSourcePack();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  
  delPack(element: Category) {
    const delElement: any = element.id
    this.cat.delPackage(delElement).subscribe(
      (data: any) => {
        this.categoryService.getDataSourcePack();
        this.theme.info = 'Категория удалена.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }
}