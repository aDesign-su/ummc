import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ApicontentService } from 'src/app/api.content.service';
import { CategoryService } from '../category.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';
import { Category } from '../category';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-equip',
  templateUrl: './equip.component.html',
  styleUrls: ['./equip.component.css']
})
export class EquipComponent implements OnInit {
  constructor(public theme: ThemeService, private cat: ApicontentService, public categoryService: CategoryService,public show:ShowService,private formBuilder: FormBuilder) { 
    this.editForm = this.formBuilder.group({
      title: "",
    });
  }
  element: number
  editForm: FormGroup
  ngOnInit() { this.categoryService.getDataSourceEqui() }

  displayedColumns: string[] = ['title', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.categoryService.dataSourceEqui.paginator = this.paginator;
  }

  editEqiup(element: Category) {
    this.element = element.id; // Установим значение свойства element
    this.editForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }
  saveEquip() {
    if (this.element) {
      let editedObject = this.editForm.value;
      this.cat.editEquip(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.categoryService.getDataSourceEqui();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  delEqiup(element: Category) {
    const delElement: any = element.id
    this.cat.delEquip(delElement).subscribe(
      (data: any) => {
        this.categoryService.getDataSourceEqui();
        this.theme.info = 'Категория удалена.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

}