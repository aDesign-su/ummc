import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Category } from './category';
import { CategoryService } from './category.service';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder, private cat: ApicontentService, public categoryService: CategoryService) { 
    this.formInit()
  }

  ngOnInit() {
  }
  createForm: FormGroup
  
  formInit() {
    this.createForm = this.formBuilder.group({
      title: "",
    });
  }
  selectedElementType: string = ''; // "obj", "equipment", "work_package"
  elementToShowBlock = {
    'obj': 0,         // Для типа 'obj' показывать showBlock[0]
    'equipment': 1,   // Для типа 'equipment' показывать showBlock[1]
    'work_package': 2 // Для типа 'work_package' показывать showBlock[2]
  };
  
  selectElementType(elementType: string): void {
    this.selectedElementType = elementType;
    const showBlockIndex = this.elementToShowBlock[elementType];
  
    if (showBlockIndex !== undefined) {
      this.show.toggleShowBlock(showBlockIndex); // Показать форму
      this.selectedElementType = elementType; // Установить тип элемента (obj, equipment, work_package)
    }
    // console.log(elementType);
  }
  
  create(elementType: string) {
    if (elementType == 'obj') {
        const obj = this.createForm.value;
        this.cat.addType(obj).subscribe(
          (response: Category[]) => {
            for (let i = 0; i < this.show.showBlock.length; i++) {
              this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
            }
            this.categoryService.getDataSourceType();
            this.theme.access = 'Данные добавлены успешно.'
            setTimeout(() => {
              this.theme.access = null;
            }, 5000);
          },
          (error: any) => {
            this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
            setTimeout(() => {
              this.theme.error = null;
            }, 5000);
          }
        );
      // console.log(1)
    } else if (elementType == 'equipment') {

      const equ = this.createForm.value;
      this.cat.addEquip(equ).subscribe(
        (response: Category[]) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.categoryService.getDataSourceEqui();
          this.theme.access = 'Данные добавлены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
        },
        (error: any) => {
          this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
      // console.log(2)
    } else if (elementType == 'work_package') {

      const pac = this.createForm.value;
      this.cat.addPack(pac).subscribe(
        (response: Category[]) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          
          this.categoryService.getDataSourcePack();
          this.theme.access = 'Данные добавлены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
        },
        (error: any) => {
          this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
      // console.log(3)
    }
  }
}
