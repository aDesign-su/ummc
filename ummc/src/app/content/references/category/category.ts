export interface Category {
    id: number
    title: string
    editing?: any
}
