export interface ResourceDirectoryData {
  id: number
  title: string
  type_id: number
  category_id: number
  cypher: string
  hour_rate: number
  type_title: string
  category_title: string
}

export interface СategoryTypeList {
  id: number
  title: string
}