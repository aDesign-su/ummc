import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { ResourceDirectoryData, СategoryTypeList } from './resource-directory';
import { CommonService } from '../../budget/project-budget/common.service';


declare var bootstrap: any;

@Component({
  selector: 'app-resource-directory',
  templateUrl: './resource-directory.component.html',
  styleUrls: ['./resource-directory.component.css']
})
export class ResourceDirectoryComponent implements OnInit {
  editMode = false;
  FormAddEditResourceDirectory: FormGroup;
  FormAddEditType: FormGroup;
  FormAddEditCategory: FormGroup;

  ResourceDirectoryTableSource = this.initTable<ResourceDirectoryData>();
  CategoryTableSource = this.initTable<СategoryTypeList>();
  TypeTableSource = this.initTable<СategoryTypeList>();

  @ViewChild('PaginatorResourceDirectory') paginatorForResourceDirectory: MatPaginator;
  @ViewChild('paginatorForResourceDirectoryType') paginatorForResourceDirectoryType: MatPaginator;
  @ViewChild('paginatorForResourceDirectoryСategory') paginatorForResourceDirectoryСategory: MatPaginator;

  // Для поиска внутри формы
  filteredSelectCategoryList: СategoryTypeList[]
  public SelectCategoryList: СategoryTypeList[]

  filteredSelectTypeList: СategoryTypeList[]
  public SelectTypeList: СategoryTypeList[]

  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }

  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.FormAddEditResourceDirectory = this.fb.group({
      id: [null],
      title: [''],
      cypher: [''],
      hour_rate: [null],
      type: [null],
      category: [null],
    });

    this.FormAddEditType = this.fb.group({
      id: [null],
      title: [''],
    });

    this.FormAddEditCategory = this.fb.group({
      id: [null],
      title: [''],

    });
  }

  ngOnInit() {
    this.getDataSeparationSheets()
    this.СategoryTypeList()
  }

  ngAfterViewInit() {
    this.ResourceDirectoryTableSource.paginator = this.paginatorForResourceDirectory;
    this.TypeTableSource.paginator = this.paginatorForResourceDirectoryType;
    this.CategoryTableSource.paginator = this.paginatorForResourceDirectoryСategory;
  }

  add() {
    this.editMode = false

    this.FormAddEditResourceDirectory.reset()
    this.FormAddEditType.reset()
    this.FormAddEditCategory.reset()
  
  }

  getDataSeparationSheets(): void {
    this.params.getResourceDirectory().subscribe(
      (data: ResourceDirectoryData[]) => {
        this.ResourceDirectoryTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  // Добавить Изменить  Ресурс
  AddEditResourceDirectory() {
    if (this.FormAddEditResourceDirectory.valid) {
      const formData = { ...this.FormAddEditResourceDirectory.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateResourceDirectory(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createResourceDirectory(formValueWithoutId);
      }

    }
  }


  // Создание
  createResourceDirectory(data: ResourceDirectoryData) {
    console.log('create:', data)
    this.params.addResourceDirectory(data).subscribe(
      (data: ResourceDirectoryData) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлен ресурс `);
        this.ResourceDirectoryTableSource.data = [...this.ResourceDirectoryTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditResourceDirectory');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditResourceDirectory.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }

  // Удаление разделительную ведомость
  deleResourceDirectory(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleResourceDirectory(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Ресурс удален');
            const index = this.ResourceDirectoryTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.ResourceDirectoryTableSource.data.splice(index, 1);
              this.ResourceDirectoryTableSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  updateResourceDirectory(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateResourceDirectory(id, data).subscribe(
      (response: ResourceDirectoryData) => {
        console.log('Новые данные:', response);

        // Находим индекс обновленного элемента
        const updatedItemIndex = this.ResourceDirectoryTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.ResourceDirectoryTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу с новым массивом данных
        this.ResourceDirectoryTableSource.data = [...this.ResourceDirectoryTableSource.data];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditResourceDirectory');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditResourceDirectory.reset()
        this.editMode = false
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  editResourceDirectory(element: ResourceDirectoryData) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditResourceDirectory.setValue({
      id: element.id,
      title: element.title,
      cypher: element.cypher,
      hour_rate: element.hour_rate,
      type: element.type_id,
      category: element.category_id,
    });
    this.editMode = true
    console.log(this.FormAddEditResourceDirectory.value)
  }




  СategoryTypeList(): void {
    this.params.getCategoryList().subscribe(
      (data: СategoryTypeList[]) => {

        this.SelectCategoryList = data;
        this.filteredSelectCategoryList = this.SelectCategoryList.slice();
        this.CategoryTableSource.data = data
        console.log(data)
        console.log(this.filteredSelectCategoryList)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

    this.params.getTypeResourceList().subscribe(
      (data: СategoryTypeList[]) => {

        this.SelectTypeList = data;
        this.filteredSelectTypeList = this.SelectTypeList.slice();
        this.TypeTableSource.data = data
        console.log(data)
        console.log(this.filteredSelectTypeList)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

  }

  editType(element: СategoryTypeList) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditType.setValue({
      id: element.id,
      title: element.title,
    });
    this.editMode = true
    console.log(this.FormAddEditType.value)
  }

  deleType(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleResourceDirectoryType(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Тип удален');
            const index = this.TypeTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.TypeTableSource.data.splice(index, 1);
              this.TypeTableSource._updateChangeSubscription();
            }
            this.СategoryTypeList()

          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  AddEditType() {
    if (this.FormAddEditType.valid) {
      const formData = { ...this.FormAddEditType.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateType(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createType(formValueWithoutId);
      }

    }
  }

  updateType(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateResourceDirectoryType(id, data).subscribe(
      (response: СategoryTypeList) => {
        console.log('Новые данные:', response);

        // Находим индекс обновленного элемента
        const updatedItemIndex = this.TypeTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.TypeTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу с новым массивом данных
        this.TypeTableSource.data = [...this.TypeTableSource.data];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditType');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditType.reset()
        this.editMode = false
        this.СategoryTypeList()
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  // Создание тип
  createType(data: СategoryTypeList) {
    console.log('create:', data)
    this.params.addResourceDirectoryType(data).subscribe(
      (data: СategoryTypeList) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлен тип `);
        this.TypeTableSource.data = [...this.TypeTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditType');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditType.reset()
        this.СategoryTypeList()

      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }




  editCategory(element: СategoryTypeList) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditCategory.setValue({
      id: element.id,
      title: element.title,
    });
    this.editMode = true
    console.log(this.FormAddEditCategory.value)
  }

  deleCategory(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deleResourceDirectoryСategory(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Категория удалена');
            const index = this.CategoryTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.CategoryTableSource.data.splice(index, 1);
              this.CategoryTableSource._updateChangeSubscription();
            }
            this.СategoryTypeList()
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
        

      }
    });
  }

  AddEditCategory() {
    if (this.FormAddEditCategory.valid) {
      const formData = { ...this.FormAddEditCategory.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updatetCategory(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createСategory(formValueWithoutId);
      }

    }
  }

  updatetCategory(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateResourceDirectoryСategory(id, data).subscribe(
      (response: СategoryTypeList) => {
        console.log('Новые данные:', response);

        // Находим индекс обновленного элемента
        const updatedItemIndex = this.CategoryTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.CategoryTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу с новым массивом данных
        this.CategoryTableSource.data = [...this.CategoryTableSource.data];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditCategory');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditCategory.reset()
        this.editMode = false
        this.СategoryTypeList()
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  // Создание категории
  createСategory(data: СategoryTypeList) {
    console.log('create:', data)
    this.params.addResourceDirectoryСategory(data).subscribe(
      (data: СategoryTypeList) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлена категория`);
        this.CategoryTableSource.data = [...this.CategoryTableSource.data, data];

        const offcanvasElement = document.getElementById('addEditCategory');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditCategory.reset()
        this.СategoryTypeList()

      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }

}
