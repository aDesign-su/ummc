export interface Role {
    id: number;
    name: string;
    desc: string;
    functional: string;
    level_of_access: string;
    data_access: string;
  }