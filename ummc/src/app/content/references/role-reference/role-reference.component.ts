import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Role } from './RoleReference';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-role-reference',
  templateUrl: './role-reference.component.html',
  styleUrls: ['./role-reference.component.css']
})
export class RoleReferenceComponent implements OnInit {
  RoleArray: MatTableDataSource<Role>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  displayedColumns: string[] = ['name','desc','functional','level_of_access','data_access',];

  constructor(public theme: ThemeService,public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService) { }
  ngOnInit(): void {
    this.RoleArray = new MatTableDataSource<Role>([]);
    this.getRoleData();
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  ngAfterViewInit() {
    this.RoleArray.paginator = this.paginator;
    this.RoleArray.sort = this.sort;
  }


  getRoleData(): void {
    this.params.getRoleReference().subscribe(
      (data: Role[]) => {
        this.RoleArray=new MatTableDataSource(data)
        console.log(this.RoleArray)
        if (this.paginator && this.sort) {
          this.RoleArray.paginator = this.paginator;
          this.RoleArray.sort = this.sort;
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
