import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { CurrencyExchange, ResultItem } from '../parameter';
import { MacroService } from '../macro.service';

@Component({
  selector: 'app-metals',
  templateUrl: './metals.component.html',
  styleUrls: ['./metals.component.css']
})
export class MetalsComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService,private macro:ApicontentService, public getMacro:MacroService) { }

  ngOnInit() {
    this.getMacro.Parameter();
  }

  displayedColumns: string[] = ['param_1', 'param_2', 'param_3','2024','2025','2026','2027','2028','2029','2030','2031','2032','actions'];
  @ViewChild('paginator') paginator!: MatPaginator;
  ngAfterViewInit() {
    this.getMacro.dataSourceMetals.paginator = this.paginator;
  }



  editMetal(id:number) {

  }
}
