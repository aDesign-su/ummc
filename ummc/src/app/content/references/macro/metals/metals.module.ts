import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MetalsComponent } from './metals.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule
  ],
  declarations: [MetalsComponent],
  exports: [MetalsComponent]
})
export class MetalsModule { }
