import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InflationIpcComponent } from './inflation-ipc.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule
  ],
  declarations: [InflationIpcComponent],
  exports: [InflationIpcComponent]
})
export class InflationIpcModule { }
