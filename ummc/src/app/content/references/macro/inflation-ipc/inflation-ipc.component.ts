import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { CurrencyExchange, ResultItem } from '../parameter';
import { MacroService } from '../macro.service';

@Component({
  selector: 'app-inflation-ipc',
  templateUrl: './inflation-ipc.component.html',
  styleUrls: ['./inflation-ipc.component.css']
})
export class InflationIpcComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService,private macro:ApicontentService,public getMacro:MacroService) { }

  ngOnInit() {
    this.Parameter();
  }

  displayedColumns: string[] = ['param_1', 'parameter', 'param_3','2024','2025','2026','2027','2028','2029','2030','2031','2032','actions'];
  dataSource = new MatTableDataSource<CurrencyExchange>([]);

  @ViewChild('paginator') paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  Parameter() {
    this.macro.getParameter().subscribe(
      (response: CurrencyExchange[]) => {
        const arr: ResultItem[] = [];
        response.forEach(item => {
          const year = item.year.toString();
          const value = parseFloat(item.value).toFixed(2);
          const parameter1 = item.parameter.type_parameter;
          const parameter2 = item.parameter.name;
          const parameter3 = item.parameter.base;
          const id = item.parameter.id;

          let obj = arr.find(element => element['id'] === id);
          if (!obj) {
            obj = {
              'id': id,
              'param_1': parameter1,
              'parameter': parameter2,
              'param_3': parameter3
            };
            arr.push(obj);
          }

          obj[year] = value;
        });
        // console.log( arr )

        // Фильтруем массив arr по полю param_1
            // Выведет массив значений "Курсы валют"
        const currencies = arr.filter(item => item.param_1 === 'Курсы валют'); 
            // Выведет массив значений "Металлы"
        const metals = arr.filter(item => item.param_1 === 'Металлы');
            // Выведет массив значений "Инфляция"
        const inflation = arr.filter(item => item.param_1 === 'Инфляция ИПЦ'); 
            // Выведет массив значений "Ключ"
        const key = arr.filter(item => item.param_1 === 'Ключ');

        this.dataSource = new MatTableDataSource<any>(inflation);
        this.dataSource.paginator = this.paginator;

      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  editInflation(id:number) {
    
  }
}
