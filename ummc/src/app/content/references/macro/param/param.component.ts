import { Component, OnInit, ViewChild } from '@angular/core';
import { MacroService } from '../macro.service';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-param',
  templateUrl: './param.component.html',
  styleUrls: ['./param.component.css']
})
export class ParamComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService,private macro:ApicontentService,private formBuilder: FormBuilder, public getMacro:MacroService) {
    this.paramForm = this.formBuilder.group({
      type_parameter: '',
      name: '',
      base: '',
    });
   }

  ngOnInit() {
    this.getMacro.SelectData();
  }

  displayedColumns: string[] = ['type_parameter', 'name', 'base','actions'];
  @ViewChild('paginator') paginator!: MatPaginator;
  ngAfterViewInit() {
    this.getMacro.paramList.paginator = this.paginator;
  }

  paramForm!: FormGroup;
  element: any;

  editParam(element:any) {
    this.element = element.id; // Установим значение свойства element
    this.paramForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }
  saveParam() {
    if (this.element) {
      let editedObject = this.paramForm.value;
      this.macro.updParam(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.getMacro.SelectData();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delParam(element: any) {
    const delElement = element.id
    this.macro.delParam(delElement).subscribe(
      (data: any) => {
        this.getMacro.SelectData();
        // this.delAccess = 'Коэффициент сложности работ удален.'
        // setTimeout(() => {
        //   this.delAccess = null;
        // }, 5000);
      },
      (error: any) => {
        // console.log('Failed to delete svz document:', error)
      }
    )
  }
}

