import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CurrencyExchange } from './parameter';
import { ApicontentService } from 'src/app/api.content.service';
import * as jmespath from 'jmespath';
import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MacroService {
constructor(private macro:ApicontentService,private formBuilder: FormBuilder) {
  this.currencyForm = this.formBuilder.group({
    // id: [],
    year: [],
    parameter: [],
    value: [''],
  });

 }

 currencyForm!: FormGroup;

dataSourceMetals = new MatTableDataSource<CurrencyExchange>([]);
dataSourceCurrencies = new MatTableDataSource<CurrencyExchange>([]);
// dataSourceCurrencies = [];
dataSourceInflation1 = new MatTableDataSource<CurrencyExchange>([]);
dataSourceInflation2 = new MatTableDataSource<CurrencyExchange>([]);
dataSourceKey = new MatTableDataSource<CurrencyExchange>([]);
paramList = new MatTableDataSource<CurrencyExchange>([]);

getType: any[] = []
getName: any[] = []
section: string = ''

isSection_1() {
  this.section = 'level-1'
}
isSection_2() {
  this.section = 'level-2'
}
isSection_3() {
  this.section = 'level-3'
}
isSection_4() {
  this.section = 'level-4'
}
SelectData() {
  this.macro.getSelect().subscribe(
    (response: any[]) => {
      this.paramList.data = response;

      const type = "[*].types[]";
      this.getType = jmespath.search(response, type);

      const name = "[*].{names:name,id:id}";
      this.getName = jmespath.search(response, name).slice(0, -1);
    },
    (error: any) => {
      console.error('Failed to add data:', error);
    }
  );
}

dataSource: any[] = []
arr: any[] = []

Parameter() {
  this.macro.getParameter().subscribe(
    (response: CurrencyExchange[]) => {
      this.dataSource = response;
  
      // const arr: any[] = [];
  
      // Создаем массив flatData, содержащий все данные из dataSource
      this.flatData = this.dataSource.map((item: any) => ({
        id: item.id,
        year: item.year,
        parameter_id: item.parameter.id,
        parameter_type: item.parameter.type_parameter,
        parameter_name: item.parameter.name,
        parameter_base: item.parameter.base,
        value: item.value
      }));
  

      // Проходим по каждому элементу flatData и создаем новый объект для каждого уникального года с уникальным yid
      this.flatData.forEach(item => {
        const year = item.year.toString();
        const value = parseFloat(item.value).toFixed(2);
        const parameter1 = item.parameter_type;
        const parameter2 = item.parameter_name;
        const parameter3 = item.parameter_base;
        const id = item.parameter_id;
        const yid = item.id; // Используем идентификатор элемента как yid
  
        // Проверяем, существует ли уже объект с таким id в массиве arr
        let obj = this.arr.find(element => element['id'] === id);
  
        // Если объект с таким id еще не существует, создаем его
        if (!obj) {
          obj = {
            // 'yid': yid, // Уникальный идентификатор элемента
            'id': id,
            'param_1': parameter1,
            'param_2': parameter2,
            'param_3': parameter3
          };
          this.arr.push(obj);
        }
  
        // Добавляем свойство с текущим yid в объект для текущего года
        obj[year] = value;
        obj[year + '_id'] = yid;
      });
  
      // В результате arr будет содержать массив объектов, где каждый объект будет иметь уникальный yid для каждого года
      console.log(this.arr);
      // Фильтруем flatData по типу параметра 'Курсы валют'
      let currency = this.arr.filter(item => item.param_1 === 'Курсы валют');
      this.dataSourceCurrencies.data = currency;
  
      let metals = this.arr.filter(item => item.param_1 === 'Металлы');
      this.dataSourceMetals.data = metals;

      let inflation1 = this.arr.filter(item => item.param_1 === 'Инфляция ИПЦ'); 
      this.dataSourceInflation1.data = inflation1;

      let inflation2 = this.arr.filter(item => item.param_1 === 'Инфляция ИЦП'); 
      this.dataSourceInflation2.data = inflation2;

      let key = this.arr.filter(item => item.param_1 === 'Ключ');
      this.dataSourceKey.data = key;
      
      // this.dataSourceCurrencies.data = currencies;
      
    },
    (error: any) => {
      console.error('Failed to add data:', error);
    }
  );
}
element
update
flatData
getId
year
dataSourceYear: any[] = []; 
dataSourceName: any[] = []; 

editCurrency(item:any) {
  this.element = item.id
  if(this.element) {
    this.currencyForm.patchValue(this.element); 
    // console.log(this.element)
  }
  // this.flatData = this.dataSource.map((item:any) => ({
  //   id: item.id,
  //   year: item.year,
  //   parameter_id: item.parameter.id,
  //   parameter_type: item.parameter.type_parameter,
  //   parameter_name: item.parameter.name,
  //   parameter_base: item.parameter.base,
  //   value: item.value
  // }));
  const selectedItem = this.arr.find(item => item.id == this.element);
  console.log('parameter_id', selectedItem);
  if (selectedItem) {
    // this.getId = selectedItem.id,
    this.currencyForm.patchValue({
      // id: selectedItem.id,
      parameter: selectedItem.id,
    });
  } else {
    console.log('No item found for the selected id.');
  }
    // console.log(this.flatData)
    // генерирруем года по id 
    // Найти объект в массиве this.arr с помощью selectedItem.id
    const selectedItems = this.arr.find(item => item.id === selectedItem.id);

    // Получить список уникальных годов для выбранного id
    const uniqueYears = Object.keys(selectedItem).filter(key => key.includes('_id')).map(key => key.replace('_id', ''));

    // Получить список объектов {year, year_id} для выбранного id
      this.dataSourceYear = uniqueYears.map(year => {
      this.getId = selectedItem[year + '_id']
      this.year = selectedItem.year
      return {
        year: year,
        year_id: selectedItem[year + '_id'],
        value: selectedItem[year]
      };
    });
  
  // const filteredYears = this.dataSourceYear.filter(item => item.idM == this.element) 


    console.log(this.dataSourceYear);

    // Создаем массив без повторов
    const uniqueFlatData = this.flatData.filter((item, index, self) =>
      index === self.findIndex((t) => (
        t.parameter_id === item.parameter_id && t.parameter_name === item.parameter_name
      ))
    );
    this.dataSourceName = uniqueFlatData;
}
onYearChange(selectedYear: any) {
   // Найти элемент с выбранным годом
   const selectedItem = this.dataSourceYear.find(item => item.year_id == selectedYear);
   if (selectedItem) {
     // Получить значение year_id и value
     const selectedId = selectedItem.year; 
     const selectedValue = selectedItem.value;
 
     // Установить значения в форму
     this.currencyForm.patchValue({ 
       year: selectedId,
       value: selectedValue
     }); 
     console.log('Item found:', selectedItem);
   } else {
     console.log('No item found for the selected year.',selectedYear );
   }
}
createLead() {
  if (this.element) {
    let data = this.currencyForm.value;
    this.macro.updVal(this.getId, data).subscribe(
      (data: any) => {
        // for (let i = 0; i < this.show.showBlock.length; i++) {
        //   this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        // }
        // this.theme.info = 'Данные изменены успешно.'
        // setTimeout(() => {
        //   this.theme.info = null;
        // }, 5000);
        this.Parameter();
      },
      (error: any) => {
        console.log('Failed to edit svz document:', error)
      }
    );
  }
}

}
