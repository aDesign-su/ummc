/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MacroService } from './macro.service';

describe('Service: Macro', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MacroService]
    });
  });

  it('should ...', inject([MacroService], (service: MacroService) => {
    expect(service).toBeTruthy();
  }));
});
