import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { CurrencyExchange, DiscountRate, ResultItem, Wacc, Parameters, ParameterValue } from './parameter';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as jmespath from 'jmespath';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';
import { MacroService } from './macro.service';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-macro',
  templateUrl: './macro.component.html',
  styleUrls: ['./macro.component.css']
})
export class MacroComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService,public isTitle: CommonService,private macro:ApicontentService,private formBuilder: FormBuilder,public getMacro:MacroService) {
    this.addDiscountRateForm = this.formBuilder.group({
      parameter: '',
      source: '',
      value: '',
    });
    this.addParametersForm = this.formBuilder.group({
      type_parameter: '',
      name: '',
      base: '',
    });
    this.addParameterValueForm = this.formBuilder.group({
      year: '',
      parameter: '',
      value: null,
    });
    this.waccForm = this.formBuilder.group({
      parameter: '',
      source: '',
      value: '',
    });
    this.valForm = this.formBuilder.group({
      year: null,
      parameter: null,
      value: '',
    });
   }

  ngOnInit() {
    this.Parameter(); this.Wacc(); this.SelectData()
    this.generateYearsRange()
  }




  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  // displayedColumns!: string[]
  dataSource1 = new MatTableDataSource<CurrencyExchange>([]);
  dataSource2 = new MatTableDataSource<Wacc>([]);

  displayedColumns1: string[] = ['param_1', 'parameter', 'param_3','2024','2025','2026','2027','2028','2029','2030','2031','2032','actions'];
  displayedColumns2: string[] = ['param_1', 'parameter', 'param_3','actions'];

  @ViewChild('paginator1') paginator1!: MatPaginator;
  @ViewChild('paginator2') paginator2!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource1.paginator = this.paginator1;
    this.dataSource2.paginator = this.paginator2;
  }

  addDiscountRateForm!: FormGroup;
  addParametersForm!: FormGroup;
  addParameterValueForm!: FormGroup;

  getParameter: ResultItem[] = []
  getWacc: Wacc[] = []

  selectedType!: string;
  selectedName!: string;




  // toggleDiscountRate() {
  //   this.show.toggleShowBlock(0);
  // }
  toggleParameters(index:number) {
    this.show.toggleShowBlock(index);
  }

  selectedYear: number = new Date().getFullYear();
  yearsRange: number[] = [];

  generateYearsRange(): void {
    const currentYear = new Date().getFullYear();
    const range = 10;
    for (let i = currentYear; i < currentYear + range; i++) {
      this.yearsRange.push(i);
    }
  }

  Parameter() {
    this.macro.getParameter().subscribe(
      (response: CurrencyExchange[]) => {
        this.key = response

        const arr: ResultItem[] = [];
        response.forEach(item => {
          const yid = item.id;
          const year = item.year.toString();
          const value = parseFloat(item.value).toFixed(2);
          const parameter1 = item.parameter.type_parameter;
          const parameter2 = item.parameter.name;
          const parameter3 = item.parameter.base;
          const id = item.parameter.id;

          let obj = arr.find(element => element['id'] === id);
          if (!obj) {
            obj = {
              'yid': yid,
              'id': id,
              'param_1': parameter1,
              'parameter': parameter2,
              'param_3': parameter3
            };
            arr.push(obj);
          }

          obj[year] = value;
        });
        

        // Фильтруем массив arr по полю param_1
            // Выведет массив значений "Курсы валют"
        const currencies = arr.filter(item => item.param_1 === 'Курсы валют'); 
            // Выведет массив значений "Металлы"
        const metals = arr.filter(item => item.param_1 === 'Металлы');
            // Выведет массив значений "Инфляция"
        const inflation = arr.filter(item => item.param_1 === 'Инфляция'); 
            // Выведет массив значений "Ключ"
        const key = arr.filter(item => item.param_1 === 'Ключ');
        // console.log( 'set', key )
        this.dataSource1 = new MatTableDataSource<any>(key);
        this.dataSource1.paginator = this.paginator1;

      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  Wacc() {
    this.macro.getWacc().subscribe(
      (response: Wacc[]) => {
        this.getWacc = response
        this.dataSource2 = new MatTableDataSource<Wacc>(this.getWacc);
        this.dataSource2.paginator = this.paginator2;
      },
    );
  }
  SelectData() {
    this.macro.getSelect().subscribe(
      (response: any[]) => {

        // const type = "[*].types[]";
        // this.getType = jmespath.search(response, type);

        // const name = "[*].{names:name,id:id}";
        // this.getName = jmespath.search(response, name).slice(0, -1);

        // console.log('get to add data:', this.getName);
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  DiscountRate() {
    const addDiscountRate = this.addDiscountRateForm.value;
    this.macro.addDiscountRate(addDiscountRate).subscribe(
      (response: DiscountRate[]) => {
        console.log('Data added successfully:', response);
        this.addDiscountRateForm.reset();
        this.show.closedAdd(0)
        this.getMacro.Parameter()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  Parameters() {
    const addParameters = this.addParametersForm.value;
    this.macro.addParameters(addParameters).subscribe(
      (response: Parameters[]) => {
        console.log('Data added successfully:', response);
        this.addParametersForm.reset();
        this.show.closedAdd(0)
        this.SelectData()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  ParameterValue() {
    const addParameterValue = this.addParameterValueForm.value;
    this.macro.addParameterValue(addParameterValue).subscribe(
      (response: ParameterValue[]) => {
        console.log('Data added successfully:', response);
        this.addParameterValueForm.reset();
        this.show.closedAdd(0)
        this.getMacro.Parameter()
        this.Parameter()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }

  key: any
  onYearChange(event: any) {
    const selectedYear = event.value; // Получаем выбранный год
    const selectId = event.id

    // Найдем соответствующий объект в массиве this.key
    const selectedYearData = this.key.find((item:any) => item.year == selectedYear && item.parameter.id == this.elementVal);

    
    if (selectedYearData) {
      // Устанавливаем значения в соответствующие поля формы
      this.valForm.patchValue({
        value: selectedYearData.value
      });
    }
  }
  test
  edit(element: any) {
    this.elementVal = element.id; // Установим значение свойства element
    this.elementValId = element.yid; // Установим значение свойства element
    this.test = element.parameter; // Установим значение свойства element
    this.valForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
  }
  saveVal() {
    if (this.elementVal) {
      let editedObject = this.valForm.value;

      const foundItem = this.key.find(item => item.parameter.id == this.elementVal );
      editedObject.parameter = foundItem.parameter.id;

      console.log(editedObject)
      this.macro.updVal(this.elementValId, editedObject).subscribe(
        (data: any) => {
          this.show.closedAdd(2)
          this.Parameter()
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  waccForm!: FormGroup;
  valForm!: FormGroup;
  elementWacc: any;
  elementVal: any;
  elementValId: any;

  editWacc(element: any) {
    this.elementWacc = element.id; // Установим значение свойства element
    this.getMacro.isSection_3()
    this.waccForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }
  saveWacc() {
    if (this.elementWacc) {
      let editedObject = this.waccForm.value;
      this.macro.updWacc(this.elementWacc, editedObject).subscribe(
        (data: any) => {
          this.show.closedAdd(0)
          this.Wacc()
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delWacc(element: any) {
    const delElement = element.id
    this.macro.delWacc(delElement).subscribe(
      (data: any) => {
        this.Wacc();
        // this.delAccess = 'Коэффициент сложности работ удален.'
        // setTimeout(() => {
        //   this.delAccess = null;
        // }, 5000);
      },
      (error: any) => {
        // console.log('Failed to delete svz document:', error)
      }
    )
  }
}