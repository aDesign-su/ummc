import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InflationComponent } from './inflation.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule
  ],
  declarations: [InflationComponent],
  exports: [InflationComponent]
})
export class InflationModule { }
