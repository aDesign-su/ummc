import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypicalAssetsComponent } from './typical-assets.component';

describe('TypicalAssetsComponent', () => {
  let component: TypicalAssetsComponent;
  let fixture: ComponentFixture<TypicalAssetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypicalAssetsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TypicalAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
