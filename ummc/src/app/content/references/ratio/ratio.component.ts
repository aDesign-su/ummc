import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Ratio } from './ratio';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-ratio',
  templateUrl: './ratio.component.html',
  styleUrls: ['./ratio.component.css']
})
export class RatioComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder, private ratio: ApicontentService) { 
    this.ratioForm = this.formBuilder.group({
    number: "",
    rate: null,
    title: "",
    comments: ""
    });
    this.editRatioForm = this.formBuilder.group({
      number: "",
      rate: null,
      title: "",
      comments: ""
    });
  }

  ngOnInit() {
    this.getDataRatio()
  }
  editRatioForm: FormGroup;
  ratioForm!: FormGroup;
  element: any;
  displayedColumns: string[] = ['number', 'rate', 'title', 'comments', 'actions'];
  dataSource = new MatTableDataSource<Ratio>([]);

  @ViewChild('paginator') paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  
  getDataRatio(): void {
    this.ratio.getDifficultyFactor().subscribe(
      (data: Ratio[]) => {
        const rate = data

        this.dataSource = new MatTableDataSource<Ratio>(rate);
        this.dataSource.paginator = this.paginator;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  
  addRatio() {
    const ratio = this.ratioForm.value;
    this.ratio.addDifficultyFactor(ratio).subscribe(
      (response: Ratio[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.getDataRatio();

        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editRatio(element: Ratio) {
    this.element = element.id; // Установим значение свойства element
    this.editRatioForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  saveRatio() {
    if (this.element) {
      let editedObject = this.editRatioForm.value;
      this.ratio.editDifficultyFactor(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getDataRatio();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delRatio(element: Ratio) {
    const delElement = element.id
    this.ratio.delDifficultyFactor(delElement).subscribe(
      (data: any) => {
        this.getDataRatio();

        this.theme.info = 'Коэффициент сложности работ удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        // console.log('Failed to delete svz document:', error)
      }
    )
  }
}
