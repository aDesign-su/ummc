import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from '../../budget/project-budget/common.service';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Unit } from './units';
import { MatDialog } from '@angular/material/dialog';
declare var bootstrap: any;

@Component({
  selector: 'app-directory-of-units-of-measurement',
  templateUrl: './directory-of-units-of-measurement.component.html',
  styleUrls: ['./directory-of-units-of-measurement.component.css']
})
export class DirectoryOfUnitsOfMeasurementComponent implements OnInit {
  dataUnitsTableSource = this.initTable<Unit>();
  @ViewChild('PaginatorUnits') paginatorForUnits: MatPaginator;

  FormAddEditUnits: FormGroup;
  editMode = false;
  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }
  constructor(public theme: ThemeService, public params: ApicontentService, private dialog: MatDialog, public isTitle: CommonService, private fb: FormBuilder, private datePipe: DatePipe,) {
    this.FormAddEditUnits = this.fb.group({
      id: [''],
      title: [''],
    });
  }

  ngOnInit() {
    this.getDataUnits()
  }
  ngAfterViewInit() {
    this.dataUnitsTableSource.paginator = this.paginatorForUnits;
  }

  add() {
    this.editMode = false
    this.FormAddEditUnits.reset()
  }
  //Получить данные
  getDataUnits(): void {
    this.params.getDataUnits().subscribe(
      (data: Unit[]) => {
        this.dataUnitsTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  createOrUpdateUnits(): void {
    if (this.FormAddEditUnits.valid) {
      const formData = { ...this.FormAddEditUnits.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;
      console.log(id, formValueWithoutId)

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        this.updateUnits(id, formValueWithoutId);
      } else {
        console.log(formValueWithoutId)
        this.createUnit(formValueWithoutId);
      }
    }
  }


  updateUnits(id, data) {
    console.log('formValueWithoutId :', data);
    this.params.updateUnit(id, data).subscribe(
      (response: Unit) => {
        // Находим индекс обновленного элемента
        const updatedItemIndex = this.dataUnitsTableSource.data.findIndex(item => item.id === response.id);
        console.log(response.id)
        // Обновляем этот элемент в массиве
        if (updatedItemIndex > -1) {
          this.dataUnitsTableSource.data[updatedItemIndex] = response;
        }
        // Обновляем таблицу с новым массивом данных
        this.dataUnitsTableSource.data = [...this.dataUnitsTableSource.data];
        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditUnits');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditUnits.reset()
        this.editMode = false
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

    // Создание
    createUnit(data: Unit) {
      console.log('create:', data)
      this.params.createUnit(data).subscribe(
        (data: Unit) => {
          console.log(data)
          this.theme.openSnackBar(`Добавлена еденица измерения`);
          this.dataUnitsTableSource.data = [...this.dataUnitsTableSource.data, data];
  
          const offcanvasElement = document.getElementById('addEditUnits');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.FormAddEditUnits.reset()
        },
        (error: any) => {
          console.error('Error fetching data:', error.error['error']);
          this.theme.openSnackBar(error.error['error']);
  
        }
      );
    }

  removeUnit(id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent); 
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delUnits(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Еденица измерения удалена');
            const index = this.dataUnitsTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataUnitsTableSource.data.splice(index, 1);
              this.dataUnitsTableSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  editUnits(element: Unit) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditUnits.setValue({
      id: element.id,
      title: element.title,
    });
    this.editMode = true
    console.log(this.FormAddEditUnits.value)
  }

}
