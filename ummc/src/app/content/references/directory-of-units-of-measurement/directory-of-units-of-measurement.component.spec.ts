/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DirectoryOfUnitsOfMeasurementComponent } from './directory-of-units-of-measurement.component';

describe('DirectoryOfUnitsOfMeasurementComponent', () => {
  let component: DirectoryOfUnitsOfMeasurementComponent;
  let fixture: ComponentFixture<DirectoryOfUnitsOfMeasurementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryOfUnitsOfMeasurementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryOfUnitsOfMeasurementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
