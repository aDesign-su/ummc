import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Equipments, WorkPackages, analogBudgets } from './analogy';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as jmespath from 'jmespath';
import { ShowService } from 'src/app/helper/show.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AnalogyService } from './analogy.service';
import { CommonService } from '../../budget/project-budget/common.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-analogy',
  templateUrl: './analogy.component.html',
  styleUrls: ['./analogy.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class AnalogyComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public analogy:AnalogyService, public isTitle: CommonService, private router: Router, public analogService:ApicontentService,private formBuilder: FormBuilder,private _adapter: DateAdapter<any>,@Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.analogForm = this.formBuilder.group({
      title: "",
      smr_comp: "",
      smr_cost: null,
      date_start: null,
      date_end: null,
      pir_comp: "",
      eng_pir: null,
      oto_cost: null,
      etc_cost: null,
      tech_description: "",
      pir_cost: 0,
      project: "",
      type: "",
      region: "",
      currency: ""
    });
    this.editAnalogForm = this.formBuilder.group({
      title: "",
      smr_comp: "",
      smr_cost: null,
      date_start: null,
      date_end: null,
      pir_comp: "",
      eng_pir: null,
      oto_cost: null,
      etc_cost: null,
      tech_description: "",
      pir_cost: 0,
      project: "",
      type: "",
      region: "",
      currency: ""
    });
    this.workPackage = this.formBuilder.group({
      "title": "",
      "type": "",
      "contractor": "",
      "cost_total": 0,
      "cost_pp": 0,
      "mh": 0,
      "comments": "",
      "project": 0,
      "region": 0
    });
    this.equipmenForm = this.formBuilder.group({
      "title": "",
      "type": "",
      "tech_description": "",
      "cost": 0,
      "description": "",
      "project": "",
      "manufacturer": ""
    });
   }

  ngOnInit() { this.getDataSource()

 // Селект Category
    this.analogService.getAnalogList().subscribe(
      (data: analogBudgets[]) => {
        this.getAnalogType = data
      },
    );
 // Селект Currency
    this.analogService.getCurrencyList().subscribe(
      (data: analogBudgets[]) => {
        this.getCurrencyList = data
      },
    );
 // Селект Region
    this.analogService.getRegionList().subscribe(
      (data: analogBudgets[]) => {
        this.getRegion = data
      },
    );
  // Иницилизация фильтров Реестры
   this.searchForm = this.formBuilder.group({
     searchText: ''
   });
   this.applyFilters();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  @Output() setIdEvent = new EventEmitter<number>();

  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  performJmesPathQuery(data: any, query: string): any {
    return jmespath.search(data, query);
  }
  displayedColumns: string[] = [
    'serial_number',
    'title',
    'date_start',
    'date_end',
    'total_cost',
    'pir_comp',
    'pir_cost',
    'oto_cost',
    'etc_cost',
    'smr_comp',
    'smr_cost',
    'tech_description',
    'project',
    'type',
    'region',
    'currency',
    'action'
  ];
  dataSource = new MatTableDataSource<analogBudgets>([]);
  anlData: analogBudgets[] = []
  equData: analogBudgets[] = []
  wrkData: WorkPackages[] = []
  analog: analogBudgets[] = []
  getAnalogBudget: analogBudgets[] = []
  getEquipment: any = []
  getWorkPackage: any = []
  getAnalogBudgets: any = []
  getComparison: any = []

  getSelect: any[] = []
  selectedRegion!: string;
  getRegion: any[] = [] /** Region */
  regionList!: string;
  getAnalogType: any[] = [] /** Category */
  AnalogType!: string;
  getCurrencyList: any[] = [] /** Currency */
  CurrencyList!: string;

  searchForm!: FormGroup;
  analogForm!: FormGroup;
  editAnalogForm!: FormGroup;
  workPackage!: FormGroup;
  equipmenForm!: FormGroup;
  
  element: any;
  setID: number
  panelOpenState: boolean = true
  create(index:number) {
    this.show.toggleShowBlock(index);
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  filteredContracts: any[] = [];
  selectedStatus: string = 'All';

  // Сортировка по полю в таблице
  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    this.filteredContracts = this.dataSource.data.filter((contract:any) => {
      return (
        this.matchesSearchText(contract, searchText)
      );
    });
  }
  matchesSearchText(contract: any, searchText: string): boolean {
    return (
      (contract.pir_comp?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.smr_comp?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.type?.toLowerCase() ?? '').includes(searchText) 
    );
  }
  Сomparison() {
    this.analogService.getСomparison().subscribe(
      (response: any[]) => {
        this.getComparison = response
        const anl = "[*].analog_budget|[]";
        this.anlData = jmespath.search(response, anl);
        console.log('anl',this.anlData);

        const equ = "[*].Equipment|[]";
        this.equData = jmespath.search(response, equ);
        console.log('equ',this.equData);

        const wrk = "[*].Work_packages|[]";
        this.wrkData = jmespath.search(response, wrk);
        console.log(this.wrkData);
      },
      (error: any) => {
        console.error('Failed to add data:', error);

      }
    );
  }
  showDetails(element: analogBudgets) {
    // this.create(2);
    if (element.id) {
      // this.getDataAnalogId(element.id);
      this.setID = element.id
      this.analogy.setSetId(this.setID);
      this.router.navigate(['analogy/more']);
    }
  }
  // getDataAnalogId(AnalogId: number): void {
  //   this.analogService.getAnalogId(AnalogId).subscribe(
  //     (data: analogBudgets[]) => {
  //       // this.analogData = data;
  //       const equ = "[*].equipment|[]";
  //       this.getEquipment = jmespath.search(data, equ);
  //       console.log(this.getEquipment);

  //       const wrk = "[*].work_package|[]";
  //       this.getWorkPackage = jmespath.search(data, wrk);
  //       console.log(this.getWorkPackage);

  //       const anl = "[*].analog_budget|[]";
  //       this.getAnalogBudgets = jmespath.search(data, anl);
  //       console.log(this.getAnalogBudgets);

  //     },
  //     (error: any) => {
  //       console.error('Error fetching data:', error);
  //     }
  //   );
  // }
  getDataSource(): void {
    this.analogService.getAnalogBudget().subscribe((data: analogBudgets[]) => {
        this.dataSource.data = data

        // //main select
        // const query = "[*].regions|[*]|[]";
        // this.getSelect = jmespath.search(data, query);

        // //select work_package
        // const response = "[*].{id: id, title: title}";
        // const getResponse = jmespath.search(data, response);
        // getResponse.pop();
        // this.getPackage = getResponse.slice(0, getResponse.length - 0);

        // data.pop();
        // this.getAnalogBudget = data.slice(0, data.length - 0);

        // this.dataSource.data = this.getAnalogBudget;
        // this.dataSource.paginator?.firstPage();
        // this.applyFilters()
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  addAnalogBudget() {
    const analogBudget = this.analogForm.value;

      // Format the date as YYYY-MM-DD
      const startdDate = moment(analogBudget.date_start).format('YYYY-MM-DD');
      const endDate = moment(analogBudget.date_end).format('YYYY-MM-DD');
      analogBudget.date_start = startdDate;
      analogBudget.date_end = endDate;

    this.analogService.addAnalogBudget(analogBudget).subscribe(
      (response: analogBudgets[]) => {
        console.log('Data added successfully:', response);
        this.show.showBlock[3] = false
        this.getDataSource()

        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        // console.error('Failed to add data:', error);

        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  selectedType: any;
  selectedRegions: any;
  selectedCurrency: any;

  editAnalogBudget(element: any) {
    this.element = element.id; // Установим значение свойства element
    element.editing = true;
    const selectedCurrency = this.getCurrencyList.find(currency => currency.title === element.currency);
    const selectedType = this.getAnalogType.find(type => type.title === element.type);
    const selectedRegion = this.getRegion.find(region => region.title === element.region);
    
    if (selectedRegion) {
      // Устанавливаем значение 'region' в выбранный регион
      this.editAnalogForm.get('region').setValue(selectedRegion.id);
    }
    if (selectedType) {
      // Устанавливаем значение 'type' в выбранный тип
      this.editAnalogForm.get('type').setValue(selectedType.id);
    }
    if (selectedCurrency) {
      // Устанавливаем значение 'currency' в выбранную валюту
      this.editAnalogForm.get('currency').setValue(selectedCurrency.id);
    }

    const startdDate = moment(element.date_start).format('YYYY-MM-DD');
    const endDate = moment(element.date_end).format('YYYY-MM-DD');

    this.editAnalogForm.patchValue({
      title: element.title,
      smr_comp: element.smr_comp,
      smr_cost: element.smr_cost,
      date_start: startdDate,
      date_end: endDate,
      pir_comp: element.pir_comp,
      eng_pir: element.eng_pir,
      oto_cost: element.oto_cost,
      etc_cost: element.etc_cost,
      tech_description: element.tech_description,
      pir_cost: element.pir_cost,
      project: element.project
    })
  }

  saveAnalogBudget() {
    if (this.element) {
      let editedObject = this.editAnalogForm.value;

        editedObject.date_start = moment(editedObject.date_start).format('YYYY-MM-DD');
        editedObject.date_end = moment(editedObject.date_end).format('YYYY-MM-DD');

      this.analogService.editAnalogBudget(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getDataSource();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  
  delAnalogBudget(elem: any) {
    const id = elem.id;
    this.analogService.delAnalogBudget(id).subscribe(
      (response: any[]) => {
        this.getDataSource();

        this.theme.info = 'Объект аналог удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
    );
  }

}
