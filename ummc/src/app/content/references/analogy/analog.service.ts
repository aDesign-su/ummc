import { Injectable } from '@angular/core';
import { analogBudgets } from './analogy';
import * as jmespath from 'jmespath';
import { ApicontentService } from 'src/app/api.content.service';
import { AnalogyService } from './analogy.service';

@Injectable({
  providedIn: 'root'
})
export class AnalogService {
constructor(public analogService:ApicontentService, public analogy:AnalogyService) {
  this.analogy.currentSetId.subscribe(id => {
    this.setID = id;
  });
     // Селект Type Equipment
   this.analogService.getAnalogTypeList().subscribe(
    (data: analogBudgets[]) => {
      this.getAnalogTypeEquipment = data
    },
  );
  // Селект Analog Type
    this.analogService.getAnalogPackageList().subscribe(
      (data: analogBudgets[]) => {
        this.getAnalogType = data
      },
    );
 // Селект Analog Currency
    this.analogService.getCurrencyList().subscribe(
      (data: analogBudgets[]) => {
        this.getCurrencyList = data
      },
    );
  // Селект Region
    this.analogService.getRegionList().subscribe(
      (data: analogBudgets[]) => {
        this.getRegionList = data
      },
    );
  // Селект Unit Of Measure
    this.analogService.getUnitOfMeasureList().subscribe(
      (data: analogBudgets[]) => {
        this.getUnitOfMeasure = data
      },
    );    
 }
  getUnitOfMeasure: any[] = []
  getAnalogTypeEquipment: any[] = []
  AnalogTypeEquipment!: string;
  getAnalogType: any[] = []
  AnalogType!: string;
  getCurrencyList: any[] = []
  CurrencyList!: string;
  getRegionList: any[] = []
  RegionList!: string;

  setID: number
  getWorkPackage: any = []
  getEquipment: any = []
  dataEquip: any = []
  dataPack = []
  getDataId() {
    this.analogService.getAnalogBudgetId(this.setID).subscribe((data: analogBudgets[]) => {
      const pack = "@.children.analog_budget_package";
      this.dataPack = jmespath.search(data, pack);
      console.log(this.dataPack)
      const equ = "@.children.analog_budget_equipment";
      this.dataEquip = jmespath.search(data, equ);
      console.log(this.dataEquip)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  getDataAnalogId(): void {
    this.analogService.getAnalogId().subscribe(
      (data: analogBudgets[]) => {
        this.getWorkPackage = data
        // console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDataEquipId(): void {
    this.analogService.getEquipId().subscribe(
      (data: analogBudgets[]) => {
        this.getEquipment = data
        // console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
