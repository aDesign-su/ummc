import { Component, OnInit } from '@angular/core';
import { analogBudgets } from '../analogy';
import * as jmespath from 'jmespath';
import { ApicontentService } from 'src/app/api.content.service';
import { AnalogyService } from '../analogy.service';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ThemeService } from 'src/app/theme.service';
import { AnalogService } from '../analog.service';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  constructor(public theme: ThemeService, public analogService:ApicontentService,private analogy:AnalogyService, public analog: AnalogService, public show:ShowService,private formBuilder: FormBuilder) {
    this.editEquipmenForm = this.formBuilder.group({
      title: '',
      manufacturer: '',
      tech_description: '',
      total_cost: '',
      description: '',
      analog_project: '',
      type: '',
      project: '',
      currency: ''
    });
   }
  editEquipmenForm!: FormGroup

  ngOnInit() {
     // Селект Type Equipment
    //  this.analogService.getAnalogTypeList().subscribe(
    //   (data: analogBudgets[]) => {
    //     this.getAnalogType = data
    //   },
    // );
    // Селект Currency
    // this.analogService.getCurrencyList().subscribe(
    //   (data: analogBudgets[]) => {
    //     this.getCurrencyList = data
    //   },
    // );
    this.analogy.currentSetId.subscribe(id => {
      this.setID = id;
    });
    this.analog.getDataEquipId()
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false;
    }
  }
  getAnalogType: any[] = []
  AnalogType!: string;
  getCurrencyList: any[] = []
  CurrencyList!: string;

  element: any;
  setID: number
  // getEquipment: any = []
  selectedType: any;
  selectedCurrency: any;

  // getDataAnalogId(AnalogId: number): void {
  //   this.analogService.getAnalogId(AnalogId).subscribe(
  //     (data: analogBudgets[]) => {
  //       // this.analogData = data;
  //       const equ = "[*].equipment|[]";
  //       this.getEquipment = jmespath.search(data, equ);
  //       console.log(this.getEquipment);

  //     },
  //     (error: any) => {
  //       console.error('Error fetching data:', error);
  //     }
  //   );
  // }
  
  editEquipment(element: any) {
    this.element = element.id; // Установим значение свойства element
    element.editing = true;
    // const selectedCurrency = this.analog.dataEquip[0].currency_id
    // const selectedType = this.getAnalogType.find(type => type.title === element.type);
    // const currency = this.analog.dataEquip.currency_id
    // console.log(currency)

    // if (selectedType) {
    //   // Устанавливаем значение 'type' в выбранный тип
    //   this.editEquipmenForm.get('type').setValue(selectedType.id);
    // }
    // if (selectedCurrency) {
    //   // Устанавливаем значение 'currency' в выбранную валюту
    //   this.editEquipmenForm.get('currency').setValue(selectedCurrency.id);
    // }
    
    this.editEquipmenForm.patchValue({
      title: element.title,
      manufacturer: element.manufacturer,
      tech_description: element.tech_description,
      total_cost: element.total_cost,
      description: element.description,
      analog_project: element.analog_project,
      project: element.project,
      currency: element.currency_id,
      type: element.type_id
    })
  }
  saveEquipment() {
    if (this.element) {
      let editedObject = this.editEquipmenForm.value;

      this.analogService.editEquipment1(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          const setId = this.setID
          this.analog.getDataId()
          this.analogy.setSetId(setId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delEquipment(elem: any) {
    const id = elem.id;
    this.analogService.delEquipment1(id).subscribe(
      (response: any[]) => {
        const setId = this.setID
        this.analogy.setSetId(setId)
        this.analog.getDataId()
        this.theme.error = 'Оборудование удалено.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      },
    );
  }
}
