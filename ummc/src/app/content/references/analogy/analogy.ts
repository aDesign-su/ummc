export interface analogBudgets {
    currency: string;
    currency_id: number;
    date_end: string;
    date_start: string;
    etc_cost: string;
    id: number;
    oto_cost: string;
    pir_comp: string;
    pir_cost: string;
    project: string;
    region: string;
    region_id: number;
    smr_comp: string;
    smr_cost: string;
    tech_description: string;
    title: string;
    total_cost: string;
    type: string;
    type_id: number;
    children: [
        analog_budget_equipment: [
            analog_project: string,
            currency: string,
            currency_id: number,
            description: string,
            etc_cost: string,
            id: number,
            manufacturer: string,
            oto_cost: string,
            pir_cost: string,
            project_id: number,
            smr_cost: string,
            tech_description: string,
            title: string,
            total_cost: string,
            type: string,
            type_id: number
        ],
        analog_budget_package: [
            comments: null | string,
            contractor: string,
            currency: string,
            currency_id: number,
            etc_cost: string,
            id: number,
            mh: number,
            oto_cost: string,
            pir_cost: string,
            pp_cost: string,
            project_id: number,
            region: string,
            region_id: number,
            smr_cost: string,
            title: string,
            total_cost: string,
            type: string,
            type_id: number,
            unit_of_measure: string,
            unit_of_measure_id: number,
            volume: string,
        ]
    ]
}
export interface WorkPackages {
    id: number
    title: string,
    type: string,
    contractor: string,
    cost_total: number,
    cost_pp: number,
    mh: number,
    comments: string,
    project: number,
    region: number
}
export interface Equipments {
    id: number
    title: string,
    type: string,
    tech_description: string,
    cost: number,
    description: string,
    project: number,
    manufacturer: number,
    estimated_budget: number
}