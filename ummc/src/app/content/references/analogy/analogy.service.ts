import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class AnalogyService {
  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  constructor() {
    const setId = this.getSetIdFromLocalStorage();
    if (setId !== null) {
      this.setIdSource.next(setId);
    }
  }

  private getSetIdFromLocalStorage(): number | null {
    const setId = localStorage.getItem(this.setIdKey);
    return setId ? +setId : null;
  }

  setSetId(id: number) {
    localStorage.setItem(this.setIdKey, id.toString());
    this.setIdSource.next(id);
  }
  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }
}
