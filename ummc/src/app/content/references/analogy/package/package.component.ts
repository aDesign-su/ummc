import { Component, OnInit } from '@angular/core';
import { analogBudgets } from '../analogy';
import { ApicontentService } from 'src/app/api.content.service';
import { AnalogyService } from '../analogy.service';
import * as jmespath from 'jmespath';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ThemeService } from 'src/app/theme.service';
import { AnalogService } from '../analog.service';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {
  constructor(public theme: ThemeService, public analogService:ApicontentService,public show:ShowService,private analogy:AnalogyService, public analog: AnalogService, private formBuilder: FormBuilder) {
    this.editWorkPackageForm = this.formBuilder.group({
      title: '',
      contractor: '',
      total_cost: '',
      mh: '',
      comments: '',
      volume: '',
      unit_of_measure: '',
      pp_cost: '',
      type: '',
      project: this.setID,
      region: '',
      currency: '',
    });
   }
   editWorkPackageForm!: FormGroup

  ngOnInit() {
  // Селект Type
    // this.analogService.getAnalogTypePackageList().subscribe(
    //   (data: analogBudgets[]) => {
    //     this.getTypePackageList = data
    //   },
    // );
  // Селект Currency
    // this.analogService.getCurrencyList().subscribe(
    //   (data: analogBudgets[]) => {
    //     this.getCurrencyList = data
    //   },
    // );
  // Селект Region
    // this.analogService.getRegionList().subscribe(
    //   (data: analogBudgets[]) => {
    //     this.getRegionList = data
    //   },
    // );
    this.analogy.currentSetId.subscribe(id => {
      this.setID = id;
    });
    this.analog.getDataAnalogId()
  }
  getSelect: any[] = []
  selectedRegion!: string;
  getAnalogType: any[] = []
  AnalogType!: string;
  getCurrencyList: any[] = []
  CurrencyList!: string;
  getRegionList: any[] = []
  RegionList!: string;
  getTypePackageList: any[] = []
  TypePackageList!: string;

  element:any
  setID: number
  // getWorkPackage: any = []
  selectedType: any;
  selectedRegions: any;
  selectedCurrency: any;

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false;
    }
  }
  // getDataAnalogId(AnalogId: number): void {
  //   this.analogService.getAnalogId(AnalogId).subscribe(
  //     (data: analogBudgets[]) => {
  //       // this.analogData = data;
  //       const getSource = data

  //       const wrk = "[*].work_package|[]";
  //       this.getWorkPackage = jmespath.search(data, wrk);
  //     },
  //     (error: any) => {
  //       console.error('Error fetching data:', error);
  //     }
  //   );
  // }
  editWorkPackage(element: any) {
    this.element = element.id; // Установим значение свойства element
    element.editing = true;
    // const selectedCurrency = this.getCurrencyList.find(currency => currency.title === element.currency);
    // const selectedType = this.getTypePackageList.find(type => type.title === element.type);
    // const selectedRegion = this.getRegionList.find(region => region.title === element.region);
    
    // if (selectedRegion) {
    //   // Устанавливаем значение 'region' в выбранный регион
    //   this.editWorkPackageForm.get('region').setValue(selectedRegion.id);
    // }
    // if (selectedType) {
    //   // Устанавливаем значение 'type' в выбранный тип
    //   this.editWorkPackageForm.get('type').setValue(selectedType.id);
    // }
    // if (selectedCurrency) {
    //   // Устанавливаем значение 'currency' в выбранную валюту
    //   this.editWorkPackageForm.get('currency').setValue(selectedCurrency.id);
    // }

    this.editWorkPackageForm.patchValue({
      title: element.title,
      contractor: element.contractor,
      cost_total: element.cost_total,
      mh: element.mh,
      comments: element.comments,
      volume: element.volume,
      unit_of_measure: element.unit_of_measure_id,
      cost_pp: element.cost_pp,
      currency: element.currency_id,
      type: element.type_id,
      region: element.region_id
    })
  }
  saveWorkPackage() {
    if (this.element) {
      let editedObject = this.editWorkPackageForm.value;

      this.analogService.editWorkPackage1(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          const setId = this.setID
          this.analog.getDataId()
          this.analogy.setSetId(setId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delWorkPackage(elem: any) {
    const id = elem.id;
    this.analogService.delWorkPackage1(id).subscribe(
      (response: any[]) => {
        const setId = this.setID
        this.analogy.setSetId(setId)
        this.analog.getDataId()
        this.theme.error = 'Пакет работ удален.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      },
    );
  }
}
