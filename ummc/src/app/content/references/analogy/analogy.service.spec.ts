/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AnalogyService } from './analogy.service';

describe('Service: Analogy', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnalogyService]
    });
  });

  it('should ...', inject([AnalogyService], (service: AnalogyService) => {
    expect(service).toBeTruthy();
  }));
});
