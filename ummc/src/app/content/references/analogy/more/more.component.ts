import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Equipments, WorkPackages, analogBudgets } from '../analogy';
import * as jmespath from 'jmespath';
import { ApicontentService } from 'src/app/api.content.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AnalogyService } from '../analogy.service';
import { AnalogService } from '../analog.service';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.css']
})
export class MoreComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public analogy:AnalogyService, public analog: AnalogService, public analogService:ApicontentService,private formBuilder: FormBuilder) {
    this.analogy.currentSetId.subscribe(id => {
      this.setID = id;
      this.initializeForm1();
      this.initializeForm2();
    });
    this.analog.getDataId()
   }
   
  ngOnInit() {
  this.getDataSource()
  }
  initializeForm1() {
    this.equipmenForm = this.formBuilder.group({
      title: '',
      manufacturer: '',
      tech_description: '',
      total_cost: null,
      description: '',
      analog_project: '',
      type: null,
      project: this.setID,
      currency: null
    });
  }
  initializeForm2() {
    this.workPackage = this.formBuilder.group({
      "title": "",
      "contractor": "",
      "total_cost": null,
      "mh": null,
      "comments": "",
      "volume": null,
      "unit_of_measure": "",
      "pp_cost": 0,
      "type": null,
      "project": this.setID,
      "region": null,
      "currency": null
    });
  }

  dataSource = new MatTableDataSource<analogBudgets>([]);
  displayedColumns: string[] = [
    'title',
    'date_start',
    'date_end',
    'cost',
    'pir_comp',
    'eng_pir',
    'oto_cost',
    'etc_cost',
    'smr_comp',
    'constr_smr',
    'tech_description',
    'project',
    'type',
    'region',
    'currency'
  ];

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  equipmenForm!: FormGroup;
  workPackage!: FormGroup;

  getEquipment: any = []
  getWorkPackage: any = []

  getSelect: any[] = []
  getPackage: any[] = []
  getAnalogBudget: analogBudgets[] = []
  getAnalogBudgets: any = []
  visibleData: any

  
  selectedRegion!: string;

  setID: number

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false;
    }
  }
  getDataSource(): void {
    this.analogService.getAnalogBudget().subscribe((data: analogBudgets[]) => {
        this.getAnalogBudget = data

        // Убираем лишние (условно пустые) строки и оставляем только видимое поле
        const visibleElement = this.getAnalogBudget.find(element => element.id === this.setID);
        this.visibleData  = visibleElement ? [visibleElement] : [];
        console.log('test', this.visibleData)

        const query = "[*].regions|[*]|[]";
        this.getSelect = jmespath.search(data, query);

        //select work_package
        const response = "[*].{id: id, title: title}";
        const getResponse = jmespath.search(data, response);
        getResponse.pop();
        this.getPackage = getResponse.slice(0, getResponse.length - 0);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  // getDataAnalogId(AnalogId: number): void {
  //   this.analogService.getAnalogId(AnalogId).subscribe(
  //     (data: analogBudgets[]) => {
  //       // this.analogData = data;

  //       const getSource = data
  //       console.log(getSource)

  //       const equ = "[*].equipment|[]";
  //       this.getEquipment = jmespath.search(data, equ);
  //       console.log(this.getEquipment);

  //       const wrk = "[*].work_package|[]";
  //       this.getWorkPackage = jmespath.search(data, wrk);
  //       console.log(this.getWorkPackage);

  //       const anl = "[*].analog_budget|[]";
  //       this.getAnalogBudgets = jmespath.search(data, anl);
  //       console.log(this.getAnalogBudgets);

  //     },
  //     (error: any) => {
  //       console.error('Error fetching data:', error);
  //     }
  //   );
  // }
  addWorkPackage() {
    const WorkPackage = this.workPackage.value;
    this.analogService.addWorkPackage(WorkPackage).subscribe(
      (response: WorkPackages[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false;
        }
        const setId = this.setID
        this.analogy.setSetId(setId)
        this.analog.getDataId()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add data:', error);

        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  addEquipments() {
    const Equipment = this.equipmenForm.value;
    this.analogService.addEquipment(Equipment).subscribe(
      (response: Equipments[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false;
        }
        const setId = this.setID
        this.analogy.setSetId(setId)
        this.analog.getDataId()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add data:', error);

        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
}
