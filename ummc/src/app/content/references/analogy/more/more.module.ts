import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MoreComponent } from './more.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    // MoreComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
  // exports: [MoreComponent]
})
export class MoreModule { }
