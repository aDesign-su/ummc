/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AnalogService } from './analog.service';

describe('Service: Analog', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnalogService]
    });
  });

  it('should ...', inject([AnalogService], (service: AnalogService) => {
    expect(service).toBeTruthy();
  }));
});
