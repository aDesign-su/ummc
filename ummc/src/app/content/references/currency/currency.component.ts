import { Component, OnInit, ViewChild } from '@angular/core';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Category } from '../category/category';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { CategoryService } from '../category/category.service';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {
  constructor(public theme: ThemeService, public isTitle: CommonService, private cat: ApicontentService, public categoryService: CategoryService,public show:ShowService,private formBuilder: FormBuilder) { 
    this.editForm = this.formBuilder.group({
      title: "",
    });
  }
  element: number
  editForm: FormGroup
  ngOnInit() { this.getDataSourceCurrency() }
  
  displayedColumns: string[] = ['title', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSourceCurrency.paginator = this.paginator;
  }
  dataSourceCurrency = new MatTableDataSource<Category>([]);
  
  getDataSourceCurrency(): void {
    this.cat.getCurrencyReference().subscribe(
      (data: Category[]) => {
        this.dataSourceCurrency.data = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  create() {
    this.editForm.reset()
  }
  addCurrency() {
    const title = this.editForm.value;

    this.cat.addCurrency(title).subscribe(
      (response: Category[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSourceCurrency();
      },
    );
  }

  editCurrency(element: Category) {
    this.element = element.id; // Установим значение свойства element
    this.editForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }

  saveCurrency() {
    if (this.element) {
      let editedObject = this.editForm.value;
      this.cat.editCurrency(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getDataSourceCurrency();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  
  delCurrency(element: Category) {
    const delElement: any = element.id
    this.cat.delCurrency(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Валюта удалена.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSourceCurrency();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }
}
