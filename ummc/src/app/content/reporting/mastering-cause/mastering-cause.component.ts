import { Component, OnInit, ViewChild } from '@angular/core';
import { FinancingDetail, MasteringCause } from './mastering-cause';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { MatPaginator } from '@angular/material/paginator';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as jmespath from 'jmespath';
import { MatTableDataSource } from '@angular/material/table';
import { ShowService } from 'src/app/helper/show.service';
@Component({
  selector: 'app-mastering-cause',
  templateUrl: './mastering-cause.component.html',
  styleUrls: ['./mastering-cause.component.css']
})
export class MasteringCauseComponent implements OnInit {
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  displayedColumns: string[] = ["id", 'code_wbs', 'name', 'financing.plan', 'financing.prediction', 'financing.deviation', 'financing.cause', 'action'];
  MasteringCauseArray: MasteringCause[] = [];
  MasteringCauseSummary: FinancingDetail[] = [];
  dataSource: MatTableDataSource<MasteringCause>;
  dataLoaded = false;
  detailForm: FormGroup;
  detailFormEdit: FormGroup;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isEditing = false;
  selectobject = null;
  showButton = false;
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });


  constructor(public theme: ThemeService, public params: ApicontentService, public show: ShowService) {
    this.dataSource = new MatTableDataSource<MasteringCause>([]);

    
  }

  ngOnInit() {
    this.detailForm = this.initializeForm();
    this.detailFormEdit = this.initializeForm();
    this.getDataMasteringCauseData();
    this.range.valueChanges.subscribe(() => {
      this.showButton = true;
    });
  }
  initializeForm(): FormGroup {
    return new FormGroup({
      financing_object: new FormControl('', Validators.required),
      plan: new FormControl('', Validators.required),
      prediction: new FormControl('', Validators.required),
      cause: new FormControl('', Validators.required)
    });
  }

  getDate() {
    this.showButton = false;
    let start_date = this.range.controls.start.value;
    let end_date = this.range.controls.end.value;
    this.getDataMasteringCauseData(start_date, end_date);
  }


  ngAfterViewInit() {
    this.dataSource.sortingDataAccessor = (data: MasteringCause, sortHeaderId: string): string | number => {
      switch (sortHeaderId) {
        case 'financing.plan':
          return data.financing ? data.financing.plan : null;
        case 'financing.prediction':
          return data.financing ? data.financing.prediction : null;
        case 'financing.deviation':
          return data.financing ? data.financing.deviation : null;
        case 'financing.cause':
          return data.financing ? data.financing.cause : null;
        case 'financing.date':
          return data.financing ? data.financing.date : null;
        default:
          return data[sortHeaderId];
      }
    };

    this.dataSource.filterPredicate = (data: MasteringCause, filter: string): boolean => {
      const strData = JSON.stringify(data).toLowerCase();
      return strData.indexOf(filter) !== -1;
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataLoaded = true;
    console.log("dataLodaed:", this.dataLoaded)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();

    }
  }
  getDataMasteringCauseData(startDate?: Date, endDate?: Date): void {
    this.dataLoaded = false;
    this.params.getDataMasteringCause(startDate, endDate).subscribe(
      (data: MasteringCause[]) => {
        this.dataLoaded = true;
        this.initTable(data)

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }


  initTable(data): void {
    this.MasteringCauseArray = jmespath.search(data, "[0:-1]");
    this.dataSource.data = this.MasteringCauseArray;
    this.MasteringCauseSummary = jmespath.search(data, "[-1]");
    console.log(this.MasteringCauseArray);
    console.log(this.MasteringCauseSummary);
  }
  // ADD
  addDetails(id_obj: MasteringCause) {
    console.log(id_obj)
    this.isEditing = false
    this.detailForm.patchValue({
      financing_object: id_obj.id,
      plan: null,
      prediction: null,
      cause: null
    });
  }

  submitFormAdd() {
    const formData = this.detailForm.value;
    this.params.addMasteringCause(formData).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        this.getDataMasteringCauseData()
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }


  // EDIT
  editDetails(obj: FinancingDetail) {
    this.selectobject=obj.id
    this.isEditing = true
    console.log('select',this.selectobject)
    console.log(obj)
    this.detailFormEdit.patchValue({
      financing_object: obj.financing_object,
      plan: obj.plan,
      prediction: obj.prediction,
      cause: obj.cause
    });

  }

  submitFormEdit() {
    const formData = this.detailFormEdit.value;
    console.log(formData)
    this.params.editMasteringCause(formData, this.selectobject).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        this.getDataMasteringCauseData();
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }


  delDetails(obj) {
    this.params.deleteMasteringCause(obj.id).subscribe(
      (response: any) => {
        console.log('Data del:', response);
        this.getDataMasteringCauseData();
      },
      (error: any) => {
        console.error('Failed to del data:', error);
      }
    );

  }
  getTotal(key: 'plan' | 'prediction' | 'deviation'): number {
    if (this.MasteringCauseSummary.length > 0 && this.MasteringCauseSummary[0].summary) {
      return this.MasteringCauseSummary[0].summary[key];
    }
    return 0;
  }

  download_to_excel() {
    this.params.downloadFile().subscribe(blob => {
      const currentDate = new Date();
      const dateString = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
      const fileName = `Освоение_Причины_отклонений_${dateString}.xlsx`;

      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = fileName;
      link.click();
      URL.revokeObjectURL(link.href);
    });
  }

}
