/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MasteringCauseComponent } from './mastering-cause.component';

describe('MasteringCauseComponent', () => {
  let component: MasteringCauseComponent;
  let fixture: ComponentFixture<MasteringCauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasteringCauseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasteringCauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
