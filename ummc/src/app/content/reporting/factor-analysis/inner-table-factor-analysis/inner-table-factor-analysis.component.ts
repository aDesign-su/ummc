import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ThemeService } from 'src/app/theme.service';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-inner-table-factor-analysis',
  templateUrl: './inner-table-factor-analysis.component.html',
  styleUrls: ['./inner-table-factor-analysis.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class InnerTableFactorAnalysisComponent implements OnInit {
  @Input() data: any[];
  @Input() columnsToDisplay: { [header: string]: string };
  @Input() toggleExpand: (element: any) => void;
  @Output() showChartEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() editFactorAnalysis: EventEmitter<number> = new EventEmitter<number>();
  @Output() delitedFactorAnalysis: EventEmitter<number> = new EventEmitter<number>();
  @Output() addInfluence: EventEmitter<number> = new EventEmitter<number>();
  @Output() updateInfluence: EventEmitter<number> = new EventEmitter<number>();
  @Output() delInfluence: EventEmitter<number> = new EventEmitter<number>();

  expandedElements: any[] = [];
  DataSourse: MatTableDataSource<any> = new MatTableDataSource<any>();
  headers: string[] = [];
  keys: string[] = [];
  columnsToDisplayWithExpand = []


  constructor(public theme: ThemeService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog,) { }


  ngOnInit() {
    this.expandedElements = [];
    this.headers = Object.keys(this.columnsToDisplay);
    this.keys = Object.values(this.columnsToDisplay);
    this.columnsToDisplayWithExpand = ['expand', ...this.keys, "action"];
    this.DataSourse.data = this.data;
  }
  isExpanded = (element: any): boolean => {
    return this.expandedElements.indexOf(element) !== -1;
  };

  isNotExpanded = (element: any): boolean => {
    return !this.isExpanded(element);
  };

  ShowChartWaterfall(element): void {
    this.showChartEvent.emit(element);
  }

  editFactorAnalysisinner(element) {
    this.editFactorAnalysis.emit(element);
  }
  delitedFactorAnalysisinner(element) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delitedFactorAnalysis.emit(element);
        const index = this.DataSourse.data.findIndex(item => item.id === element.id);
        if (index > -1) {
          this.DataSourse.data.splice(index, 1);
          this.DataSourse._updateChangeSubscription();
        }
      }
    });
  }

  AddInfluence(element) {
    this.addInfluence.emit(element.id);
  }

  UpdateInfluence(element) {
    this.updateInfluence.emit(element);
  }

  DelInfluence(element) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Здесь мы ищем родительский элемент, в котором содержится влияние с данным id
        const parentElement = this.DataSourse.data.find(item => item.factor_analysis_influence && item.factor_analysis_influence.some(influence => influence.id === element.id));
        if (parentElement) {
          // Теперь найдем индекс влияния внутри родительского элемента
          const influenceIndex = parentElement.factor_analysis_influence.findIndex(influence => influence.id === element.id);
          if (influenceIndex > -1) {
            // Удаляем влияние из родительского элемента
            parentElement.factor_analysis_influence = [
              ...parentElement.factor_analysis_influence.slice(0, influenceIndex),
              ...parentElement.factor_analysis_influence.slice(influenceIndex + 1)
            ];
          }
          this.DataSourse._updateChangeSubscription();
        }
        this.delInfluence.emit(element.id);
      }
    });
  }
}
