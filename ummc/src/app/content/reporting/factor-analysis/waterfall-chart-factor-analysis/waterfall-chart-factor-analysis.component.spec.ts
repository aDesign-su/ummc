/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WaterfallChartFactorAnalysisComponent } from './waterfall-chart-factor-analysis.component';

describe('WaterfallChartFactorAnalysisComponent', () => {
  let component: WaterfallChartFactorAnalysisComponent;
  let fixture: ComponentFixture<WaterfallChartFactorAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterfallChartFactorAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterfallChartFactorAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
