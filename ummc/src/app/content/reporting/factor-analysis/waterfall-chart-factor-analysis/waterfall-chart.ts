export interface detailFactorAnalysis {
    id: number
    budget: number
    factor_analysis_influence: FactorAnalysisChart[]
    current_budget: number
}

export interface FactorAnalysisChart {
    id?: number
    title: string
    desc?: string
    influence: number
}
