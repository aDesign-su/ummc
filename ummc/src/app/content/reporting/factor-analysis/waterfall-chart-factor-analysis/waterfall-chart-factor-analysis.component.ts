import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import {
  ChartComponent, ApexAxisChartSeries, ApexChart, ApexFill, ApexTooltip, ApexXAxis, ApexLegend, ApexDataLabels, ApexTitleSubtitle, ApexYAxis, ApexPlotOptions
} from "ng-apexcharts";
import { ChartOptions } from '../../mastering/mastering.component';
import { CustomNumberPipe } from 'src/app/helper/customNumber/customNumber.pipe';
import { FactorAnalysisChart } from './waterfall-chart';


@Component({
  selector: 'app-waterfall-chart-factor-analysis',
  templateUrl: './waterfall-chart-factor-analysis.component.html',
  styleUrls: ['./waterfall-chart-factor-analysis.component.css']
})
export class WaterfallChartFactorAnalysisComponent implements OnInit {
  private customNumberPipe = new CustomNumberPipe();
  public waterfallChart: Partial<ChartOptions>;
  @Input() factorAnalysisData: FactorAnalysisChart[] = [];

  constructor() { }

  ngOnInit() {
    this.createWaterfallChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['factorAnalysisData'] && changes['factorAnalysisData'].currentValue) {
      this.createWaterfallChart();
    }
  }

  transformData(data) {
    let accumulatedValue = 0;
    return data.map((item, index) => {
      const isSpecialItem = item.id === null;
      const startValue = (index === data.length - 1) ? 0 : accumulatedValue;

      accumulatedValue = isSpecialItem ? item.influence : accumulatedValue + item.influence;

      return {
        x: item.title,
        y: [startValue, accumulatedValue],
        dicription: item.desc || null,
      };
    });
  }
  getColor(dataPointIndex, length) {
    return dataPointIndex === 0 || dataPointIndex === length - 1 ? "#000" : "#EB8B2D";
  }

  createWaterfallChart() {
    const rangeBarData = this.transformData(this.factorAnalysisData)
    console.log('Вот это выводится в графике', rangeBarData)

    this.waterfallChart = {
      series: [
        {
          data: rangeBarData
        }
      ],
      chart: {
        type: "rangeBar",
        height: 350,

      },
      colors: [({ dataPointIndex }) => this.getColor(dataPointIndex, rangeBarData.length)],
      dataLabels: {
        enabled: true,
        formatter: function (val, opts) {
          let index = opts.dataPointIndex;
          let difference = rangeBarData[index].y[1] - rangeBarData[index].y[0];
          return `${this.customNumberPipe.transform(difference)}`;
        }.bind(this),
        offsetY: 0,
        style: {
          fontSize: "12px",
          colors: [({ dataPointIndex }) => this.getColor(dataPointIndex, rangeBarData.length)]
        },
        background: {
          enabled: true,
          foreColor: '#fff',
          borderRadius: 2,
          padding: 4,

          borderWidth: 0,
        },
      },
      yaxis: {
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: true,
          formatter: function (val) {
            return this.customNumberPipe.transform(val) + "";  // Используем pipe здесь
          }.bind(this),
        }
      },
      title: {
        text: "Бюджет строительства проекта",
        align: "left"
      },
      tooltip: {
        enabled: true,
        shared: true,
        intersect: false,

        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          const currentItem = rangeBarData[dataPointIndex];
          const difference = currentItem.y[1] - currentItem.y[0];

          return '<div class="card">' +
            '<div class="card-body">' +
            '<h3 class="card-title">' + currentItem.x + '</h3>' +
            '<p class="card-text">' + (currentItem.dicription || '') + '</p>' +
            '</div>' +
            '<div class="card-footer text-muted">' + difference + '' + '</div>' +
            '</div>'
        }
      }
    };
  }

}
