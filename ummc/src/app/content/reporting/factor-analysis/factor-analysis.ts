export interface FactorAnalysis {
    id: number
    name: string
    factor_analysis?: Analysis[]
    title:string
}

export interface Analysis {
    id: number
    budget: number
    factor_analysis_influence: FactorAnalysisInfluence[]
    current_budget: number
}

export interface FactorAnalysisInfluence {
    id: number
    title: string
    desc: string
    influence: number
}