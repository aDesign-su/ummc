import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ThemeService } from 'src/app/theme.service';
import { Analysis, FactorAnalysis } from './factor-analysis';
import { detailFactorAnalysis } from './waterfall-chart-factor-analysis/waterfall-chart';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProjectRegister } from '../../project-register/project-register';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;


@Component({
  selector: 'app-factor-analysis',
  templateUrl: './factor-analysis.component.html',
  styleUrls: ['./factor-analysis.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class FactorAnalysisComponent implements OnInit {
  expandedElements: any[] = [];
  dataFactorAnalysisTable: MatTableDataSource<FactorAnalysis> = new MatTableDataSource<FactorAnalysis>();
  columnsToDisplay = ['name'];//Главная таблица
  columnsToDisplayWithExpand = ['expand', ...this.columnsToDisplay, 'action'];//Главная таблица/Стрелка для раскрытия 
  factorAnalysisData: any[] = [];
  dataLoaded = false;

  editMode: boolean
  FormAddEditFactorAnalysis: FormGroup;
  ProjectList: ProjectRegister[];
  selectedFactorAnalysis: number;


  FormAddEditInfluence: FormGroup;
  selectedInfluence: number;
  constructor(public theme: ThemeService, private fb: FormBuilder, public isTitle: CommonService, public params: ApicontentService,) {
    this.FormAddEditFactorAnalysis = this.fb.group({
      budget: null,
      title: "",
      project: null
    });

    this.FormAddEditInfluence = this.fb.group({
      factor_analysis: null,
      title: "",
      desc: "",
      influence: null
    });
  }

  ngOnInit() {
    this.getDataFactorAnalysis()
    this.getProject();
  }

  // Полуить данные Проекты
  getDataFactorAnalysis(): void {
    this.params.getFactorAnalysis().subscribe(
      (data) => {
        console.log(data)
        this.dataFactorAnalysisTable.data = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  add() {
    this.editMode = false
    this.FormAddEditFactorAnalysis.reset()
  }

  //Раскрывается табличка с табличками
  toggleExpand(element: any): void {
    const index = this.expandedElements.indexOf(element);
    if (index === -1) {
      this.expandedElements.push(element);
    } else {
      this.expandedElements.splice(index, 1);
    }
  }


  getFactorAnalysisChart(id: number) {
    this.dataLoaded = true;
    this.params.getFactorAnalysisChart(id).subscribe(
      (data: detailFactorAnalysis) => {
        this.dataLoaded = false;
        console.log('Данные по id:' + id, data.factor_analysis_influence)
        this.factorAnalysisData = data.factor_analysis_influence;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getProject() {
    this.params.getProjects().subscribe(
      (response) => {
        console.log('ProjectList:', response);
        this.ProjectList = response;
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }

  showChartWaterfall(element) {
    console.log('Вот element', element)
    this.getFactorAnalysisChart(element.id)
  }


  editFactorAnalysis(element) {
    console.log('теперь тут', element)
    this.editMode = true;
    this.selectedFactorAnalysis = element.id;
    this.FormAddEditFactorAnalysis.setValue({
      budget: element.budget,
      title: element.title,
      project: element.project
    });
  }
  delitedFactorAnalysis(element) {
    const id = element.id
    this.params.deliteFactorAnalysis(id).subscribe(
      (response) => {
        // Удаляем элемент локально
        this.theme.openSnackBar('Факторный анализ удален')
        const index = this.dataFactorAnalysisTable.data.findIndex(item => item.id === id);
        if (index > -1) {
          this.dataFactorAnalysisTable.data.splice(index, 1);
          this.dataFactorAnalysisTable._updateChangeSubscription();
          this.dataFactorAnalysisTable.data.splice(index, 1);
          this.dataFactorAnalysisTable._updateChangeSubscription();
        }


        // Удалите элемент также из внутренней таблицы
        const expandedElement = this.expandedElements.find(e => e.id === id);
        if (expandedElement && expandedElement.factor_analysis) {
          const innerIndex = expandedElement.factor_analysis.findIndex(item => item.id === id);
          if (innerIndex > -1) {
            expandedElement.factor_analysis.splice(innerIndex, 1);
          }
        }
      },
      (error: any) => {
        console.error('Failed to delete :', error);
      }
    );
  }

  public createOrUpdateFactorAnalysis(): void {
    if (this.FormAddEditFactorAnalysis.valid) {
      const formData = { ...this.FormAddEditFactorAnalysis.value };
      if (this.editMode) {
        this.updateFactorAnalysis(this.selectedFactorAnalysis, formData);
      } else {
        this.createFactorAnalysis(formData);
      }

    }
  }

  updateFactorAnalysis(id: number, data: detailFactorAnalysis): void {
    console.log('Upadte to add:', data);
    this.params.updateFactorAnalysi(id, data).subscribe(
      (data: any) => {
        console.log(data)
        this.theme.openSnackBar('Сохранено')
        this.getDataFactorAnalysis()
        const offcanvasElement = document.getElementById('EditAddScrolling');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.editMode = false;
        this.selectedFactorAnalysis = null;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );

  }


  // Создать факторный анализ 
  createFactorAnalysis(data): void {
    this.params.addFactorAnalysis(data).subscribe(
      (response: any) => {
        console.log('ответ', response);
        this.theme.openSnackBar('Добавлен факторный анализ');

        this.getDataFactorAnalysis()

        const offcanvasElement = document.getElementById('EditAddScrolling');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.editMode = false;
        this.selectedFactorAnalysis = null;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );


  }

  // Добавить Влияние на текущий бюджет проекта
  AddInfluence(id) {
    this.editMode = false
    this.FormAddEditInfluence.setValue({
      factor_analysis: id,
      title: "",
      desc: "",
      influence: null
    });
  }

  createOrUpdateInfluence(): void {
    if (this.FormAddEditInfluence.valid) {
      const formData = { ...this.FormAddEditInfluence.value };
      if (this.editMode) {
        this.updateInfluence(formData);
      } else {
        this.createInfluence(formData);
      }

    }
  }
  // Создать Влияние на Тек бюджет 
  createInfluence(data): void {
    this.params.addInfluence(data).subscribe(
      (response: any) => {
        this.theme.openSnackBar('Добавлено влияние на текущий бюджет');

        this.getDataFactorAnalysis()

        const offcanvasElement = document.getElementById('EditAddInfluence');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.editMode = false;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  // Изменить Влияние на текущий бюджет проекта
  editInfluence(element) {
    console.log('Вот элемнент' ,element)
    this.editMode = true
    this.FormAddEditInfluence.setValue({
      factor_analysis: null,
      // id: element.id,
      title: element.title,
      desc: element.desc,
      influence: element.influence
    });
  }


  updateInfluence(data): void {

    this.params.updateInfluence(data.id, data).subscribe(
      (data: any) => {
        console.log(data)
        this.theme.openSnackBar('Сохранено')
        this.getDataFactorAnalysis()
        const offcanvasElement = document.getElementById('EditAddInfluence');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.editMode = false;
        this.selectedFactorAnalysis = null;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );

  }

  delitedInfluence(id) {
    console.log('Удаляем вот',id)
    this.params.deliteInfluence(id).subscribe(
      (response) => {
        this.theme.openSnackBar('Удалено')
       
      },
      (error: any) => {
        console.error('Failed to delete :', error);
      }
    );
  }
}
