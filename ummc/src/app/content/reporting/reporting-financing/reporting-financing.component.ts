import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Mastering, PlanDeviationData } from './reporting-financing';
import * as jmespath from 'jmespath';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexTooltip,
  ApexXAxis,
  ApexLegend,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexYAxis,
  ApexPlotOptions
} from "ng-apexcharts";
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormGroup } from '@angular/forms';


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  markers: any; //ApexMarkers;
  stroke: any; //ApexStroke;
  yaxis: ApexYAxis | ApexYAxis[];
  dataLabels: ApexDataLabels;
  title: ApexTitleSubtitle;
  legend: ApexLegend;
  fill: ApexFill;
  tooltip: ApexTooltip;
  colors?: string[];
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-reporting-financing',
  templateUrl: './reporting-financing.component.html',
  styleUrls: ['./reporting-financing.component.css']
})
export class ReportingFinancingComponent implements OnInit {
  MasteringArray: Mastering[] = [];
  selectedYear: number;
  selectedYearShortfall: number
  years: number[] = [];
  showNoDataMessage: boolean = true;
  dataLoaded = false;
  @ViewChild("chart") chart: ChartComponent;

  public chartOptions: Partial<ChartOptions>;
  public chartOptionsPlanDeviation: Partial<ChartOptions>;
  public chartShortfalPercentage: Partial<ChartOptions>;

  formGroup: FormGroup;
  checked: boolean = false;

  constructor(public theme: ThemeService, public params: ApicontentService, public show: ShowService) {


  }

  ngOnInit() {
    this.getMasteringData('')

  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }


  months = Array.from({ length: 12 }).map((_, i) => {
    const date = new Date(2020, i, 1); // год не имеет значения, так как нам нужны только месяцы
    const monthName = date.toLocaleDateString('ru-RU', { month: 'long' }); // получение полного имени месяца на русском языке

    // Преобразование первой буквы в верхний регистр
    const capitalizedMonthName = monthName.charAt(0).toUpperCase() + monthName.slice(1);

    return {
      value: (i + 1).toString().padStart(2, '0'), // преобразование числа в строку формата '01', '02', ...
      name: capitalizedMonthName
    };
  });


  selectedMonth1: string;
  selectedMonth2: string;

  yearChanged() {
    this.selectedMonth1 = null;
    this.selectedMonth2 = null;
  }
  updateDataIfFilled(): void {
    console.log(this.checked)
    if (this.selectedYearShortfall && this.selectedMonth1 && this.selectedMonth2) {
      this.showNoDataMessage = false;
      this.submitForm();
    } else {
      this.showNoDataMessage = true;
    }
  }

  submitForm() {
    const firstDate = `${this.selectedYearShortfall}-${this.selectedMonth1}-01`;
    const secondDate = `${this.selectedYearShortfall}-${this.selectedMonth2}-01`;
    this.PlanDeviationData(firstDate, secondDate)
    this.params.getDataMastering(this.selectedYearShortfall).subscribe(
      (data: Mastering[]) => {
        console.log('data', data)
        this.processShortfallPercentage(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }



  processShortfallPercentage(data) {
    console.log('data123', data)
    const shortfall_percentage = jmespath.search(data, '[].shortfall_percentage');
    const accumulated_shortfall_percentage = jmespath.search(data, '[].accumulated_shortfall_percentage');
    const monthNames = this.months.map(month => month.name);
    const dataToShow = this.checked ? accumulated_shortfall_percentage : shortfall_percentage;

    this.chartShortfalPercentage = {
      series: [
        {
          name: "Отклонение",
          data: dataToShow
        }
      ],
      chart: {
        height: 350,
        type: "bar"
      },

      dataLabels: {
        enabled: true,
        formatter: function (val) {
          return val + "%";
        },
        offsetY: -20,
        style: {
          fontSize: "12px",
          colors: ["#304758"]
        }
      },
      colors: ['#ed4317'],
      xaxis: {
        categories: monthNames,
        position: "bottom",
        labels: {
          offsetY: -20
        },
        axisBorder: {
          show: true
        },
        axisTicks: {
          show: false
        },

        tooltip: {
          enabled: true,
          offsetY: -10
        }
      },
      yaxis: {
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: true,
          formatter: function (val) {
            return val + "%";
          }
        }
      },
      title: {
        text: "Отклонения по месяцам за " + this.selectedYearShortfall + "г.",
        align: "left",
      },
    };
  }


  PlanDeviationData(first_date, second_date): void {
    this.params.getDataPlanDeviation(first_date, second_date).subscribe(
      (data: PlanDeviationData[]) => {
        console.log(data)
        this.processPlanDeviation(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getMonthNameByValue(value: string): string {
    const month = this.months.find(m => m.value === value);
    return month ? month.name : 'Неизвестный месяц';
  }
  private processPlanDeviation(data: PlanDeviationData[]) {
    const firstRecordAccumulatedPlan = jmespath.search(data, 'first_record.accumulated_plan');
    const secondRecordAccumulatedFact = jmespath.search(data, 'second_record.accumulated_fact');
    const growthPercentage = jmespath.search(data, 'growth_percentage');
    console.log("firstRecordAccumulatedPlan:", firstRecordAccumulatedPlan);
    console.log("secondRecordAccumulatedFact:", secondRecordAccumulatedFact);
    this.chartOptionsPlanDeviation = {
      series: [
        {
          name: "План",
          data: [
            {
              x: "",
              y: [0, firstRecordAccumulatedPlan]
            },
          ]
        },
        {
          name: "Разница",
          data: [
            {
              x: "",
              y: [firstRecordAccumulatedPlan, secondRecordAccumulatedFact]
            },
          ]
        },
        {
          name: "Факт",
          data: [
            {
              x: "",
              y: [0, secondRecordAccumulatedFact]
            },
          ]
        }
      ],
      colors: ['#c1c1c1', '#ed4317', '#ff8c1c'],
      chart: {
        type: "rangeBar",
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      title: {
        text: "Накопительно с " + this.getMonthNameByValue(this.selectedMonth1) + " " + this.selectedYearShortfall + " по " + this.getMonthNameByValue(this.selectedMonth2) + " " + this.selectedYearShortfall,
        align: "left",
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ["#000"] // Черный цвет текста
        },
        formatter: function (val, opts) {
          if (typeof val !== 'number') {
            return val.toString();
          }
          const seriesIndex = opts.seriesIndex;

          switch (seriesIndex) {
            case 0:
              return val;
            case 1:
              return growthPercentage + ' %';
            case 2:
              return val;
            default:
              return val.toString();
          }
        }
      },
      tooltip: {
        enabled: false
      }
    }
  }



  getMasteringData(date): void {
    this.dataLoaded = false;
    this.params.getDataMastering(date).subscribe(
      (data: Mastering[]) => {
        this.processData(data, date);
        this.dataLoaded = true;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  roundUp(number, precision) {
    return Math.ceil(number / precision) * precision;
  }




  private processData(data: Mastering[], year: number): void {
    this.MasteringArray = jmespath.search(data, "[?!years]");
    this.years = jmespath.search(data, "[?years]")[0].years;
    console.log('Go', this.MasteringArray);
    console.log('Years', this.years);

    const dataMasteringArray = this.MasteringArray;

    const categories = dataMasteringArray.map(item => item.date); // Даты
    const start_year_planData = dataMasteringArray.map(item => item.accumulated_plan);
    const start_year_factData = dataMasteringArray.map(item => item.accumulated_fact);
    const planData = dataMasteringArray.map(item => item.plan);
    const factData = dataMasteringArray.map(item => item.fact);
    const predictionData = dataMasteringArray.map(item => item.prediction);
    const prediction_cumulativeData = dataMasteringArray.map(item => item.accumulated_prediction);
    console.log(categories)

    const maxPlanFactPrediction = Math.max(...planData, ...factData, ...predictionData);
    const maxYearStart = Math.max(...start_year_planData, ...start_year_factData, ...prediction_cumulativeData);
    const roundedMaxPlanFactPrediction = this.roundUp(maxPlanFactPrediction, 1000);
    const roundedMaxYearStart = this.roundUp(maxYearStart, 1000);
    this.chartOptions = {
      series: [
        {
          name: "План",
          type: "column",
          data: planData,
        },
        {
          name: "Факт",
          type: "column",
          data: factData
        },
        {
          name: "Прогноз",
          type: "column",
          data: predictionData
        },
        {
          name: "С начала года План",
          type: "line",
          data: start_year_planData
        },
        {
          name: "С начала года Факт",
          type: "line",
          data: start_year_factData
        },
        {
          name: "Прогноз накопительный",
          type: "line",
          data: prediction_cumulativeData
        }
      ],
      colors: ['#c1c1c1', '#ff8c1c', '#06ad55', '#828282', '#ed4317', '#27b569'],
      chart: {
        height: 350,
        type: "line",
        stacked: false,

      },

      dataLabels: {
        enabled: true,
        enabledOnSeries: [0, 1, 2]
      },
      fill: {
        opacity: [1, 1, 0, 1, 1, 1]
      },
      stroke: {
        dashArray: [[], [], [5, 5], [], [], [5, 5]],
        width: 4
      },
      title: {
        text: "Исполнение плана финансирования по состоянию на " + year + " г. млн руб. без НДС",
        align: "left",
        offsetX: 110
      },
      xaxis: {
        categories: categories
      },
      yaxis: [
        {
          min: 0,
          max: roundedMaxPlanFactPrediction,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxPlanFactPrediction,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxPlanFactPrediction,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxYearStart,
          opposite: true,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxYearStart,
          opposite: true,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxYearStart,
          opposite: true,
          labels: {
            show: false
          }
        },
        {
          min: 0,
          max: roundedMaxYearStart,
          opposite: true,
        }, {
          min: 0,
          max: roundedMaxPlanFactPrediction,
        },
      ]

    };
  }
}

