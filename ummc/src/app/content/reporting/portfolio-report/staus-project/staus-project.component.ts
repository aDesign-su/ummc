import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThemeService } from 'src/app/theme.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { ShowService } from 'src/app/helper/show.service';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import moment from 'moment';
import { StatusProject } from './status-proj';
declare var bootstrap: any;


@Component({
  selector: 'app-staus-project',
  templateUrl: './staus-project.component.html',
  styleUrls: ['./staus-project.component.css']
})
export class StausProjectComponent implements OnInit {
  StatusProjectTableInfo = this.theme.initTable<StatusProject>();//Информация в шапке
  editMode = false;
  FormAddEditStatus: FormGroup;

  constructor(public theme: ThemeService, private router: Router, private fb: FormBuilder, public show: ShowService, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.FormAddEditStatus = this.fb.group({
      id:[null],
      title: [null],
    });

  }

  ngOnInit() {
    this.getStatusProjectDataSheets()
  }

  add() {
    this.editMode = false
    this.FormAddEditStatus.reset()
  }

  getStatusProjectDataSheets(): void {
    this.params.getStatusProjectDataSheets().subscribe(
      (data: StatusProject[]) => {
        this.StatusProjectTableInfo.data = data;
        console.log(this.StatusProjectTableInfo.data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  AddEditStatus() {
    if (this.FormAddEditStatus.valid) {
      const formData = { ...this.FormAddEditStatus.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;

      if (this.editMode) {
        this.updateStatusProject(id, formValueWithoutId);
      } else {
        this.createStatusProject(formValueWithoutId);
      }

    }
  }

  updateStatusProject(id, formValueWithoutId) {
    this.params.editStatusProject(id, formValueWithoutId).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addEditStatus');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditStatus.reset()

        // Найти индекс элемента, который нужно обновить
        const index = this.StatusProjectTableInfo.data.findIndex(item => item.id === response.id);
        // Проверить, найден ли элемент
        if (index !== -1) {
          // Заменить элемент в массиве
          this.StatusProjectTableInfo.data[index] = response;
          this.StatusProjectTableInfo.data = [...this.StatusProjectTableInfo.data];
        }
        console.log(this.StatusProjectTableInfo.data)
        this.editMode = false;
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }




  createStatusProject(data) {
    console.log('create:', data)
    this.params.addStatusProject(data).subscribe(
      (data: StatusProject) => {
        console.log(data)

        this.theme.openSnackBar(`Добавлен статус `);
        this.getStatusProjectDataSheets()
        const offcanvasElement = document.getElementById('addEditStatus');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditStatus.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );

  }
  delStatusProject(id) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delStatusProject(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Статус удален');
            const index = this.StatusProjectTableInfo.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.StatusProjectTableInfo.data.splice(index, 1);
              this.StatusProjectTableInfo._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }


  // Изменить
  editStatusProject(element) {
    console.log(element)
    this.FormAddEditStatus.setValue({
      id: element.id,
      title: element.title,
    });
    this.editMode = true
  }
}
