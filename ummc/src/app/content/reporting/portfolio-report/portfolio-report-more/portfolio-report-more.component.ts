import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { PortfolioReporDocument, PortfolioReportData } from '../portfolio-report';
import { ShowService } from 'src/app/helper/show.service';
import { StatusProject } from '../staus-project/status-proj';
declare var bootstrap: any;

@Component({
  selector: 'app-portfolio-report-more',
  templateUrl: './portfolio-report-more.component.html',
  styleUrls: ['./portfolio-report-more.component.css']
})
export class PortfolioReportMoreComponent implements OnInit {
  dataPortfolioReportItemTable = this.theme.initTable<PortfolioReportData>();
  DataPortfolioReportItem: PortfolioReportData[]
  dataPortfolioReportDocument = this.theme.initTable<PortfolioReporDocument>();
  editMode = false;
  FormAddEditDocument: FormGroup;
  public SelectStatusList: StatusProject[]
  filteredListSelectStatusListList: StatusProject[]

  constructor(private route: ActivatedRoute, public show: ShowService, public theme: ThemeService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe) {
    this.resetForm()
  }
  add() {
    this.editMode = false
    this.resetForm()
  }
  resetForm() {
    const requestId = this.route.snapshot.paramMap.get('id');
    this.FormAddEditDocument = this.fb.group({
      id: [null],
      document: [null],
      files: [null],
      portfolio_report_id: requestId,
      status_id: [null],
    });
  }
  ngOnInit() {
    const requestId = this.route.snapshot.paramMap.get('id');
    if (requestId) {
      this.getPortfolioReporDataSheetsItem(requestId)
      this.getStatusProjectDataSheets()
      console.log(requestId)
    }
  }

  getPortfolioReporDataSheetsItem(requestId): void {
    this.params.getPortfolioReportItemData(requestId).subscribe(
      (data: any) => {
        // Предполагаем, что data[0] содержит основные данные, а data[1] - данные о статусе
        const mainData = data[0];
        const statusData = data[1]['status'];

        // Объединение данных о статусе с основными данными
        mainData.status = statusData;

        // Установка объединенных данных как источника данных для таблицы
        this.dataPortfolioReportItemTable.data = [mainData];
        this.DataPortfolioReportItem = data;

        this.dataPortfolioReportDocument.data = data[1]['Documents']
        console.log(this.dataPortfolioReportDocument.data);

        console.log(this.dataPortfolioReportItemTable.data);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  AddEditDocument() {
    if (this.FormAddEditDocument.valid) {
      const formData = { ...this.FormAddEditDocument.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;

      if (this.editMode) {
        this.updateDocument(id, formValueWithoutId);
      } else {
        this.createDocument(formValueWithoutId);
      }

    }
  }

  createDocument(data) {
    console.log('create:', data)
    this.params.addPortfolioReportDocument(data).subscribe(
      (data: any) => {
        console.log(data)

        this.theme.openSnackBar(`Добавлен документ`);
        // this.getStatusProjectDataSheets()
        const offcanvasElement = document.getElementById('addDocument');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.resetForm()
        const requestId = this.route.snapshot.paramMap.get('id');
        if (requestId) {
          this.getPortfolioReporDataSheetsItem(requestId)
          this.getStatusProjectDataSheets()
          console.log(requestId)
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );

  }

  getStatusProjectDataSheets(): void {
    this.params.getStatusProjectDataSheets().subscribe(
      (data: StatusProject[]) => {
        this.SelectStatusList = data;
        this.filteredListSelectStatusListList = this.SelectStatusList.slice();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  updateDocument(id, formValueWithoutId) {
    this.params.editPortfolioReportDocument(id, formValueWithoutId).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addDocument');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.resetForm()

        // // Найти индекс элемента, который нужно обновить
        // const index = this.dataPortfolioReportDocument.data.findIndex(item => item.id === response.id);
        // // Проверить, найден ли элемент
        // if (index !== -1) {
        //   // Заменить элемент в массиве
        //   this.dataPortfolioReportDocument.data[index] = response;
        //   this.dataPortfolioReportDocument.data = [...this.dataPortfolioReportDocument.data];
        // }
        // console.log(this.dataPortfolioReportDocument.data)
        const requestId = this.route.snapshot.paramMap.get('id');
        if (requestId) {
          this.getPortfolioReporDataSheetsItem(requestId)
          this.getStatusProjectDataSheets()
          console.log(requestId)
        }
        this.editMode = false;
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  delDocument(id) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delPortfolioReportDocument(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Документ удален');
            const index = this.dataPortfolioReportDocument.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataPortfolioReportDocument.data.splice(index, 1);
              this.dataPortfolioReportDocument._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  // Изменить
  editDocument(element) {
    console.log(element)
    this.FormAddEditDocument.setValue({
      id: element.id,
      files: element.files,
      status_id: element.status_id_id,
      portfolio_report_id: element.portfolio_report_id_id,
      document: element.document
    });
    this.editMode = true;
  }

}
