import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { PortfolioReportData } from './portfolio-report';
import { StatusProject } from './staus-project/status-proj';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;

@Component({
  selector: 'app-portfolio-report',
  templateUrl: './portfolio-report.component.html',
  styleUrls: ['./portfolio-report.component.css']
})
export class PortfolioReportComponent implements OnInit {
  dataPortfolioReportSource = this.initTable<PortfolioReportData>();
  FormAddProject: FormGroup;
  editMode = false;
  public SelectStatusList: StatusProject[]
  filteredListSelectStatusListList: StatusProject[]

  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }
  private transformDate(date: any): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  add() {
    this.editMode = false
    this.FormAddProject.reset()
  }
  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {

    this.FormAddProject = this.fb.group({
      id: [null],
      code_wbs: [null],
      name_wbs: [null],
      project_type: [null],
      head_of_project: [null],
      company: [null],
      date_start_plan: [null],
      date_end_fact: [null],
      date_start_prediction: [null],
      date_end_prediction: [null],
      budget_plan: [null],
      budget_prediction: [null],
      budget_saving: [null],
      npv: [null],
      irr: [null],
      pi: [null],
      status: [null],
    });
  }

  ngOnInit() {
    this.getPortfolioReporDataSheets()
    this.getStatusProjectDataSheets()
  }

  getStatusProjectDataSheets(): void {
    this.params.getStatusProjectDataSheets().subscribe(
      (data: StatusProject[]) => {
        this.SelectStatusList = data;
        this.filteredListSelectStatusListList = this.SelectStatusList.slice();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getPortfolioReporDataSheets(): void {
    this.params.getPortfolioReportData().subscribe(
      (data: PortfolioReportData[]) => {
        this.dataPortfolioReportSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  morePortfolioReport(id: number) {
    this.router.navigate(['/portfolio-report', id]);
  }


  AddEditProjectPortfolioReport() {
    if (this.FormAddProject.valid) {
      const formData = { ...this.FormAddProject.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;

      // Преобразуем даты в формат YYYY-MM-DD
      formValueWithoutId.date_end_fact = this.transformDate(formValueWithoutId.date_end_fact);
      formValueWithoutId.date_end_prediction = this.transformDate(formValueWithoutId.date_end_prediction);
      formValueWithoutId.date_start_plan = this.transformDate(formValueWithoutId.date_start_plan);
      formValueWithoutId.date_start_prediction = this.transformDate(formValueWithoutId.date_start_prediction);


      if (this.editMode) {
        this.updateProjectPortfolioRepor(id, formValueWithoutId);
      } else {
        this.createProjectPortfolioRepor(formValueWithoutId);
      }

    }
  }

  createProjectPortfolioRepor(data) {
    console.log('create:', data)
    this.params.addPortfolioReportProject(data).subscribe(
      (data: PortfolioReportData) => {
        console.log(data)



        this.theme.openSnackBar(`Добавлен проект`);
        this.getPortfolioReporDataSheets()
        const offcanvasElement = document.getElementById('addProject');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddProject.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );

  }


  updateProjectPortfolioRepor(id, formValueWithoutId) {
    this.params.editPortfolioReportProject(id, formValueWithoutId).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addProject');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddProject.reset()
        this.getPortfolioReporDataSheets()

        // // Найти индекс элемента, который нужно обновить
        // const index = this.dataPortfolioReportSource.data.findIndex(item => item.id === response.id);
        // // Проверить, найден ли элемент
        // if (index !== -1) {
        //   // Заменить элемент в массиве
        //   this.dataPortfolioReportSource.data[index] = response;
        //   this.dataPortfolioReportSource.data = [...this.dataPortfolioReportSource.data];
        // }
        // console.log(this.dataPortfolioReportSource.data)
        this.editMode = false;
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }



  editPortfolioReport(element: PortfolioReportData) {
    console.log(element)
    // Создаём копию объекта, исключая свойство 'budget_deviations'
    const formValue = { ...element };
    delete formValue.budget_deviations;

    // Используем patchValue для обновления значений формы
    this.FormAddProject.patchValue(formValue);
    this.editMode = true;
  }

}
