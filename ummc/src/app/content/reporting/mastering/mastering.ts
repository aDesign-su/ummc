export interface Mastering {
  id: number;
  date: string;
  accumulated_plan: number;
  accumulated_fact: number | null;
  accumulated_prediction: number;
  plan: number;
  fact: number | null;
  prediction: number;
}

export interface Years {
  years: number[];
}

export interface PlanDeviation {
  id: number;
  accumulated_plan: number;
  accumulated_fact: number;
  accumulated_prediction?: number | null;
  shortfall_percentage?: number | null;
  accumulated_shortfall_percentage?: number | null;
  date: string;
  plan: number;
  fact: number;
  prediction?: number | null;
}

export interface PlanDeviationData {
  first_record: PlanDeviation;
  second_record: PlanDeviation;
  growth_percentage: number;
}

