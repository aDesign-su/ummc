// export interface MasteringCause {
//     id?: number;
//     date?: string;
//     financing_object?: string;
//     plan: number;
//     prediction: number;
//     deviation: number;
//     cause?: string;
// }
// export interface SummaryWrapper {
//     summary: Summary;
// }
// export interface Summary {
//     plan: number;
//     prediction: number;
//     deviation: number;
// }

export interface FinancingDetail {
    id: number;
    date: string;
    financing_object: number;
    plan: number;
    prediction: number;
    deviation: number;
    cause?: string;
    summary?:string;
    code_wbs:string;

}

export interface MasteringCause {
    id: number;
    name: string;
    level: number;
    financing?: FinancingDetail | null;
    code_wbs:string;
}