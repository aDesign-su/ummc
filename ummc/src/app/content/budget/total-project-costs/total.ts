export interface Total {
    expandable: boolean;
    name: string;
    level: number;
}
export interface getMathingBudget {
    id: number,
    common_budget_id: number,
    expandable: boolean,
    name: string,
    title: string,
    code_wbs: string,
    parent_id: number,
    total_budget: number,
    smr_budget: number,
    pir_budget: number,
    oto_budget: number,
    date_end: string,
    date_start: string,
    other_budget: number,
    tree_id: number,
    level: number,
    editing?: any,
    children?: getMathingBudget[];
}
export interface getTotal {
    id: number,
    common_budget_id: number,
    expandable: boolean,
    name: string,
    code_wbs: string,
    parent_id: number,
    total_price: number,
    smr_price: number,
    pir_price: number,
    oto_price: number,
    date_end: string,
    date_start: string,
    other_price: number,
    tree_id: number,
    level: number,
    editing?: any,
    children?: getTotal[];
}
export interface getRegisterBudget {
    id: number,
    is_current: boolean,
}
