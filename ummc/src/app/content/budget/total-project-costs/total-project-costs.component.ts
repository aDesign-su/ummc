import { Component, Inject, OnInit, TemplateRef, inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ObjectBudget } from '../project-budget/registerBudget';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { getRegisterBudget, getTotal, Total } from './total';
import * as jmespath from 'jmespath';
import * as moment from 'moment';
import { EMPTY, switchMap } from 'rxjs';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { dateRangeValidatorCommon } from 'src/app/helper/dateFilter.directive';
import { NavigationStart, Router } from '@angular/router';
import { CommonService } from '../project-budget/common.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { DialogModule } from 'primeng/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WbsService } from '../intangible/wbs.service';

declare var bootstrap: any;

@Component({
  selector: 'app-total-project-costs',
  templateUrl: './total-project-costs.component.html',
  styleUrls: ['./total-project-costs.component.css']
})
export class TotalProjectCostsComponent implements OnInit {
  constructor(
    public theme: ThemeService, 
    public show: ShowService, 
    public isTitle: CommonService,
    public wbsServer:WbsService, 
    private dialog: MatDialog, 
    private total: ApicontentService, 
    private router: Router, 
    private formBuilder: FormBuilder, 
    private _adapter: DateAdapter<any>, 
    @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.createCommonCosts = this.formBuilder.group({
      comment: [''],
      // other_price: ['', [Validators.required]], 
      // oto_price: ['', [Validators.required]], 
      // pir_price: ['', [Validators.required]], 
      // smr_price: ['', [Validators.required]], 
      // total_price: ['', [Validators.required]], 
      date_start: [null, [Validators.required]],
      date_end: [null, [Validators.required]],
      // budget_source: ['', [Validators.required]], 
      register_budget: ['', [Validators.required]],
      wbs: [null, [Validators.required]],
      user_created: [1]
    }, { validator: dateRangeValidatorCommon });
    this.editCommonCosts = this.formBuilder.group({
      // comment: ['', [Validators.required]], 
      other_price: ['', [Validators.required]],
      oto_price: ['', [Validators.required]],
      pir_price: ['', [Validators.required]],
      smr_price: ['', [Validators.required]],
      auto: false,
      // total_price: ['', [Validators.required]], 
      // date_start: ['', [Validators.required]], 
      // date_end: ['', [Validators.required]], 
      // budget_source: ['', [Validators.required]], 
      // register_budget: ['', [Validators.required]], 
      // wbs: [],
      user_created: [1]
    });
    this.createSystem = this.formBuilder.group({
      name: [''],
      code_wbs: [null],
      date_start: [''],
      date_end: [''],
      category: [''],
      parent: [],
      // name_package: ['0'],
      user_created: [1],
    });
    this.select1 = this.formBuilder.group({
      level: [0],
    });
    this.select2 = this.formBuilder.group({
      parent_id: [this.showId],
    });
    this.select3 = this.formBuilder.group({
      parent_id: [this.showId],
    });
    this.LinkForm = this.formBuilder.group({
      id_common_budget: [],
      common_id: [],
      rate: new FormControl(100, [
        Validators.required,
        Validators.min(0),
        Validators.max(100)
      ]),
    })
  }
  LinkForm!: FormGroup
  createSystem: FormGroup
  select1: FormGroup
  select2: FormGroup
  select3: FormGroup
  createCommonCosts: FormGroup
  editCommonCosts: FormGroup
  isLoadingListLink: boolean
  getCategory: any[] = []
  categoryList!: string;
  visibleeditRate: boolean = false;
  ngOnInit() {
    this.getIsCurrent()

    this.total.getSourceBudget().subscribe(
      (response: any[]) => {
        this.getSource = response
      },
    ); 
    this.total.getRegisterBudget().subscribe(
      (response: any[]) => {
        const row = "@.data[]";
        const data = jmespath.search(response, row);
        // console.log('get', data)
        this.getBudget = data
      },
    ); 
    this.total.getCategory().subscribe(
      (data: any) => {
        console.log(data)
        this.getCategory = data[0]['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
    // getSvzWbsByLevelFilter()
  }

  addBudget4() {
    const budget = this.createSystem.value;

    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.total.addTypicalObjectFilter(budget).subscribe(
      (response: any[]) => {
        this.show.showBlock[11] = false
        this.show.showBlock[10] = true
        this.getSelect3()
        this.theme.access = 'Пакет работ добавлен успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);

        // this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'дд-мм-гггг';
    } else if (this._locale === 'fr') {
      return 'дд-мм-гггг';
    }
    return '';
  }
  budgetId: number
  setIdWbs: number
  setIsCurrent: number
  CurrentBudgetObjectsData: any[] = [];
  getIsCurrent() {
    
    this.total.getRegisterBudget().pipe(
      switchMap((data: getRegisterBudget[]) => {
        const row = "@.data[]";
        const response = jmespath.search(data, row);
        console.log('1',response)
        const currentObject = response.find(item => item.is_current);
        console.log('2',currentObject)
        console.log('3',data)
        if (currentObject) {
          this.setIsCurrent = currentObject.id;
          console.log('ID текущего объекта:', this.setIsCurrent);
          return this.total.getGeneralProjectBudget(this.setIsCurrent);
        } else {
          console.log('Текущий объект не найден');
          return EMPTY;
        }
      })
    ).subscribe((data: getTotal[]) => {
      const row = "@.data[]";
      const response = jmespath.search(data, row);
      this.dataSources.data = response;
      console.log('4', this.dataSources.data)
    });
  }

  getBudget: any[] = []
  budgetList: string;

  getSource: any[] = []
  sourceList: any = 2;

  getWbsLevel1: any[] = []
  wbsList1!: string;
  getWbsLevel2: any[] = []
  wbsList2!: string;
  getWbsLevel3: any[] = []
  wbsList3!: string;

  getSelect1() {
    const filter = this.select1.value;
    this.total.addFilterNodeFlat(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect2() {
    const filter = this.select2.value;
    this.total.addFilterNodeFlat(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel2 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect3() {
    const filter = this.select3.value;
    this.total.addFilterNodeFlat(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel3 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }
  items: any = ''
  addCommonCosts() {
    const budget = this.createCommonCosts.value;

    // Format the date as YYYY-MM-DD
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.total.createGeneralProjectCosts(budget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        // this.createCommonObjectBudgetForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getIsCurrent();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  editCommonCostsDirectory(element: getTotal) {
    this.budgetId = element.common_budget_id; // Установим значение свойства element
    this.editCommonCosts.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.common_budget_id)
  }
  saveCommonCostsDirectory() {
    if (this.budgetId) {
      console.log(this.editCommonCosts.value)
      let editedObject = this.editCommonCosts.value;
      this.total.updGeneralProjectBudget(this.budgetId, editedObject).subscribe(
        (data: any) => {
          this.show.showBlock[4] = false; // Закроем все блоки, устанавливая значение в false
          // this.element.editing = false

          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getIsCurrent();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  showId: number
  delCommonCosts(element: any) {
    const delElement = element
    console.log(delElement)
    this.total.delGeneralProjectBudget(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getIsCurrent();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

  getId2(element: any) {
    this.showId = element
    this.select2.patchValue({
      parent_id: this.showId,
    });
    // console.log(this.showId)
  }
  getId3(element: any) {
    this.showId = element
    this.select3.patchValue({
      parent_id: this.showId,
    });
    this.createSystem.patchValue({
      parent: this.showId,
    });
    console.log(this.showId)
  }
  //  showDetails(element: any) {
  //   const id = element
  //   if(id) {
  //     console.log(id);
  //     this.total.getGeneralProjectCostsId(id).subscribe(
  //       (data: any[]) => {
  //         this.items = data
  //         console.log(data);
  //       }
  //     );
  //   }
  // }

  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price', 'actions'];
  dataSource = new MatTableDataSource<getTotal>([]);
  dataLinkBudgetList: MatTableDataSource<any> = new MatTableDataSource<any>();

  private _transformer = (node: getTotal, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      code_wbs: node.code_wbs,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      date_end: node.date_end,
      date_start: node.date_start,
      other_price: node.other_price,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;

  getCurrentBudgetObjects() {
    this.total.getRegisterBudget().subscribe(
      (data) => {
        console.log(data['data'])
        const budgets = data['data']
        const currentBudget = budgets.find(budget => budget.is_current === true);
        if (currentBudget) {
          this.total.getCommonBudget(currentBudget.id, { "level": 2 }).subscribe(
            (data) => {
              this.CurrentBudgetObjectsData = data['data'];
              console.log('Объекты текущего бюджета', this.CurrentBudgetObjectsData)

            },
            (error: any) => {
              console.error('Error fetching data:', error);
            });
        } else {
          console.log('Текущий бюджет не найден');
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  //  Связать 
  linkCommonCosts(element) {
    console.log(element)
    this.getCurrentBudgetObjects()
    this.getLinkBudgetList(element.common_budget_id)//Получаем список связанных
    this.LinkForm.reset()
    // Заполняем форму редактирования данными из элемента
    this.LinkForm.patchValue({
      id_common_budget: element.common_budget_id,
      rate: 100,
    });
    console.log(this.LinkForm.value)
  }

  LinkCommonCostsDirectory(FormData) {
    if (this.LinkForm.valid) {
      this.total.LinkCommonCosts(this.LinkForm.value['id_common_budget'], this.LinkForm.value).subscribe(
        (data) => {
          console.log('Связано с ', data)
          const offcanvasElement = document.getElementById('linkCommonCosts');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.theme.openSnackBar('Добавлена связь с ' + data.common_object);
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        });
    }
  }
  getLinkBudgetList(id_budget) {
    this.isLoadingListLink = true
    this.total.getLinkBudgetList(id_budget).subscribe(
      (data) => {
        this.isLoadingListLink = false

        // Связанные
        console.log('Связанные', data)
        this.dataLinkBudgetList.data = data
        console.log('Связанные_t', data)

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  delElementLinkBudgetList(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.total.delElementLinkBudgetList(id).subscribe(
          (data: any) => {
            this.theme.openSnackBar('Связь удалена');
            const index = this.dataLinkBudgetList.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataLinkBudgetList.data.splice(index, 1);
              this.dataLinkBudgetList._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }
  openDialog(element) {
    const dialogRef = this.dialog.open(DialogEditRateDialog, {
      data: { element: element }
    });


    dialogRef.afterClosed().subscribe(response => {
      console.log(response)
      if (response) {
        this.getLinkBudgetList(this.LinkForm.value.id_common_budget);
      }
    });
  }
  editRateLinkBudgetList(element) {
    console.log(element)
    this.openDialog(element)
  }

}

@Component({
  selector: 'edit-rate-dialog',
  templateUrl: 'edit-rate-dialog.html',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, FormsModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule, MatInputModule,],
})
export class DialogEditRateDialog {
  editRateForm!: FormGroup

  constructor(
    private dialogRef: MatDialogRef<DialogEditRateDialog>,
    public theme: ThemeService,
    public show: ShowService,
    public isTitle: CommonService,
    private dialog: MatDialog,
    private total: ApicontentService,
    private router: Router,
    private formBuilder: FormBuilder,
    private _adapter: DateAdapter<any>,
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.editRateForm = this.formBuilder.group({
      id: data.element.id,
      rate: new FormControl(data.element.rate, [
        Validators.required,
        Validators.min(0),
        Validators.max(100)
      ]),
    })
    console.log(this.editRateForm.value);

  }
  updateRate(data) {

    console.log(data)
    this.total.updateRateLinkBudgetList(data.id, data).subscribe(
      (response) => {
        this.theme.openSnackBar('Изменения сохранены')
        this.dialogRef.close(response);
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

}
