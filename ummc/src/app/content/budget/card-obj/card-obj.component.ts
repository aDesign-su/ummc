import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { FormBuilder, FormControl, FormGroup, } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { cardObj } from './card-obj';
import * as jmespath from 'jmespath';
import { CardObjService } from './card-obj.service';
import { Router } from '@angular/router';
import { ShowService } from 'src/app/helper/show.service';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import moment from 'moment';
import { MatSort } from '@angular/material/sort';
import { CommonService } from '../project-budget/common.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-card-obj',
  templateUrl: './card-obj.component.html',
  styleUrls: ['./card-obj.component.css'],
})

export class CardObjComponent implements OnInit {
  constructor( public show:ShowService, public theme: ThemeService, public isTitle: CommonService, private formBuilder: FormBuilder, private fb: FormBuilder, public cardObjService: ApicontentService,private card:CardObjService,private router: Router, private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.createPackage = this.formBuilder.group({
      start_date: [''], 
      end_date: [''], 
      node_parent: [], 
      category: [], 
      code_sap: [''], 
      code_assoi: [''],
      code_wbs: [''],
      name: [''],
      user_created: [1],
    });
    this.toWbs = this.formBuilder.group({
      level: [1], 
    });
    this.searchForm = this.formBuilder.group({
      searchText: ''
    });
   }
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
  searchForm!: FormGroup;
  createPackage: FormGroup
  
  getCategory: any[] = []
  categoryObservable: Observable<any[]>;
  categoryList: any[];

  getToWbs: any[] = []
  wbsList: string;

  setId: number
  toWbs!: FormGroup

  filteredContracts: any[] = []
  selectedPriceFields: string;
  toppings = new FormControl('');
  
  displayedColumns: string[] = ['serial_number', 'code_wbs', 'name', 'date_start', 'date_end', 'total_price', 'actions'];
  dataSource = new MatTableDataSource<cardObj>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getCard()

    // Селект Get Category
    this.categoryObservable = this.cardObjService.getCategory();
    this.categoryObservable.subscribe(
      (data: any[]) => {
        // Предполагается, что data[0].data содержит массив объектов категорий
        this.getCategory = data[0].data;
        console.log(this.getCategory);
      },
    );
  }


  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file, file.name);
      this.cardObjService.uploaFileObjectBudget(formData).subscribe(
        (data) => {
          this.theme.openSnackBar('Смета загружена');
          // this.getDataRegisterEstimatesGSFX()

        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  addToWbs() {
    const wbs = this.toWbs.value;
    // console.log(wbs)
    this.cardObjService.addFilter(wbs).subscribe(
      (response: any) => {
        this.getToWbs = response;
        console.log(response)
      },
    );
  }

  addCreatePackage() {
    const wbs = this.createPackage.value;

    // Format the date as YYYY-MM-DD
      const startdDate = moment(wbs.start_date).format('YYYY-MM-DD');
      const endDate = moment(wbs.end_date).format('YYYY-MM-DD');
      wbs.start_date = startdDate;
      wbs.end_date = endDate;

    this.cardObjService.addCreateObj(wbs).subscribe(
      (response: any) => {
        // this.getToWbs = response;
          this.show.showBlock[0] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getCard()
        console.log(response)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  getCard() {
    this.cardObjService.getData().subscribe(
      (data: cardObj[]) => {
        const query = "@.data[]";
        this.dataSource.data = jmespath.search(data, query);
        this.filteredContracts = this.dataSource.data;
        this.dataSource.paginator = this.paginator;
        // console.log(this.filteredContracts);
      }
    )
  }

  test(event: any) {
  // Устанавливаем фильтр для dataSource
  this.dataSource.filterPredicate = (data:any, filter) => {
    const showAll = filter === 'all';
    const isCurrent = data.is_current === true;
    const isBase = data.is_base === true;

    return (showAll || (filter === 'true' && isCurrent) || (filter === 'false' && isBase));
  };

  // Применяем фильтр
  this.dataSource.filter = event;
  }
  matchesSearchText(contract: any): boolean {
    return (
      (contract.is_current?.toString().toLowerCase() ?? '') ||
      (contract.amount_with_VAT?.toString().toLowerCase() ?? '')
    );
  }

  applyFilters(event: any): void {
    const filterValue = event.target.value.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  showDetails(id:number) {
    this.setId = id
    if(id) {
      this.router.navigate(['registry-obj/more']);
      this.card.setSetId(this.setId);
    }
  }
}

