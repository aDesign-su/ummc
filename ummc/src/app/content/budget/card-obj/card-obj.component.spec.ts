/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CardObjComponent } from './card-obj.component';

describe('CardObjComponent', () => {
  let component: CardObjComponent;
  let fixture: ComponentFixture<CardObjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardObjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardObjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
