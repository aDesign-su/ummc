/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CardObjService } from './card-obj.service';

describe('Service: CardObj', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardObjService]
    });
  });

  it('should ...', inject([CardObjService], (service: CardObjService) => {
    expect(service).toBeTruthy();
  }));
});
