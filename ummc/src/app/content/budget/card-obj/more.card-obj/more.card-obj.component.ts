import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { CardObjService } from '../card-obj.service';
import { ThemeService } from 'src/app/theme.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { cardObj, currentObj, differenceObj, inObjectTree } from '../card-obj';
import { ApicontentService } from 'src/app/api.content.service';
import * as jmespath from 'jmespath';
import { ShowService } from 'src/app/helper/show.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { ObjectBudget, ObjectBudgetNode } from '../../project-budget/registerBudget';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { NodeObject } from '../../svz-documents/svz/svz';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];

@Component({
  selector: 'app-more-card-obj',
  templateUrl: './more.card-obj.component.html',
  styleUrls: ['./more.card-obj.component.css']
})
export class MoreCardObjComponent implements OnInit {
  constructor(public cardObjService: ApicontentService,private card:CardObjService,public show:ShowService,public theme: ThemeService,private formBuilder: FormBuilder,private _adapter: DateAdapter<any>,@Inject(MAT_DATE_LOCALE) private _locale: string) { 
    this.card.currentSetId.subscribe(id => {
      this.setId = id;
      console.log(this.setId)
    });
    this.createBudget1 = this.formBuilder.group({
      date_start: [''], 
      date_end: [''], 
      total_price: [0], 
      smr_price: [null], 
      pir_price: [null],
      oto_price: [null],
      other_price: [null],
      budget_source: [''],
      register_budget: [null],
      user_created: [1],
      wbs: [''],
      auto: false
    });
    this.createBudget2 = this.formBuilder.group({
      date_start: [''], 
      date_end: [''], 
      total_price: [0], 
      smr_price: [null], 
      pir_price: [null],
      oto_price: [null],
      other_price: [null],
      budget_source: [''],
      register_budget: [null],
      user_created: [1],
      wbs: [''],
      auto: false
    });
    this.createBaseForm = this.formBuilder.group({
      date_start: [''], 
      date_end: [''], 
      comment: [''], 
      total_price: [0], 
      smr_price: [null], 
      pir_price: [null],
      oto_price: [null],
      other_price: [null],
      problem: [''],
      register_budget: [null],
      user_created: [1],
      wbs: [''],
      auto: false
    });
    this.createIscurrentForm = this.formBuilder.group({
      date_start: [''], 
      date_end: [''], 
      comment: [''], 
      total_price: [0], 
      smr_price: [null], 
      pir_price: [null],
      oto_price: [null],
      other_price: [null],
      problem: [''],
      budget_source: [''],
      register_budget: [null],
      user_created: [1],
      wbs: [''],
      auto: false
    });
    this.createSystem = this.formBuilder.group({
      name: [''], 
      code_wbs: [null], 
      date_start: [''], 
      date_end: [''], 
      category: [''],
      parent: [this.setId],
      // name_package: ['0'],
      user_created: [1],
    });
    this.select1 = this.formBuilder.group({
      parent_id: [''], 
    });
    this.select2 = this.formBuilder.group({
      parent_id: [''], 
    });
    this.editBudget = this.formBuilder.group({
      date_start: [''], 
      date_end: [''], 
      // comment: [''], 
      total_price: [0], 
      smr_price: [], 
      pir_price: [],
      oto_price: [],
      other_price: [],
      register_budget: [],
      budget_source: [''],
      problem: [''],
      user_created: [1],
      auto: false
    });
  }
  createSystem: FormGroup
  editBudget: FormGroup
  createCommonObjectBudgetForm: FormGroup
  createBaseForm: FormGroup
  createIscurrentForm: FormGroup
  createBudget1: FormGroup
  createBudget2: FormGroup
  select1: FormGroup
  select2: FormGroup

  displayedColumnsObject: string[] = ['code_wbs', 'code_assoi', 'code_sap', 'name', 'category', 'problem'];
  displayedColumnsInfo: string[] = ['comment', 'code_wbs', 'code_assoi', 'category', 'name', 'code_sap', 'problem', 'actions'];
  displayedColumnsDate: string[] = ['date_start', 'date_end', 'current_date_start', 'current_date_end', 'actions'];
  displayedColumns: string[] = ['serial_number', 'code_wbs', 'code_assoi', 'name', 'date_start', 'date_end', 'problem'];
  dataSourceObject = new MatTableDataSource<cardObj>([]);
  dataSourceBudget = new MatTableDataSource<cardObj>([]);
  dataSourceDate = new MatTableDataSource<cardObj>([]);
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('dataSourceBudget2') paginator2: MatPaginator;
  @ViewChild(MatPaginator) page: MatPaginator;
  ngAfterViewInit() {
    this.dataSourceBudget.paginator = this.paginator2;
    this.dataSource.paginator = this.paginator;
    this.getHistoryObject.paginator = this.page;
  }
  
  displayedColumnsTime: string[] = ['name', 'base_date_start', 'base_date_end', 'current_date_start', 'current_date_end', 'actions'];
  displayedColumnsObj: string[] = ['name', 'budget_source_name', 'current_total_price', 'current_pir_price', 'current_smr_price', 'current_oto_price', 'current_other_price', 'actions'];
  dataSource = new MatTableDataSource<inObjectTree>([]);

  private _transformer = (node: inObjectTree, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      current_id: node.current_id,
      name: node.name,
      is_current: node.is_current,
      is_base: node.is_base,
      code_wbs: node.code_wbs,
      budget_source: node.budget_source,
      current_total_price: node.current_total_price,
      current_smr_price: node.current_smr_price,
      current_pir_price: node.current_pir_price,
      current_oto_price: node.current_oto_price,
      current_other_price: node.current_other_price,
      base_smr_price: node.base_smr_price,
      base_pir_price: node.base_pir_price,
      base_oto_price: node.base_oto_price,
      base_other_price: node.base_other_price,
      base_total_price: node.base_total_price,
      base_date_start: node.base_date_start,
      base_date_end: node.base_date_end,
      current_date_start: node.current_date_start,
      current_date_end: node.current_date_end,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<ObjectBudgetNode>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: ObjectBudgetNode) => node.expandable;

    // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  
  ngOnInit() {
    this.getShowDetails()
    this.getIsCurrent()
    this.getIsBase()
    this.getHistoryObj()

    this.cardObjService.getSourceBudget().subscribe(
      (response: any[]) => {
        this.getSource = response
      },
    );

    this.cardObjService.getCategory().subscribe(
      (data: any) => {
        console.log(data)
        this.getCategory = data[0]['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  getCategory: any[] = []
  categoryList!: string;

  getSource: any[] = []
  sourceList!: string;

  getWbsLevel1: any[] = []
  wbsList1!: string;

  isLevel: number
  setId: number
  projectName: any
  objName: any
  directName: any

  displayedCol: string[] = ['serial_number', 'user', 'date', 'type', 'pir', 'smr', 'oto', 'other'];
  getHistoryObject = new MatTableDataSource<History>([]);

  getHistoryObj() {
    this.cardObjService.getHistoryObjectBudget(this.setId).subscribe(
      (data: any[]) => {
        this.getHistoryObject.data = data
      }
    )
  }

  getSourceNameById(id: number): string {
    const source = this.getSource.find(item => item.id === id);
    return source ? source.name : '';
  }

  showId: number
  getId1(element: any) {
    this.showId = element
    this.select1.patchValue({
      parent_id: this.showId,
    });
    console.log(this.showId)
   }
   getId2(element: any) {
    this.showId = element
    this.select2.patchValue({
      parent_id: this.showId,
    });
    console.log(this.showId)
   }

  getSelect1() {
    const filter = this.select1.value;
    this.cardObjService.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }
  getSelect2() {
    const filter = this.select2.value;
    this.cardObjService.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getShowDetails() {
    this.cardObjService.getDataMore(this.setId).subscribe(
      (data: any[]) => {
        const getDataProject = "@.data_project.{name:name, code_wbs:code_wbs}";
        this.projectName = jmespath.search(data, getDataProject);

        const getDataObject = "@.data_object.{name:name}";
        this.objName = jmespath.search(data, getDataObject);

        const getDataDirection = "@.data_direction.{name:name, code_wbs:code_wbs}";
        this.directName = jmespath.search(data, getDataDirection);

        const getDataTable1 = "@.data_object";
        const object = jmespath.search(data, getDataTable1);

        const getDataTable2 = "@.budgets";
        const budget = jmespath.search(data, getDataTable2);

        const getDataTable3 = "@.in_object_tree";
        const inObjectTree = jmespath.search(data, getDataTable3);

        const getDataTable4 = "@.diff_budgets";
        const diff = jmespath.search(data, getDataTable4);
        
        this.dataSourceObject.data = [object]
        this.dataSourceBudget.data = diff
        this.dataSourceDate.data = [budget]
        this.dataSources.data = inObjectTree
      }
    )
  }

  isBase: any
  getIsBase() {
    this.cardObjService.getRegisterBudget().subscribe(
      (data: any[]) => {
        const getDataProject = "@.data[?is_base].{is_base:is_base,id:id}";
        this.isBase = jmespath.search(data, getDataProject);
        this.isBase = this.isBase.length > 0 ? this.isBase[0] : null;
        console.log('get',this.isBase.id)
      }
    )
  }

  isCurrent: any
  getIsCurrent() {
    this.cardObjService.getRegisterBudget().subscribe(
      (data: any[]) => {
        const getDataProject = "@.data[?is_current].{is_current:is_current,id:id}";
        this.isCurrent = jmespath.search(data, getDataProject);
        this.isCurrent = this.isCurrent.length > 0 ? this.isCurrent[0] : null;
        console.log('set', this.isCurrent.id)
      }
    )
  }

  Budget1() {
    const commonBudget = this.createBudget1.value;

      // Format the date as YYYY-MM-DD
      const startdDate = moment(commonBudget.date_start).format('YYYY-MM-DD');
      const endDate = moment(commonBudget.date_end).format('YYYY-MM-DD');
      commonBudget.date_start = startdDate;
      commonBudget.date_end = endDate;

    this.cardObjService.addCommonObjectBudget(commonBudget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getShowDetails();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  Budget2() {
    const commonBudget = this.createBudget2.value;

      // Format the date as YYYY-MM-DD
      const startdDate = moment(commonBudget.date_start).format('YYYY-MM-DD');
      const endDate = moment(commonBudget.date_end).format('YYYY-MM-DD');
      commonBudget.date_start = startdDate;
      commonBudget.date_end = endDate;

    this.cardObjService.addCommonObjectBudget(commonBudget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getShowDetails();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addBudget1() {
    const budget = this.createBaseForm.value;
    
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.cardObjService.addCommonObjectBudget(budget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addBudget2() {
    const budget = this.createIscurrentForm.value;
    
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.cardObjService.addCommonObjectBudget(budget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addBudget3() {
    const budget = this.createSystem.value;
    
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.cardObjService.addSystemFilter(budget).subscribe(
      (response: any[]) => {
        this.show.showBlock[5] = false
        this.show.showBlock[1] = true
        this.getSelect1()
        this.theme.access = 'Система добавлена успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        
        // this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addBudget4() {
    const budget = this.createSystem.value;
    
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.cardObjService.addSystemFilter(budget).subscribe(
      (response: any[]) => {
        this.show.showBlock[7] = false
        this.show.showBlock[6] = true
        this.getSelect2()
        this.theme.access = 'Пакет работ добавлен успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        
        // this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  element: any
  elemented: any

  editBudget1(element:currentObj) {
    this.elemented = element.current.current_id; 
    this.editBudget.patchValue({
      date_start: element.current.date_start,
      date_end: element.current.date_end,
      total_price: element.current.total_price,
      smr_price: element.current.smr_price,
      pir_price: element.current.pir_price,
      oto_price: element.current.oto_price,
      other_price: element.current.other_price,
      budget_source: element.current.budget_source,
      problem: '',
    });
    console.log('get1',element)
  }

  editBudget2(element:inObjectTree) {
    this.element = element.current_id; 
    this.editBudget.patchValue({
      date_start: element.current_date_start,
      date_end: element.current_date_end,
      comment: element.work_package_code,
      total_price: element.current_total_price,
      smr_price: element.current_smr_price,
      pir_price: element.current_pir_price,
      oto_price: element.current_oto_price,
      other_price: element.current_other_price,
      budget_source: element.budget_source,
      problem: '',
    });
    console.log('set2',element)
  }

  editBudget3(element:differenceObj) {
    console.log('get3',element)
    this.element = element.current_id; 
    this.editBudget.patchValue({
      date_start: element.date_start,
      date_end: element.date_end,
      total_price: element.total_price,
      smr_price: element.smr_price,
      pir_price: element.pir_price,
      oto_price: element.oto_price,
      other_price: element.other_price,
      budget_source: element.budget_source,
      problem: '',
    });
    
  }

  saveBudget() {
    if (this.element) {
      let editedBudget = this.editBudget.value;

      const startdDate = moment(editedBudget.date_start).format('YYYY-MM-DD');
      const endDate = moment(editedBudget.date_end).format('YYYY-MM-DD');
      editedBudget.date_start = startdDate;
      editedBudget.date_end = endDate;

      editedBudget.total_price = parseFloat(editedBudget.total_price);
      editedBudget.smr_price = parseFloat(editedBudget.smr_price);
      editedBudget.pir_price = parseFloat(editedBudget.pir_price);
      editedBudget.oto_price = parseFloat(editedBudget.oto_price);
      editedBudget.other_price = parseFloat(editedBudget.other_price);

      console.log('set',editedBudget)
      this.cardObjService.updateCommonObjectBudget(this.element, editedBudget).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getShowDetails();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
}
