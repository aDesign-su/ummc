import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoreCardObjComponent } from './more.card-obj.component';
import { TableModule } from 'primeng/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    MatTabsModule,
    MatCardModule,
    MatIconModule
  ],
  declarations: [
    // MoreCardObjComponent
  ],
  // exports: [MoreCardObjComponent]
})
export class MoreCardObjModule { }
