import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardObjService {
  public url: string = '';
  
  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  constructor() {
    const objectId = this.getSetIdFromLocalStorage();
    if (objectId !== null) {
      this.setIdSource.next(objectId);
    }
  }
  private getSetIdFromLocalStorage(): number | null {
    const objectId = localStorage.getItem(this.setIdKey);
    return objectId ? +objectId : null;
  }
  setSetId(id: number) {
    localStorage.setItem(this.setIdKey, id.toString());
    this.setIdSource.next(id);
  }
  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }
}
