export interface cardObj {
    id: number;
    wbs_id: number;
    budget_source_id: number | null;
    register_budget_id: number;
    user_created_id: number;
    date_start: string; 
    date_end: string; 
    comment: string;
    total_price: number;
    smr_price: number;
    pir_price: number;
    oto_price: number;
    other_price: number;
    problem: string;
    delete_flg: boolean;
    editing?: boolean;
}
export interface differenceObj {
    id: number,
    date_start: string,
    date_end: string,
    total_price: number,
    smr_price: number,
    pir_price: number,
    oto_price: number,
    other_price: number,
    title_cost: string,
    budget_source: string,
    budget_source_name: string,
    current_id: number
}
export interface currentObj { 
    current:
        {
          id: number,
          wbs: number,
          budget_source: number,
          register_budget: number,
          user_created: number,
          date_start: string,
          date_end: string,
          comment: string,
          total_price: number,
          smr_price: number,
          pir_price: number,
          oto_price: number,
          other_price: number,
          problem: string,
          delete_flg: boolean,
          auto: boolean,
          is_base: boolean,
          is_current: boolean,
          base_id: string,
          current_id: number,
          status_budget: string,
          budget_source_name: string,
          category: string,
          title_cost: string
        }
}
export interface inObjectTree {
        id: number,
        current_id: number,
        expandable: boolean,
        code_wbs: number,
        budget_source: number,
        register_budget: number,
        user_created: number,
        comment: string,
        problem: string,
        is_base: boolean,
        is_current: boolean,
        parent_id: number,
        level: number,
        name: string,
        wbs: string,
        category: string,
        base_date_start: string,
        base_date_end: string,
        base_total_price: string,
        base_smr_price: string,
        base_pir_price: string,
        base_oto_price: string,
        base_other_price: string,
        current_date_start: string,
        current_date_end: string,
        current_total_price: number,
        current_smr_price: number,
        current_pir_price: number,
        current_oto_price: number,
        current_other_price: number,
        work_package_code: string,
        children?: inObjectTree[];
}
