export interface RiskItemBase {
    id: number;
    WBS_code: string;
    name: string;
    start_plan: string | null;
    ending_plan: string | null;
}

export interface RiskPlan extends RiskItemBase {
    planned_cost: string | number;
    risk_description: string | null;
    estimated_cost: string | number;
    source: string | null;
    plan_deviation: string | number;
}

export interface RiskActual extends RiskItemBase {
    contract_price: string | null;
    contract_number: string | null;
    contract_date: string | null;
    contract_name: string | null;
    payments: string | null;
    acts: string | null;
}