import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { RiskActual, RiskPlan } from './Risk';
import { Observable, concatAll, map, startWith } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonService } from '../project-budget/common.service';
declare var bootstrap: any;


@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.css']
})
export class RiskComponent implements OnInit {

  // Инициализация MatTableDataSource
  dataRiskPlanTableSource = this.initTable<RiskPlan>();
  dataRiskActualTableSource = this.initTable<RiskActual>();
  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }


  displayedColumnsRiskPlan: string[] = ['id', 'WBS_code', 'name', 'planned_cost', 'start_plan', 'ending_plan', 'risk_description', 'estimated_cost', 'source', 'plan_deviation', 'action'];
  displayedColumnsRiskActual: string[] = ['id', 'WBS_code', 'name', 'contract_price', 'start_plan', 'ending_plan', 'contract_number', 'contract_date', 'contract_name', 'payments', 'acts', 'action'];
  @ViewChild('PaginatorPlan') paginatorForRiskPlan: MatPaginator;
  @ViewChild('PaginatorAnalog') paginatorForRiskActual: MatPaginator;

  FormAddEditRiskPlan: FormGroup;
  FormEditRiskAnalog: FormGroup;

  selectedRisk: number;
  editMode = false;

  myControl = new FormControl();
  options: string[] = ['Договор', 'Расчёт'];
  filteredOptions: Observable<string[]>;

  constructor(public theme: ThemeService, public params: ApicontentService, public isTitle: CommonService, private fb: FormBuilder, private datePipe: DatePipe,) {

    this.FormAddEditRiskPlan = this.fb.group({
      name: ['', Validators.required],
      planned_cost: [null, Validators.required],
      start_plan: [null, Validators.required],
      ending_plan: [null, Validators.required],
      risk_description: [''],
      estimated_cost: [null, Validators.required],
      source: [''],
      plan_deviation: [null, Validators.required]
    });

    this.FormEditRiskAnalog = this.fb.group({
      name: ['', Validators.required],
      contract_price: [null, Validators.required],
      start_plan: [null, Validators.required],
      ending_plan: [null, Validators.required],
      contract_number: ['', Validators.required],
      contract_date: [null, Validators.required],
      contract_name: ['', Validators.required],
      payments: [null, Validators.required],
      acts: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getDatRisk()

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }
  ngAfterViewInit() {
    this.dataRiskPlanTableSource.paginator = this.paginatorForRiskPlan;
    this.dataRiskActualTableSource.paginator = this.paginatorForRiskActual;
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  add() {
    this.editMode = false
    this.FormAddEditRiskPlan.reset()
  }

  // Полуить данные Риски
  getDatRisk(): void {
    this.params.getDataRiskPlan().subscribe(
      (data: RiskPlan[]) => {
        this.dataRiskPlanTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

    this.params.getDataRiskActual().subscribe(
      (data: RiskActual[]) => {
        this.dataRiskActualTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  editRiskPlan(element: RiskPlan): void {
    this.editMode = true;
    this.selectedRisk = element.id;
    this.FormAddEditRiskPlan.setValue({
      name: element.name,
      planned_cost: element.planned_cost,
      start_plan: element.start_plan ? new Date(element.start_plan) : null,
      ending_plan: element.ending_plan ? new Date(element.ending_plan) : null,
      risk_description: element.risk_description,
      estimated_cost: element.estimated_cost,
      source: element.source,
      plan_deviation: element.plan_deviation
    });

  }

  private transformDate(date: any): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  createOrUpdateRiskPlan(): void {
    if (this.FormAddEditRiskPlan.valid) {
      const formData = { ...this.FormAddEditRiskPlan.value };
      if (formData.start_plan) {
        formData.start_plan = this.transformDate(formData.start_plan);
      }
      if (formData.ending_plan) {
        formData.ending_plan = this.transformDate(formData.ending_plan);
      }
      if (this.editMode) {
        this.updateRiskPlan(this.selectedRisk, formData);
      } else {
        this.createRiskPlan(formData);
      }

    }
  }

  //Добавить План
  createRiskPlan(data): void {
    if (this.FormAddEditRiskPlan.valid) {
      const formData: RiskPlan = data
      console.log('Data to add:', formData);
      this.params.addRiskPlan(formData).subscribe(
        (data: RiskPlan[]) => {
          console.log(data)
          this.theme.openSnackBar(`Пакет работ добавлен`);

          this.getDatRisk()
          const offcanvasElement = document.getElementById('EditAddScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.editMode = false;
          this.selectedRisk = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  removeRisk(id) {
    this.params.delRisk(id).subscribe(
      (response) => {
        this.theme.openSnackBar(`Пакет работ удален`);
        // Удаляем элемент локально
        const index = this.dataRiskPlanTableSource.data.findIndex(item => item.id === id);
        if (index > -1) {
          this.dataRiskPlanTableSource.data.splice(index, 1);
          this.dataRiskPlanTableSource._updateChangeSubscription(); // обновляем таблицу
          this.dataRiskActualTableSource.data.splice(index, 1)
          this.dataRiskActualTableSource._updateChangeSubscription();
        }
      },
      (error: any) => {
        console.error('Failed to delete :', error);
      }
    );
  }
  updateRiskPlan(id: number, data: RiskPlan): void {
    if (this.FormAddEditRiskPlan.valid) {
      const formData: RiskPlan = data
      if (formData.start_plan) {
        formData.start_plan = this.datePipe.transform(formData.start_plan, 'yyyy-MM-dd');
      }
      if (formData.ending_plan) {
        formData.ending_plan = this.datePipe.transform(formData.ending_plan, 'yyyy-MM-dd');
      }
      this.params.updateRiskPlan(id, formData).subscribe(
        (data: any) => {
          console.log(data)
          this.getDatRisk()
          const offcanvasElement = document.getElementById('EditAddScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.theme.openSnackBar(`Сохранено`);
          this.editMode = false;
          this.selectedRisk = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  editRiskActual(element): void {

    this.selectedRisk = element.id;
    this.FormEditRiskAnalog.setValue({
      name: element.name,
      contract_price: element.contract_price,
      start_plan: element.start_plan ? new Date(element.start_plan) : null,
      ending_plan: element.ending_plan ? new Date(element.ending_plan) : null,
      contract_number: element.contract_number ? element.contract_number : null,
      contract_date: element.contract_date ? new Date(element.contract_date) : null,
      contract_name: element.contract_name ? element.contract_name : null,
      payments: element.payments ? element.payments : null,
      acts: element.acts ? element.acts : null,
    });
    this.FormEditRiskAnalog.get('contract_price').disable();
  }

  UpdateRiskActual() {
    const formData = { ...this.FormEditRiskAnalog.value };
    if (formData.start_plan) {
      formData.start_plan = this.transformDate(formData.start_plan);
    }
    if (formData.ending_plan) {
      formData.ending_plan = this.transformDate(formData.ending_plan);
    }
    if (formData.contract_date) {
      formData.contract_date = this.transformDate(formData.contract_date);
    }

    this.params.updateRiskActual(this.selectedRisk, formData).subscribe(
      (data: any) => {
        console.log(data)
        this.getDatRisk()
        const offcanvasElement = document.getElementById('editActualScrolling');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.theme.openSnackBar(`Сохранено`);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

}
