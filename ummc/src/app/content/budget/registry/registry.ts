export interface TableBudget {
  name: string,
  date_year: number,
  comment: string
}

export interface BudgetObject {
  id: number,
  user_created: number,
  name: string,
  is_base: boolean,
  is_current: boolean,
  is_active: boolean,
  date_year: number,
  comment: string
}
