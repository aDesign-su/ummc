import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BudgetObject, TableBudget } from './registry';
import { ApicontentService } from '../../../api.content.service'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {

  columnsName: string[] = ['serial_number', 'name', 'date_year', 'comment', 'actions'];
  dataSource = new MatTableDataSource<BudgetObject>([]);

  @ViewChild('paginator') paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  temporaryData: TableBudget[] = [{
    name: "asd", date_year: 2020, comment: "asd"
  }, {
    name: "asd", date_year: 2020, comment: "asd"
  }, {
    name: "asd", date_year: 2020, comment: "asd"
  }
  ];
  //TODO подставить в dataSource вместо временных данных
  budgets: BudgetObject[] = [];

  private isCreateFormVisible = false;
  private isEditFormVisible = false;

  constructor(
    public theme: ThemeService,
    private formBuilder: FormBuilder,
    private wbsService: ApicontentService,
    private _ngZone: NgZone
  ) {
    this.ObjectCreate = this.formBuilder.group({
      id: null,
      name: ["", [Validators.required, Validators.maxLength(256)]],
      is_base: false,
      is_current: false,
      is_active: false,
      date_year: [null, [Validators.required, Validators.max(2100), Validators.min(1980)]],
      comment: ["", [Validators.required, Validators.maxLength(1024)]],
      user_created: 36
    })
    this.ObjectEdit = this.formBuilder.group({
      id: null,
      name: ["", [Validators.required, Validators.maxLength(256)]],
      is_base: false,
      is_current: false,
      is_active: false,
      date_year: [null, [Validators.required, Validators.max(2100), Validators.min(1980)]],
      comment: ["", [Validators.required, Validators.maxLength(1024)]],
      user_created: 36
    })
    this.ObjectDelete = this.formBuilder.group({
      id: null
    })
  }

  ngOnInit() {
    this.getObjects();
  }

  ObjectCreate!: FormGroup;
  ObjectEdit!: FormGroup;
  ObjectDelete!: FormGroup;

  get _nameCreate() {
    return this.ObjectCreate.get('name')
  }

  get _yearCreate() {
    return this.ObjectCreate.get('date_year')
  }

  get _commentCreate() {
    return this.ObjectCreate.get('comment')
  }

  get _nameEdit() {
    return this.ObjectEdit.get('name')
  }

  get _yearEdit() {
    return this.ObjectEdit.get('date_year')
  }

  get _commentEdit() {
    return this.ObjectEdit.get('comment')
  }

  getObjects() {
    this.wbsService.getBudgetObjects().subscribe(
      (data: BudgetObject[]) => {
        this.budgets = data;
        console.log(this.budgets)

        this.dataSource = new MatTableDataSource<any>(this.budgets);
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  addObject() {
    let addedObject = this.ObjectCreate.value;
    console.log(addedObject)
    this.wbsService.addBudgetObject(addedObject).subscribe(
      (data: any) => {
        console.log('Budget added successfully:', data)
        this.getObjects();
        this.closeCreateForm();
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to add budget:', error)
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  editObject() {
    let editedObject = this.ObjectEdit.value;
    this.wbsService.editBudgetObject(editedObject).subscribe(
      (data: any) => {
        console.log('Budget edited successfully:', data)
        this.getObjects();
        this.closeEditForm();
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to edit budget:', error)
      }
    )
    console.log(editedObject)
  }

  deleteObject(id: number) {
    this.wbsService.deleteBudgetObject(id).subscribe(
      (data: any) => {
        console.log('Budget deleted successfully:', data)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getObjects();
      },
      (error: any) => {
        console.log('Failed to delete budget:', error)
      }
    )
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  openCreateForm() {
    this.isCreateFormVisible = !this.isCreateFormVisible;
    this.ObjectCreate.reset();
    this.ObjectCreate.patchValue({
      id: null,
      name: "",
      date_year: null,
      is_base: false,
      is_current: false,
      is_active: false,
      comment: "",
      user_created: 36
    })
  }

  closeCreateForm() {
    this.isCreateFormVisible = false;
    this.ObjectCreate.reset();
  }

  getCreateFormVisible() {
    return this.isCreateFormVisible;
  }

  openEditForm(data: BudgetObject) {
    this.isEditFormVisible = true;
    this.ObjectEdit.patchValue({
      id: data.id,
      name: data.name,
      date_year: data.date_year,
      comment: data.comment,
      is_base: data.is_base,
      is_current: data.is_current,
      is_active: data.is_active,
      user_created: data.user_created
    })
  }

  closeEditForm() {
    this.isEditFormVisible = false;
    this.ObjectEdit.reset();
  }

  getEditFormVisible() {
    return this.isEditFormVisible;
  }
  // Изменение размера при наборе текста в mat-textarea
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;
  triggerResize() {
    // Изменения размера текстовой области триггера
    this._ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }
}
