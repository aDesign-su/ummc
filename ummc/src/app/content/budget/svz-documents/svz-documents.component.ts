import { Component, OnInit, ViewChild } from '@angular/core';
import {SvzDocumentObject, SvzDocumentTableObject} from './svz-documents'
import {ThemeService} from '../../../theme.service'
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {ApicontentService} from '../../../api.content.service'
import {BudgetObject} from '../registry/registry'
import { ShowService } from 'src/app/helper/show.service';
import * as jmespath from 'jmespath';
import { Router } from '@angular/router';
import { SvzService } from './svz.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SvzObject } from './svz/svz';

@Component({
  selector: 'app-svz-documents',
  templateUrl: './svz-documents.component.html',
  styleUrls: ['./svz-documents.component.css']
})
export class SvzDocumentsComponent implements OnInit {

  columnsName: string[] = ['serial_number', 'date', 'name', 'budget', 'actions'];
  dataSource = new MatTableDataSource<SvzDocumentObject>([]);

  @ViewChild('paginator') paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  budgets: BudgetObject[] = [];
  svzRegistries: SvzDocumentObject[] = [];

  private isCreateFormVisible = false;
  private isEditFormVisible = false;

  constructor(
    public theme: ThemeService,
    private formBuilder: FormBuilder,
    private wbsService: ApicontentService,
    public svz: SvzService,
    public show:ShowService,
    private router: Router
  ) {
    this.showDetails(this.setId)
    this.ObjectCreate = this.formBuilder.group({
      id: null,
      name: ["", [Validators.required]],
      // budget: [null, [Validators.required]],
      user_created: 1
    })
    this.ObjectEdit = this.formBuilder.group({
      id: null,
      name: ["", [Validators.required]],
      // budget: [null, [Validators.required]],
      user_created: 1
    })
  }
  dataSvzSource = []
  dataSvzName = []
  getSvzId!: FormGroup;
  setId: any

  ngOnInit() {
    this.getBudgets();
    this.getObjects();
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  ObjectCreate!: FormGroup;
  ObjectEdit!: FormGroup;

  get _nameCreate() {
    return this.ObjectCreate.get('name')
  }

  get _budgetCreate() {
    return this.ObjectCreate.get('budget')
  }

  get _nameEdit() {
    return this.ObjectEdit.get('name')
  }

  get _budgetEdit() {
    return this.ObjectEdit.get('budget')
  }

  getBudgets() {
    this.wbsService.getBudgetObjects().subscribe(
      (data: BudgetObject[]) => {
        this.budgets = data;
      }
    )
  }

  getObjects() {
    this.wbsService.getSvzDocumentObjects().subscribe(
      (data: SvzDocumentObject[]) => {
        this.svzRegistries = data['data'];

        this.dataSource = new MatTableDataSource<any>(this.svzRegistries);
        this.dataSource.paginator = this.paginator;
        this.changeLoading();
      }
    )
  }

  addObject() {
    this.changeLoading();
    let addedObject = this.ObjectCreate.value;
    this.wbsService.addSvzDocumentObject(addedObject).subscribe(
      (data: any) => {
        console.log('Svz document added successfully:', data)
        this.closeCreateForm();
        this.getObjects();
        this.theme.access = 'Объект добавлен успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to add svz document:', error)
        this.theme.error = 'Не удалось добавить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
    console.log(addedObject)
  }

  editObject() {
    this.changeLoading();
    let editedObject = this.ObjectEdit.value;
    this.wbsService.editSvzDocumentObject(editedObject).subscribe(
      (data: any) => {
        console.log('Svz document edited successfully:', data)
        this.closeEditForm();
        this.getObjects();
        this.theme.info = 'Объект изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to edit svz document:', error)
        this.theme.error = 'Не удалось обновить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
    console.log(editedObject)
  }

  deleteObject(id: number) {
    this.changeLoading();
    this.wbsService.deleteSvzDocumentObject(id).subscribe(
      (data: any) => {
        console.log('Svz document deleted successfully:', data)
        this.getObjects();
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  openCreateForm() {
    this.isCreateFormVisible = !this.isCreateFormVisible;
    this.ObjectCreate.reset();
    this.ObjectCreate.patchValue({
      name: "",
      budget: null,
      user_created: 36
    })
  }

  closeCreateForm() {
    this.isCreateFormVisible = false;
    this.ObjectCreate.reset();
  }

  getCreateFormVisible() {
    return this.isCreateFormVisible;
  }

  openEditForm(data: SvzDocumentObject) {
    this.isEditFormVisible = true;
    this.ObjectEdit.patchValue({
      id: data.id,
      name: data.name,
      budget: data.budget,
      date: data.date,
      user_created: data.user_created,
      budget_name: data.budget_name
    })
  }

  closeEditForm() {
    this.isEditFormVisible = false;
    this.ObjectEdit.reset();
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  getEditFormVisible() {
    return this.isEditFormVisible;
  }
  showDetails(elem:any) {
    this.setId = elem
    this.getSvzId = this.formBuilder.group({
      id: this.setId
    })

    if(elem) {
      this.router.navigate(['svz']);
      this.svz.setSetId(this.setId);
    }
  }
}
