/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SvzService } from './svz.service';

describe('Service: Svz', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SvzService]
    });
  });

  it('should ...', inject([SvzService], (service: SvzService) => {
    expect(service).toBeTruthy();
  }));
});
