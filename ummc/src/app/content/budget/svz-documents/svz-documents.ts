export interface SvzDocumentTableObject {
  name: string,
  budget_name: string,
  date: Date
}

export interface SvzDocumentObject {
  id: number,
  user_created: number,
  name: string,
  date: Date,
  budget: string,
  budget_name: string;
}
