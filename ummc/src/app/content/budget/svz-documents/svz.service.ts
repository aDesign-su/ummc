import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class SvzService {
  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  constructor() {
    const svzId = this.getSetIdFromLocalStorage();
    if (svzId !== null) {
      this.setIdSource.next(svzId);
    }
  }
  private getSetIdFromLocalStorage(): number | null {
    const svzId = localStorage.getItem(this.setIdKey);
    return svzId ? +svzId : null;
  }
  setSetId(id: number) {
    localStorage.setItem(this.setIdKey, id.toString());
    this.setIdSource.next(id);
  }
  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }
}
