/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SvzComponent } from './svz.component';

describe('SvzComponent', () => {
  let component: SvzComponent;
  let fixture: ComponentFixture<SvzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SvzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
