export interface SvzObject {
  id: number,
  document: number,
  objects_wbs: number,
  total_price: number,
  actual_price: number,
  fact_price: number,
  reamains_price: number,
  comment: string;
}

export interface NodeObject {
  id: number,
  name: string,
  parent_id: number,
  parent_id_delete: number,
  id_package: number,
  delete_flg: boolean,
  lft: number,
  rght: number,
  tree_id: number,
  level: number
}
