import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import {ThemeService} from '../../../../theme.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {NodeObject, SvzObject} from './svz'
import {ApicontentService} from '../../../../api.content.service'
import {BudgetObject} from '../../registry/registry'
import { SvzService } from '../svz.service';
import * as jmespath from 'jmespath';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PeriodicElement } from 'src/app/content/references/analogy/analogy.component';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs';
import { ShowService } from 'src/app/helper/show.service';

@Component({
  selector: 'app-svz',
  templateUrl: './svz.component.html',
  styleUrls: ['./svz.component.css']
})
export class SvzComponent implements OnInit {

  columnsName: string[] = [
    'serial_number', 'id', 'name', 'total_price', 'actual_price', 'fact_price', 'reamains_price', 'comment', 'actions'
  ];
  dataSvzName = []
  dataSvzSource = []
  svz: SvzObject[] = [];
  wbs: NodeObject[] = [];
  estId: any
  docId: any

    private isCreateFormVisible = false;
    private isEditFormVisible = false;

  constructor(
    public theme: ThemeService,
    private formBuilder: FormBuilder,
    private wbsService: ApicontentService,
    public getSvz: SvzService,
    private _ngZone: NgZone,
    public show: ShowService
  ) {
    this.getSvz.currentSetId.subscribe(id => {
      this.estId = {"id":id}
      this.docId = id

      console.log(this.estId)
    });
    this.ObjectCreate = this.formBuilder.group({
      id: null,
      document: [this.docId, [Validators.required]],
      objects_wbs: [null, [Validators.required]],
      total_price: [null, [Validators.required]],
      actual_price: [null, [Validators.required]],
      fact_price: [null, [Validators.required]],
      reamains_price: [null, [Validators.required]],
      comment: "",
    })
    this.ObjectEdit = this.formBuilder.group({
      id: null,
      document: [1, [Validators.required]],
      objects_wbs: [null, [Validators.required]],
      total_price: [null, [Validators.required]],
      actual_price: [null, [Validators.required]],
      fact_price: [null, [Validators.required]],
      reamains_price: [null, [Validators.required]],
      comment: "",
    })
  }

  ngOnInit() {
    this.getObjects();
    this.getWbs();
    this.showDetails(this.estId)
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  ObjectCreate!: FormGroup;
  ObjectEdit!: FormGroup;

  displayedColumns: string[] = ['number', 'code_sdr', 'id_assoi', 'name', 'actual_price', 'fact_price', 'reamains_price', 'total_price', 'comment', 'action'];
  dataSource = new MatTableDataSource<SvzObject>([]);

  @ViewChild('paginator') paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  get _wbsObjectCreate() {
    return this.ObjectCreate.get('objects_wbs')
  }

  get _totalCreate() {
    return this.ObjectCreate.get('total_price')
  }

  get _actualCreate() {
    return this.ObjectCreate.get('actual_price')
  }

  get _factCreate() {
    return this.ObjectCreate.get('fact_price')
  }

  get _reamainsCreate() {
    return this.ObjectCreate.get('reamains_price')
  }

  get _commentCreate() {
    return this.ObjectCreate.get('comment')
  }


  get _wbsObjectEdit() {
    return this.ObjectEdit.get('objects_wbs')
  }

  get _totalEdit() {
    return this.ObjectEdit.get('total_price')
  }

  get _actualEdit() {
    return this.ObjectEdit.get('actual_price')
  }

  get _factEdit() {
    return this.ObjectEdit.get('fact_price')
  }

  get _reamainsEdit() {
    return this.ObjectEdit.get('reamains_price')
  }

  get _commentEdit() {
    return this.ObjectCreate.get('comment')
  }

  showDetails(elem:any) {

      this.wbsService.editSvzDocumentMore(this.estId).subscribe(
        (response: number[]) => {
          const query = "@.data[]";
          this.dataSvzSource = jmespath.search(response, query);

          const name = "@.info_document[]";
          this.dataSvzName = jmespath.search(response, name);

          this.dataSource = new MatTableDataSource<any>(this.dataSvzSource);
          this.dataSource.paginator = this.paginator;

          this.changeLoading();
        },
        (error: any) => {
          console.error('Failed to add data:', error);
        }
      );
  }
  getWbs() {
    this.wbsService.getSvzWbs().subscribe(
      (data: NodeObject[]) => {
        const wbsSelect = "[?level == `1`] | [?delete_flg == `false`].{id: id, name: name}"
        this.wbs = jmespath.search(data, wbsSelect)
        console.log(this.wbs);
      }
    )
  }

  getObjects() {
    this.wbsService.getSvzObjects().subscribe(
      (data: SvzObject[]) => {
        this.svz = data;
        console.log(this.svz)
      }
    )
  }

  addObject() {
    this.changeLoading();
    let addedObject = this.ObjectCreate.value;
    this.wbsService.addSvzObject(addedObject).subscribe(
      (data: any) => {
        console.log('Svz added successfully:', data)
        this.closeCreateForm();
        this.showDetails(this.estId);
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to add svz:', error)
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
    console.log(addedObject)
  }

  editObject() {
    this.changeLoading();
    let editedObject = this.ObjectEdit.value;
    console.log(editedObject)
    this.wbsService.editSvzObject(editedObject).subscribe(
      (data: any) => {
        console.log('Svz edited successfully:', data)
        this.closeEditForm();
        this.showDetails(this.estId);
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to edit svz:', error)
        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  deleteObject(id: number) {
    this.changeLoading();
    this.wbsService.deleteSvzObject(id).subscribe(
      (data: any) => {
        console.log('Svz deleted successfully:', data)
        this.showDetails(this.estId);
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to delete svz:', error)
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  getWbsName(id: number) {
    let wbsName = "";
    this.wbs.every(object => {
      if (object.id == id) {
        wbsName = object.name;
        return false;
      }
      return true;
    })
    return wbsName;
  }

    openCreateForm() {
      this.isCreateFormVisible = !this.isCreateFormVisible;
      this.ObjectCreate.reset();
      this.ObjectCreate.patchValue({
        id: null,
        document: this.docId,
        objects_wbs: null,
        total_price: null,
        actual_price: null,
        fact_price: null,
        reamains_price: null,
        comment: "",
      })
    }

    closeCreateForm() {
      this.isCreateFormVisible = false;
      this.ObjectCreate.reset();
    }

    getCreateFormVisible() {
      return this.isCreateFormVisible;
    }

    openEditForm(data: SvzObject) {
      this.isEditFormVisible = true;
      this.ObjectEdit.patchValue({
          id: data.id,
          document: data.document,
          objects_wbs: data.objects_wbs,
          total_price: data.total_price,
          actual_price: data.actual_price,
          fact_price: data.fact_price,
          reamains_price: data.reamains_price,
          comment: data.comment,
      })
    }

    closeEditForm() {
      this.isEditFormVisible = false;
      this.ObjectEdit.reset();
    }

    getEditFormVisible() {
      return this.isEditFormVisible;
    }
    // Изменение размера при наборе текста в mat-textarea
    @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;
    triggerResize() {
      // Изменения размера текстовой области триггера
      this._ngZone.onStable.pipe(take(1))
          .subscribe(() => this.autosize.resizeToFitContent(true));
    }
}
