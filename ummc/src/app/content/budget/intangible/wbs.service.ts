import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ObjectBudget } from '../project-budget/registerBudget';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class WbsService {

  constructor(private formBuilder:FormBuilder,private assets: ApicontentService) { 
    this.select1 = this.formBuilder.group({
      level: [0], 
    });
    this.select2 = this.formBuilder.group({
      parent_id: [this.showId], 
    });
    this.select3 = this.formBuilder.group({
      parent_id: [this.showId], 
    });
    this.select4 = this.formBuilder.group({
      level: [0],
    });
    this.createSystem = this.formBuilder.group({
      name: [''], 
      code_wbs: [null], 
      date_start: [''], 
      date_end: [''], 
      category: [''],
      parent: [],
      // name_package: ['0'],
      user_created: [1],
    });
  }
  createSystem: FormGroup
  select1: FormGroup
  select2: FormGroup
  select3: FormGroup
  select4: FormGroup
  showId: number

  getId2(element: any) {
    this.showId = element
    this.select2.patchValue({
      parent_id: this.showId,
    });
    // console.log(this.showId)
  }
  getId3(element: any) {
    this.showId = element
    this.select3.patchValue({
      parent_id: this.showId,
    });
    this.createSystem.patchValue({
      parent: this.showId,
    });
    // console.log(this.showId)
  }

  getWbsLevel1: any[] = []
  wbsList1!: string;
  getWbsLevel2: any[] = []
  wbsList2!: string;
  getWbsLevel3: any[] = []
  wbsList3!: string;

  getSelect1() {
    const filter = this.select1.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          console.log('text',this.getWbsLevel1)
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          // this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          // setTimeout(() => {
          //   this.show.showBlock[1] = false
          //   this.theme.error = null;
          // }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect2() {
    const filter = this.select2.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel2 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          // this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          // setTimeout(() => {
          //   this.show.showBlock[1] = false
          //   this.theme.error = null;
          // }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect3() {
    const filter = this.select3.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel3 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          // this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          // setTimeout(() => {
          //   this.show.showBlock[1] = false
          //   this.theme.error = null;
          // }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect4() {
    const filter = this.select4.value;
    this.assets.addFilterNodeFlat(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
        //   this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
        //   setTimeout(() => {
        //     this.show.showBlock[1] = false
        //     this.theme.error = null;
        //   }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }
}
