import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import * as jmespath from 'jmespath';
import * as moment from 'moment';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { switchMap, EMPTY } from 'rxjs';
import { getTotal, Total, getRegisterBudget } from '../total-project-costs/total';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { ObjectBudget } from '../project-budget/registerBudget';
import { CommonService } from '../project-budget/common.service';
import { WbsService } from './wbs.service';

@Component({
  selector: 'app-intangible',
  templateUrl: './intangible.component.html',
  styleUrls: ['./intangible.component.css']
})
export class IntangibleComponent implements OnInit {
  constructor(
    public theme: ThemeService,
    public show:ShowService,
    private formBuilder: FormBuilder,
    public isTitle: CommonService,
    private assets: ApicontentService,
    private _adapter: DateAdapter<any>,
    public wbsServer:WbsService,
    @Inject(MAT_DATE_LOCALE) private _locale: string
    ) { 
    this.createCommonCosts = this.formBuilder.group({
      comment: [''], 
      // other_price: ['', [Validators.required]], 
      // oto_price: ['', [Validators.required]], 
      // pir_price: ['', [Validators.required]], 
      // smr_price: ['', [Validators.required]], 
      // total_price: ['', [Validators.required]], 
      date_start: ['', [Validators.required]], 
      date_end: ['', [Validators.required]], 
      // budget_source: ['', [Validators.required]], 
      register_budget: ['', [Validators.required]], 
      wbs: [null, [Validators.required]],
      user_created: [1]
    });
    this.editCommonCosts = this.formBuilder.group({
      // comment: ['', [Validators.required]], 
      other_price: ['', [Validators.required]], 
      oto_price: ['', [Validators.required]], 
      pir_price: ['', [Validators.required]], 
      smr_price: ['', [Validators.required]], 
      total_price: ['', [Validators.required]], 
      // date_start: ['', [Validators.required]], 
      // date_end: ['', [Validators.required]], 
      // budget_source: ['', [Validators.required]], 
      // register_budget: ['', [Validators.required]], 
      // wbs: [],
      user_created: [1]
    });
    this.createSystem = this.formBuilder.group({
      name: [''], 
      code_wbs: [null], 
      date_start: [''], 
      date_end: [''], 
      category: [''],
      parent: [],
      // name_package: ['0'],
      user_created: [1],
    });
    this.select1 = this.formBuilder.group({
      level: [0], 
    });
    this.select2 = this.formBuilder.group({
      parent_id: [this.showId], 
    });
    this.select3 = this.formBuilder.group({
      parent_id: [this.showId], 
    });
  }
  createSystem: FormGroup
  select1: FormGroup
  select2: FormGroup
  select3: FormGroup
  createCommonCosts: FormGroup
  editCommonCosts: FormGroup

  getCategory: any[] = []
  categoryList!: string;

  ngOnInit() {
    this.getIsCurrent()

    this.assets.getSourceBudget().subscribe(
      (response: any[]) => {
        this.getSource = response
      },
    );
  }

  addBudget4() {
    const budget = this.createSystem.value;
    
    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.assets.addIntangibleAssetsObjectFilter(budget).subscribe(
      (response: any[]) => {
        this.show.showBlock[7] = false
        this.show.showBlock[5] = true
        this.wbsServer.getSelect3()
        this.theme.access = 'Пакет работ добавлен успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        
        // this.getShowDetails();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  getId2(element: any) {
    this.showId = element
    this.select2.patchValue({
      parent_id: this.showId,
    });
    // console.log(this.showId)
   }
   getId3(element: any) {
    this.showId = element
    this.select3.patchValue({
      parent_id: this.showId,
    });
    this.createSystem.patchValue({
      parent: this.showId,
    });
    // console.log(this.showId)
   }

   getSelect1() {
    const filter = this.select1.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect2() {
    const filter = this.select2.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel2 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect3() {
    const filter = this.select3.value;
    this.assets.getNmaFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel3 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  showId: number
  setIsCurrent: number
  nmaId: number

  getSource: any[] = []
  sourceList!: string;
  getWbsLevel1: any[] = []
  wbsList1!: string;
  getWbsLevel2: any[] = []
  wbsList2!: string;
  getWbsLevel3: any[] = []
  wbsList3!: string;


  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end','total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price',  'actions'];
  dataSource = new MatTableDataSource<getTotal>([]);
  
  private _transformer = (node: getTotal, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      code_wbs: node.code_wbs,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      other_price: node.other_price,
      date_end: node.date_end,
      date_start: node.date_start,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;

  getIsCurrent() {
    this.assets.getRegisterBudget().pipe(
      switchMap((data: getRegisterBudget[]) => {
        const row = "@.data[]";
        const response = jmespath.search(data, row);
        const currentObject = response.find(item => item.is_current);
  
        if (currentObject) {
          this.setIsCurrent = currentObject.id;
          console.log('ID текущего объекта:', this.setIsCurrent);
          return this.assets.getIntangibleAssetsBudget(this.setIsCurrent);
        } else {
          console.log('Текущий объект не найден');
          return EMPTY; 
        }
      })
    ).subscribe((data: getTotal[]) => {
      const row = "@.data[]";
      const response = jmespath.search(data, row);
      this.dataSources.data = response;
      console.log('data:', response);
    });
  }

  addCommonCosts() {
    const budget = this.createCommonCosts.value;
    
      // Format the date as YYYY-MM-DD
      const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
      const endDate = moment(budget.date_end).format('YYYY-MM-DD');
      budget.date_start = startdDate;
      budget.date_end = endDate;

    this.assets.addIntangibleAssetsBudget(budget).subscribe(
      (response: getTotal[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        // this.createCommonObjectBudgetForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getIsCurrent();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  editCommonCostsDirectory(element: getTotal) {
    this.nmaId = element.common_budget_id; // Установим значение свойства element
    this.editCommonCosts.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.common_budget_id)
   }

   saveCommonCostsDirectory() {
    if (this.nmaId) {
      let editedObject = this.editCommonCosts.value;
      this.assets.updIntangibleAssetsBudget(this.nmaId, editedObject).subscribe(
        (data: any) => {
          this.show.showBlock[3] = false; // Закроем все блоки, устанавливая значение в false
          // this.element.editing = false

          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getIsCurrent();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
   }

  delCommonCosts(element: any) {
    const delElement = element
    console.log(delElement)
    this.assets.delIntangibleAssetsBudget(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getIsCurrent();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
   }
}
