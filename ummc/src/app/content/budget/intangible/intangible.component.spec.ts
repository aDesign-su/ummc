/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { IntangibleComponent } from './intangible.component';

describe('IntangibleComponent', () => {
  let component: IntangibleComponent;
  let fixture: ComponentFixture<IntangibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntangibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntangibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
