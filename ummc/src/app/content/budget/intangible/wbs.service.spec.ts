/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WbsService } from './wbs.service';

describe('Service: Wbs', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WbsService]
    });
  });

  it('should ...', inject([WbsService], (service: WbsService) => {
    expect(service).toBeTruthy();
  }));
});
