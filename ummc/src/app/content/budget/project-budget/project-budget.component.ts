import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TreeNode } from 'primeng/api';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { RegisterBudget } from './registerBudget';
import * as jmespath from 'jmespath';
import { CommonService } from './common.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { GetTotalCommonService } from './getTotalCommon.service';
import { Total, getMathingBudget } from '../total-project-costs/total';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-project-budget',
  templateUrl: './project-budget.component.html',
  styleUrls: ['./project-budget.component.css']
})
export class ProjectBudgetComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService,private registerBudget: ApicontentService,private tree:GetTotalCommonService ,private router: Router,public common:CommonService,private formBuilder: FormBuilder) { 
    this.createRegisterBudgetForm = this.formBuilder.group({
      name: [''], 
      comment: [''], 
      is_base: [false], 
      is_current: [false], 
      user_created: [1]
    });
    this.editRegisterBudgetForm = this.formBuilder.group({
      name: [''], 
      comment: [''], 
      is_base: [false], 
      is_current: [false], 
      user_created: [1]
    });

  }
  error: string
  getProjectBudget: any[] = []
  selectedProjectBudget: number
  selectedPeriod: string = ''; // Инициализация значением "Все"
  selectedPriceFields: string[] = []; // Инициализация пустым массивом

  frequencyAll = ''
  pkAll = ''

  use() {
    // let baseUrl = 'https://asbudget.ugmk.com/api'; // local
    // let baseUrl: string = 'http://darkdes-django-z38bs.tw1.ru'
    let pk: number = this.selectedProjectBudget // id Бюджет проекта
    let frequency: any = this.selectedPeriod // Выбранный период
    let fields_price = this.selectedPriceFields // Выбранные поля цены
    let url: string = `${this.registerBudget.baseUrl}/budget/get_transposed_budget/?pk=${pk}&frequency=${frequency}&fields_price=(${fields_price})`;
    let generalTransposed: string = `${this.registerBudget.baseUrl}/budget/get_general_transposed_budget/?pk=${pk}&frequency=${frequency}&fields_price=(${fields_price})`;
    let intangibleTransposed: string = `${this.registerBudget.baseUrl}/budget/get_intangible_transposed_budget/?pk=${pk}&frequency=${frequency}&fields_price=(${fields_price})`;

    localStorage.setItem('budgetUrl', url);
    localStorage.setItem('generalBudgetUrl', generalTransposed);
    localStorage.setItem('intangibleBudgetUrl', intangibleTransposed);
    
    this.common.url = url;
    if (pk && frequency) {
      this.router.navigate(['project-budget/transposed-budget']);
    } else {
      if (!pk) {
        this.error = 'Необходимо выбрать Бюджет проекта';
      } else if (!frequency) {
        this.error = 'Необходимо выбрать период';
      } 
      setTimeout(() => {
        this.error = null;
      }, 5000);
    }
  }

  ngOnInit() {
    this.getSourceRegisterBudget()
  };
  test() {
    this.isDialogVisible = true;
  }
  fb: string[] = ['Добавить бюджет проекта','Редактировать бюджет проекта']
  isDialogVisible = false;

  createRegisterBudgetForm!: FormGroup;
  editRegisterBudgetForm!: FormGroup;

  toppings = new FormControl('');

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  displayedColumns: string[] = ['serial_number', 'name', 'comment', 'select', 'actions'];
  dataSource = new MatTableDataSource<RegisterBudget>([]);
  source: RegisterBudget[] = []
  setId: number
  element: any

  @ViewChild('paginator') paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getSourceRegisterBudget() {
    this.registerBudget.getRegisterBudget().subscribe(
      (data: RegisterBudget[]) => {
        const query = "@.data[]";
        this.source = jmespath.search(data, query);

        // Селект для get_transposed_budget
        const selected = "@.data[].[{id:id, name:name}]|[]";
        this.getProjectBudget = jmespath.search(data, selected);

        this.dataSource = new MatTableDataSource<any>(this.source);
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  checked = false;
  indeterminate = false;
    
  addRegisterBudget() {
    const estimated = this.createRegisterBudgetForm.value;
    this.registerBudget.addCreateRegisterBudget(estimated).subscribe(
      (response: RegisterBudget[]) => {
        this.show.showBlock[0] = false;
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getSourceRegisterBudget();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editRegisterBudget(element: any) {
    this.element = element.id; // Установим значение свойства element
    element.editing = true;

    const updatedData = {
      name: element.name,
      comment: element.comment,
      is_base: element.is_base,
      is_current: element.is_current,
      user_created: 1
    }
    this.editRegisterBudgetForm.patchValue(updatedData)
  }
  saveRegisterBudget() {
    if (this.element) {
      let editData = this.editRegisterBudgetForm.value;

      this.registerBudget.updateRegisterBudget(this.element, editData).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        this.getSourceRegisterBudget()
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  deleteRegisterBudget(item: any) {
    const id = item
    console.log(id)
    this.registerBudget.delRegisterBudget(id).subscribe(
      (response: any[]) => {
        this.theme.info = 'Реестр бюджета удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getSourceRegisterBudget()
      },
    );
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  showDetails(id:number) {
    this.setId = id
    if(id) {
      this.router.navigate(['project-budget/more']);
      this.common.setSetId(this.setId);
    }
  }

  selection = new SelectionModel<any>(true, []);
  clickedRowsArray: any[] = [];
  registerBudgetSource

  formJsonStructure(): void {
    const selectedIds = this.clickedRowsArray.map(row => row.id);
    this.registerBudgetSource = { "pk_list": selectedIds };
  }

  displayedCol: string[] = ['serial_number', 'name', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price'];
  displayedColHead: string[] = []
  defaultValue = null

  private _transformer = (node: getMathingBudget, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      title: node.title,
      name: node.name,
      code_wbs: node.code_wbs,
      total_budget: node.total_budget,
      smr_budget: node.smr_budget,
      pir_budget: node.pir_budget,
      oto_budget: node.oto_budget,
      other_budget: node.other_budget,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  displayedColBody: any[] = []
  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: Total) => node.expandable;

  displayedColumns2: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource2 = ELEMENT_DATA;

  compare() {
    const data = this.registerBudgetSource;
    this.registerBudget.compareEstimate(data).subscribe(
      (response: getMathingBudget[]) => {
        
        const anl = "@.data[]";
        const res = jmespath.search(response, anl);
        this.dataSources.data = res
        this.displayedColBody = res
        
        const head = "@._meta";
        const meta = jmespath.search(response, head);
        // console.log(this.displayedColBody);
        
        // Преобразовать объект в массив ключей
        this.displayedColHead = Object.keys(meta);
        console.log(this.clickedRowsArray);
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected?.length;
    const numRows = this.dataSource.data?.length;
    return numSelected === numRows;
  }

  clickedRows = new Set<any>();

  isRowClicked(row: any): boolean {
    return this.clickedRows.has(row);
  }
  toggleRow(node: any) {
    if (this.isRowClicked(node)) {
      this.clickedRows.delete(node);
    } else {
      this.clickedRows.add(node);
    }
  
    this.clickedRowsArray = Array.from(this.clickedRows);
    // console.log(this.clickedRowsArray)
  }
  updateArray() {
    this.clickedRowsArray = Array.from(this.clickedRows);
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(node?: any): string {
    if (!node) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(node) ? 'deselect' : 'select'} node ${node.position + 1}`;
  }
}