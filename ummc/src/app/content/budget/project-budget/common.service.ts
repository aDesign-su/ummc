import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ApicontentService } from 'src/app/api.content.service';
import { ObjectBudget } from './registerBudget';
import * as jmespath from 'jmespath';
import { Wbs } from '../wbs/wbs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public url: string = '';
  
  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  constructor(private registerBudget: ApicontentService) {
    const objectId = this.getSetIdFromLocalStorage();
    if (objectId !== null) {
      this.setIdSource.next(objectId);
    }
    this.getWbs()
  }
  private getSetIdFromLocalStorage(): number | null {
    const objectId = localStorage.getItem(this.setIdKey);
    return objectId ? +objectId : null;
  }
  setSetId(id: number) {
    localStorage.setItem(this.setIdKey, id.toString());
    this.setIdSource.next(id);
  }
  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }

  isName: string
  getWbs(): void {
    this.registerBudget.getWbs().subscribe(
      (data: Wbs[]) => {
        this.isName = data[0].name
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
