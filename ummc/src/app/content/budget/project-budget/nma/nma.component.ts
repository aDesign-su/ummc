import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { getRegisterBudget, getTotal, Total } from '../../total-project-costs/total';
import { FormBuilder } from '@angular/forms';
import { switchMap, EMPTY } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import * as jmespath from 'jmespath';
import * as moment from 'moment';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-nma',
  templateUrl: './nma.component.html',
  styleUrls: ['./nma.component.css']
})
export class NmaComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, private formBuilder: FormBuilder, private nma: ApicontentService,public common:CommonService) { 
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
  }

  ngOnInit() {
    this.getIsCurrent()
  }

  getId: number

  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price'];
  dataSource = new MatTableDataSource<getTotal>([]);
  
  private _transformer = (node: getTotal, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      code_wbs: node.code_wbs,
      date_start: node.date_start,
      date_end: node.date_end,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      other_price: node.other_price,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;

  getIsCurrent() {
    this.nma.getIntangibleAssetsBudget(this.getId).subscribe(
      (data: any[]) => {
        const anl = "@.data[]";
        const response = jmespath.search(data, anl);
        this.dataSources.data = response;
        console.log('total', response)

        // const anl = "@.data[]";
        // const response = jmespath.search(data, anl);
        // this.dataSources.data = response;
        // console.log('total', response)
      }
    );
    
    // this.nma.getRegisterBudget().pipe(
    //   switchMap((data: getRegisterBudget[]) => {
    //     const row = "@.data[]";
    //     const response = jmespath.search(data, row);
    //     const currentObject = response.find(item => item.is_current);
  
    //     if (currentObject) {
    //       this.setIsCurrent = currentObject.id;
    //       console.log('ID текущего объекта:', this.setIsCurrent);
    //       return this.assets.getIntangibleAssetsBudget(this.setIsCurrent);
    //     } else {
    //       console.log('Текущий объект не найден');
    //       return EMPTY; 
    //     }
    //   })
    // ).subscribe((data: getTotal[]) => {
    //   const row = "@.data[]";
    //   const response = jmespath.search(data, row);
    //   this.dataSources.data = response;
    //   console.log('data:', response);
    // });
  }

}
