import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { switchMap, EMPTY } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Total, getRegisterBudget, getTotal } from '../../total-project-costs/total';
import * as jmespath from 'jmespath';
import * as moment from 'moment';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { ObjectBudget } from '../registerBudget';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-total-costs',
  templateUrl: './total-costs.component.html',
  styleUrls: ['./total-costs.component.css']
})
export class TotalCostsComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, private total: ApicontentService, private formBuilder: FormBuilder, public common:CommonService) { 
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
  }

  ngOnInit() {
    this.getIsCurrent()
    this.getTotal()
  }
  totals: any
  getId: number
  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price' ];
  displayedColumnsTotal: string[] = ['item', 'title','total','pir','smr','oto', 'other'];
  dataSource = new MatTableDataSource<getTotal>([]);
  
  private _transformer = (node: getTotal, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      date_start: node.date_start,
      date_end: node.date_end,
      code_wbs: node.code_wbs,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      other_price: node.other_price,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;

  setIsCurrent: number

  getIsCurrent() {
    this.total.getRegister(this.getId).subscribe(
      (data: any[]) => {
        const anl = "@.data[]";
        const response = jmespath.search(data, anl);
        this.dataSources.data = response;
        console.log('total', response)
      }
    );
  }
  getTotal() {
    this.total.getTotalCost(this.getId).subscribe(
      (data: ObjectBudget[]) => {
        const anl = "@.total_cost_general_project";
        const total = jmespath.search(data, anl);
        this.totals = total;
        console.log('total', total.total_price)
      }
    );
  }
}
