import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TotalCostsComponent } from './total-costs.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule
  ],
  declarations: [
    // TotalCostsComponent
  ]
})
export class TotalCostsModule { }
