import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { CommonService } from '../common.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-transposed-budget',
  templateUrl: './transposed-budget.component.html',
  styleUrls: ['./transposed-budget.component.css']
})
export class TransposedBudgetComponent implements OnInit {
  general!: TreeNode[]
  intangible!: TreeNode[]
  sales!: TreeNode[];
  colspan_price!: number;
  price_column!: string[];
  years!: string[];
  months!: string[];
  days!: string[];
  column_name_price!: Map<string, string>;
  unpuck_name_price!: string[];
  unpack_name_year!: Map<string, string>;
  count_column_price!:number;
  unpack_name_month!:Map<string, string>;
  unpack_name_day!:Map<string, string>;
  constructor(public theme: ThemeService, public show:ShowService, private http: HttpClient,public urlService:CommonService){}
  getData(url: string){
    return this.http.get(url)
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  ngOnInit() {
      // Получим URL из localStorage
    let url = localStorage.getItem('budgetUrl');
    let generalTransposed = localStorage.getItem('generalBudgetUrl');
    let intangibleTransposed = localStorage.getItem('intangibleBudgetUrl');

    // Проверим, был ли URL сохранен в localStorage
    if (url) {
      let url: string = this.urlService.url;
    } else {
      console.log('URL не найден в localStorage.');
    }

    let name_column: Map<string, string> = new Map<string, string>();
    name_column.set('total_price', 'Сумма общая');
    name_column.set('smr_price', 'Сумма СМР');
    name_column.set('pir_price', 'Сумма ПИР');
    name_column.set('oto_price', 'Сумма ОТО');
    name_column.set('other_price', 'Сумма прочее');

    // let baseUrl: string = 'http://darkdes-django-z38bs.tw1.ru'
    // let pk: number = 2

    let frequency: Map<string, string> = new Map<string, string>();
    frequency.set('year', 'YEARLY');
    frequency.set('month', 'MONTHLY');
    frequency.set('day', 'DAILY');


    this.price_column = ["total_price", "pir_price"]

    // let url: string = this.urlService.url;

    this.getData(url).subscribe({next: (data: any) => (
        this.sales = data['data'],
        this.colspan_price = data['colspan_price'],
        this.column_name_price = data['column_name_price'],
        this.unpuck_name_price = data['unpuck_name_price'],
        this.unpack_name_year = data['unpack_name_year'],
        this.unpack_name_month = data['unpack_name_month'],
        this.unpack_name_day = data['unpack_name_day']
      )});

    this.getData(generalTransposed).subscribe({next: (data: any) => (
      this.general = data['data'],
      this.colspan_price = data['colspan_price'],
      this.column_name_price = data['column_name_price'],
      this.unpuck_name_price = data['unpuck_name_price'],
      this.unpack_name_year = data['unpack_name_year'],
      this.unpack_name_month = data['unpack_name_month'],
      this.unpack_name_day = data['unpack_name_day']
    )});

    this.getData(intangibleTransposed).subscribe({next: (data: any) => (
      this.intangible = data['data'],
      this.colspan_price = data['colspan_price'],
      this.column_name_price = data['column_name_price'],
      this.unpuck_name_price = data['unpuck_name_price'],
      this.unpack_name_year = data['unpack_name_year'],
      this.unpack_name_month = data['unpack_name_month'],
      this.unpack_name_day = data['unpack_name_day']
    )});
  }

}
