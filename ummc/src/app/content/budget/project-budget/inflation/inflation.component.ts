import { Component, OnInit } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import * as jmespath from 'jmespath';
import { CommonService } from '../common.service';
import { GetTotalCommonService } from '../getTotalCommon.service';

@Component({
  selector: 'app-inflation-budget',
  templateUrl: './inflation-budget.component.html',
  styleUrls: ['./inflation-budget.component.css']
})
export class InflationBudgetComponent implements OnInit {
  constructor(private inflation: ApicontentService, public common:CommonService, public getInfo: GetTotalCommonService) {
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
   }

  ngOnInit() {
    this.getInfo.getTotalInflation()
    this.currentYear = new Date().getFullYear()
  }
  // dataSouce: any
  currentYear: number
  getId: number
  // getTotal() {
  //   this.inflation.getInflation(this.getId).subscribe(
  //     (data: any[]) => {
  //       const row = "@.ammount_inflation";
  //       const inflation = jmespath.search(data, row);
  //       this.dataSouce = inflation;
  //       console.log('total', inflation)
  //     }
  //   );
  // }

}
