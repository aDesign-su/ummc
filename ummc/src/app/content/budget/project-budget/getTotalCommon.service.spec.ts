/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetTotalCommonService } from './getTotalCommon.service';

describe('Service: GetTotalCommon', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetTotalCommonService]
    });
  });

  it('should ...', inject([GetTotalCommonService], (service: GetTotalCommonService) => {
    expect(service).toBeTruthy();
  }));
});
