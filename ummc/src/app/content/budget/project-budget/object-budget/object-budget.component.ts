import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { CommonService } from '../common.service';
import { ObjectBudget, ObjectBudgetNode } from '../registerBudget';
import { ApicontentService } from 'src/app/api.content.service';
import * as jmespath from 'jmespath';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { GetTotalCommonService } from '../getTotalCommon.service';
import { RiskPlan } from '../../risk/Risk';
import { OperatingCostItemPlan } from '../../operating_costs/OperatingСosts';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
  { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];

@Component({
  selector: 'app-object-budget',
  templateUrl: './object-budget.component.html',
  styleUrls: ['./object-budget.component.css']
})
export class ObjectBudgetComponent implements OnInit {

  dataOperatingCostsPlanTableSource = this.initTable<OperatingCostItemPlan>();

  constructor(public theme: ThemeService, public show: ShowService, private registerBudget: ApicontentService, public getInfo: GetTotalCommonService, public common: CommonService, private formBuilder: FormBuilder, private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
    this.getDataOperatingCosts()

    this.createCommonObjectBudgetForm = this.formBuilder.group({
      date_start: [''],
      date_end: [''],
      // comment: [''], 
      // total_price: [], 
      smr_price: [],
      pir_price: [],
      oto_price: [],
      other_price: [],
      problem: [''],
      register_budget: [],
      user_created: [1],
      wbs: [''],
      auto: false
    });
    this.select0 = this.formBuilder.group({
      level: [0],
    });
    this.select2 = this.formBuilder.group({
      parent_id: [this.showId],
    });
    this.select3 = this.formBuilder.group({
      parent_id: [this.showId],
    });
    this.createBaseToCurrent = this.formBuilder.group({
      register_budget_id: [this.showId],
      name_current_register_budget: [this.showId],
    });
  }
  select0: FormGroup
  select2: FormGroup
  select3: FormGroup
  isLevel: number
  showId: number
  link_general:boolean=false
  transaction_reserve:boolean=false

  //Получить данные
  getDataOperatingCosts(): void {
    this.registerBudget.getDataOperatingCostsPlan().subscribe(
      (data: OperatingCostItemPlan[]) => {
        this.dataOperatingCostsPlanTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

  }


  getSelect0() {
    const filter = this.select0.value;
    this.registerBudget.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel0 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect1() {
    const filter = this.select2.value;
    this.registerBudget.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel1 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[0] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }

  getSelect2() {
    const filter = this.select3.value;
    this.registerBudget.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        if (response && response.length > 0 && response[0].hasOwnProperty('level')) {
          this.getWbsLevel2 = response;
          // this.isLevel = response[0].level;
        } else {
          // Вывести сообщение об ошибке, так как свойство "level" отсутствует
          console.error("Ошибка: свойство 'level' не найдено в ответе.");
          this.theme.error = 'Не удалось добавить уровень. свойство level не найдено в ответе. Ошибка #Cannot read properties of undefined (reading level).'
          setTimeout(() => {
            this.show.showBlock[1] = false
            this.theme.error = null;
          }, 5000);
        }
      },
      (error) => {
        // Обработка ошибки при выполнении запроса
        console.error("Ошибка при выполнении запроса:", error);
      }
    );
  }
  getId1(element: any) {
    this.showId = element
    this.select2.patchValue({
      parent_id: this.showId,
    });
    // console.log(this.showId)
  }
  getId2(element: any) {
    this.showId = element
    this.select3.patchValue({
      parent_id: this.showId,
    });
    // console.log(this.showId)
  }
  panelOpenState = false;
  createCommonObjectBudgetForm!: FormGroup;
  createBaseToCurrent!: FormGroup;
  getWbsLevel1: any[] = []
  wbsList1!: string;

  getWbsLevel2: any[] = []
  wbsList2!: string;

  getWbsLevel0: any[] = []
  wbsList0!: string;

  ngOnInit() {
    this.getDatRisk()
    this.getSourceObjectBudget()
    this.getSourceRegisterBudget()
    this.getTotal()
    this.getInfo.getTotal()
  }

  displayedColumns: string[] = ['serial_number', 'name', 'date_start', 'date_end', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price', 'reserve_price'];
  displayedColumnsTotal: string[] = ['item', 'title', 'start', 'end', 'total', 'pir', 'smr', 'oto', 'other'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  private _transformer = (node: ObjectBudget, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      name: node.name,
      code_wbs: node.code_wbs,
      date_start: node.date_start,
      date_end: node.date_end,
      total_price: node.total_price,
      smr_price: node.smr_price,
      pir_price: node.pir_price,
      oto_price: node.oto_price,
      other_price: node.other_price,
      reserve_price: node.reserve_price,
      wbs_id: node.wbs_id,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<ObjectBudgetNode>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  // Инициализация MatTableDataSource
  dataRiskPlanTableSource = this.initTable<RiskPlan>();
  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }
  hasChild = (_: number, node: ObjectBudgetNode) => node.expandable;

  source: any[] = []
  getId: number
  getName: any
  total: any
  summ: any

  getDatRisk(): void {
    this.registerBudget.getDataRiskPlan().subscribe(
      (data: RiskPlan[]) => {
        this.dataRiskPlanTableSource.data = data;
        console.log('dataRisk', data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });

  }


  getTotal() {
    this.registerBudget.getTotalCost(this.getId).subscribe(
      (data: ObjectBudget[]) => {
        const anl = "@.total_cost_object";
        const total = jmespath.search(data, anl);
        this.total = total;

        // const row = "@.total_cost";
        // const summ = jmespath.search(data, row);
        // this.summ = summ;
        // console.log('total', summ.total_price)
      }
    );
  }

  getSourceObjectBudget() {
    console.log('воть ',this.link_general,this.transaction_reserve)
    this.registerBudget.getObjectBudgetGeneral(this.getId,this.link_general,this.transaction_reserve).subscribe(
      (data: ObjectBudget[]) => {
        const anl = "@.data[]";
        this.source = jmespath.search(data, anl);
        this.dataSources.data = this.source;
        console.log(this.source)
      }
    );
  }
  addRegisterBudget() {
    const budget = this.createCommonObjectBudgetForm.value;

    const startdDate = moment(budget.date_start).format('YYYY-MM-DD');
    const endDate = moment(budget.date_end).format('YYYY-MM-DD');
    budget.date_start = startdDate;
    budget.date_end = endDate;

    this.registerBudget.addCommonObjectBudget(budget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        // this.createCommonObjectBudgetForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getSourceObjectBudget();
        this.getInfo.getTotal();
        this.getInfo.getTotalInflation();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addBaseToCurrent() {
    const budget = this.createBaseToCurrent.value;

    this.registerBudget.addBaseToCurrentBudget(budget).subscribe(
      (response: ObjectBudget[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        // this.createCommonObjectBudgetForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getSourceObjectBudget();
        this.getInfo.getTotal();
        this.getInfo.getTotalInflation();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  // Доабвление Breadcrumb в Tools
  getSourceRegisterBudget() {
    this.registerBudget.getRegisterBudget().subscribe(
      (data: any[]) => {
        const query = "@.data[]";
        this.getName = jmespath.search(data, query);
        // console.log(this.getName)
      }
    )
  }
}

