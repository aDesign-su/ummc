export interface RegisterBudget {
    id: number,
    user_created_id: number,
    name: string,
    is_base: boolean,
    is_current: boolean,
    comment: string,
    delete_flg: boolean,
    editing: boolean
}
export interface History {
  id: number,
  user: string,
  date: string,
  type: string,
  amount: number,
  pir: number,
  smr: number,
  oto: number,
  other: number
}
export interface Commons {
    data: ObjectBudget[]
}
export interface ObjectBudgetNode {
  expandable: boolean;
  name: string;
  level: number;
}
export interface ObjectBudget {
        id: number,
        expandable: boolean,
        name: string,
        parent_id: null,
        delete_flg: boolean,
        node_parent: number,
        code_wbs: string,
        category_id: null,
        level: number,
        wbs_id: number,
        budget_source_id: null,
        register_budget_id: number,
        user_created_id: number,
        date_start: string,
        date_end: string,
        comment: string,
        total_price: number,
        smr_price: number,
        pir_price: number,
        oto_price: number,
        other_price: number,
        reserve_price: string,
        problem: string,
        common_budget_id: number,
        editing?: boolean;
        children?: ObjectBudget[];
}