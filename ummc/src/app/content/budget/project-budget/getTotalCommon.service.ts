import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import * as jmespath from 'jmespath';
import { ObjectBudget } from './registerBudget';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class GetTotalCommonService {

  constructor(private registerBudget: ApicontentService, private inflation: ApicontentService, public common:CommonService) { 
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
  }
  public getMathingBudget
  dataSouce: any
  getId: number
  summ: any
  getTotal() {
    this.registerBudget.getTotalCost(this.getId).subscribe(
      (data: ObjectBudget[]) => {
        const row = "@.total_cost";
        const summ = jmespath.search(data, row);
        this.summ = summ;
        console.log('00', this.summ)
      }
    );
  }
  getTotalInflation() {
    this.inflation.getInflation(this.getId).subscribe(
      (data: any[]) => {
        const row = "@.ammount_inflation";
        const inflation = jmespath.search(data, row);
        this.dataSouce = inflation;
        console.log('total', inflation)
      }
    );
  }
}
