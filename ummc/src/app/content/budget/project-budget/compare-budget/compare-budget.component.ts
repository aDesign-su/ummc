import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { getMathingBudget, getTotal, Total } from '../../total-project-costs/total';
import { CommonService } from '../common.service';
import * as jmespath from 'jmespath';
import { GetTotalCommonService } from '../getTotalCommon.service';

@Component({
  selector: 'app-compare-budget',
  templateUrl: './compare-budget.component.html',
  styleUrls: ['./compare-budget.component.css']
})
export class CompareBudgetComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, private formBuilder: FormBuilder,public tree:GetTotalCommonService ,private nma: ApicontentService,public common:CommonService) { 
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
  }

  ngOnInit() {
    this.getIsCurrent()
  }

  getId: number

  source: any[] = []
  displayedColumns: string[] = ['serial_number', 'name', 'total_price', 'pir_price', 'smr_price', 'oto_price', 'other_price'];
   
  private _transformer = (node: getMathingBudget, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      common_budget_id: node.common_budget_id,
      name: node.name,
      code_wbs: node.code_wbs,
      total_budget: node.total_budget,
      smr_price: node.smr_budget,
      pir_price: node.pir_budget,
      oto_price: node.oto_budget,
      other_price: node.other_budget,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Total>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Total) => node.expandable;

  getIsCurrent() {
   this.tree.getMathingBudget = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }
}
