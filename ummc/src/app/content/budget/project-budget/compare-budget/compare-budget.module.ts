import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompareBudgetComponent } from './compare-budget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    // CompareBudgetComponent
  ]
})
export class CompareBudgetModule { }
