import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { Reserve, ReserveData, ReserveManagement } from './reserve-management';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { ShowService } from 'src/app/helper/show.service';
import { SelectionModel } from '@angular/cdk/collections';
import { Estimated } from '../estimated/estimated';
import { CommonService } from '../project-budget/common.service';
declare var bootstrap: any;
@Component({
  selector: 'app-reserve-management',
  templateUrl: './reserve-management.component.html',
  styleUrls: ['./reserve-management.component.css']
})
export class ReserveManagementComponent implements OnInit {
  dataReserveManagementTable: MatTableDataSource<any> = new MatTableDataSource<any>();
  displayedColumnsReserve: string[] = ['serial_number', 'select', 'title', 'date', 'reserve_price', 'comments', 'status', 'action'];
  statuses: any[] = [
    {
      "id": 1,
      "name": 'Базовый',
    },
    {
      "id": 2,
      "name": 'Текущий',
    }
  ];
  FormAddEditReserve: FormGroup;
  RegisterBudgetData: any[] = [];
  editMode: boolean
  selectedReserve: number;

  isGeneratedMode = false;
  
  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) { }
  reserveData: ReserveManagement;
  ngOnInit() {
    localStorage.setItem('elementReserve', JSON.stringify(null));
    this.getDataReserveManagement()
    this.getRegisterBudget()
    this.initForm()

    this.reserveData = JSON.parse(localStorage.getItem('elementReserve'));
    if (this.reserveData) {
      console.log('Данные на этой странице', this.reserveData)
      this.getReserves(this.reserveData.id)
    } else {
      this.router.navigate(['/reserve-management']);
    }

  }
  setStatus = 1
  initForm(): void {
    this.FormAddEditReserve = this.fb.group({
      title: [''],
      reserve_price: [0],
      comments: [''],
      status: [null],
      register_budget_id: [0]
    });
  }
  
  
  add() {
    this.editMode = false
    this.FormAddEditReserve.reset()
    this.isGeneratedMode = false;
  }
  generate_reserv() {
    this.isGeneratedMode = true;
    this.FormAddEditReserve.reset()
  }

  private closeForm() {
    const offcanvasElement = document.getElementById('EditAddScrollingReserve');
    const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    if (offcanvasInstance) {
      offcanvasInstance.hide();
    }
  }
  // Полуить данные Резервы
  getDataReserveManagement(): void {
    this.params.getReserveManagement().subscribe(
      (data) => {
        console.log(data['data'])
        this.dataReserveManagementTable.data = data['data']

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getRegisterBudget(): void {
    this.params.getRegisterBudget().subscribe(
      (data) => {
        this.RegisterBudgetData = data['data']
        console.log('getRegisterBudget', this.RegisterBudgetData)

        // this.wbsData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  editReserve(element) {
    this.isGeneratedMode = false;
    console.log('element', element)
    this.editMode = true;
    this.selectedReserve = element.id;
    // Заполняем форму редактирования данными из элемента резерва
    this.FormAddEditReserve.setValue({
      title: element.title,

      reserve_price: element.reserve_price,
      comments: element.comments,
      status: element.status,
      register_budget_id: element.register_budget_id
    });
    console.log(this.FormAddEditReserve)
  }

  // Обновление
  updateReserveManagement(id: number, data: any): void {
    this.params.updateReserveManagement(id, data).subscribe(
      (data: any) => {
        this.getDataReserveManagement()
        this.closeForm()
        this.editMode = false;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );

  }
  generateOrUpdateReserve() {
    if (this.FormAddEditReserve.valid) {
      let formData = { ...this.FormAddEditReserve.value };
      console.log('Data to add:', formData);
      this.params.generateReserveManagement(formData).subscribe(
        (data: ReserveManagement[]) => {
          console.log(data);
          this.theme.openSnackBar('Резерв сгенерирован ');
          this.getDataReserveManagement();
          this.closeForm();
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }


  // Создания
  createReserve(data: any): void {
    console.log('Creating new reserve:', data);
    const formData: ReserveManagement = data
    console.log('Data to add:', formData);
    this.params.addReserveManagement(formData).subscribe(
      (data: ReserveManagement[]) => {
        console.log(data)
        this.theme.openSnackBar('Резерв добавлен');
        this.getDataReserveManagement();
        this.closeForm()
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  // Удаление
  delReserve(element) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {  // Если пользователь нажал "Да"
        this.params.delReserveManagement(element.id).subscribe(
          (data: any) => {
            this.getDataReserveManagement();
            this.theme.openSnackBar('Резерв удален');
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }
  // Подробнее
  moreDetailsReserve(element) {
    console.log(element)
    localStorage.setItem('elementReserve', JSON.stringify(element));
    this.router.navigate(['/reserve-management/reserve']);
    // this.getReserve(element.id)

  }
  // Получить все резервы одной записи 
  getReserve(id) {
    this.params.getReserve(id).subscribe(
      (data) => {
        console.log('РЕЗЕРВЫ ТЕКУЩЕЙ КАРТОЧКИ', data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }



  createOrUpdateReserve(): void {
    if (this.FormAddEditReserve.valid) {
      const formData = { ...this.FormAddEditReserve.value };
      if (this.editMode) {
        this.updateReserveManagement(this.selectedReserve, formData);
      } else {
        this.createReserve(formData);
      }
      this.selectedReserve = null;
    }
  }

  getReserves(element) {
    console.log(element.id)

    const selectId = element.id
    this.params.getReserve(selectId).subscribe(
      (data) => {
        const mapId = data['data'].map((x) => x.id)
        console.log(mapId)
        if (510 === mapId) {
          // Значения id одинаковы
          console.log('Значения id одинаковы.');
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  /** Сравнение */
  selection = new SelectionModel<Estimated>(true, []);
  clickedRows = new Set<Estimated>();
  clickedRowsArray: Estimated[] = [];

  isRowClicked(row: Estimated): boolean {
    return this.clickedRows.has(row);
  }
  toggleRow(row: Estimated) {
    if (this.isRowClicked(row)) {
      this.clickedRows.delete(row);
    } else {
      this.clickedRows.add(row);
    }

    this.clickedRowsArray = Array.from(this.clickedRows);
  }
    /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Estimated): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  isAllSelected() {
    const numSelected = this.selection.selected?.length;
    const numRows = this.dataReserveManagementTable.data?.length;
    return numSelected === numRows;
  }
  updateArray() {
    this.clickedRowsArray = Array.from(this.clickedRows);
  }
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.clickedRows.clear();
      return;
    }

    this.dataReserveManagementTable.data.forEach(row => {
      this.selection.select(row);
      this.clickedRows.add(row);
    });
  }
}
