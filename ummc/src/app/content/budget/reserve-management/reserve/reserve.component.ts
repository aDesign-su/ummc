import { Component, OnInit } from '@angular/core';
import { Reserve, ReserveData, ReserveManagement } from '../reserve-management';
import { ThemeService } from 'src/app/theme.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicontentService } from 'src/app/api.content.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { DatePipe } from '@angular/common';
import { ShowService } from 'src/app/helper/show.service';
declare var bootstrap: any;
@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.css']
})
export class ReserveComponent implements OnInit {
  reserveData: ReserveManagement;
  dataReserveTable: MatTableDataSource<Reserve> = new MatTableDataSource<Reserve>();
  displayedColumns: string[] = ['serial_number', 'code_wbs', 'name', 'plan_price', 'reserve_price', 'budget_source_name', 'budget_source_percent_rate', 'action'];
  FormAddEditReserveItem: FormGroup;
  TransactionReserveForm: FormGroup;
  wbsData: any[] = [];
  BudgetSourceData: any[] = [];
  editMode = false;
  selectedReserve: number;


  remainingReserve: number;
  initialReserve: number;
  constructor(public theme: ThemeService, public show: ShowService, public params: ApicontentService, private fb: FormBuilder, private datePipe: DatePipe, private router: Router, private route: ActivatedRoute, private dialog: MatDialog,) {
    this.reserveData = JSON.parse(localStorage.getItem('elementReserve'));
    if (this.reserveData) {
      console.log('Данные на этой странице', this.reserveData)
      this.getReserve(this.reserveData.id)
    } else {
      this.router.navigate(['/reserve-management']);
    }

  }

  ngOnInit() {
    this.initForm()
  }

  initForm(): void {
    this.FormAddEditReserveItem = this.fb.group({
      wbs: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      plan_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      reserve_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      budget_source: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      register_reserve: this.reserveData.id
    });
    this.TransactionReserveForm = this.fb.group({
      id: [],
      // total_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      // smr_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      // pir_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      // oto_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
      other_price: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
    })

  }
  add() {
    this.initForm()
    this.editMode = false
  }
  private closeForm() {
    const offcanvasElement = document.getElementById('EditAddScrollingReserve');
    const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    if (offcanvasInstance) {
      offcanvasInstance.hide();
    }
  }

  // Получить все резервы одной записи 
  getReserve(id) {
    this.params.getReserve(id).subscribe(
      (data) => {
        this.getWbs()
        this.BudgetSource()
        this.dataReserveTable.data = data['data']
        console.log(this.dataReserveTable.data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  createOrUpdateReserve() {
    if (this.FormAddEditReserveItem.valid) {
      const formData = { ...this.FormAddEditReserveItem.value };
      console.log(formData)
      if (this.editMode) {
        this.updateReserve(this.selectedReserve, formData);
      } else {
        this.createReserve(formData);
      }
    }
  }
  // Создать 
  createReserve(formData) {
    console.log('Data to add:', formData);
    this.params.addReserve(formData).subscribe(
      (data) => {
        console.log(data)
        this.theme.openSnackBar('Резерв добавлен');
        this.getReserve(this.reserveData.id)
        this.closeForm()
        this.editMode = false;

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }



  // Изменить резерв
  editReserve(element: any) {
    console.log(element)
    this.selectedReserve = element.common_budget_id;
    this.editMode = true;
    this.FormAddEditReserveItem.patchValue({
      wbs: element.wbs_id,//+
      plan_price: element.plan_price,//+
      reserve_price: element.reserve_price,//+
      budget_source: element.budget_source_id
    });
    console.log(this.FormAddEditReserveItem.value)

  }
  // Обновить резерв
  updateReserve(id, data) {
    console.log(id, data)
    this.params.updateReserve(id, data).subscribe(
      (data: any) => {
        console.log(data)
        this.getReserve(this.reserveData.id)
        this.theme.openSnackBar('Резерв обновлен');
        this.closeForm()
        this.editMode = false;
        this.selectedReserve = null;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  // Удаление резерва
  delReserve(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {  // Если пользователь нажал "Да"
        this.params.deliteReserve(id).subscribe(
          (data: any) => {
            this.getReserve(this.reserveData.id)
            this.theme.openSnackBar('Резерв удален');
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }





  //Получаем все пакеты СВЗ
  getWbs(): void {
    const filter = { "name": "", "level": 3, "parent_id": null, "type_node": "" };
    this.params.getSvzWbsByLevelFilter(filter).subscribe(
      (data) => {
        console.log(data)
        this.wbsData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  // Получить источники бюджета
  BudgetSource(): void {
    this.params.getBudgetSource().subscribe(
      (data) => {
        console.log(data)
        this.BudgetSourceData = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  TransactionReserve(element) {
    this.TransactionReserveForm.reset();
    this.initialReserve = element.reserve_price;
    this.remainingReserve = this.initialReserve;
    this.TransactionReserveForm.patchValue({
      id: element.id,
      // total_price: [null],
      // smr_price: [null],
      // pir_price: [null],
      // oto_price: [null],
      other_price: [null],
    });
    console.log(this.TransactionReserveForm.value)
  }

  calculateRemainingReserve() {
    const formValues = this.TransactionReserveForm.value;
    let totalDeduction = 0;

    // totalDeduction += formValues.total_price || 0;
    // totalDeduction += formValues.smr_price || 0;
    // totalDeduction += formValues.pir_price || 0;
    // totalDeduction += formValues.oto_price || 0;
    totalDeduction += formValues.other_price || 0;

    // Вычитание из остатка резерва
    this.remainingReserve = this.initialReserve - totalDeduction;
  }

  addTransaction(data) {
    let formData = this.TransactionReserveForm.value;

    Object.keys(formData).forEach(key => {
      if (formData[key] === null) {
        formData[key] = 0;
      }
    });

    console.log(formData);
    this.params.addTransactionReserve(formData).subscribe(
      (data) => {
        console.log(data);
        this.theme.openSnackBar('Транзакция добавлена');
        // this.getReserve(this.reserveData.id);
        const offcanvasElement = document.getElementById('TransactionReserve');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }


        this.reserveData = JSON.parse(localStorage.getItem('elementReserve'));
        if (this.reserveData) {
          console.log('Данные на этой странице', this.reserveData)
          this.getReserve(this.reserveData.id)
        } else {
          this.router.navigate(['/reserve-management']);
        }

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
