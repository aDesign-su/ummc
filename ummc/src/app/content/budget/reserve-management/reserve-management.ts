export interface ReserveManagement {
    id: number;
    title: string;
    date: string;
    reserve_price: number;
    comments: string;
    status: string;
    register_budget_id: number | null;
    editing?: any;
}

export interface ReserveData {
    data: ReserveManagement[];
}

export interface Reserve {
    id: number;
    name: string;
    parent_id: number;
    delete_flg: boolean;
    node_parent: number;
    code_wbs: string;
    code_assoi: string;
    code_sap: string;
    category_id: any;
    level: number;
    wbs_id: number;
    plan_price: number;
    budget_source_id: number;
    budget_source_name:string;
    register_reserve_id: number;
    reserve_price: number;
    common_budget_id: number;
    
}