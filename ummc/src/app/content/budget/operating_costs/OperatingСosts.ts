export interface OperatingCostItemBase {
    id: number;
    WBS_code:string;
    operating_cost_item: string;
    planned_cost: string | number;
    start_plan: string | null;
    ending_plan: string | null;
    description: string | null;
  }
  
  export interface OperatingCostItemPlan extends OperatingCostItemBase {
    planned_cost: number;
    description: string;
    editing: boolean;
  }
  
  export interface OperatingCostItemActual extends OperatingCostItemBase {
    actual_cost: string | null;
  }