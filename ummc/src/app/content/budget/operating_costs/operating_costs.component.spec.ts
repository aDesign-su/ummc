/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Operating_costsComponent } from './operating_costs.component';

describe('Operating_costsComponent', () => {
  let component: Operating_costsComponent;
  let fixture: ComponentFixture<Operating_costsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Operating_costsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Operating_costsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
