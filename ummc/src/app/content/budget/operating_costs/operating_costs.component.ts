import { Component, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { OperatingCostItemActual, OperatingCostItemPlan } from './OperatingСosts';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from '../project-budget/common.service';
declare var bootstrap: any;

@Component({
  selector: 'app-operating_costs',
  templateUrl: './operating_costs.component.html',
  styleUrls: ['./operating_costs.component.css']
})
export class Operating_costsComponent implements OnInit {
  // Общие поля
  displayedColumnsBase: string[] = ['id', 'WBS_code', 'operating_cost_item', 'planned_cost', 'start_plan', 'ending_plan'];

  // Добавляем дополнительные поля
  displayedColumnsPlan: string[] = [...this.displayedColumnsBase, 'description', 'action'];
  displayedColumnsActual: string[] = [...this.displayedColumnsBase, 'actual_cost', 'description', 'action'];

  // Инициализация MatTableDataSource
  dataPlanTableSource = this.initTable<OperatingCostItemPlan>();
  dataActualTableSource = this.initTable<OperatingCostItemActual>();
  @ViewChild('PaginatorPlan') paginatorForRiskPlan: MatPaginator;
  @ViewChild('PaginatorAnalog') paginatorForRiskActual: MatPaginator;

  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }

  FormAddEditOperatingCostsPlan: FormGroup;
  FormEditOperatingCostsActual: FormGroup;
  editMode = false; 
  selectedOperatingCost: number; 



  constructor(public theme: ThemeService, public params: ApicontentService, public isTitle: CommonService, private fb: FormBuilder, private datePipe: DatePipe,) {
    this.FormAddEditOperatingCostsPlan = this.fb.group({
      operating_cost_item: ['', Validators.required],
      planned_cost: ['', Validators.required],
      start_plan: ['', Validators.required],
      ending_plan: ['', Validators.required],
      description: ['']
    });
    this.FormEditOperatingCostsActual = this.fb.group({
      operating_cost_item: ['', Validators.required],
      planned_cost: ['', Validators.required],
      start_plan: ['', Validators.required],
      ending_plan: ['', Validators.required],
      actual_cost: ['', Validators.required],
      description: ['']
    });
  }
  ngOnInit() {
    this.getDataOperatingCosts()

  }
  ngAfterViewInit() {
    this.dataPlanTableSource.paginator = this.paginatorForRiskPlan;
    this.dataActualTableSource.paginator = this.paginatorForRiskActual;
  }
  add() {
    this.editMode = false
    this.FormAddEditOperatingCostsPlan.reset()
  }


  //Получить данные
  getDataOperatingCosts(): void {
    this.params.getDataOperatingCostsPlan().subscribe(
      (data: OperatingCostItemPlan[]) => {
        this.dataPlanTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
    this.params.getDataOperatingCostsActual().subscribe(
      (data: OperatingCostItemActual[]) => {
        this.dataActualTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  private transformDate(date: any): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  public createOrUpdateOperatingCost(): void {
    if (this.FormAddEditOperatingCostsPlan.valid) {
      const formData = { ...this.FormAddEditOperatingCostsPlan.value };
      if (formData.start_plan) {
        formData.start_plan = this.transformDate(formData.start_plan);
      }
      if (formData.ending_plan) {
        formData.ending_plan = this.transformDate(formData.ending_plan);
      }
      if (this.editMode) {
        this.updateOperatingCosts(this.selectedOperatingCost, formData);
      } else {
        this.createOperatingCosts(formData);
      }

    }
  }

// Обновить Акткльные 
  UpdateOperatingCostActual(): void {
    if (this.FormEditOperatingCostsActual.valid) {
      const formData = { ...this.FormEditOperatingCostsActual.value };
      if (formData.start_plan) {
        formData.start_plan = this.transformDate(formData.start_plan);
      }
      if (formData.ending_plan) {
        formData.ending_plan = this.transformDate(formData.ending_plan);
      }

      this.params.updateOperatingCostsActual(this.selectedOperatingCost, formData).subscribe(
        (data: any) => {
          console.log(data)
          this.theme.openSnackBar('Сохранено')
          this.getDataOperatingCosts()
          const offcanvasElement = document.getElementById('editActualScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );

    }

  }



  //Добавить План
  createOperatingCosts(data): void {
    if (this.FormAddEditOperatingCostsPlan.valid) {
      const formData: OperatingCostItemPlan = data
      this.params.addOperatingCostsPlan(formData).subscribe(
        (data: OperatingCostItemPlan[]) => {
          console.log(data)
          this.theme.openSnackBar('Добавлена операционная затрпата')
          this.getDataOperatingCosts()
          const offcanvasElement = document.getElementById('addScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.editMode = false;
          this.selectedOperatingCost = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  // Обновить План
  updateOperatingCosts(id: number, data: OperatingCostItemPlan): void {
    if (this.FormAddEditOperatingCostsPlan.valid) {
      const formData: OperatingCostItemPlan = data
      if (formData.start_plan) {
        formData.start_plan = this.datePipe.transform(formData.start_plan, 'yyyy-MM-dd');
      }
      if (formData.ending_plan) {
        formData.ending_plan = this.datePipe.transform(formData.ending_plan, 'yyyy-MM-dd');
      }
      console.log('Upadte to add:', formData);
      this.params.updateOperatingCostsPlan(id, formData).subscribe(
        (data: any) => {
          console.log(data)
          this.theme.openSnackBar('Сохранено')
          this.getDataOperatingCosts()
          const offcanvasElement = document.getElementById('addScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.editMode = false;
          this.selectedOperatingCost = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }

  editOperatingCostPlan(element: OperatingCostItemPlan): void {
    this.editMode = true;
    this.selectedOperatingCost = element.id;
    this.FormAddEditOperatingCostsPlan.setValue({
      operating_cost_item: element.operating_cost_item,
      planned_cost: element.planned_cost,
      start_plan: element.start_plan ? new Date(element.start_plan) : null,
      ending_plan: element.ending_plan ? new Date(element.ending_plan) : null,
      description: element.description,
    });
  }


  editOperatingCostActual(element: OperatingCostItemActual): void {
    this.selectedOperatingCost = element.id;
    this.FormEditOperatingCostsActual.setValue({
      operating_cost_item: element.operating_cost_item,
      planned_cost: element.planned_cost,
      start_plan: element.start_plan ? new Date(element.start_plan) : null,
      ending_plan: element.ending_plan ? new Date(element.ending_plan) : null,
      actual_cost: element.actual_cost,
      description: element.description,
    });
    this.FormEditOperatingCostsActual.get('operating_cost_item').disable();
    this.FormEditOperatingCostsActual.get('planned_cost').disable();
    this.FormEditOperatingCostsActual.get('start_plan').disable();
    this.FormEditOperatingCostsActual.get('ending_plan').disable();
  }


  //Удалить План
  removeOperatingCost(id) {
    this.params.delOperatingCostPlan(id).subscribe(
      (response) => {
        // Удаляем элемент локально
        this.theme.openSnackBar('Операционная затрата удалена')
        const index = this.dataPlanTableSource.data.findIndex(item => item.id === id);
        if (index > -1) {
          this.dataPlanTableSource.data.splice(index, 1);
          this.dataPlanTableSource._updateChangeSubscription(); 
          this.dataActualTableSource.data.splice(index, 1);
          this.dataActualTableSource._updateChangeSubscription(); 
        }
      },
      (error: any) => {
        console.error('Failed to delete :', error);
      }
    );
  }
}
