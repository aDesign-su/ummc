export interface Estim {
    expandable: boolean;
    level: number;
}
export interface getEstim {
    id: number,
    title: string,
    date_end_plan: string,
    date_start_plan: string,
    version: string,
    comments: string,
    currency: number,
    cost_total: string,
    expandable: boolean,
    level: number,
    level_name: string,
    total_cost: number,
    editing?: any,
    children?: getEstim[];
}
export interface getProject {
    id: number,
    level: number,
    title: string,
    version: string,
    date_created: string,
    smr_cost: string,
    pir_cost: string,
    oto_cost: string,
    etc_cost: string,
    total_cost: string,
    cost: null,
    comments: string,
    currency_id: null,
    currency: string,
    date_start_plan: null,
    date_end_plan: null,
    expandable: boolean,
    editing?: any,
    children?: getProject[];
}

export interface getLevel1 {
    id: number;
    level: number;
    title: string;
    cost: number | null;
    smr_cost: number;
    pir_cost: string;
    oto_cost: string;
    etc_cost: string;
    total_cost: string;
    children: getLevel1[];
}





export interface Estimated {
    id: number,
    id_assoi: string,
    id_SAP: string,
    id_srd: string,
    title: string,
    version: string,
    comments: string,
    macro: number,
    cost: number,
    currency: number,
    position: number,
}
export interface workPackage {
    id: number,
    title: string,
    type: number,
    contractor: string,
    cost_total: number,
    cost_pp: number,
    mh: number,
    comments: string,
    project: number,
    region: number,
    estimated_budget: null
}
export interface analog { 
    id: number,
    title: string,
    constr_smr: number,
    date_start: string,
    date_end: string,
    eng_pir: number,
    tech_description: string,
    cost: number,
    type: number,
    region: number,
    estimated_budget: null
}
export interface equipment {
    id: number,
    title: string,
    type: string,
    tech_description: string,
    cost: number,
    description: string,
    project: number,
    manufacturer: number,
    estimated_budget: number
 }