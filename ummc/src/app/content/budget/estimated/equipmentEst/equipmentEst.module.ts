import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentEstComponent } from './equipmentEst.component';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
  declarations: [
    // EquipmentEstComponent,
  ]
})
export class EquipmentEstModule { }
