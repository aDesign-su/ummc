import { Component, OnInit } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { EstimatedService } from '../estimated.service';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { analogBudgets } from 'src/app/content/references/analogy/analogy';

@Component({
  selector: 'app-equipmentEst',
  templateUrl: './equipmentEst.component.html',
  styleUrls: ['./equipmentEst.component.css']
})
export class EquipmentEstComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, private estimService: ApicontentService, public estimated:EstimatedService,private formBuilder: FormBuilder) {
    this.estimated.currentSetId.subscribe(id => {
      this.estId = id;
    })
    this.editEquipmenForm = this.formBuilder.group({
      title: '',
      manufacturer: '',
      tech_description: '',
      cost: '',
      description: '',
      analog_project: '',
      type: '',
      project: '',
      currency: ''
    });
   }
   editEquipmenForm!: FormGroup

  ngOnInit() {
    this.estimated.getEstimatedId(this.estId)
  // Селект Type Equipment
  this.estimService.getAnalogTypeList().subscribe(
    (data: analogBudgets[]) => {
      this.getAnalogTypeEquipment = data
    },
  );
 // Селект Type Currency
    this.estimService.getCurrencyList().subscribe(
      (data: analogBudgets[]) => {
        this.getCurrencyList = data
      },
    );
  }
  closed() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  getAnalogTypeEquipment: any[] = []
  AnalogTypeEquipment!: string;
  getCurrencyList: any[] = []
  CurrencyList!: string;

  selectedType: any;
  selectedCurrency: any;
  estId: number
  Equ: any;

  editEquipment(Equ: any) {
    this.Equ = Equ.id; // Установим значение свойства Equ
    Equ.editing = true;
    const selectedCurrency = this.getCurrencyList.find(currency => currency.title === Equ.currency);
    const selectedType = this.getAnalogTypeEquipment.find(type => type.title === Equ.type);
    // console.log(this.Equ)
    if (selectedType) {
      // Устанавливаем значение 'type' в выбранный тип
      this.editEquipmenForm.get('type').setValue(selectedType.id);
    }
    if (selectedCurrency) {
      // Устанавливаем значение 'currency' в выбранную валюту
      this.editEquipmenForm.get('currency').setValue(selectedCurrency.id);
    }

    this.editEquipmenForm.patchValue({
      title: Equ.title,
      manufacturer: Equ.manufacturer,
      tech_description: Equ.tech_description,
      cost: Equ.cost,
      description: Equ.description,
      analog_project: Equ.analog_project,
      project: Equ.project
    })
  }
  saveEquipment(Equ: any) {
    const selectedEstimatedBudget = Equ.estimated_budget;
    const id = Equ.id;
    
    const updatedData = {
      title: Equ.title,
      type: Equ.type,
      tech_description: Equ.tech_description,
      cost: Equ.cost,
      description: Equ.description,
      // project: Equ.project,

      manufacturer: Equ.manufacturer,
      estimated_budget: selectedEstimatedBudget
    };
    let editedObject = this.editEquipmenForm.value;
    this.estimService.editEquipment(this.Equ, editedObject).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        this.estimated.getEstimatedId(this.estId)
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.show.showBlock[8] = false;
        Equ.editing = false;
      },
    );
  }
  delEquipment(Equ: any) {
    const id = Equ.id;
    this.estimService.delEquipment(id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Оборудование удалено.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.estimated.getEstimatedId(this.estId)
      },
    );
  }
}
