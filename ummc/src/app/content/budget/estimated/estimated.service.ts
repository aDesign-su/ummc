import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';
import { Estimated } from './estimated';
import * as jmespath from 'jmespath';
import { Ratio } from '../../references/ratio/ratio';

@Injectable({
  providedIn: 'root'
})
export class EstimatedService {
  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  private setTitleSource = new BehaviorSubject<string | null>(null);
  currentSetTitle = this.setTitleSource.asObservable();
  private setTitleKey = 'setTitle';

  constructor(private estimService: ApicontentService) {
    const estId = this.getSetIdFromLocalStorage();
    const estTitle = this.getSetTitleFromLocalStorage();
    if (estId !== null) {
      this.setIdSource.next(estId);
    }
    if (estTitle !== null) {
      this.setTitleSource.next(estTitle);
    }
  }
  private getSetTitleFromLocalStorage(): string | null {
    const estTitle = localStorage.getItem(this.setTitleKey);
    return localStorage.getItem(this.setTitleKey);
  }
  private getSetIdFromLocalStorage(): number | null {
    const estId = localStorage.getItem(this.setIdKey);
    return estId ? +estId : null;
  }
  setSetTitle(title: string) {
    localStorage.setItem(this.setTitleKey, title);
    this.setTitleSource.next(title);
  }
  setSetId(id: number) {
    localStorage.setItem(this.setIdKey, id.toString());
    this.setIdSource.next(id);
  }
  clearSetTitle() {
    localStorage.removeItem(this.setTitleKey);
    this.setTitleSource.next(null);
  }
  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }
  ratio: Ratio[] = []
  getWorkPackage: any = []
  getEquipment: any = []
  changeIndex: boolean
  public toggleStyle(): void {
    this.changeIndex = true;
  }
  public toggleStyleFalse(): void {
    this.changeIndex = false;
  }

  getRatio() {
    this.estimService.getRatio().subscribe(
      (data: Ratio[]) => {
        this.ratio = data
      }
    );
  }
  
  getEstimatedId(EstimatedId: number): void {
    // this.estimService.getEstimatedId(EstimatedId).subscribe(
    //   (data: Estimated[]) => {

    //     const wrk = "[*].work_package|[]";
    //     this.getWorkPackage = jmespath.search(data, wrk);

    //     const equ = "[*].equipment|[]";
    //     this.getEquipment = jmespath.search(data, equ);

    //   },
    //   (error: any) => {
    //     console.error('Error fetching data:', error);
    //   }
    // );
  }
}
