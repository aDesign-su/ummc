import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EstimatedService } from "../estimated.service";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class More {  
    constructor(private formBuilder: FormBuilder,private estimated:EstimatedService) { 
      this.estimated.currentSetId.subscribe(id => {
      this.estId = id;
    });
  }
  initializeForm1() {
    this.equipmenForm1 = this.formBuilder.group({
      "title": ['', Validators.required],
      "manufacturer": '',
      "tech_description": '',
      "cost": [null, [Validators.maxLength(20), Validators.pattern(/^[0-9,.]+$/)]],
      "description": '',
      "analog_project": '',
      "type": null,
      "project": null,
      "currency": null,
      "estimated_budget": null
    });
  }
  initializeForm2() {
    this.workPackage1 = this.formBuilder.group({
      "title": [''],
      "contractor": [''],
      "cost_total": [null],
      "mh": [null],
      "comments": [''],
      "volume": [null],
      "unit_of_measure": [''],
      "cost_pp": [null],
      "type": [null],
      "project": [null],
      "region": [null],
      "currency": [null],
      "estimated_budget": [null]
    });
  }
  initializeForm3() {
    this.equipmenForm2 = this.formBuilder.group({
      "title": ['', Validators.required],
      "manufacturer": [''],
      "tech_description": [''],
      "cost": [null, [Validators.maxLength(20), Validators.pattern(/^[0-9,.]+$/)]],
      "description": [''],
      "analog_project": [''],
      "type": [null],
      "project": [null],
      "currency": [null],
      "estimated_budget": this.estId
    });
  }
  initializeForm4() {
    this.workPackage2 = this.formBuilder.group({
      "title": ['', Validators.required],
      "contractor": '',
      "cost_total": [null, [Validators.maxLength(20), Validators.pattern(/^[0-9,.]+$/)]],
      "mh": [null, Validators.required],
      "comments": '',
      "volume": null,
      "unit_of_measure": '',
      "cost_pp": null,
      "type": null,
      "project": null,
      "region": null,
      "currency": null,
      "estimated_budget": this.estId
    });
  }
  initializeForm5() {
    this.ratio = this.formBuilder.group({
      "number": ['', Validators.required],
      "rate": [null, Validators.required],
      "title": ['', Validators.required],
      "comments": "",
      "estimated_budget": this.estId
    });
  }
  equipmenForm1!: FormGroup;
  equipmenForm2!: FormGroup;
  workPackage1!: FormGroup;
  workPackage2!: FormGroup;
  ratio!: FormGroup;
  estId: number

// Главный селект
  selectedTitleOA: any
  selectedTitlePW: any
  selectedTitleEQ: any
  selectedTitleCF: any

// Переменные для каждого поля
  selectedTitle1: any
  selectedContractor1: any
  selectedCost_total1: any
  selectedMh1: any
  selectedComments1: any
  selectedVolume1: any
  selectedUnit_of_measure1: any
  selectedCost_pp1: any
  selectedType1: any
  selectedProject1: any
  selectedRegion1: any
  selectedCurrency1: any

  selectedTitle2: any
  selectedManufacturer2: any
  selectedTech_description2: any
  selectedCost2: any
  selectedDescription2: any
  selectedAnalog_project2: any
  selectedType2: any
  selectedProject2: any
  selectedCurrency2: any
  selectedEstimated_budget2: any

  selectedTitle3: any
  selectedSmr_comp3: any
  selectedConstr_smr3: any
  selectedDate_start3: any
  selectedDate_end3: any
  selectedPir_comp3: any
  selectedEng_pir3: any
  selectedOto_cost3: any
  selectedEtc_cost3: any
  selectedTech_description3: any
  selectedCost3: any
  selectedProject3: any
  selectedType3: any
  selectedRegion3: any
  selectedCurrency3: any
  selectedEstimated_budget3: any

  selectedNumber4: any
  selectedRate4: any
  selectedTitle4: any
  selectedComments4: any
  selectedEstimated_budget4: any
}