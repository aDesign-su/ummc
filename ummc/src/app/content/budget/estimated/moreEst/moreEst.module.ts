import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoreEstComponent } from './moreEst.component';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { More } from './more';

@NgModule({
  declarations: [
    // MoreEstComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
})
export class MoreEstModule { }
