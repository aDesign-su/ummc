import { Component, Inject, OnInit } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { Estim, Estimated, analog, getLevel1, getProject, workPackage } from '../estimated';
import * as jmespath from 'jmespath';
import * as moment from 'moment';
import { EstimatedService } from '../estimated.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Equipments, analogBudgets } from 'src/app/content/references/analogy/analogy';
import { DatePipe } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { More } from './more';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-moreEst',
  templateUrl: './moreEst.component.html',
  styleUrls: ['./moreEst.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
    animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0', minHeight: '0', })), // Include minHeight and display properties
      state('expanded', style({ height: '*', })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MoreEstComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService,private estimService: ApicontentService, public estimated:EstimatedService,private formBuilder: FormBuilder,public analogService:ApicontentService,private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string, private datePipe: DatePipe, public getMore: More) {
    this.estimated.currentSetId.subscribe(id => {
      this.estId = id;
      this.getMore.initializeForm1();
      this.getMore.initializeForm2();
      this.getMore.initializeForm3();
      this.getMore.initializeForm4();
      this.getMore.initializeForm5();
    });
    this.estimated.currentSetTitle.subscribe(title => {
      this.title = title;
    });
    this.factorForm = this.formBuilder.group({
      number: [''],
      rate: [null],
      title: [''],
      comments: [''],
      estimated_object: [null],
    })
    this.factorReferenceForm = this.formBuilder.group({
      estimated_object_id: ['',Validators.required],
      complexity_factor_id: [null]
    })
    this.directionForm = this.formBuilder.group({
      title: ['',Validators.required],
      estimated_budget: [this.estId]
    })
    this.objectReferencesForm = this.formBuilder.group({
      estimated_budget_direction_id: ['',Validators.required],
      analog_budget_id: [null]
    })
    this.packageReferencesForm = this.formBuilder.group({
      estimated_object_id: ['',Validators.required],
      analog_package_id: [null]
    })
    this.equipReferencesForm = this.formBuilder.group({
      estimated_object_id: ['',Validators.required],
      analog_equipment_id: [null]
    })
    this.objectForm = this.formBuilder.group({
      title: ['',Validators.required],
      smr_comp: [''],
      smr_cost: [0,Validators.required],
      date_start: [null],
      date_end: [null],
      pir_comp: [''],
      pir_cost: [0,Validators.required],
      oto_cost: [0,Validators.required],
      etc_cost: [0,Validators.required],
      tech_description: [''],
      project: [''],
      is_calculated_flag: [false],
      estimated_budget_direction: [null],
      type: [null],
      region: [null],
      currency: [null]
    })
    this.packageForm = this.formBuilder.group({
      title: ['',Validators.required],
      contractor: [''],
      mh: [0],
      comments: ['-'],
      volume: [1],
      smr_cost: [0,Validators.required],
      pir_cost: [0,Validators.required],
      oto_cost: [0,Validators.required],
      etc_cost: [0,Validators.required],
      estimated_analog: [null],
      type: [null],
      region: [null],
      unit_of_measure: [null],
      currency: [null]
    })
    this.equipForm = this.formBuilder.group({
      title: ['',Validators.required],
      manufacturer: [''],
      tech_description: [''],
      smr_cost: [0,Validators.required],
      pir_cost: [0,Validators.required],
      oto_cost: [0,Validators.required],
      etc_cost: [0,Validators.required],
      description: [''],
      analog_project: [''],
      estimated_analog: [null],
      type: [null],
      currency: [null]
    })
    this.analogForm = this.formBuilder.group({
      "title": "",
      "smr_comp": "",
      "constr_smr": null,
      "date_start": null,
      "date_end": null,
      "pir_comp": "",
      "eng_pir": null,
      "oto_cost": null,
      "etc_cost": null,
      "tech_description": "",
      "cost": null,
      "project": "",
      "type": null,
      "region": null,
      "currency": null,
      "estimated_budget": this.estId
    });

    this.editEquipmenForm = this.formBuilder.group({
      title: ['', Validators.required],
      manufacturer: '',
      tech_description: '',
      cost: [null, [Validators.maxLength(20), Validators.pattern(/^[0-9,.]+$/)]],
      description: '',
      analog_project: '',
      type: '',
      project: '',
      currency: ''
    });

    this.editWorkPackageForm = this.formBuilder.group({
      title: '',
      contractor: '',
      cost_total: '',
      mh: '',
      comments: '',
      volume: '',
      unit_of_measure: '',
      cost_pp: '',
      type: '',
      project: '',
      region: '',
      currency: ''
    });
    this.editAnalogForm = this.formBuilder.group({
      title: "",
      smr_comp: "",
      constr_smr: null,
      date_start: null,
      date_end: null,
      pir_comp: "",
      eng_pir: null,
      oto_cost: null,
      etc_cost: null,
      tech_description: "",
      cost: 0,
      project: "",
      type: "",
      region: "",
      currency: ""
    });
    this.toWbs = this.formBuilder.group({
      estimated_budget_id: [this.estId], 
      code_wbs: [0]
    });
    this.goToWbs = this.formBuilder.group({
      parent_id: [],
      estimated_budget_id: [],
      code_wbs: [],
      // category_id: [],
      budget_source: [],
    });
   }
   directionForm!: FormGroup
   objectForm!: FormGroup
   objectReferencesForm!: FormGroup
   packageReferencesForm!: FormGroup
   equipReferencesForm!: FormGroup
   factorReferenceForm!: FormGroup
   factorForm!: FormGroup
   packageForm!: FormGroup
   equipForm!: FormGroup
   toWbs!: FormGroup
   goToWbs!: FormGroup
   editEquipmenForm!: FormGroup
   editWorkPackageForm!: FormGroup
   editAnalogForm!: FormGroup

   selectRub: number = 1

  ngOnInit() {
    this.getDataSource()
  //   this.getEstimatedId(this.estId)
  //   this.getRatio()
  //   this.getEstimated()
  //   this.getBudget()
  //   this.DataWorkPackage()
    // Селект Напаравления
      this.estimService.getDirectionList().subscribe(
        (data: any[]) => {
          this.getDirectionList = data
        },
      );
    // Селект Region
      this.estimService.getRegionList().subscribe(
        (data: analogBudgets[]) => {
          this.getRegionList = data
        },
      );
    // Селект Type Currency
      this.analogService.getCurrencyList().subscribe(
        (data: analogBudgets[]) => {
          this.getCurrencyList = data
        },
      );
    // Селект Analog Type
      this.analogService.getAnalogList().subscribe(
        (data: analogBudgets[]) => {
          this.getAnalogType = data
        },
      );
    // Селект Unit Of Measure
    this.analogService.getUnitList().subscribe(
      (data: analogBudgets[]) => {
        this.getUnitList = data
      },
    );
    // Селект Analog Budget List
    this.analogService.getAnalogBudget().subscribe(
      (data: analogBudgets[]) => {
        this.getAnalogList = data
      },
    );
    // Селект Analog Budget Package List
    this.analogService.getAnalogId().subscribe(
      (data: analogBudgets[]) => {
        this.getPackegeList = data
      },
    );
    // Селект Analog Budget Equipment List
    this.analogService.getEquipId().subscribe(
      (data: analogBudgets[]) => {
        this.getEquipList = data
      },
    );
    // Селект Complexity Factor List
    this.analogService.getComplexityList().subscribe(
      (data: analogBudgets[]) => {
        this.getFactorList = data
      },
    );
  }

  addToWbs() {
    const wbs = this.toWbs.value;
    console.log(wbs)
    this.analogService.addFilter(wbs).subscribe(
      (response: any) => {
        this.theme.access = 'Отправлено в бюджет.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        console.log(response)
      },
    );
  }
  addWbs() {
    const wbs = this.goToWbs.value;
    // console.log(wbs)
    this.analogService.addToWbs(wbs).subscribe(
      (response: any) => {
        // this.getToWbs = response;
          this.show.showBlock[12] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Отправлено в бюджет.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.analogForm.reset()
        // this.getEstimatedId(this.estId)
        // console.log(response)
      },
    );
  }

  createElem(id:number) {
    if(id) {
      this.setEqpId = id
    }
  }

// Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';

    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  
  analogForm!: FormGroup;
  activeButton = 1
  setEqpId: number  // id оценочного бюджета
  estId: number // id предпологаемого бюджета
  title: string

  getAnalogBudgets: any = []
  getAnalog: any = []
  getEquip: any = []

  getCategory: any[] = []
  categoryList!: string;
  getBudgetSource: any[] = []
  budgetSourceList: any = 1
  getToWbs: any[] = []
  wbsList!: string;

  getDirectionList: any[] = []
  getUnitList: any[] = []
  getAnalogList: any[] = []
  getPackegeList: any[] = []
  getEquipList: any[] = []
  getFactorList: any[] = []

  getAnalogTypeEquipment: any[] = []
  AnalogTypeEquipment!: string;
  
  getAnalogType: any[] = []
  AnalogType!: string;

  getCurrencyList: any[] = []
  CurrencyList!: string;

  getTypePackageList: any[] = []
  TypePackageList!: string;

  getRegionList: any[] = []
  RegionList!: string;

  fields1: any[] = []
  fields2: any[] = []
  fields3: any[] = []
  fields4: any[] = []

  selectedmh: any; 
  selectedComments: any; 
  selectedProject: any; selectedRegions: any; 
  selectedEstimatedBudget:any;
  selectedType: any;

  selectedContractor: any
  selectedCostTotal: any
  selectedCostPp: any

  Equ: any;
  Wor: any;
  Est: any
  // Пакеты работ
  onSelectionChange1() {
    const selectedOption = this.fields1.find((option) => option.title === this.getMore.selectedTitlePW);
    // Создаем объект для хранения соответствия title к id
    this.getTypePackageList.forEach((equipment) => {
      this.workPackageToIdMap[equipment.title] = equipment.id;
    });
    // Сравниваем поля title и type и выбираем id
    const result = this.fields1.map((item) => ({
      id: this.workPackageToIdMap[item.type] || this.workPackageToIdMap[item.title],
    }));

    // Создаем объект для хранения соответствия currency к id
    this.getCurrencyList.forEach((currency) => {
      this.currencyToIdMap[currency.title] = currency.id;
    });
    // Сравниваем поле currency и выбираем id
    const upshot = this.getAnalogTypeEquipment.map((item) => ({
      id: this.currencyToIdMap[item.currency] || this.currencyToIdMap[item.title],
    }));

    // Создаем объект для хранения соответствия region к id
    this.getRegionList.forEach((currency) => {
      this.regionToIdMap[currency.title] = currency.id;
    });
    // Сравниваем поле region и выбираем id
    const res = this.getAnalogTypeEquipment.map((item) => ({
      id: this.regionToIdMap[item.region] || this.regionToIdMap[item.title],
    }));

    if (selectedOption) {
      const workPackagetId = this.workPackageToIdMap[selectedOption.type] || this.workPackageToIdMap[selectedOption.title];
      const currencyId = this.currencyToIdMap[selectedOption.currency] || this.currencyToIdMap[selectedOption.title];
      const regionId = this.regionToIdMap[selectedOption.region] || this.regionToIdMap[selectedOption.title];

      this.getMore.selectedTitle1 = selectedOption.title;
      this.getMore.selectedContractor1 = selectedOption.contractor;
      this.getMore.selectedCost_total1 = selectedOption.cost_total;
      this.getMore.selectedMh1 = selectedOption.mh;
      this.getMore.selectedComments1 = selectedOption.comments;
      this.getMore.selectedVolume1 = selectedOption.volume;
      this.getMore.selectedUnit_of_measure1 = selectedOption.unit_of_measure;
      this.getMore.selectedCost_pp1 = selectedOption.cost_pp;
      this.getMore.selectedType1 = workPackagetId;
      this.getMore.selectedProject1 = selectedOption.project;
      this.getMore.selectedRegion1 = regionId;
      this.getMore.selectedCurrency1 = currencyId;

    } else {
      this.selectedContractor = '';
      this.selectedCostTotal = null;
      this.selectedCostPp = null;
    }
  }
  titleToIdMap = {}; 
  objToIdMap = {}; 
  currencyToIdMap = {}; 
  regionToIdMap = {}; 
  workPackageToIdMap = {}; 

  // Оборудование
  onSelectionChange2() {
    const selectedOption = this.fields2.find((option) => option.title === this.getMore.selectedTitleEQ);
   
    // Создаем объект для хранения соответствия title к id
    this.getAnalogTypeEquipment.forEach((equipment) => {
      this.titleToIdMap[equipment.title] = equipment.id;
    });
    // Сравниваем поля title и type и выбираем id
    const result = this.fields2.map((item) => ({
      id: this.titleToIdMap[item.type] || this.titleToIdMap[item.title],
    }));
    
    // Создаем объект для хранения соответствия currency к id
    this.getCurrencyList.forEach((currency) => {
      this.currencyToIdMap[currency.title] = currency.id;
    });
    // Сравниваем поле currency и выбираем id
    const upshot = this.getAnalogTypeEquipment.map((item) => ({
      id: this.currencyToIdMap[item.currency] || this.currencyToIdMap[item.title],
    }));

    if (selectedOption) {
      const EquipmentId = this.titleToIdMap[selectedOption.type] || this.titleToIdMap[selectedOption.title];
      const currencyId = this.currencyToIdMap[selectedOption.currency] || this.currencyToIdMap[selectedOption.title];

      this.getMore.selectedTitle2 = selectedOption.title;
      this.getMore.selectedManufacturer2 = selectedOption.manufacturer;
      this.getMore.selectedTech_description2 = selectedOption.tech_description;
      this.getMore.selectedCost2 = selectedOption.cost;
      this.getMore.selectedDescription2 = selectedOption.description;
      this.getMore.selectedAnalog_project2 = selectedOption.analog_project;
      this.getMore.selectedType2 = EquipmentId;
      this.getMore.selectedProject2 = selectedOption.project;
      this.getMore.selectedCurrency2 = currencyId;
      this.getMore.selectedEstimated_budget2 = selectedOption.estimated_budget;
      // this.selectedManufacturer = selectedOption.manufacturer;
      // console.log('set',  eqp)
    } else {
      this.selectedContractor = '';
      this.selectedCostTotal = null;
      this.selectedCostPp = null;
    }
  }
  
 // Объекты
 onSelectionChange3() {
  const selectedOption = this.fields3.find((option) => option.title === this.getMore.selectedTitleOA);
    // Создаем объект для хранения соответствия title к id
    this.getAnalogType.forEach((equipment) => {
      this.objToIdMap[equipment.title] = equipment.id;
    });
    // Сравниваем поля title и type и выбираем id
    const result = this.fields3.map((item) => ({
      id: this.objToIdMap[item.type] || this.objToIdMap[item.title],
    }));

    // Создаем объект для хранения соответствия currency к id
    this.getCurrencyList.forEach((currency) => {
      this.currencyToIdMap[currency.title] = currency.id;
    });
    // Сравниваем поле currency и выбираем id
    const upshot = this.getAnalogTypeEquipment.map((item) => ({
      id: this.currencyToIdMap[item.currency] || this.currencyToIdMap[item.title],
    }));

    // Создаем объект для хранения соответствия region к id
    this.getRegionList.forEach((currency) => {
      this.regionToIdMap[currency.title] = currency.id;
    });
    // Сравниваем поле region и выбираем id
    const res = this.getAnalogTypeEquipment.map((item) => ({
      id: this.regionToIdMap[item.region] || this.regionToIdMap[item.title],
    }));


  if (selectedOption) {
    const objId = this.objToIdMap[selectedOption.type] || this.objToIdMap[selectedOption.title];
    const currencyId = this.currencyToIdMap[selectedOption.currency] || this.currencyToIdMap[selectedOption.title];
    const regionId = this.regionToIdMap[selectedOption.region] || this.regionToIdMap[selectedOption.title];

    this.getMore.selectedTitle3 = selectedOption.title;
    this.getMore.selectedSmr_comp3 = selectedOption.smr_comp
    this.getMore.selectedConstr_smr3 = selectedOption.constr_smr
    this.getMore.selectedDate_start3 = selectedOption.date_start
    this.getMore.selectedDate_end3 = selectedOption.date_end
    this.getMore.selectedPir_comp3 = selectedOption.pir_comp
    this.getMore.selectedEng_pir3 = selectedOption.eng_pir
    this.getMore.selectedOto_cost3 = selectedOption.oto_cost
    this.getMore.selectedEtc_cost3 = selectedOption.etc_cost
    this.getMore.selectedTech_description3 = selectedOption.tech_description
    this.getMore.selectedCost3 = selectedOption.cost
    this.getMore.selectedProject3 = selectedOption.project
    this.getMore.selectedType3 = objId
    this.getMore.selectedRegion3 = regionId
    this.getMore.selectedCurrency3 = currencyId
    this.getMore.selectedEstimated_budget3 = selectedOption.estimated_budget

  } else {
    // Обработайте ситуацию, если selectedOption не найден
    // Например, очистите значения полей
    this.selectedContractor = '';
    this.selectedCostTotal = null;
    this.selectedCostPp = null;
  }
}

 // Коэффициент сложности
 onSelectionChange4() {
  const selectedOption = this.fields4.find((option) => option.title === this.getMore.selectedTitleCF);

  if (selectedOption) {
    this.getMore.selectedNumber4 = selectedOption.number;
    this.getMore.selectedRate4 = selectedOption.rate
    this.getMore.selectedTitle4 = selectedOption.title
    this.getMore.selectedComments4 = selectedOption.comments
    this.getMore.selectedEstimated_budget4 = selectedOption.estimated_budget

  } else {
    // Обработайте ситуацию, если selectedOption не найден
    // Например, очистите значения полей
    this.selectedContractor = '';
    this.selectedCostTotal = null;
    this.selectedCostPp = null;
  }
}
  
  getEstimatedId(EstimatedId: number): void {
    this.estimService.getEstimatedId(EstimatedId).subscribe(
      (data: Estimated[]) => {

        const anl = "[*].analog_budget|[]";
        this.getAnalogBudgets = jmespath.search(data, anl);
        
        const test = "[*].analog_budget[].id"
        this.setEqpId = jmespath.search(data, test);

        const analog = "[*].analog.work_package[]";
        this.getAnalog = jmespath.search(data, analog);

        const eqp = "[*].analog.equipment[]";
        this.getEquip = jmespath.search(data, eqp);
        
        // Currency для analog.work_package
        for (const equipment of this.getAnalog) {
          const currencyId = equipment.currency_id;
          const currency = this.getCurrencyList.find(item => item.id === currencyId);
          if (currency) {
            equipment.currency = currency.title;
          // Сохраните значение currency в Local Storage currency
            localStorage.setItem(`currencyW_${equipment.id}`, currency.title);
          }
        }
        for (const equipment of this.getAnalog) {
          const storedCurrency = localStorage.getItem(`currencyW_${equipment.id}`);
          if (storedCurrency) {
            equipment.currency = storedCurrency;
          }
        }

        // TypeEquipment для analog.work_package
        for (const equipment of this.getAnalog) {
          const typeId = equipment.type_id;
          const type = this.getTypePackageList.find(item => item.id === typeId);
          if (type) {
            equipment.type = type.title;
          // Сохраните значение type в Local Storage TypeEquipment
            localStorage.setItem(`typeW_${equipment.id}`, type.title);
          }
        }
        for (const equipment of this.getAnalog) {
          const storedtype = localStorage.getItem(`typeW_${equipment.id}`);
          if (storedtype) {
            equipment.type = storedtype;
          }
        }

        // Region для analog.work_package
        for (const equipment of this.getAnalog) {
          const regionId = equipment.region_id;
          const region = this.getRegionList.find(item => item.id === regionId);
          if (region) {
            equipment.region = region.title;
          // Сохраните значение region в Local Storage regionEquipment
            localStorage.setItem(`regionW_${equipment.id}`, region.title);
          }
        }
        for (const equipment of this.getAnalog) {
          const storedregion = localStorage.getItem(`regionW_${equipment.id}`);
          if (storedregion) {
            equipment.region = storedregion;
          }
        }

        // Currency для analog.equipment
        for (const equipment of this.getEquip) {
          const currencyId = equipment.currency_id;
          const currency = this.getCurrencyList.find(item => item.id === currencyId);
          if (currency) {
            equipment.currency = currency.title;
          // Сохраните значение currency в Local Storage currency
            localStorage.setItem(`currencyE_${equipment.id}`, currency.title);
          }
        }
        for (const equipment of this.getEquip) {
          const storedCurrency = localStorage.getItem(`currencyE_${equipment.id}`);
          if (storedCurrency) {
            equipment.currency = storedCurrency;
          }
        }

        // TypeEquipment для analog.equipment
        for (const equipment of this.getEquip) {
          const typeId = equipment.type_id;
          const type = this.getAnalogTypeEquipment.find(item => item.id === typeId);
          if (type) {
            equipment.type = type.title;
          // Сохраните значение type в Local Storage TypeEquipment
            localStorage.setItem(`typeE_${equipment.id}`, type.title);
          }
        }
        for (const equipment of this.getEquip) {
          const storedtype = localStorage.getItem(`typeE_${equipment.id}`);
          if (storedtype) {
            equipment.type = storedtype;
          }
        }

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getRatio(): void {
    this.estimService.getDifficultyFactor().subscribe(
      (data: any[]) => {
        this.fields4 = data;
        // this.fields3.pop();
      }
    );
  }

  getBudget(): void {
    this.estimService.getAnalogBudget().subscribe(
      (data: any[]) => {
        this.fields3 = data;
        this.fields3.pop();
      }
    );
  }
  // Выборка оборудование из списка
  getEstimated(): void {
    this.estimService.getEquipment().subscribe(
      (data: any[]) => {
        this.fields2 = data
        // this.fields2.pop();
        console.log(this.fields2)
      }
    );
  }
  // Выборка пакета работ из списка
  DataWorkPackage() {
    this.estimService.getworkPackage().subscribe(
      (data: workPackage[]) => {
        this.fields1 = data;
        // this.fields1.pop();
      }
    );
  }
  addRatio() {
    const ratio = this.getMore.ratio.value

    this.estimService.addRatio(ratio).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.estimated.getRatio()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  addObjAnalog() {
    const ObjAnalog = this.analogForm.value

    // Format the date as YYYY-MM-DD
    const startdDate = moment(ObjAnalog.date_start).format('YYYY-MM-DD');
    const endDate = moment(ObjAnalog.date_end).format('YYYY-MM-DD');
    ObjAnalog.date_start = startdDate;
    ObjAnalog.date_end = endDate;
    // this.closed()
    this.estimService.addBudget(ObjAnalog).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.analogForm.reset()
        this.getEstimatedId(this.estId)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editEstimated(Est: any) {
    this.Est = Est.id; // Установим значение свойства Est
    Est.editing = true;
    const selectedCurrency = this.getCurrencyList.find(currency => currency.title === Est.currency);
    const selectedType = this.getAnalogType.find(type => type.title === Est.type);
    const selectedRegion = this.getRegionList.find(region => region.title === Est.region);
    
    if (selectedRegion) {
      // Устанавливаем значение 'region' в выбранный регион
      this.editAnalogForm.get('region').setValue(selectedRegion.id);
    }
    if (selectedType) {
      // Устанавливаем значение 'type' в выбранный тип
      this.editAnalogForm.get('type').setValue(selectedType.id);
    }
    if (selectedCurrency) {
      // Устанавливаем значение 'currency' в выбранную валюту
      this.editAnalogForm.get('currency').setValue(selectedCurrency.id);
    }

    this.editAnalogForm.patchValue({
      title: Est.title,
      smr_comp: Est.smr_comp,
      constr_smr: Est.constr_smr,
      date_start: Est.date_start,
      date_end: Est.date_end,
      pir_comp: Est.pir_comp,
      eng_pir: Est.eng_pir,
      oto_cost: Est.oto_cost,
      etc_cost: Est.etc_cost,
      tech_description: Est.tech_description,
      cost: Est.cost,
      project: Est.project
    })
  }
  saveEstimated(Est: any) {
    if (this.Est) {
      let editedObject = this.editAnalogForm.value;

        editedObject.date_start = moment(editedObject.date_start).format('YYYY-MM-DD');
        editedObject.date_end = moment(editedObject.date_end).format('YYYY-MM-DD');

      this.estimService.editEstimated(this.Est, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getEstimatedId(this.estId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delEstimated(Est: any) {
    const id = Est.id;
    this.estimService.delEstimated(id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Объект аналог удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getEstimatedId(this.estId)
      },
    );
  }
  addEquipments1() {
    const Equipment = this.getMore.equipmenForm1.value;
    this.estimService.addEquipment1(Equipment).subscribe(
      (response: Equipments[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getEstimatedId(this.estId)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editEquipment(Equ: any) {
    this.Equ = Equ.id; // Установим значение свойства Equ
    Equ.editing = true;
    const selectedCurrency = this.getCurrencyList.find(currency => currency.title === Equ.currency);
    const selectedType = this.getAnalogTypeEquipment.find(type => type.title === Equ.type);
    
    if (selectedType) {
      // Устанавливаем значение 'type' в выбранный тип
      this.editEquipmenForm.get('type').setValue(selectedType.id);
    }
    if (selectedCurrency) {
      // Устанавливаем значение 'currency' в выбранную валюту
      this.editEquipmenForm.get('currency').setValue(selectedCurrency.id);
    }

    this.editEquipmenForm.patchValue({
      title: Equ.title,
      manufacturer: Equ.manufacturer,
      tech_description: Equ.tech_description,
      cost: Equ.cost,
      description: Equ.description,
      analog_project: Equ.analog_project,
      project: Equ.project
    })
  }
  saveEquipment(Equ: any) {
    if (this.Equ) {
      let editedObject = this.editEquipmenForm.value;

      this.estimService.editEquipment(this.Equ, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
        // Currency для analog.equipment
        for (const equipment of this.getEquip) {
          const currencyId = equipment.currency_id;
          const currency = this.getCurrencyList.find(item => item.id === currencyId);
          if (currency) {
            equipment.currency = currency.title;
          // Сохраните значение currency в Local Storage currency
            localStorage.removeItem(`currencyE_${equipment.id}`);
          }
        }
        // TypeEquipment для analog.equipment
        for (const equipment of this.getEquip) {
          const typeId = equipment.type_id;
          const type = this.getAnalogTypeEquipment.find(item => item.id === typeId);
          if (type) {
            equipment.type = type.title;
          // Сохраните значение type в Local Storage TypeEquipment
            localStorage.removeItem(`typeE_${equipment.id}`);
          }
        }
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getEstimatedId(this.estId)
          // const setId = this.estId
          // this.analogy.setSetId(setId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delEquipment(Equ: any) {
    const id = Equ.id;
    this.estimService.delEquipment(id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Оборудование удалено.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getEstimatedId(this.estId)
      },
    );
  }
  addEquipments2() {
    const Equipment = this.getMore.equipmenForm2.value;
    this.estimService.addEquipment2(Equipment).subscribe(
      (response: Equipments[]) => {
        console.log('Data added successfully:', response);
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getMore.equipmenForm2.reset()
        this.estimated.getEstimatedId(this.estId)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  addWorkPackage1() {
    const workPackage = this.getMore.workPackage1.value
    this.estimService.addAnalog(workPackage).subscribe(
      (response: analog[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getEstimatedId(this.estId)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  editWorkPackage(Wor: any) {
    this.Wor = Wor.id; // Установим значение свойства Wor
    Wor.editing = true;
    const selectedCurrency = this.getCurrencyList.find(currency => currency.title === Wor.currency);
    const selectedType = this.getTypePackageList.find(type => type.title === Wor.type);
    const selectedRegion = this.getRegionList.find(region => region.title === Wor.region);
    
    if (selectedRegion) {
      // Устанавливаем значение 'region' в выбранный регион
      this.editWorkPackageForm.get('region').setValue(selectedRegion.id);
    }
    if (selectedType) {
      // Устанавливаем значение 'type' в выбранный тип
      this.editWorkPackageForm.get('type').setValue(selectedType.id);
    }
    if (selectedCurrency) {
      // Устанавливаем значение 'currency' в выбранную валюту
      this.editWorkPackageForm.get('currency').setValue(selectedCurrency.id);
    }

    this.editWorkPackageForm.patchValue({
      title: Wor.title,
      contractor: Wor.contractor,
      cost_total: Wor.cost_total,
      mh: Wor.mh,
      comments: Wor.comments,
      volume: Wor.volume,
      unit_of_measure: Wor.unit_of_measure,
      cost_pp: Wor.cost_pp,
      project: Wor.project
    })
  }
  saveWorkPackage(Wor: any) {
    if (this.Wor) {
      let editedObject = this.editWorkPackageForm.value;

      this.estimService.editWorkPackage(this.Wor, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
        // Currency для analog.work_package
        for (const equipment of this.getAnalog) {
          const currencyId = equipment.currency_id;
          const currency = this.getCurrencyList.find(item => item.id === currencyId);
          if (currency) {
            equipment.currency = currency.title;
          // Сохраните значение currency в Local Storage currency
            localStorage.removeItem(`currencyW_${equipment.id}`);
          }
        }
        // TypeEquipment для analog.work_package
        for (const equipment of this.getAnalog) {
          const typeId = equipment.type_id;
          const type = this.getTypePackageList.find(item => item.id === typeId);
          if (type) {
            equipment.type = type.title;
          // Сохраните значение type в Local Storage TypeEquipment
            localStorage.removeItem(`typeW_${equipment.id}`);
          }
        }
        // Region для analog.work_package
        for (const equipment of this.getAnalog) {
          const regionId = equipment.region_id;
          const region = this.getRegionList.find(item => item.id === regionId);
          if (region) {
            equipment.region = region.title;
          // Сохраните значение region в Local Storage regionEquipment
            localStorage.removeItem(`regionW_${equipment.id}`);
          }
        }
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getEstimatedId(this.estId)
          // const setId = this.setID
          // this.analogy.setSetId(setId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delWorkPackage(Wor: any) {
    const id = Wor.id;
    this.estimService.delWorkPackage(id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Пакет работ удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getEstimatedId(this.estId)
      },
    );
  }
  addWorkPackage2() {
    const workPackage = this.getMore.workPackage2.value
    this.estimService.addAnalog(workPackage).subscribe(
      (response: analog[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        // this.getMore.workPackage2.reset()
        this.estimated.getEstimatedId(this.estId)
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  showDetails: boolean = false

  paramDetails(budget: any) {
    budget.showDetails = true;
  }
  close(budget: any): void {
    budget.showDetails = false;
  }
  closed() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  addSection_1() {
    this.block = 'section-1'
    this.activeButton = 1
  }
  addSection_2() {
    this.block = 'section-2'
    this.activeButton = 2
  }
  addSection_3() {
    this.block = 'section-3'
    this.activeButton = 3
  }
  addDirections() {
    this.section = 'level-2'
  }
  createObjectAllReferences() {
    const direction = this.objectReferencesForm.value
    this.estimService.addDirectionAllReferences(direction).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  createObjectReferences() {
    const direction = this.objectReferencesForm.value
    this.estimService.addDirectionReferences(direction).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  addObject(element: any) {
    this.section = 'level-5'
    this.show.showBlock[1] = true;
    if(element.id) {
      this.objId = element.id;
      console.log(this.selectedItemId)
    }
  }
  createObject() {
    const object = this.objectForm.value

    const datePipe = new DatePipe('en-US');

    const startDate = datePipe.transform(object.date_end, 'yyyy-MM-dd');
    const endDate = datePipe.transform(object.date_start, 'yyyy-MM-dd');
    
    object.date_start = startDate;
    object.date_end = endDate;

    this.estimService.addObject(object).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  ObjectId: number
  editObject(element: any) {
    this.ObjectId = element.id
    if(element.id) {
      console.log(element)
      this.section2 = 'level-5'
      this.show.showBlock[2] = true;
      if (element.cost_source?.manual) {
        // Если выбран ручной ввод
        this.objectForm.patchValue({
          smr_cost: element.cost_source.manual.smr_cost,
          pir_cost: element.cost_source.manual.pir_cost,
          oto_cost: element.cost_source.manual.oto_cost,
          etc_cost: element.cost_source.manual.etc_cost,
          total_cost: element.cost_source.manual.total_cost,
        });
      } 
      this.objectForm.patchValue({
        title: element.title,
        smr_comp: element.smr_comp,
        date_start: element.date_start,
        date_end: element.date_end,
        pir_comp: element.pir_comp,
        tech_description: element.tech_description,
        project: element.project,
        type: element.type_id,
        region: element.region_id,
        currency: element.currency_id,
        is_calculated_flag: element.is_calculated_flag,
      })
    }
  }
  saveObject() {
    const object = this.objectForm.value
    const datePipe = new DatePipe('en-US');

    const startDate = datePipe.transform(object.date_end, 'yyyy-MM-dd');
    const endDate = datePipe.transform(object.date_start, 'yyyy-MM-dd');
    
    object.date_start = startDate;
    object.date_end = endDate;

    if (this.ObjectId) {
      this.estimService.editObject(this.ObjectId,object).subscribe(
        (response: analog[]) => {
            this.show.showBlock[2] = false; // Закрываем все блоки, устанавливая значение в false
          this.theme.access = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
          this.getDataSource()
        },
        (error: any) => {
          console.error('Failed to add data:', error);
          this.theme.error = 'Не удалось измененить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
    }
  }
  delObject(element: any) {
    const delElement: any = element.id
    this.estimService.delObject(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSource();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }
  createDirection() {
    const direction = this.directionForm.value
    this.estimService.addDirection(direction).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  directionId: number
  editDirection(element: any) {
    this.directionId = element.id
    if(element.id) {
      this.section2 = 'level-2'
      this.show.showBlock[2] = true;
      this.directionForm.patchValue(element)
    }
  }
  saveDirection() {
    const direction = this.directionForm.value
    if (this.directionId) {
      this.estimService.editDirection(this.directionId,direction).subscribe(
        (response: analog[]) => {
            this.show.showBlock[2] = false; // Закрываем все блоки, устанавливая значение в false
          this.theme.access = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
          this.getDataSource()
        },
        (error: any) => {
          console.error('Failed to add data:', error);
          this.theme.error = 'Не удалось измененить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
    }
  }
  delDirection(element: any) {
    const delElement: any = element.id
    this.estimService.delDirection(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Направление удалено.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSource();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

  addPackage(element: any) {
    this.section = 'level-3'
    this.show.showBlock[1] = true;
    if(element) {
      this.setId = element;
    }
  }
  createPackageReferences() {
    const packages = this.packageReferencesForm.value
    this.estimService.addPackageReferences(packages).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  createPackage() {
    const packages = this.packageForm.value
    this.estimService.addPackage(packages).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  packageId: number
  editPackage(element: any) {
    const selectedPackage = element
    console.log(selectedPackage)
    this.packageId = element.id
    if(element.id) {
      this.section2 = 'level-3'
      this.show.showBlock[2] = true;
      this.packageForm.patchValue({
        title: element.title,
        smr_cost: element.smr_cost,
        pir_cost: element.pir_cost,
        oto_cost: element.oto_cost,
        etc_cost: element.etc_cost,
        pp_cost: element.pp_cost,
        mh: element.mh,
        volume: element.volume,
        comments: element.comments,
        contractor: element.contractor,
        total_cost: element.total_cost,
        unit_of_measure: element.unit_of_measure_id,
        type: element.type_id,
        region: element.region_id,
        currency: element.currency_id,
        estimated_analog: element.estimated_analog_id,
      })
    }
  }
  savePackage() {
    const packages = this.packageForm.value
    if (this.packageId) {
      this.estimService.editPackages(this.packageId,packages).subscribe(
        (response: analog[]) => {
            this.show.showBlock[2] = false; // Закрываем все блоки, устанавливая значение в false
          this.theme.access = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
          this.getDataSource()
        },
        (error: any) => {
          console.error('Failed to add data:', error);
          this.theme.error = 'Не удалось измененить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
    }
  }
  delPackage(element: any) {
    const delElement: any = element.id
    this.estimService.delPackages(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Пакет удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSource();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

  addEquip(element: any) {
    this.section = 'level-4'
    this.show.showBlock[1] = true;
    if(element) {
      this.setId = element;
    }
  }

  createEquipReferences() {
    const equip = this.equipReferencesForm.value
    this.estimService.addEquipentReferences(equip).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  createEquip() {
    const equip = this.equipForm.value
    this.estimService.addEquipent(equip).subscribe(
      (response: analog[]) => {
          this.show.showBlock[1] = false; // Закрываем все блоки, устанавливая значение в false
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getDataSource()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  equipId: number
  editEquip(element) {
    this.equipId = element.id
    if(element.id) {
      this.section2 = 'level-4'
      this.show.showBlock[2] = true;
      this.equipForm.patchValue({
        title: element.title,
        smr_cost: element.smr_cost,
        pir_cost: element.pir_cost,
        oto_cost: element.oto_cost,
        etc_cost: element.etc_cost,
        manufacturer: element.manufacturer,
        tech_description: element.tech_description,
        comments: element.comments,
        description: element.description,
        analog_project: element.analog_project,
        type: element.type_id,
        region: element.region_id,
        currency: element.currency_id,
        estimated_analog: element.estimated_analog_id,
      })
    }
  }
  saveEquip() {
    const equip = this.equipForm.value
    if (this.equipId) {
      this.estimService.editEquipent(this.equipId,equip).subscribe(
        (response: analog[]) => {
            this.show.showBlock[2] = false; // Закрываем все блоки, устанавливая значение в false
          this.theme.access = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
          this.getDataSource()
        },
        (error: any) => {
          console.error('Failed to add data:', error);
          this.theme.error = 'Не удалось измененить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
    }
  }
  delEquip(element: any) {
    const delElement: any = element.id
    this.estimService.delEquipent(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Оборудование удалено.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getDataSource();
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }
  
  createComplexity() {
    const object = this.factorForm.value
    this.estimService.addFactor(object).subscribe(
      (response: analog[]) => {
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.show.showBlock[3] = false;
        this.show.showBlock[1] = true;
        this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
          const test = "[*].children[].children[]";
          this.getData = jmespath.search(data, test);
        });
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  createComplexityReferences() {
    const object = this.factorReferenceForm.value
    this.estimService.addReferencesFactor(object).subscribe(
      (response: analog[]) => {
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.show.showBlock[3] = false;
        this.show.showBlock[1] = true;
        this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
          const test = "[*].children[].children[]";
          this.getData = jmespath.search(data, test);
        });
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  complexityId: number
  editComplexity(element: any) {
    this.complexityId = element.id
    if(element.id) {
      this.section2 = 'level-2'
      this.show.showBlock[3] = true;
      this.factorForm.patchValue(element)
    }
  }
  
  saveComplexity() {
    const complexity = this.factorForm.value
    if (this.complexityId) {
      this.estimService.editFactor(this.complexityId,complexity).subscribe(
        (response: analog[]) => {
            this.show.showBlock[3] = false; // Закрываем все блоки, устанавливая значение в false
          this.theme.access = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.access = null;
          }, 5000);
          this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
            const test = "[*].children[].children[]";
            this.getData = jmespath.search(data, test);
          });
        },
        (error: any) => {
          console.error('Failed to add data:', error);
          this.theme.error = 'Не удалось измененить данные. Ошибка #400 Bad Request.'
          setTimeout(() => {
            this.theme.error = null;
          }, 5000);
        }
      );
    }
  }
  delComplexity(element: any) {
    const delElement: any = element.id
    this.estimService.delFactor(delElement).subscribe(
      (data: any) => {
        this.theme.info = 'Коэффициент сложности удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
          const test = "[*].children[].children[]";
          this.getData = jmespath.search(data, test);
        });
      },
      (error: any) => {
        console.log('Failed to delete svz document:', error)
      }
    )
  }

  getInfo(element: any) {
    if(element.id) {
      this.selectedItemId = element.id;
    }
  }
  showInfo(element: any) {
    if(element.id) {
        this.selectedItemId = element.id;
        this.section = 'level-1'
        this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
          const test = "[*].children[].children[]";
          this.getData = jmespath.search(data, test);
        });
        
    }
  }
  showComplexity(element: any) {
    this.complexity = element.id
    this.section2 = 'level-1'
    this.show.showBlock[3] = true;
    if(element.id) {
      this.objId = element.id;
    }
    if(element.id) {
      this.selectedItemId = element.id;
      this.section = 'level-1'
      this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
        const test = "[*].children[].children[]";
        this.getData = jmespath.search(data, test);
      });
    }
  }
  getDataSource() {
    this.estimService.getEstimatedBudgetId(this.estId).subscribe((data)=> {
      this.dataSources.data = data;
      this.dataCostSource = data;
      // this.getData = data;

      const query = "[*].children[]";
      this.dataSource = jmespath.search(data, query);
     
      const cost = "[*].children[].children[]";
      this.expandedElement = jmespath.search(data, cost);
    });
  }
  complexity: number
  setId: number
  objId: number
  block: string = 'section-1'
  section: string = ''
  section2: string = ''
  getData: any[] = [];
  dataSource: any[] = [];
  dataCostSource: getLevel1[] = []

  columnsToDisplay = ['title', 'smr_cost', 'pir_cost', 'oto_cost', 'etc_cost', 'total_cost'];
  columnsToDisplayWithExpand = [...this.columnsToDisplay, 'expand'];
  expandedElement: PeriodicElement | null;

  translationMap: { [key: string]: string } = {
    'title': 'Название',
    'smr_cost': 'СМР стоимость',
    'pir_cost': 'ПИР стоимость',
    'oto_cost': 'Оборудование стоимость',
    'etc_cost': 'Прочее стоимость',
    'total_cost': 'Итого',
  };

  displayedColumns: string[] = ['serial_number', 'title', 'date_start', 'date_end', 'type', 'region', 'actions'];
  // dataSource = new MatTableDataSource<getProject>([]);
  // dataDetails: any[] = []
  
  dataDirectionSource: any[] = []
  modal: boolean
  setDirectionId
  detail: boolean
  selectedItemId: number | null = null;

  getDetail(id: number) {
    if(id) {
      this.detail = true
      this.selectedItemId = id;
    }
  }

  private _transformer = (node: any, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      title: node.title,
      type: node.type,
      region: node.region,
      date_start: node.date_start,
      date_end: node.date_end,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Estim>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  // dataSources: any
  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Estim) => node.expandable;
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
  title: string;
  smr_cost: number
  children: PeriodicElement[]
}