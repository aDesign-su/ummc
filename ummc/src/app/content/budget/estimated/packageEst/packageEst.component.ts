import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { ApicontentService } from 'src/app/api.content.service';
import { EstimatedService } from '../estimated.service';
import { analogBudgets } from 'src/app/content/references/analogy/analogy';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';

@Component({
  selector: 'app-packageEst',
  templateUrl: './packageEst.component.html',
  styleUrls: ['./packageEst.component.css']
})
export class PackageEstComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, private estimService: ApicontentService, public estimated:EstimatedService,private formBuilder: FormBuilder) {
    this.estimated.currentSetId.subscribe(id => {
      this.estId = id;
      console.log(this.estId)
    })
    this.editWorkPackageForm = this.formBuilder.group({
      title: '',
      contractor: '',
      cost_total: '',
      mh: '',
      comments: '',
      volume: '',
      unit_of_measure: '',
      cost_pp: '',
      type: '',
      project: '',
      region: '',
      currency: ''
    });
   }
   editWorkPackageForm!: FormGroup
  ngOnInit() {
    this.estimated.getEstimatedId(this.estId)
  // Селект Type Package
  this.estimService.getAnalogTypePackageList().subscribe(
    (data: analogBudgets[]) => {
      this.getTypePackageList = data
    },
  );
   // Селект Type Currency
   this.estimService.getCurrencyList().subscribe(
    (data: analogBudgets[]) => {
      this.getCurrencyList = data
    },
  );
  // Селект Region
  this.estimService.getRegionList().subscribe(
    (data: analogBudgets[]) => {
      this.getRegionList = data
    },
  );
  }
  getCurrencyList: any[] = []
  CurrencyList!: string;
  getRegionList: any[] = []
  RegionList!: string;
  getTypePackageList: any[] = []
  TypePackageList!: string;

  estId: number
  getSelect: any[] = []
  element: any

  closed() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  
  editWorkPackage(element: any) {
      this.element = element.id; // Установим значение свойства element
      element.editing = true;
      const selectedCurrency = this.getCurrencyList.find(currency => currency.title === element.currency);
      const selectedType = this.getTypePackageList.find(type => type.title === element.type);
      const selectedRegion = this.getRegionList.find(region => region.title === element.region);
      
      if (selectedRegion) {
        // Устанавливаем значение 'region' в выбранный регион
        this.editWorkPackageForm.get('region').setValue(selectedRegion.id);
      }
      if (selectedType) {
        // Устанавливаем значение 'type' в выбранный тип
        this.editWorkPackageForm.get('type').setValue(selectedType.id);
      }
      if (selectedCurrency) {
        // Устанавливаем значение 'currency' в выбранную валюту
        this.editWorkPackageForm.get('currency').setValue(selectedCurrency.id);
      }
  
      this.editWorkPackageForm.patchValue({
        title: element.title,
        contractor: element.contractor,
        cost_total: element.cost_total,
        mh: element.mh,
        comments: element.comments,
        volume: element.volume,
        unit_of_measure: element.unit_of_measure,
        cost_pp: element.cost_pp,
        project: element.project
      })
  }
  saveWorkPackage(Wor: any) {
    if (this.element) {
      let editedObject = this.editWorkPackageForm.value;

      this.estimService.editWorkPackage(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.estimated.getEstimatedId(this.estId)
          // const setId = this.setID
          // this.analogy.setSetId(setId)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delWorkPackage(Wor: any) {
    const id = Wor.id;
    this.estimService.delWorkPackage(id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Пакет работ удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.estimated.getEstimatedId(this.estId)
      },
    );
  }
}