import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageEstComponent } from './packageEst.component';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
  declarations: [
    // PackageEstComponent
  ]
})
export class PackageEstModule { }
