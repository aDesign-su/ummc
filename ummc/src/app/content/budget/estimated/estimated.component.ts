import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Estim, Estimated, analog, getEstim } from './estimated';
import * as jmespath from 'jmespath';
import { SelectionModel } from '@angular/cdk/collections';
import { analogBudgets } from '../../references/analogy/analogy';
import { trigger, style, animate, transition } from '@angular/animations';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';
import { EstimatedService } from './estimated.service';
import { ShowService } from 'src/app/helper/show.service';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { dateRangeValidator } from 'src/app/helper/dateFilter.directive';
import { CommonService } from '../project-budget/common.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { getTotal, Total } from '../total-project-costs/total';

@Component({
  selector: 'app-estimated',
  templateUrl: './estimated.component.html',
  styleUrls: ['./estimated.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 1, transform: 'translateY(-120px)' }),
        animate('0.2s ease-in-out', style({ opacity: 1, transform: 'translateY(0)' }))
      ]),
      transition(':leave', [
        animate('0.4s ease-in-out', style({ opacity: 1, transform: 'translateY(-1000px)' }))
      ])
    ])
  ]
})
export class EstimatedComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private estimService: ApicontentService, private formBuilder: FormBuilder,private router: Router,public estimated: EstimatedService, private _adapter: DateAdapter<any>,@Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.estimatedForm = this.formBuilder.group({
      title: ['', Validators.required],
      version: ['', [Validators.maxLength(4), Validators.pattern(/^[0-9,.]+$/)]],
      comments: [''],
      date_start_plan: [null, Validators.required],
      date_end_plan: [null, Validators.required],
      currency: [''],
    }, { validator: dateRangeValidator });
    this.editEstimatedForm = this.formBuilder.group({
      title: ['', Validators.required],
      version: ['', [Validators.required, Validators.maxLength(4), Validators.pattern(/^[0-9,.]+$/)]],
      comments: [''],
      date_end_plan:[null, Validators.required],
      date_start_plan:[null, Validators.required],
      macro: [0],
      currency: ['']
    }, { validator: dateRangeValidator });
  }
  estimatedForm!: FormGroup;
  editEstimatedForm!: FormGroup;

  getCompareList: any[] = []
  
  ngOnInit() {
    this.Estimated()
    this.getDataSource()
    this.getDataSourceCurrency()

    this.estimated.getEstimatedId(this.estId)
  }

  selection = new SelectionModel<Estimated>(true, []);
  // displayedColumns: string[] = ['select', 'serial_number', 'title', 'date_start_plan', 'date_end_plan', 'version', 'comments', 'cost', 'currency', 'action'];
  // dataSource = new MatTableDataSource<Estimated>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getComparison: any = []
  getAnalogBudget: analogBudgets[] = []

  setID: number  // id оценочного бюджета
  estId: number // id предпологаемого бюджета
  title: string

  getName: any[] = []
  selectedName!: string;
  section: string;

  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }

  dateRangeValidator(startControl: string, endControl: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const start = control.get(startControl)?.value;
      const end = control.get(endControl)?.value;
  
      // Проверка, если оба значения определены и дата окончания меньше даты начала
      if (start && end && new Date(end) < new Date(start)) {
        return { 'dateRangeError': true };
      }
  
      return null;
    };
  }


  clickedRowsArray: any[] = [];

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'дд-мм-гггг';
    } else if (this._locale === 'fr') {
      return 'дд-мм-гггг';
    }
    return '';
  }

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }

  Estimated(): void {
    this.estimService.getEstimated().subscribe(
      (response: any) => {
        this.dataSources.data = response

        // const query = "[*].{id: id, title: title}";
        // this.getName = jmespath.search(response, query);
      },
      (error: any) => {
        console.error('Error:', error);
      }
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected?.length;
    const numRows = this.dataSource.data?.length;
    return numSelected === numRows;
  }

  clickedRows = new Set<Estimated>();

  isRowClicked(row: Estimated): boolean {
    return this.clickedRows.has(row);
  }
  toggleRow(node: Estimated) {
    if (this.isRowClicked(node)) {
      this.clickedRows.delete(node);
    } else {
      this.clickedRows.add(node);
    }

    
    this.clickedRowsArray = Array.from(this.clickedRows);
    console.log(this.clickedRowsArray)
  }
  updateArray() {
    this.clickedRowsArray = Array.from(this.clickedRows);
    this.getTotalCost()
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(node?: Estimated): string {
    if (!node) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(node) ? 'deselect' : 'select'} node ${node.position + 1}`;
  }

  getDataSource(): void {
    this.estimService.getAnalogBudget().subscribe(
      (data: analogBudgets[]) => {
        data.pop();
        this.getAnalogBudget = data.slice(0, data.length - 0);

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDataSourceCurrency(): void {
    this.estimService.getCurrencyReference().subscribe(
      (data: any[]) => {
        this.getSelect = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  selectRub: number = 1
  getSelect: any[] = []
  selectedRegion: string
  totalCost

  getTotalCost(): number[]  {
    // Массив для хранения суммы для каждого проекта.
    let projectTotals = [];

    // Сначала вычисляем cost_total для каждого проекта.
    for (let project of this.clickedRowsArray) {
        let costTotal = 0;

        // Здесь предполагается некоторая логика для вычисления cost_total для проекта.
        // Например, если есть массив items внутри project:
        // for (let item of project.items) {
        //    costTotal += item.cost;
        // }

        // Для нашего случая, предположим, что cost_total уже присутствует:
        costTotal = project.cost_total;

        // Добавляем вычисленное значение cost_total в массив.
        projectTotals.push(costTotal);
    }

    // Теперь вычисляем разницу между суммами соседних проектов.
    this.totalCost = [];
    for (let i = 1; i < projectTotals.length; i++) {
        // Вычисляем разницу между текущим и предыдущим значением.
        let difference = projectTotals[i] - projectTotals[i - 1];
        this.totalCost.push(difference);
    }

    // Выводим разницы в консоль или возвращаем их для дальнейшего использования.
    console.log('text22', this.clickedRowsArray);
    console.log('text', this.totalCost);
    return this.totalCost;
}



  saveBudget(bud: any) {
    bud.editing = false;

    const id = bud.id;
    const updatedData = {
      title: bud.title,
      version: bud.version,
      comments: bud.comments,
      macro: bud.macro,
      cost: bud.cost,
      currency: bud.currency,
    };

    this.estimService.editBudget(id, updatedData).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        bud.editing = false;
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }


  addEstimated() {
    const estimated = this.estimatedForm.value;

    const startdDate = moment(estimated.date_start_plan).format('YYYY-MM-DD');
    const endDate = moment(estimated.date_end_plan).format('YYYY-MM-DD');
    estimated.date_start_plan = startdDate;
    estimated.date_end_plan = endDate;

    this.estimService.addEstimated(estimated).subscribe(
      (response: Estimated[]) => {
        console.log('Data added successfully:', response);
        this.Estimated();
        this.show.closedAdd(1)
        this.estimatedForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  element: number
  editEstimated(element: any) {
    this.element = element.id; 
    this.section = 'level-2'
    this.show.showBlock[1] = true;
    
    // const selectedCurrency = this.getSelect.find(currency => currency.title === element.currency);
    // if (selectedCurrency) {
    //   // Устанавливаем значение 'currency' в выбранную валюту
    //   this.estimatedForm.get('currency').setValue(selectedCurrency.id);
    // }
    // console.log(element.currency)
    this.estimatedForm.patchValue({
      date_start_plan: element.date_start_plan,
      date_end_plan: element.date_end_plan,
      comments: element.comments,
      version: element.version,
      title: element.title,
      currency: element.currency
    });
  }
  saveEstimated() {
    let editedObject = this.estimatedForm.value;

    const startdDate = moment(editedObject.date_start_plan).format('YYYY-MM-DD');
    const endDate = moment(editedObject.date_end_plan).format('YYYY-MM-DD');
    editedObject.date_start_plan = startdDate;
    editedObject.date_end_plan = endDate;

    if (this.element) {
      this.estimService.editBudget(this.element, editedObject).subscribe(
        (data: any) => {
            this.show.showBlock[1] = false;
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.Estimated();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delEstimated(bud: any) {
    const id = bud.id;
    this.estimService.delBudget(id).subscribe(
      (response: any[]) => {
        this.theme.info = 'Оценочный бюджет удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.Estimated();
      },
    );
  }
  selectAllChecked = false;

  toggleSelectAll() {
    this.dataSources.data.forEach((row:any) => {
      if (row.level !== 1 && row.level !== 2 && row.level !== 3) {
        row.isSelected = this.selectAllChecked;
      }
    });
    this.updateArray();
  }



  showMore(element: Estimated) {
    this.show.toggleShowBlockTrue(2)
    if (element.id) {
      this.estimated.getEstimatedId(element.id);
    }
  }

  showElement(id: getEstim){
    const element = id
    if (element.id) {
      this.estimated.getEstimatedId(element.id);
      this.estId = element.id

      this.title = element.title
      this.estimated.setSetTitle(this.title);
      
      this.estimated.setSetId(this.estId);
      this.router.navigate(['estimated/more']);
    }
    // if (element) {
    //   console.log(element)
    //   this.router.navigate(['estimated/more']); 
    //   this.estimated.setSetId(this.setID); 
    // }
  }

  columns: string[] = ['title', 'macro', 'cost', 'discription', 'version', 'action'];

  getColumnHeader(column: string): string {
    // Добавьте логику для преобразования названия столбца, если необходимо
    return column;
  }

  displayedColumns: string[] = ['serial_number', 'title', 'date_start_plan', 'date_end_plan', 'comments', 'version', 'select', 'actions'];
  dataSource = new MatTableDataSource<getEstim>([]);
  
  private _transformer = (node: getEstim, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      title: node.title,
      date_start_plan: node.date_start_plan,
      date_end_plan: node.date_end_plan,
      version: node.version,
      comments: node.comments,
      currency: node.currency,
      cost_total: node.cost_total,
      total_cost: node.total_cost,
      level_name: node.level_name,
      level: level,
      children: node.children
    };
  };

  treeControl = new FlatTreeControl<Estim>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Estim) => node.expandable;

  showDetails(element: Estimated) {
    if (element.id) {
      this.estimated.getEstimatedId(element.id);
      this.estId = element.id

      this.title = element.title
      this.estimated.setSetTitle(this.title);
      
      this.estimated.setSetId(this.estId);
      this.router.navigate(['estimated/more']);
    }
  }
}