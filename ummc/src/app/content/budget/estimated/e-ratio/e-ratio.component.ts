import { Component, OnInit } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { EstimatedService } from '../estimated.service';
import { Ratio } from 'src/app/content/references/ratio/ratio';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';

@Component({
  selector: 'app-difficulty-factor',
  templateUrl: './e-ratio.component.html',
  styleUrls: ['./e-ratio.component.css']
})
export class ERatioComponent implements OnInit {
  constructor(public theme: ThemeService, private estimService: ApicontentService, public estimated:EstimatedService,private formBuilder: FormBuilder,public show:ShowService) {
    this.estimated.currentSetId.subscribe(id => {
      this.estId = id;
    })
    this.editRatioForm = this.formBuilder.group({
      number: '',
      rate: '',
      title: '',
      comments: '',
      estimated_budget: ''
    });
   }
  ngOnInit() {
    this.estimated.getRatio()
  }
  element: any
  estId: number
  editRatioForm!: FormGroup;

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
    this.estimated.changeIndex = false;
  }

  editRatio(element: any) {
    this.element = element.id; // Установим значение свойства element
    element.editing = true;

    this.editRatioForm.patchValue({
      number: element.number,
      rate: element.rate,
      title: element.title,
      comments: element.comments,
      estimated_budget: element.estimated_budget,
    })
  }

  saveRatio() {
    if (this.element) {
      let editedObject = this.editRatioForm.value;

      this.estimService.editRatio(this.element, editedObject).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.estimated.getRatio();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  delRatio(Equ: any) {
    const id = Equ.id;
    this.estimService.delRatio(id).subscribe(
      (response: any[]) => {
        this.estimated.getRatio()
        this.theme.info = 'Коэффициент сложности удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
    );
  }
}
