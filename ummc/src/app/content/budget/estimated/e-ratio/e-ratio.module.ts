import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ERatioComponent } from './e-ratio.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { DateFilterPipe } from './media/oem/CE329A41329A2F09/GitLab/ugmk/budgeting/ummc/src/app/content/budget/estimated/date-filter.pipe';
import { DateFilterDirective } from './media/oem/CE329A41329A2F09/GitLab/ugmk/budgeting/ummc/src/app/content/budget/estimated/dateFilter.directive';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule
  ],
  declarations: [		
,    // ERatioComponent
      DateFilterPipe,
      DateFilterDirective
   ]
})
export class ERatioModule { }
