/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EstimatedService } from './estimated.service';

describe('Service: Estimated', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstimatedService]
    });
  });

  it('should ...', inject([EstimatedService], (service: EstimatedService) => {
    expect(service).toBeTruthy();
  }));
});
