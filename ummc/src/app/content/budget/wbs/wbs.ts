export interface Wbs {
    id: number,
    name: string,
    id_package: number,
    delete_flg: boolean,
    lft: number,
    rght: number,
    tree_id: number,
    level: number,
    parent: number,
    name_package: string,
    type_node: string,
    code_wbs: string,
    part_code_wbs: string,
    category_id: number,
    start_date: string,
    end_date: string,
    percent_date: string,
    name_level: string,
    category: string,
    ancestor_code_wbs: string,
    children?: Wbs[]
}
export interface WbsTable {
  level0: WbsTableElement;
  level1: WbsTableElement;
  level2: WbsTableElement;
  level3: WbsTableElement;
  level4: WbsTableElement;
}
export interface WbsTableElement {
  name: string,
  package_id: number,
  wbs: Wbs|null,
  filtered: boolean;
}
export interface Package {
  id: number,
  date_created: string,
  name: string,
  start_date: Date,
  end_date: Date,
  delete_flg: false,
  version_id: number,
  type_version: string,
  user_created: number,
  package: number,
  category: number,
  category_id: number,
  ancestor_code_wbs: string,
  part_code_wbs: string,
  parent: number,
  id_package: number
}

export interface ProjectFilter {
  id: number,
  date_created: string,
  name: string,
  start_date: Date,
  end_date: Date,
  delete_flg: false,
  version_id: number,
  type_version: string,
  user_created: number,
  package: number,
  category: number,
  category_id: number,
  ancestor_code_wbs: string,
  part_code_wbs: string,
  parent: number,
  id_package: number
}

export interface DirectionFilter {
    id: number,
    date_created: string,
    name: string,
  start_date: Date,
  end_date: Date,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
  category: number,
  category_id: number
}
export interface ObjectFilter {
    id: number,
    date_created: string,
    name: string,
  start_date: Date,
  end_date: Date,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
  category: number,
  category_id: number
}
export interface SystemFilter {
    id: number,
    date_created: string,
    name: string,
  start_date: Date,
  end_date: Date,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
  category: number,
  category_id: number
}
export interface WorkFilter {
    id: number,
    date_created: string,
    name: string,
  start_date: Date,
  end_date: Date,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
  category: number,
  category_id: number
}
export interface EstimateFilter {
    id: number,
    date_created: string,
    name: string,
  start_date: Date,
  end_date: Date,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
  category: number,
  category_id: number
}
export interface PackageElements {
  data?: WbsElement[],
  users?: User[]
}
export interface WbsElement {
  id:           number;
  date_created: Date;
  name:         string;
  delete_flg:   boolean;
  version_id:   number;
  user_created: number;
  package:      number;
}
export interface User {
  id: number,
  username: string,
  first_name: string,
  last_name: string
}
export interface ElementFilter {
  name: string,
  user_created: number
}
export interface DeleteEntity {
  id: number,
  delete_flg: string
}
export interface Category {
  id: number,
  name: string,
  level: number,
  user_created: number
}
/** Flat node with expandable and level information */
export interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: number;
  type_node: string;
  start_date: string;
  end_date: string;
  percent_date: string;
  code_wbs: string;
  part_code_wbs: string;
  category: string;
  ancestor_code_wbs: string;
  category_id: number;
  children: Wbs[] | undefined;
  id_package: number;
  name_package: string;
  filtered: boolean;
}
