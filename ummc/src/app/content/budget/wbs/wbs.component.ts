import _ from 'lodash';
import {Component, OnInit} from '@angular/core';
import {ThemeService} from 'src/app/theme.service';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {ApicontentService} from 'src/app/api.content.service';
import {
  DirectionFilter,
  EstimateFilter,
  ObjectFilter,
  Package,
  Wbs,
  WbsTable,
  Category,
  ExampleFlatNode
} from './wbs';
import { AuthenticationService } from '../../../authentication/login/login/authentication.service'
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CommonService } from '../project-budget/common.service';
import { ShowService } from 'src/app/helper/show.service';
import { ObjectBudget } from '../project-budget/registerBudget';
import { WbsService } from '../intangible/wbs.service';

@Component({
  selector: 'app-wbs',
  templateUrl: './wbs.component.html',
  styleUrls: ['./wbs.component.css']
})

export class WbsComponent implements OnInit {
  constructor(public theme: ThemeService, public show: ShowService,
              private wbsService: ApicontentService,
              private formBuilder: FormBuilder, public isTitle: CommonService,
              public wbsSource:WbsService,
              auth: AuthenticationService) {
    this.CreatedPackage = this.formBuilder.group({
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [null, [Validators.required]],
      part_code_wbs: [0, [Validators.required]],
      parent: '',
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [null]
    });
    this.UpdatedPackage = this.formBuilder.group({
      id: null,
      date_created: '',
      name: ['', [Validators.required]],
      start_date: null,
      end_date: null,
      delete_flg: false,
      code_wbs: [null, [Validators.required]],
      part_code_wbs: [0, [Validators.required]],
      parent: 0,
      user_created: 1,
      package: 0,
      category_id: 0,
      category: [null]
    });
    this.DeletePackage = this.formBuilder.group({
      id: -1,
      name_package: "",
      user_created: "36"
    });

    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      node => node.level,
      node => node.expandable,
      node => node.children
    );
    this.treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level,
      node => node.expandable
    );
    this.structureViewDataSource = [
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener),
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener),
      new MatTreeFlatDataSource(this.treeControl, this.treeFlattener)
    ]
  }

  private transformer = (node: Wbs, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id,
      type_node: node.type_node,
      start_date: node.start_date,
      end_date: node.end_date,
      percent_date: node.percent_date,
      code_wbs: node.code_wbs,
      category: node.category,
      category_id: node.category_id,
      children: node.children,
      id_package: node.id_package,
      name_package: node.name_package,
      ancestor_code_wbs: node.ancestor_code_wbs,
      part_code_wbs: node.part_code_wbs,
      filtered: false
    };
  };

  structureViewColumns: string[] = ['name', 'type_node', 'start_date', 'end_date', 'category'];
  tablesViewColumns: string[][] = [
    ['Проект', 'Направление', 'Объект', 'Система', 'Пакет работ или Смета'],
    ['Проект', 'Тип общепроектных работ', 'Пакет работ или Смета'],
    ['Уровень 0', 'Уровень 1', 'Уровень 2']
  ];
  clueViewButtons: string[][] = [
    ['проект', 'направление', 'объект', 'систему', 'пакет работ или смету'],
    ['проект', 'общепроектную работу', 'пакет работ или смету'],
    ['уровень 0', 'уровень 1', 'уровень 2']
  ];
  clueFormHeader: string[][] = [
    ['проекта', 'направления', 'объекта', 'системы', 'пакета работ или сметы'],
    ['проекта', 'общепроектной работы', 'пакета работ или сметы'],
    ['уровня 0', 'уровня 1', 'уровня 2']
  ];

  private selectedWbs!: Wbs;

  set setSelectedWbs(wbsNew: Wbs) {
    this.selectedWbs = wbsNew;
  }

  get getSelectedWbs() {
    return this.selectedWbs;
  }

  private selectedWbsSourceId: number = 0;

  set setSelectedWbsSource(newSourceId: number) {
    this.selectedWbsSourceId = newSourceId;
  }

  get getSelectedWbsSource() {
    return this.selectedWbsSourceId;
  }

  nameFilter: string = "";
  currentFilter: number = 6;

  loading: boolean = true;

  private activLoading() {
    this.loading = true;
  }

  private deactivLoading() {
    this.loading = false;
  }

  currentViewIndex: number = 0;

  changeCurrentView(index: number) {
    setTimeout(() => {
      this.currentViewIndex = index;
      this.activLoading();      
    }, 1000);
    setTimeout(() => {
      this.deactivLoading();
    }, 1000);
  }
  showNmaToggle: boolean = false;
  showGeneralToggle: boolean = false;

  wbs: Wbs[][] = [];
  treeControlStorage: ExampleFlatNode[][] = [];

  ProjectFilter: Package[] = []
  DirectionFilter: DirectionFilter[] = []
  ObjectFilter: ObjectFilter[] = []

  structureViewDataSource: MatTreeFlatDataSource<Wbs, ExampleFlatNode>[];
  tableViewDataSources: WbsTable[][] = [];

  treeControl: FlatTreeControl<ExampleFlatNode>;
  treeFlattener: MatTreeFlattener<Wbs, ExampleFlatNode>;

  categories: Category[] = [];

  CreatedPackage!: FormGroup;
  UpdatedPackage!: FormGroup;
  DeletePackage!: FormGroup;

  private creatingApi: Function[][] = [
    [
      this.wbsService.addDirectionFilter, 
      this.wbsService.addObjectFilter, 
      this.wbsService.addSystemFilter, 
      this.wbsService.addWorkFilter, 
      this.wbsService.addEstimateFilter
    ], [
      this.wbsService.addTypicalDirectionFilter, 
      this.wbsService.addTypicalObjectFilter, 
      this.wbsService.addTypicalSystemFilter, 
      this.wbsService.addTypicalWorkFilter, 
      this.wbsService.addTypicalEstimateFilter
    ], [
      this.wbsService.addIntangibleAssetsDirectionFilter, 
      this.wbsService.addIntangibleAssetsObjectFilter, 
      this.wbsService.addIntangibleAssetsSystemFilter, 
      this.wbsService.addIntangibleAssetsWorkFilter, 
      this.wbsService.addIntangibleAssetsEstimateFilter
    ]
  ];

  private updatingApi: Function[][] = [
    [
      this.wbsService.updateProjectPackage,
      this.wbsService.updateDirectionPackage,
      this.wbsService.updateObjectPackage,
      this.wbsService.updateSystemPackage,
      this.wbsService.updateWorkPackage,
      this.wbsService.updateEstimatePackage
    ], [
      this.wbsService.updateTypicalProjectPackage,
      this.wbsService.updateTypicalDirectionPackage,
      this.wbsService.updateTypicalObjectPackage,
      this.wbsService.updateTypicalSystemPackage,
      this.wbsService.updateTypicalWorkPackage,
      this.wbsService.updateTypicalEstimatePackage
    ], [
      this.wbsService.updateIntangibleAssetsProjectPackage,
      this.wbsService.updateIntangibleAssetsDirectionPackage,
      this.wbsService.updateIntangibleAssetsObjectPackage,
      this.wbsService.updateIntangibleAssetsSystemPackage,
      this.wbsService.updateIntangibleAssetsWorkPackage,
      this.wbsService.updateIntangibleAssetsEstimatePackage
    ]
  ];

  private deletingApi: Function[][] = [
    [
      this.wbsService.deleteProjectPackage,
      this.wbsService.deleteDirectionPackage,
      this.wbsService.deleteObjectPackage,
      this.wbsService.deleteSystemPackage,
      this.wbsService.deleteWorkPackage,
      this.wbsService.deleteEstimatePackage
    ], [
      this.wbsService.deleteTypicalProjectPackage,
      this.wbsService.deleteTypicalDirectionPackage,
      this.wbsService.deleteTypicalObjectPackage,
      this.wbsService.deleteTypicalSystemPackage,
      this.wbsService.deleteTypicalWorkPackage,
      this.wbsService.deleteTypicalEstimatePackage
    ], [
      this.wbsService.deleteIntangibleAssetsProjectPackage,
      this.wbsService.deleteIntangibleAssetsDirectionPackage,
      this.wbsService.deleteIntangibleAssetsObjectPackage,
      this.wbsService.deleteIntangibleAssetsSystemPackage,
      this.wbsService.deleteIntangibleAssetsWorkPackage,
      this.wbsService.deleteIntangibleAssetsEstimatePackage
    ]
  ];

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  //! Temp, fix after presentation
  ngOnInit() {
    (async () => {
      await this.getWbs();
    })().then(() => {
      Promise.all([this.getCategory(), 
        // this.getWbs(), 
        this.getTypicalWbs(), this.getIntangibleAssetsWbs()]).then(done => {
        (async () => {
          for (let i = 2; i >= 0; i--) {
            await this.handleSourceData(i);
          };
        })().then(() => {
          this.deactivLoading(); 
        }); 
      })
    });
  }

  //! Temp, fix after presentation
  mainProjectCode: string = "";

  async handleSourceData(i: number) {
    this.setSelectedWbsSource = i;
    this.structureViewDataSource[this.getSelectedWbsSource].data = this.wbs[this.getSelectedWbsSource];
    this.treeControlStorage[this.getSelectedWbsSource] = this.treeControl.dataNodes;

    this.convertToTableView(this.wbs[this.getSelectedWbsSource]).then(() => {
      this.toggleToLevel();
    });
  }

  toggleToLevel(level: number = 0) {
    this.treeControlStorage[this.getSelectedWbsSource].forEach(element => {
      if (this.treeControl.isExpanded(element)) {
        this.treeControl.toggle(element);
      }
      if (element.level <= level && !this.treeControl.isExpanded(element)) {
        this.treeControl.toggle(element);
      }
    })
  }

  toggleCreatingForm(wbs: Wbs) {
    this.setSelectedWbs = wbs;
    this.CreatedPackage.controls['part_code_wbs'].setValue(0);

    this.show.toggleShowBlockTrue(1);
  }

  toggleUpdatingForm(wbs: Wbs) {
    this.setSelectedWbs = wbs;
    this.setFormFields(this.getSelectedWbs);
    
    this.show.toggleShowBlockTrue(2);
  }

  private setFormFields(selectedWbs: Wbs) {
    let form = this.UpdatedPackage;
    Object.keys(form.controls).forEach(key => {
      form.get(key).setValue(selectedWbs[key]);
    })
    form.get('category').setValue(selectedWbs.category_id);
  }

  closeForm(form: FormGroup, blockIndex: number) {
    this.show.closedAdd(blockIndex);
    form.reset();
  }

  togglePackagesList(selectedWbs: Wbs) {
    this.setSelectedWbs = selectedWbs;
    this.show.toggleShowBlock(0);
  }

  async getCategory() {
    return new Promise((resolve, reject) => {
      this.wbsService.getCategory().subscribe(
        (data: any) => {
          console.log(data)
          this.categories = data[0]['data'];
          resolve("done");
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    })
  }

  async getWbs() {
    return new Promise((resolve, reject) => {
      this.wbsService.getWbs().subscribe(
        (data: Wbs[]) => {
          console.log(data)

          //! Temp, fix after presentation
          this.mainProjectCode = data[0].code_wbs;

          this.wbs[0] = this.filterData(data);
          console.log(this.wbs)
          resolve("done");
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    })
  }

  async getTypicalWbs() {
    return new Promise((resolve, reject) => {
      this.wbsService.getTypicalWbs().subscribe(
        (data: Wbs[]) => {
          console.log(data)
          this.wbs[1] = this.filterData(data);
          // console.log(this.wbs)
          resolve("done");
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    })
  }

  async getIntangibleAssetsWbs() {
    return new Promise((resolve, reject) => {
      this.wbsService.getIntangibleAssetsWbs().subscribe(
        (data: Wbs[]) => {
          console.log(data)
          this.wbs[2] = this.filterData(data);
          // console.log(this.wbs)
          resolve("done");
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    })
  }

  filterData(data: Wbs[]): Wbs[] {
    data.forEach(project => {

      //! Temp, fix after presentation
      project.code_wbs = this.mainProjectCode + project.code_wbs.slice(3);
      
      project.children.forEach(direction => {

      //! Temp, fix after presentation
      direction.code_wbs = this.mainProjectCode + direction.code_wbs.slice(3);

        direction.children.forEach(object => {

          //! Temp, fix after presentation
          object.code_wbs = this.mainProjectCode + object.code_wbs.slice(3);

          if (!direction.start_date || !direction.end_date) {
            let newStartDate;
            let newEndDate;

            if (!newStartDate || newStartDate < object.start_date) {
              newStartDate = object.start_date;
            }
            if (!newEndDate || newEndDate > object.start_date) {
              newEndDate = object.end_date;
            }

            direction.start_date = newStartDate;
            direction.end_date = newEndDate;
          }
          object.children.forEach(system => {

            //! Temp, fix after presentation
            system.code_wbs = this.mainProjectCode + system.code_wbs.slice(3);

            system.children.forEach(estimate => {
              
              //! Temp, fix after presentation
              estimate.code_wbs = this.mainProjectCode + estimate.code_wbs.slice(3);

              estimate.children = []
            })
          })
        })
      })
    })
    return data;
  }

  resetFound() {
    this.nameFilter = "";
  }

  filterLeafNode(node: ExampleFlatNode): boolean {
    if (!this.nameFilter) {
      return false
    }
    return (node.name.toLowerCase()
      .indexOf(this.nameFilter?.toLowerCase()) === -1 
      && node.code_wbs.toLowerCase()
      .indexOf(this.nameFilter?.toLowerCase()) === -1)
  }

  filterLeafParentNode(node: ExampleFlatNode): boolean {
    if (!this.nameFilter) {
      return false
    }

    for (let child of node.children) {
      if (child.name.toLowerCase().indexOf(this.nameFilter?.toLowerCase()) !== -1 || child.code_wbs.toLowerCase()
      .indexOf(this.nameFilter?.toLowerCase()) === -1) {
        return false;
      }
    }

    return true;
  }

  private includes(string: string, filterString: string) {
    return (!!filterString && filterString.length)
      ? string.includes(filterString)
      : false;
  }

  tableRowspanInfo = [
    [[], [], [], [], []], 
    [[], [], [], [], []], 
    [[], [], [], [], []]
  ];

  //TODO Levels count more than columns count, remake after specify
  // ! Remove filtered property
  async convertToTableView(data: Wbs[]) {

    let levelNames = [];
    let packagesId = [];
    let wbs = [];
    let coloreds = [];

    let innerCycle = (data) => {
      data?.forEach(element => {
      const rowsCount = this.getTableDataRows(element, element.level);

      const levelName = element.name;
      const levelColored = this.includes(element.name, this.nameFilter);

      levelNames.push(levelName);
      packagesId.push(element.id_package);
      wbs.push(element);
      coloreds.push(levelColored);

      if (element.level == 4) {
        this.tableRowspanInfo[this.getSelectedWbsSource][0].push(0);
        this.tableRowspanInfo[this.getSelectedWbsSource][1].push(0);
        this.tableRowspanInfo[this.getSelectedWbsSource][2].push(0);
        this.tableRowspanInfo[this.getSelectedWbsSource][3].push(0);
        this.tableRowspanInfo[this.getSelectedWbsSource][4].push(rowsCount);

        for (let i = 0; i < rowsCount; i++) {
          const row = [
            {name: levelNames[0], package_id: packagesId[0], wbs: wbs[0], filtered: coloreds[0]},
            {name: levelNames[1], package_id: packagesId[1], wbs: wbs[1], filtered: coloreds[1]},
            {name: levelNames[2], package_id: packagesId[2], wbs: wbs[2], filtered: coloreds[2]},
            {name: levelNames[3], package_id: packagesId[3], wbs: wbs[3], filtered: coloreds[3]},
            {name: levelNames[4], package_id: packagesId[4], wbs: wbs[4],filtered: coloreds[4]}
          ]

          convertedData.push(row);
        }
      } else {
        this.tableRowspanInfo[this.getSelectedWbsSource][element.level].push(rowsCount);

        let filteredElement =
          element.children?.filter(child => child.level <= this.currentFilter);

        innerCycle(filteredElement);

        if (!filteredElement || filteredElement.length <= 0) {
          const row = [];
  
          for (let i = 0; i < 5; i++) {
            if (element.level >= i) {
              this.tableRowspanInfo[this.getSelectedWbsSource][i].push(0);
              row.push({name: levelNames[i], package_id: packagesId[i], wbs: wbs[i], filtered: coloreds[i]});
            } else {
              this.tableRowspanInfo[this.getSelectedWbsSource][i].push(1);
              row.push({name: "", package_id: -1, wbs: null, filtered: false});
            }
          }
  
          convertedData.push(row);
        }

        this.tableRowspanInfo[this.getSelectedWbsSource][element.level].pop();
      }

      levelNames.pop();
      packagesId.pop();
      wbs.pop();
      coloreds.pop();
    })}

    const convertedData = [];

    innerCycle(data);

    this.tableViewDataSources[this.getSelectedWbsSource] = convertedData;
  }

  getRowspan(column: number, elementIndex: number, tableIndex: number) {
    return this.tableRowspanInfo[tableIndex][column][elementIndex];
  }

  getTableDataRows(data: Wbs, level: number) {
    let row = (data.children && data.children.length > 0) ? 0 : 1;

    data.children?.forEach((children) =>
      row += this.getTableDataRows(children, level + 1)
    )

    return row;
  }

  setFilterData(index: number) {
    this.currentFilter = index;
    this.convertToTableView(this.wbs[this.getSelectedWbsSource]);
  }

  addPackage(form: FormGroup, level: number) {
    this.activLoading();
    this.show.closedAdd(1);
    let packageValues = form.value;
    packageValues.parent = this.getSelectedWbs.id;
    packageValues.delete_flg = false;
    packageValues.user_created = 36;
    packageValues.id_package = 1;
    // packageValues.ancestor_code_wbs = this.getSelectedWbs.code_wbs;
    // packageValues.code_wbs = packageValues.ancestor_code_wbs + "." + packageValues.part_code_wbs;
    this.addPackageResponse(this.creatingApi[this.getSelectedWbsSource][level], packageValues);
    form.reset();
  }

  addPackageResponse(handler: Function, value) {
    handler.call(this.wbsService, value).subscribe(
      (response: EstimateFilter[]) => {
        console.log('Data added successfully:', response);
        this.ngOnInit();
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }

  updatePackage(form: FormGroup, level: number) {
    this.activLoading();
    this.show.closedAdd(2);
    let packageValues = form.value;
    packageValues.parent = this.getSelectedWbs.parent;
    packageValues.delete_flg = false;
    packageValues.user_created = 36;
    packageValues.id_package = 1;
    // packageValues.ancestor_code_wbs = this.getSelectedWbs.code_wbs;
    // packageValues.code_wbs = packageValues.ancestor_code_wbs + "." + packageValues.part_code_wbs;
    this.updatePackageResponse(this.updatingApi[this.getSelectedWbsSource][level], packageValues);
    form.reset();
  }

  updatePackageResponse(handler: Function, value: any) {
    handler.call(this.wbsService, value).subscribe(
      (response: EstimateFilter[]) => {
        console.log('Data added successfully:', response);
        this.ngOnInit();
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }


  //Удаление пакетов
  deletePackage(wbs: Wbs) {
    this.activLoading();
    this.show.closedAdd(0);
    let packageValues = this.DeletePackage.value;
    packageValues.id = wbs.id;
    packageValues.name_package = wbs.name_package;
    this.deletePackageResponse(this.deletingApi[this.getSelectedWbsSource][wbs.level], packageValues);
    this.DeletePackage.reset();
  }

  deletePackageResponse(handler: Function, value: any) {
    handler.call(this.wbsService, value).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.ngOnInit();
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
        // Handle error case
      }
    );
  }

}
