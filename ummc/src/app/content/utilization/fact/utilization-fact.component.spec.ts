import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilizationFactComponent } from './utilization-fact.component';

describe('FactComponent', () => {
  let component: UtilizationFactComponent;
  let fixture: ComponentFixture<UtilizationFactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilizationFactComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UtilizationFactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
