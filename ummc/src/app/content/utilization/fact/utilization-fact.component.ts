import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service'
import {ShowService} from '../../../helper/show.service'
import {ApicontentService} from '../../../api.content.service'
import {UtilizationFact, MonthlyPayment} from '../../utilization/fact/utilization-fact'
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-fact',
  templateUrl: './utilization-fact.component.html',
  styleUrls: ['./utilization-fact.component.css']
})
export class UtilizationFactComponent implements OnInit {

  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private wbsService: ApicontentService) {
  }

  ngOnInit(): void {
    this.getFact();
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  displayedColumns: string[] = ["wbs_id", "name", "date_start", "date_end", "amount"]
  years: any = [];
  filterYears: any = [];
  months: string[] = [];
  factData: UtilizationFact[] = [];

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  getSelection(level: number, year: number = 0) {
    let classes: Object;
    switch (level) {
      case 0:
        classes = {
          level0Selection: true,
        }
        break;
      case 1:
        classes = {
          level1Selection: true,
        }
        break;
      case 2:
        classes = {
          level2Selection: true,
        }
        break;
      case 3:
        classes = {
          level3Selection: true,
        }
        break;
      default:
        classes = {
          level4Selection: true,
        }
    }

    if (isNaN(Number(year))) {
      classes['monthsColumn'] = true;
    }

    return classes;
  }

  getFact() {
    this.loading = true;
    this.wbsService.getUtilizationFact().subscribe(
      (data: UtilizationFact[]) => {
        console.log(data)
        this.factData = this.formatCommonData(data);
        this.changeLoading();
      }
    )
  }

  formatCommonData(data: UtilizationFact[]) {
    this.months = [];
    this.displayedColumns = ["wbs_id", "name", "date_start", "date_end", "amount"]

    this.filterYears = data.slice(-1)[0];
    this.years = data.slice(-1)[0];

    this.years = this.years.map(item => {
      const yearNumber = item["year"]
      this.displayedColumns.push(String(yearNumber));
      return yearNumber;
    })

    return data.slice(0, -1);
  }

  getAnnualFact(year: number) {
    this.changeLoading();
    this.wbsService.getUtilizationFact(year).subscribe(
      (data: UtilizationFact[]) => {
        console.log(data)
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length);
        this.factData = this.formatDataByYear(data, year);
        this.changeLoading();
      }
    )
  }

  formatDataByYear(data: UtilizationFact[], filteredYear: number) {
    this.years = data.slice(-1)[0];
    this.months = this.getMonths(data[0].plan_fin);

    this.years = this.years.map(item => {
      return String(item["year"]);
    });

    this.months.reverse().forEach(month => {
      this.years.splice(this.years.indexOf(String(filteredYear)) + 1, 0, month);
    });

    this.displayedColumns = this.displayedColumns.concat(this.years);

    return data.slice(0, -1);
  }

  getMonths(data: MonthlyPayment[]) {
    this.months = [];

    data.filter(payment => isNaN(Number(payment.title))).forEach(payment => {
      this.months.push(payment.title)
    })

    return this.months;
  }

  updateFact() {
    this.loading = true;
    this.wbsService.updateUtilizationFact().subscribe(
      (data: any) => {
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length - 1);
        this.getFact();
      }
    )
  }

  protected readonly isNaN = isNaN
  protected readonly Number = Number
}
