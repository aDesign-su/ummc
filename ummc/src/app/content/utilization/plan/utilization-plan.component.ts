import { Component, OnInit } from '@angular/core';
import {ThemeService} from '../../../theme.service'
import {ShowService} from '../../../helper/show.service'
import {ApicontentService} from '../../../api.content.service'
import {MonthlyPayment, Plan} from './plan'
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms'
import { CommonService } from '../../budget/project-budget/common.service';
import {FinancingFact} from '../../financing/fact/financing-fact'

@Component({
  selector: 'app-plan',
  templateUrl: './utilization-plan.component.html',
  styleUrls: ['./utilization-plan.component.css']
})
export class PlanUtilizationComponent implements OnInit {

  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private wbsService: ApicontentService, private formBuilder: FormBuilder) {
    this.UpdatedPlan = this.formBuilder.group({
      mode: [null, [Validators.required]],
      days_for_delay: [null, [Validators.required]]
    })
    this.searchForm = formBuilder.group({
      searchText: ''
    });
    this.applyFilters();
  }

  ngOnInit(): void {
    this.getPlan();
  }

  UpdatedPlan!: FormGroup;

  _resetFields() {
    Object.keys(this.UpdatedPlan.controls).filter(key => key != "mode").forEach(key => {
      this.UpdatedPlan.get(key).reset();
    });
    if (this._checkFieldByMode('Отсрочка')) {
      this.UpdatedPlan.get('days_for_delay').enable()
    }
  }

  _getControlValue(name: string) {
    return this.UpdatedPlan.get(name);
  }

  _checkFieldByMode(mode: string) {
    return this.UpdatedPlan.get('mode').value == mode;
  }

  _setValidatorsByMode(name: string) {
    Object.keys(this.UpdatedPlan.controls).filter(key => key != "mode").forEach(key => {
      if (name == "Отсрочка") {
        this.UpdatedPlan.get(key).setValidators(Validators.required);
        this.UpdatedPlan.get(key).updateValueAndValidity();
      }
    });
  }

  _disabledSubmitByUsedMode() {
    const mode = this.UpdatedPlan.get("mode").value;
    this._setValidatorsByMode(mode);
    switch (mode) {
      case "Отсрочка":
        return !(this._getControlValue("days_for_delay").valid);
      default:
        return false;
    }
  }

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  displayedColumns: string[] = ["wbs_id", "name", "date_start", "date_end", "amount", "pir", "smr", "equipment", "other", "point_of_fin", "actions"]
  years: any = [];
  filterYears: any = [];
  months: string[] = [];
  planData: Plan[] = [];
  filteredData: Plan[] = [];

  searchForm!: FormGroup;
  modes: string[] = [];

  isPlanUpdateVisible: boolean = false;
  editedPlanId!: number;

  setPlanUpdateVisible(bool: boolean) {
    this.isPlanUpdateVisible = bool;
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  getSelection(level: number, year: number = 0) {
    let classes: Object;
    switch (level) {
      case 0:
        classes = {
          level0Selection: true,
        }
        break;
      case 1:
        classes = {
          level1Selection: true,
        }
        break;
      case 2:
        classes = {
          level2Selection: true,
        }
        break;
      case 3:
        classes = {
          level3Selection: true,
        }
        break;
      default:
        classes = {
          level4Selection: true,
        }
    }

    if (isNaN(Number(year))) {
      classes['monthsColumn'] = true;
    }

    return classes;
  }

  getPlan() {
    this.loading = true;
    this.wbsService.getUtilizationPlan().subscribe(
      (data: Plan[]) => {
        console.log(data)
        this.planData = this.formatCommonData(data);
        this.changeLoading();
        this.applyFilters()
      }
    )
  }

  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    this.filteredData = this.planData.filter((plan: Plan) => {
      return this.matchesSearchText(plan, searchText)
    });
  }

  matchesSearchText(plan:Plan, searchText: string): boolean {
    return (plan.name_wbs.toString().toLowerCase() ?? '').includes(searchText);
  }

  getModes(id: number) {
    this.wbsService.getUtilizationModes(id).subscribe(
      (data: any[]) => {
        console.log(data)
        for (let [key, mode] of Object.entries(data[1])) {
          this.modes.push(String(mode));
        }
        this.UpdatedPlan.get('mode').setValue(data[0]['mode']);
        if (this._checkFieldByMode('По умолчанию')) {
          this.UpdatedPlan.get('days_for_delay').disable()
        }

        this.setPlanUpdateVisible(true);
      }
    )
  }

  formatCommonData(data: Plan[]) {
    this.months = [];
    this.displayedColumns = ["wbs_id", "name", "date_start", "date_end", "amount", "pir", "smr", "equipment", "other", "point_of_fin", "actions"]

    this.filterYears = data.slice(-1)[0];
    this.years = data.slice(-1)[0];

    this.years = this.years.map(item => {
      const yearNumber = item["year"]
      this.displayedColumns.push(String(yearNumber));
      return yearNumber;
    })

    return data.slice(0, -1);
  }

  getAnnualPlan(year: number) {
    this.changeLoading();
    this.wbsService.getUtilizationPlan(year).subscribe(
      (data: Plan[]) => {
        console.log(data)
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length);
        this.planData = this.formatDataByYear(data, year);
        this.changeLoading();
      }
    )
  }

  formatDataByYear(data: Plan[], filteredYear: number) {
    this.years = data.slice(-1)[0];
    this.months = this.getMonths(data[0]['plan_util']);

    this.years = this.years.map(item => {
      return String(item["year"]);
    });

    this.months.reverse().forEach(month => {
      this.years.splice(this.years.indexOf(String(filteredYear)) + 1, 0, month);
    });

    this.displayedColumns = this.displayedColumns.concat(this.years);

    return data.slice(0, -1);
  }

  getMonths(data: MonthlyPayment[]) {
    this.months = [];

    data.filter(payment => isNaN(Number(payment.title))).forEach(payment => {
      this.months.push(payment.title)
    })

    return this.months;
  }

  editPlan(id: number) {
    this.editedPlanId = id;
    this.getModes(id);
  }

  updatePlan() {
    this.loading = true;
    this.wbsService.updateUtilizationPlan().subscribe(
      (data: any) => {
        this.displayedColumns = this.displayedColumns.slice(0, - this.years.length - 1);
        this.getPlan();
        this.theme.info = 'Обновление успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  savePlan() {
    this.setPlanUpdateVisible(false);
    this.modes = [];
    this.wbsService.updateUtilizationModes(this.UpdatedPlan.value, this.editedPlanId).subscribe(
      (data: any) => {
        this.UpdatedPlan.reset();
        this.getPlan();
        this.theme.info = 'План обновлен успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить план. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  protected readonly isNaN = isNaN
  protected readonly Number = Number
}
