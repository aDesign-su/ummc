import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanUtilizationComponent } from './utilization-plan.component';

describe('PlanComponent', () => {
  let component: PlanUtilizationComponent;
  let fixture: ComponentFixture<PlanUtilizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanUtilizationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanUtilizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
