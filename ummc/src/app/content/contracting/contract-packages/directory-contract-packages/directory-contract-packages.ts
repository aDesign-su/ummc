export type DirectoryContractPackages = DataDirectoryContractPackages[]

export interface DataDirectoryContractPackages {
  id?: number
  code?: string
  name?: string
  discipline?: string
  project_list?: ProjectList[]
}

export interface ProjectList {
  id: number
  code_wbs: string
  name: string
  discipline?: string
  project_id: number
}
