import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { DataDirectoryContractPackages, DirectoryContractPackages, ProjectList } from './directory-contract-packages';
import * as jmespath from 'jmespath';
import { ShowService } from 'src/app/helper/show.service';
declare var bootstrap: any;

@Component({
  selector: 'app-directory-contract-packages',
  templateUrl: './directory-contract-packages.component.html',
  styleUrls: ['./directory-contract-packages.component.css']
})
export class DirectoryContractPackagesComponent implements OnInit {
  dataDirectoryContractPackagesTable = this.theme.initTable<DataDirectoryContractPackages>();
  @ViewChild('PaginatorDirectoryContractPackages') paginatorForDirectoryContractPackages: MatPaginator;
  SelectProjectList: ProjectList[]
  filteredListProjectList: ProjectList[]
  public SelectProject: number;
  FormAddEditDirectoryContractPackages: FormGroup;
  isLoading = true;
  editMode = false;
  constructor(public theme: ThemeService, private router: Router, private fb: FormBuilder, public show: ShowService, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {

    this.FormAddEditDirectoryContractPackages = this.fb.group({
      id: [null],
      project_id: [null],
      typical_code: [''],
      name: [''],
      discipline: [''],
    });

  }

  ngOnInit() {
    this.SelectProject = 0;
    this.getDataDirectoryContractPackages(0)
  }
  ngAfterViewInit() {
    this.dataDirectoryContractPackagesTable.paginator = this.paginatorForDirectoryContractPackages;
  }

  add() {
    this.editMode = false
    this.FormAddEditDirectoryContractPackages.reset()
  }
  // Получить Справочник
  getDataDirectoryContractPackages(key: number): void {
    this.isLoading = true; // Начало загрузки данных
    this.params.getDirectoryContractPackages(key).subscribe(
      (data: DirectoryContractPackages) => {
        this.dataDirectoryContractPackagesTable.data = data.slice(0, -1);
        this.SelectProjectList = data[data.length - 1].project_list;
        this.filteredListProjectList = this.SelectProjectList.slice();
        this.isLoading = false; // Загрузка завершена
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  // Добавить
  createDirectoryContractPackages(data) {
    console.log('create:', data)
    this.params.addDirectoryContractPackages(data).subscribe(
      (data: ProjectList) => {
        console.log(data)

        this.theme.openSnackBar(`Добавлен типовой контрактный пакет `);
        this.getDataDirectoryContractPackages(0)
        const offcanvasElement = document.getElementById('addDirectoryContractPackages');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditDirectoryContractPackages.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }

  // Добавить Изменить  
  AddEditDirectoryContractPackages() {
    if (this.FormAddEditDirectoryContractPackages.valid) {
      const formData = { ...this.FormAddEditDirectoryContractPackages.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;

      if (this.editMode) {
        this.updateDirectoryContractPackages(id, formValueWithoutId);
      } else {
        this.createDirectoryContractPackages(formValueWithoutId);
      }
      
    }

  }

  // Изменить
  editDirectoryContractPackages(element) {
    console.log(element)
    this.FormAddEditDirectoryContractPackages.setValue({
      id: element.id,
      project_id: element.project_id,
      typical_code: element.typical_code,
      name: element.name,
      discipline: element.discipline,
    });
    console.log(this.FormAddEditDirectoryContractPackages.value)
    this.editMode = true
  }

  // Обновление 
  updateDirectoryContractPackages(id: number, UpdatedData) {
    this.params.editDirectoryContractPackages(id, UpdatedData).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addDirectoryContractPackages');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditDirectoryContractPackages.reset()

        // Найти индекс элемента, который нужно обновить
        const index = this.dataDirectoryContractPackagesTable.data.findIndex(item => item.id === response.id);
        console.log(this.dataDirectoryContractPackagesTable.data[index])
        // Проверить, найден ли элемент
        if (index !== -1) {
          // Заменить элемент в массиве
          this.dataDirectoryContractPackagesTable.data[index] = response;
          this.dataDirectoryContractPackagesTable.data = [...this.dataDirectoryContractPackagesTable.data];
        }
        console.log(this.dataDirectoryContractPackagesTable.data)
        this.editMode = false;
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  // Удаление
  delDirectoryContractPackages(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delDirectoryContractPackages(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Типовой контрактный пакет удален');
            const index = this.dataDirectoryContractPackagesTable.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataDirectoryContractPackagesTable.data.splice(index, 1);
              this.dataDirectoryContractPackagesTable._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  applyFilterByProject(id: any) {
    console.log(id.value)
    this.SelectProject = id.value
    this.getDataDirectoryContractPackages(id.value)
  }
}
