import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThemeService } from 'src/app/theme.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { ShowService } from 'src/app/helper/show.service';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import moment from 'moment';
import { ContractPackage, ContractPackageItems, Deviation, ObjectWBSList, Plan, Tender } from './contract-packages';
import { ContractPackagesData } from '../contract_packages';
declare var bootstrap: any;


@Component({
  selector: 'app-contract-packages-item',
  templateUrl: './contract-packages-item.component.html',
  styleUrls: ['./contract-packages-item.component.css']
})

export class ContractPackagesItemComponent implements OnInit {

  DataContractPackagesItem: ContractPackageItems
  ContractPackageItemsTableInfo = this.theme.initTable<ContractPackage>();//Информация в шапке
  ContractPackagesItemPlanTable = this.theme.initTable<Plan>();//Плановая стоимость контрактного пакета
  ContractPackagesItemTenderTable = this.theme.initTable<Tender>();//Стоимость контрактного пакета по результатам тендера
  ContractPackagesItemDeviationTable = this.theme.initTable<Deviation>();//Отклонение тенедреной стоимости от плановой
  FormWorkPackages: FormGroup;

  ObjectList: ObjectWBSList[]
  WBSList: ObjectWBSList[]
  filteredListObjectList: ObjectWBSList[]
  filteredListWBSList: ObjectWBSList[]
  isLoadingWBS: boolean = false;
  FormEditContractPackagesItemTender: FormGroup;


  constructor(private route: ActivatedRoute, public show: ShowService, public theme: ThemeService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe) {
    this.FormWorkPackages = this.fb.group({
      object: [null],
      wbs: [{ value: '', disabled: false }, Validators.required]
    });
    this.FormEditContractPackagesItemTender = this.fb.group({
      id: [null],
      pir: [null],
      smr: [null],
      equipment: [null],
      other: [null],
    });
  }

  ngOnInit() {
    const requestId = this.route.snapshot.paramMap.get('id');
    if (requestId) {
      console.log(requestId)
      this.getContractPackagesItem(requestId)
      this.getObject_WBS(0)
    }

  }
  add() {
    this.FormWorkPackages.reset()
  }
  getContractPackagesItem(id): void {
    console.log(id)
    this.params.getContractPackagesDetail(id).subscribe(
      (data: ContractPackageItems) => {
        this.DataContractPackagesItem = data
        this.ContractPackageItemsTableInfo.data = data.map(item => item.contract_package);
        this.ContractPackagesItemPlanTable.data = data[0].plan
        this.ContractPackagesItemTenderTable.data = data[0].tender
        this.ContractPackagesItemDeviationTable.data = data[0].deviation
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }


  /////////////////////////////Для выбора СДР////////////////////////////////
  getObject_WBS(id_object) {
    this.isLoadingWBS = true; 
    console.log('выбран обект', id_object)
    const id_contract_package = this.route.snapshot.paramMap.get('id');
    console.log(id_contract_package, id_object)
    this.params.getWBSContractPackagesDetail(id_contract_package, id_object).subscribe(
      
      (data) => {
        if (id_object == 0) {
          this.ObjectList = data
          this.filteredListObjectList = this.ObjectList.slice();
          console.log(this.ObjectList)
          this.isLoadingWBS = false;
        }
        else {
          this.WBSList = data
          this.filteredListWBSList = this.WBSList.slice();
          console.log('WBSList', this.WBSList)
          this.isLoadingWBS = false;
        }

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  // Добавить
  AddWorkPackagse() {
    console.log('create:', this.FormWorkPackages)
    const formData = { ...this.FormWorkPackages.value };
    const id_contract_package = this.route.snapshot.paramMap.get('id');
    delete formData.object;

    this.params.addContractPackagesItem(id_contract_package, formData).subscribe(
      (data: any) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлен пакет работ`);
        const offcanvasElement = document.getElementById('addWorkPackages');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        console.log(data.tender)
        this.ContractPackagesItemPlanTable.data = [...this.ContractPackagesItemPlanTable.data, data.plan];
        this.ContractPackagesItemTenderTable.data = [...this.ContractPackagesItemTenderTable.data, data.tender];
        this.ContractPackagesItemDeviationTable.data = [...this.ContractPackagesItemDeviationTable.data, data.deviation];

        this.FormWorkPackages.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }


  editContractPackagesItemTender(element: Tender) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormEditContractPackagesItemTender.setValue({
      id: element.id,
      pir: element.pir,
      smr: element.smr,
      equipment: element.equipment,
      other: element.other
    });
    console.log(this.FormEditContractPackagesItemTender)
  }

  updateContractPackagesItemTender(data) {
    const id = data.id;
    const formValueWithoutId = { ...data };
    delete formValueWithoutId.id;
    console.log('formValueWithoutId :', formValueWithoutId);
    this.params.editContractPackagesItemTender(id, formValueWithoutId).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('editContractPackagesItemTender');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormEditContractPackagesItemTender.reset()


        // Найти индекс элемента, который нужно обновить
        const index_contract_packages = this.ContractPackageItemsTableInfo.data.findIndex(item => item.id === response.contract_package.id);
        console.log(this.ContractPackageItemsTableInfo.data[index_contract_packages])
        // Проверить, найден ли элемент
        if (index_contract_packages !== -1) {
          // Заменить элемент в массиве
          this.ContractPackageItemsTableInfo.data[index_contract_packages] = response.contract_package;
          this.ContractPackageItemsTableInfo.data = [...this.ContractPackageItemsTableInfo.data];
        }


        // Найти индекс элемента, который нужно обновить
        const index_tender = this.ContractPackagesItemTenderTable.data.findIndex(item => item.id === response.tender.id);
        console.log(this.ContractPackagesItemTenderTable.data[index_tender])
        // Проверить, найден ли элемент
        if (index_tender !== -1) {
          // Заменить элемент в массиве
          this.ContractPackagesItemTenderTable.data[index_tender] = response.tender;
          this.ContractPackagesItemTenderTable.data = [...this.ContractPackagesItemTenderTable.data];
        }

        // Найти индекс элемента, который нужно обновить
        const index_deviation = this.ContractPackagesItemDeviationTable.data.findIndex(item => item.id === response.deviation.id);
        console.log(this.ContractPackagesItemDeviationTable.data[index_deviation])
        // Проверить, найден ли элемент
        if (index_deviation !== -1) {
          // Заменить элемент в массиве
          this.ContractPackagesItemDeviationTable.data[index_deviation] = response.deviation;
          this.ContractPackagesItemDeviationTable.data = [...this.ContractPackagesItemDeviationTable.data];
        }
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  // Удаление разделительную ведомость
  delContractPackagesItem(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delContractPackagesItem(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Пакет работ удален');

            const index = this.ContractPackagesItemPlanTable.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.ContractPackagesItemPlanTable.data.splice(index, 1);
              this.ContractPackagesItemPlanTable._updateChangeSubscription();
              this.ContractPackagesItemTenderTable.data.splice(index, 1);
              this.ContractPackagesItemTenderTable._updateChangeSubscription();
              this.ContractPackagesItemDeviationTable.data.splice(index, 1);
              this.ContractPackagesItemDeviationTable._updateChangeSubscription();
            }
            // const index_tender = this.ContractPackagesItemTenderTable.data.findIndex(item => item.id === id);
            // if (index_tender > -1) {
            //   this.ContractPackagesItemTenderTable.data.splice(index_tender, 1);
            //   this.ContractPackagesItemTenderTable._updateChangeSubscription();
            // }
            // const index_deviation = this.ContractPackagesItemDeviationTable.data.findIndex(item => item.id === id);
            // if (index_deviation > -1) {
            //   this.ContractPackagesItemDeviationTable.data.splice(index_deviation, 1);
            //   this.ContractPackagesItemDeviationTable._updateChangeSubscription();
            // }


          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }
}
