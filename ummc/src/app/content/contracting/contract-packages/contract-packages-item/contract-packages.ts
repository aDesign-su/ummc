
export interface ContractPackageItems {
  map(arg0: (item: any) => any): import("../contract_packages").ContractPackagesData[]
  contract_package?: ContractPackage
  plan: Plan
  tender: Tender[]
  deviation: Deviation[]
}

export interface ContractPackage {
  id: number
  code?: string
  name?: string
  discipline?: string
  discipline_id: number,
  cost_planned?: number
  cost_trending?: number
  cost_deviation?: number

}


export interface Plan {
  id: number
  code_wbs: string
  name: string
  total_cost: string
  pir: string
  smr: string
  equipment: string
  other: string
  category: string
}

export interface Tender {
  id: number
  code_wbs: string
  name: string
  total_cost: string
  pir: string
  smr: string
  equipment: string
  other: string
  category: string
}

export interface Deviation {
  id: number
  code_wbs: string
  name: string
  total_cost: string
  pir: string
  smr: string
  equipment: string
  other: string
  category: string
}

export interface ObjectWBSList{
  id: number
  code_name: string
}