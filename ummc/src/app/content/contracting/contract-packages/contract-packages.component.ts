import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractPackages, ContractPackagesData, DirectoryList, ProjectList } from './contract_packages';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;
@Component({
  selector: 'app-contract-packages',
  templateUrl: './contract-packages.component.html',
  styleUrls: ['./contract-packages.component.css']
})
export class ContractPackagesComponent implements OnInit {
  dataContractPackagesTableSource = this.theme.initTable<ContractPackagesData>();
  @ViewChild('PaginatorContractPackages') paginatorForContractPackages: MatPaginator;
  SelectProjectList: ProjectList[]
  public SelectProject: number;
  FormAddEditContractPackages: FormGroup;



  isLoading = true;
  editMode = false;

  // Для поиска внутри формы
  filteredListDirectoryList: DirectoryList[]
  public SelectDirectoryList: ProjectList[]

  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.FormAddEditContractPackages = this.fb.group({
      id: [null],
      name: [''],
      discipline: [null],
    });

  }

  ngOnInit() {
    this.getDataContractPackages(0)
    this.SelectProject = 0;
  }

  ngAfterViewInit() {
    this.dataContractPackagesTableSource.paginator = this.paginatorForContractPackages;
  }
  add() {
    this.editMode = false
    this.FormAddEditContractPackages.reset()
  }
  getDataContractPackages(key: number): void {
    this.isLoading = true;
    this.params.getContractPackages(key).subscribe(
      (data: any) => {
        console.log(data)
        this.dataContractPackagesTableSource.data = data.slice(0, -1);
        console.log(this.dataContractPackagesTableSource.data)
        this.SelectProjectList = data[data.length - 1].project_list;


        this.SelectDirectoryList = data[data.length - 1].directory_list;
        this.filteredListDirectoryList = this.SelectDirectoryList.slice();
        this.isLoading = false;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }



  applyFilterByProject(id: any) {
    console.log(id.value)
    this.SelectProject = id.value
    this.getDataContractPackages(id.value)
  }


  // Добавить Изменить  
  AddEditContractPackages() {
    if (this.FormAddEditContractPackages.valid) {
      const formData = { ...this.FormAddEditContractPackages.value };
      const id = formData.id;
      const formValueWithoutId = { ...formData };
      delete formValueWithoutId.id;

      if (this.editMode) {
        console.log(id, formValueWithoutId)
        // this.updateDirectoryContractPackages(id, formValueWithoutId);
        this.updateContractPackages(id, formValueWithoutId) 
      } else {
        console.log(formValueWithoutId)

        this.createDirectoryContractPackages(formValueWithoutId);
      }

    }

  }

  // Изменить
  editContractPackages(element) {
    console.log(element)
    this.FormAddEditContractPackages.setValue({
      id: element.id,
      name: element.name,
      discipline: element.discipline_id,
    });
    console.log(this.FormAddEditContractPackages.value)
    this.editMode = true
  }

  // Обновление 
  updateContractPackages(id: number, UpdatedData) {
    this.params.editContractPackages(id, UpdatedData).subscribe(
      (response: any) => {
        console.log('Новые данные:', response);

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('addContractPackages');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditContractPackages.reset()

        // Найти индекс элемента, который нужно обновить
        const index = this.dataContractPackagesTableSource.data.findIndex(item => item.id === response.id);
        console.log(this.dataContractPackagesTableSource.data[index])
        // Проверить, найден ли элемент
        if (index !== -1) {
          // Заменить элемент в массиве
          this.dataContractPackagesTableSource.data[index] = response;
          this.dataContractPackagesTableSource.data = [...this.dataContractPackagesTableSource.data];
        }
        console.log(this.dataContractPackagesTableSource.data)
        this.editMode = false;
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

  // Добавить
  createDirectoryContractPackages(data) {
    console.log('create:', data)
    this.params.addContractPackages(data).subscribe(
      (data: ProjectList) => {
        console.log(data)
        this.theme.openSnackBar(`Добавлен контрактный пакет `);
        this.getDataContractPackages(0)
        const offcanvasElement = document.getElementById('addContractPackages');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormAddEditContractPackages.reset()
      },
      (error: any) => {
        console.error('Error fetching data:', error.error['error']);
        this.theme.openSnackBar(error.error['error']);

      }
    );
  }

  // Удаление
  delContractPackages(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delContractPackages(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Контрактный пакет удален');
            const index = this.dataContractPackagesTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataContractPackagesTableSource.data.splice(index, 1);
              this.dataContractPackagesTableSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  // Разделительная ведомость// подробнее
  moreContractPackages(requestId: number): void {
    this.router.navigate(['/contract-packages-item', requestId]);
  }
}
