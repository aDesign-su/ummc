export type ContractPackages = ContractPackagesData[]

export interface ContractPackagesData {
  id: number
  code?: string
  name?: string
  discipline?: string
  discipline_id: number,
  cost_planned?: number
  cost_trending?: number
  cost_deviation?: number
  project_list?: ProjectList[]
  directory_list?: DirectoryList[]
}

export interface ProjectList {
  id: number
  code: string
  name: string
}

export interface DirectoryList {
  id: number
  code: string
  name: string
}