import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { SeparationSheetData } from './separation-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;
@Component({
  selector: 'app-separation-sheet',
  templateUrl: './separation-sheet.component.html',
  styleUrls: ['./separation-sheet.component.css']
})
export class SeparationSheetComponent implements OnInit {
  dataPlanTableSource = this.initTable<SeparationSheetData>();
  dataFactTableSource = this.initTable<SeparationSheetData>();
  @ViewChild('PaginatorPlan') paginatorForRiskPlan: MatPaginator;
  @ViewChild('PaginatorFact') paginatorForRiskActual: MatPaginator;
  FormAddSeparationSheetPlan: FormGroup;
  FormEditSeparationSheet: FormGroup;

  contractsData: any[] = [];
  EstimatesData: any[] = [];
  // Общая функция для инициализации
  private initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }
  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.FormAddSeparationSheetPlan = this.fb.group({
     
      estimate: ['', Validators.required],
      name: ['', Validators.required],

    });
    this.FormEditSeparationSheet = this.fb.group({
      id_element: ['', Validators.required],
      request_id: ['', Validators.required],
      name: ['', Validators.required],
      contract: ['', Validators.required],
    });

  }

  ngOnInit() {
    this.getDataSeparationSheets()
    this.getContractList()
    this.getDataRegisterEstimates()
  }


  ngAfterViewInit() {
    this.dataPlanTableSource.paginator = this.paginatorForRiskPlan;
    this.dataFactTableSource.paginator = this.paginatorForRiskActual;
  }

  getDataSeparationSheets(): void {
    this.params.getSeparationSheetsPlan().subscribe(
      (data: SeparationSheetData[]) => {
        this.dataPlanTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
    this.params.getSeparationSheetsFact().subscribe(
      (data: SeparationSheetData[]) => {
        this.dataFactTableSource.data = data;
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  createSeparationSheetPlan(): void {
    if (this.FormAddSeparationSheetPlan.valid) {
      const formData: SeparationSheetData = this.FormAddSeparationSheetPlan.value
      console.log(formData)
      this.params.addSeparationSheet(formData).subscribe(
        (data: SeparationSheetData) => {
          console.log(data)

          // Вместо повторного получения всех данных просто добавляем новую запись
          this.dataPlanTableSource.data = [...this.dataPlanTableSource.data, data];

          this.theme.openSnackBar('Добавлена разделительная ведомость')
          const offcanvasElement = document.getElementById('addScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.FormAddSeparationSheetPlan.reset()
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  // Разделительная ведомость// подробнее
  moreeparationSheetPlan(requestId: number): void {
    this.router.navigate(['/separation-sheet-item', requestId]);
  }
  // Удаление разделительную ведомость
  delSeparationSheetPlan(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.deliteSeparationSheet(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Разделительная ведомость удалена');
            const index = this.dataPlanTableSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataPlanTableSource.data.splice(index, 1);
              this.dataPlanTableSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  editSeparationShee(element) {
    console.log(element)
    // Заполняем форму редактирования данными из элемента резерва
    this.FormEditSeparationSheet.setValue({
      id_element: element.id,
      name: element.name,
      request_id: element.request_id,
      contract: ''
    });
  }


  // Полуить данные Сметы
  getDataRegisterEstimates() {
    this.params.getRegisterEstimates().subscribe(
      (data) => {
        console.log('вернкломь ',data['data'])
        this.EstimatesData = data['data'];

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  //Получаем все Договора
  getContractList(): void {
    this.params.getContractList().subscribe(
      (data) => {
        console.log(data)
        this.contractsData = data;

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  updateSeparationSheet(data) {
    const id = data.id_element;
    const formValueWithoutId = { ...data };
    delete formValueWithoutId.id_element;
    console.log('formValueWithoutId :', formValueWithoutId);
    this.params.updateSeparationSheets(id, formValueWithoutId).subscribe(
      (response: SeparationSheetData) => {
        console.log('Новые данные:', response);

        // Вместо повторного получения всех данных просто добавляем новую запись
        this.dataFactTableSource.data = [...this.dataFactTableSource.data, response];

        this.theme.openSnackBar('Изменения сохранены')
        const offcanvasElement = document.getElementById('editSeparationShee');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.FormEditSeparationSheet.reset()
        const index = this.dataPlanTableSource.data.findIndex(item => item.id === id);
        if (index > -1) {
          this.dataPlanTableSource.data.splice(index, 1);
          this.dataPlanTableSource._updateChangeSubscription();
        }
      },
      (error: any) => {
        this.theme.openSnackBar(error.error['error'])
      }
    )
  }

}
