export interface SeparationSheetsItems {
  id: number
  request_id: number
  name: string
  status: boolean
  code_wbs: string
  updated: string
  items: Item[]
}

export interface Item {
  id: number
  request_number: number
  request_date: string
  item_id: number
  material_or_equipment: string
  code_wbs: string
  spp_element_number: string
  object_name: string
  p_object: string
  mtp_name: string
  technical_specifications: string
  unit_of_measurement: string
  quantity: string
  quantity_per_request: string
  remaining_quantity: string
  survey_sheet_or_technical_assignment: string
  issue_date: string
  title: string
  brand: string
  delivery_split: string
  delivery_plan: string
  mto_responsible: string
  notes: string
  separation_sheet: number
}