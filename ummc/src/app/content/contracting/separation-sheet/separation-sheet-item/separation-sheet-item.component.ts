import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThemeService } from 'src/app/theme.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Item, SeparationSheetsItems } from './separation-sheet-item';
import { ShowService } from 'src/app/helper/show.service';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import moment from 'moment';
declare var bootstrap: any;

@Component({
  selector: 'app-separation-sheet-item',
  templateUrl: './separation-sheet-item.component.html',
  styleUrls: ['./separation-sheet-item.component.css']
})
export class SeparationSheetItemComponent implements OnInit {
  DataSeparationSheetItem: SeparationSheetsItems
  dataSource = new MatTableDataSource<Item>([])
  stickyHeadersValue = ['header-1'];
  FormAddSeparationSheetItem: FormGroup;
  tables = [0];
  editMode = false;
  selectItem: number
  displayedColumns: string[] = ['id_position', 'request_number', 'request_date', 'item_id', 'material_or_equipment', 'code_wbs',
    'spp_element_number', 'object_name', 'p_object', 'mtp_name', 'technical_specifications',
    'unit_of_measurement', 'quantity', 'quantity_per_request', 'remaining_quantity',
    'survey_sheet_or_technical_assignment', 'issue_date', 'title', 'brand', 'delivery_split',
    'delivery_plan', 'mto_responsible', 'notes', 'separation_sheet', 'action'
  ];
  ///////////

  constructor(private route: ActivatedRoute, private router: Router, public show: ShowService, public theme: ThemeService, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe,) {
    this.displayedColumns.length = 24;
    this.displayedColumns[0] = 'id_position';
    this.displayedColumns[this.displayedColumns.length - 1] = 'action';
    console.log(this.displayedColumns.length)

    this.FormAddSeparationSheetItem = this.fb.group({
      material_or_equipment: [null],
      spp_element_number: [''],
      mtp_name: [''],
      technical_specifications: [''],
      unit_of_measurement: [''],
      quantity: [null],
      quantity_per_request: [null],
      survey_sheet_or_technical_assignment: [''],
      issue_date: [null],
      title: [''],
      brand: [''],
      delivery_split: [''],
      delivery_plan: [null],
      mto_responsible: [''],
      notes: [''],
      separation_sheet: [null]
    });
  }

  isSticky(buttonToggleGroup: MatButtonToggleGroup, id: string) {
    return (buttonToggleGroup.value || []).indexOf(id) !== -1;
  }
  ngOnInit() {
    const requestId = this.route.snapshot.paramMap.get('id');
    if (requestId) {
      console.log(requestId)
      this.getDataSeparationSheetsItems(requestId)
    }

  }


  getDataSeparationSheetsItems(id): void {
    console.log(id)
    this.params.getSeparationSheetsDetail(id).subscribe(
      (data: SeparationSheetsItems) => {
        this.DataSeparationSheetItem = data
        this.dataSource.data = this.DataSeparationSheetItem['items'];
        console.log(this.dataSource)
        console.log(this.DataSeparationSheetItem)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  add() {
    this.editMode = false
    this.FormAddSeparationSheetItem.reset()
  }
  CreateEditSeparationSheetsItems(): void {
    this.FormAddSeparationSheetItem.patchValue({
      separation_sheet: this.DataSeparationSheetItem.id
    });
    // Преобразование даты issue_date 
    const issueDate = moment(this.FormAddSeparationSheetItem.value.issue_date).format('YYYY-MM-DD');
    this.FormAddSeparationSheetItem.get('issue_date').setValue(issueDate);

    // Преобразование даты delivery_plan 
    const deliveryPlan = moment(this.FormAddSeparationSheetItem.value.delivery_plan).format('YYYY-MM-DD');
    this.FormAddSeparationSheetItem.get('delivery_plan').setValue(deliveryPlan);

    if (this.FormAddSeparationSheetItem.valid) {
      const formData: Item = this.FormAddSeparationSheetItem.value
      console.log(formData)
      if (!this.editMode) {
        this.params.addSeparationSheetItem(formData).subscribe(
          (request: Item) => {
            console.log(request)
            // Вместо повторного получения всех данных просто добавляем новую запись
            this.dataSource.data = [...this.dataSource.data, request];

            this.theme.openSnackBar('Добавлена разделительная ведомость')
            const offcanvasElement = document.getElementById('addSeparationSheetItem');
            const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
            if (offcanvasInstance) {
              offcanvasInstance.hide();
            }
            this.FormAddSeparationSheetItem.reset()
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
      else {
        this.params.editSeparationSheetItem(this.selectItem, formData).subscribe(
          (request: Item) => {
            console.log(request)
            // Находим индекс обновленного элемента
            const updatedItemIndex = this.dataSource.data.findIndex(item => item.id === request.id);
            // Обновляем этот элемент в массиве
            if (updatedItemIndex > -1) {
              this.dataSource.data[updatedItemIndex] = request;
            }
            // Обновляем таблицу с новым массивом данных
            this.dataSource.data = [...this.dataSource.data];
            
            this.theme.openSnackBar('Изменения сохранены')
            const offcanvasElement = document.getElementById('addSeparationSheetItem');
            const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
            if (offcanvasInstance) {
              offcanvasInstance.hide();
            }
            this.FormAddSeparationSheetItem.reset()
            this.editMode = false
            this.selectItem = null
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }



    }
  }
  //Скачать Excel
  downloadExcelSeparationSheetItemFile(): void {
    this.params.downloadSeparationSheetItemFile(this.route.snapshot.paramMap.get('id')).subscribe(blob => {
      const currentDate = new Date();
      const dateString = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
      const fileName = `Разделительная ведомость (${this.DataSeparationSheetItem.name}) ${dateString}.xlsx`;

      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = fileName;
      link.click();
      URL.revokeObjectURL(link.href);
    }, error => {
      // Обработка ошибки
      console.error('Download error!', error);
    });
  }

  // Удаление Item из разделительной ведомости
  delSeparationSheetItem(id: number) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params.delSeparationSheetItem(id).subscribe(
          (data: any) => {

            this.theme.openSnackBar('Материал/Оборудование удалено');
            const index = this.dataSource.data.findIndex(item => item.id === id);
            if (index > -1) {
              this.dataSource.data.splice(index, 1);
              this.dataSource._updateChangeSubscription();
            }
          },
          (error: any) => {
            console.error('Error fetching data:', error);
          }
        );
      }
    });
  }

  // Изменить МРТ/О 
  editSeparationSheetItem(element) {
    this.FormAddSeparationSheetItem.patchValue(element);
    this.editMode = true
    this.selectItem = element.id
  }
}

