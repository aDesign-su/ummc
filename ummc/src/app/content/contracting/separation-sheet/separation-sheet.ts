export interface SeparationSheetData {
    id:number
    request_id: number
    name: string,
    estimate_name:string,
    code_wbs: string
    contract?: number
    updated: string
}
