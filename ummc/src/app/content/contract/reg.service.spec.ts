/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RegService } from './reg.service';

describe('Service: Reg', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegService]
    });
  });

  it('should ...', inject([RegService], (service: RegService) => {
    expect(service).toBeTruthy();
  }));
});
