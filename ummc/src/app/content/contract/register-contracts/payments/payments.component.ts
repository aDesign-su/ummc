import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { WBSList, listContracts, payments } from '../reg-contracts';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { RegService } from '../../reg.service';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css'],
  providers: [
    
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class PaymentsComponent implements OnInit {
  constructor(public theme: ThemeService, public contractsService: ApicontentService,public reg:RegService,public show:ShowService,private formBuilder: FormBuilder,private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.reg.currentSetId.subscribe((id:number) => {
      this.getId = id
      console.log(this.getId)
    });
    this.reg.currentFactId.subscribe((id:number) => {
      this.getFactId = id
      console.log(this.getFactId)
    });
    this.editPayment = this.formBuilder.group({
      type_pay: [],
      date_pay: [],
      stage: [],
      budget_item: [''],
      amount: []
    });
    this.editWBSPlan = this.formBuilder.group({
      amount: [''],
    });
    // this.contractsService.getPaymentsFactWBS(this.getId, this.getFactId).subscribe(
    //   (response: WBSList[]) => {
    //     console.log(response);
    //     this.WBSList = response
    //     this.WBSList.forEach(item => {
    //       if (this.addWBSListPlan.some(fact => fact.code === item.code)) {
    //         item.selected = true;
    //       }
    //     });
    //   },
    //   (error: any) => {
    //     console.error('Failed to update data:', error);
    //   }
    // )
   }

  ngOnInit() {
    this.reg.getPaymentsPlan(this.getId)
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  editPayment: FormGroup
  editWBSPlan: FormGroup

  WBSList: WBSList[] = []
  WBSListPlan: any[] = []
  WBSListPlans: any[] = []
  
  getId: number
  getFactId: number

  typePay: any | null = null
  typePays!: any
  // payments: payments[] = []
  dataSource = new MatTableDataSource<listContracts>([]);
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  // Пагинация для вложенной таблицы Платежи
  onPagePayments(event: PageEvent): void {
    this.paginator.pageIndex = event.pageIndex;
    this.paginator.pageSize = event.pageSize;
  }

  // Получение входных данных в таблицу Платежи
  getPaymentsList(contractId: number): void {
    this.contractsService.getPaymentsList(contractId).subscribe(
      (response: any[]) => {
        // this.payments = response.slice(0, response.length - 1);
        // this.dataSource.paginator = this.paginator;

        // Тип платежа
        const types = response[response.length - 1].types;
        const varRate = response[response.length - 1].VARrate;

        // Convert the types and VAT rates to an array of key-value pairs
        this.typePays = Object.entries(types);
        // this.varRateSets = Object.entries(varRate);

      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  
  pay: any
  editPayments(payment: any) {
    this.pay = payment.id;
    console.log(payment)
    this.editPayment.patchValue(payment)
  }

  savePayments() {
    if (this.pay) {
      let editedBudget = this.editPayment.value;
      console.log('set',editedBudget)

      const date_pay = moment(editedBudget.date_pay).format('YYYY-MM-DD');
      editedBudget.date_pay = date_pay;

      this.contractsService.editPaymentsList(this.getId, this.pay, editedBudget).subscribe(
        (response: payments[]) => {
          console.log('Payment updated successfully:', response);
          this.reg.getPaymentsPlan(this.getId);
          this.show.showBlock[5] = false;
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        },
        (error: any) => {
          console.error('Failed to update payment:', error);
        }
      );
    }
  }

    // Удалить данные в таблице Платежи
  deletePayment(contractId: number, paymentId: number): void {
    this.contractsService.delPaymentsList(contractId, paymentId).subscribe(
      (response: payments[]) => {
        this.reg.getPaymentsPlan(contractId);
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  // WBSListPlan: WBSList[] = []
  currentPaymentID: number;

  // Получить данные о СДР ПЛАН
  getWBSPlan(id: number) {
    this.currentPaymentID = id;
    this.contractsService.getPaymentsPlanWBS(this.getId, id).subscribe(
      (response: WBSList[]) => {
        console.log(response);
    
        this.WBSListPlan = response
        // this.WBSList.forEach(item => {
        //   item.selected = false;
        // });
        this.WBSList.forEach(item => {
          if (this.WBSListPlan.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });

      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  

  // Удалить данные СДР (Платежи План)
  delWBSPlan(id): void {
    // const currentPaymentID  = id
    this.contractsService.deliteWBSPlan(this.getId, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        this.getWBSPlan(this.currentPaymentID)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  addPaymentWBS(typePay: string) {
    const selectedIds = this.WBSList.filter(item => item.selected).map(item => {
      return { id: item.id };
    });
    const updatedData = { wbs: selectedIds };
    console.log(updatedData);
    this.contractsService.addWbsPayments(this.getId, this.currentPaymentID, typePay, updatedData).subscribe(
      (response) => {
        console.log('add successfully:', response);
        if (typePay == 'payments_plan') {
          this.getWBSPlan(this.currentPaymentID)
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add WBS:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  
  toppingsWBSList = new FormControl();
  toggleShowBlock: boolean
  selectedCheck: any
  selectedCheckboxes: any[] = [];
   
  toggleShowWbs() {   
    this.toggleShowBlock = true;
    this.contractsService.getWBSList(this.getId).subscribe(
      (response: WBSList[]) => {
        this.WBSList = response.slice(0, response.length - 1);
  
        const initialToppings = this.WBSList.map(item => item.id);
        this.toppingsWBSList.setValue(initialToppings);
  
        const selectedIds = this.WBSListPlans.map(item => {
          this.selectedCheck = item.id;
          return item.id; // Вернуть только id элемента
        });
        // Сохраним выбранные элементы в selectedCheckboxes
        this.selectedCheckboxes = selectedIds;

        this.WBSList.forEach(item => {
          item.selected = false;
          if (this.WBSListPlan.some(planItem => planItem.code === item.code)) {
            item.selected = true;
          }
        });

        console.log('set set', this.selectedCheckboxes);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  
  closedWbs() {this.toggleShowBlock = false}

  plans: any
  amount: number

  editPlan(plan: any) {
    this.plans = plan.id;
    this.amount = plan.amount;
    console.log(plan)
    this.editWBSPlan.patchValue(plan)
    // acts.editing = true;
  }
    // Изменить цену СДР АКТ 
  saveWBSPlanAmount(Id: number, wbs): void {
    console.log(Id, wbs)
    if (Id) {
      let editedObject = this.editWBSPlan.value;
      this.contractsService.editWBSPlanAmount(this.getId, this.currentPaymentID, Id, editedObject.amount).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getWBSPlan(this.currentPaymentID)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
}
