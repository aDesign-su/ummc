import { Component, Inject, OnInit } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { RegService } from '../../reg.service';
import { WBSList, factDetails, invoices, items, listContracts, prepay_invoices } from '../reg-contracts';
import * as jmespath from 'jmespath';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-fact.more',
  templateUrl: './fact.more.component.html',
  styleUrls: ['./fact.more.component.css']
})
export class FactMoreComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, public contractsService: ApicontentService,public fact:RegService, private formBuilder: FormBuilder, private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.fact.currentFactId.subscribe((id:number) => {
      this.getFactId = id
      console.log(this.getFactId)
    });
    this.fact.currentSetId.subscribe((id:number) => {
      this.getId = id
      console.log(this.getId)
    });
    this.createDocx = this.formBuilder.group({
      whom_company: ['АО Святогор', Validators.required],
      whom_FCs: ['Тропникову Д.Л.', Validators.required],
      pfm_code: ['13091000', Validators.required],
      construction_manager: ['Федоров А.И.', Validators.required],
      commercial_director: ['Барашев Р.Х.', Validators.required],
      executor: ['Соловьева О.Д.', Validators.required],
      num_tel: ['+71234567890', Validators.required],
    });
    this.ItemsForm = this.formBuilder.group({
      item: ['', Validators.required],
    });
    this.InvoicesForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.editInvoicesForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.PrepayForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.editPrepayForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.editForm = this.formBuilder.group({
      status: [''], // Здесь указывайте поля, которые нужно редактировать
      date_pay: [''],
      stage: [''],
      accounting_group: [''],
    });
   }
   editForm: FormGroup;
   PrepayForm!: FormGroup;
   editPrepayForm!: FormGroup;
   InvoicesForm!: FormGroup;
   editInvoicesForm!: FormGroup;
   ItemsForm!: FormGroup;
   createDocx!: FormGroup;
   
  ngOnInit() {
    this.getDetails()
    this.getPaymentsFactDetails(this.getId,this.getFactId)
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  panelOpenState = false;
  newPlanItemVisible = false;

  prepay_invoices: any[] = []
  invoices: any[] = []
  WBSList: WBSList[] = []
  plan_items: any[] = []

  contractDetailsFact: any | null = null;
  factDetails: factDetails[] = []
  contractDetails: any | null = null;
  getFactId: number
  getId: number

  savePrepay(contract_id: number, payment_id: number) {
    const PrepayForm = this.PrepayForm.value as factDetails;

    // Format the date as YYYY-MM-DD
    const StartdDate = moment(PrepayForm.date_document).format('YYYY-MM-DD');

    // Update the PrepayForm object with the formatted date
    PrepayForm.date_document = StartdDate;

    this.contractsService.addPaymentsFactPrepay(contract_id, payment_id, PrepayForm).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to edit svz document:', error)
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  saveInvoices(contract_id: number, payment_id: number) {
    const Invoices = this.InvoicesForm.value as factDetails;
    this.InvoicesForm.reset()
    console.log('id', this.invoices);
    // Format the date as YYYY-MM-DD
    const StartdDate = moment(Invoices.date_document).format('YYYY-MM-DD');

    // Update the Invoices object with the formatted date
    Invoices.date_document = StartdDate;

    this.contractsService.addPaymentsFactInvoices(contract_id, payment_id, Invoices).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.log('Failed to edit svz document:', error)
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  ediInvoices(facttId: any, paymentId: any, item: any) {
    if (this.elementId) {
      let updatedData = this.editInvoicesForm.value;
      this.contractsService.editPaymentsFactInvoices(facttId, paymentId, this.elementId, updatedData).subscribe(
        (response: invoices[]) => {
          this.getPaymentsFactDetails(this.getId,this.getFactId)
          this.show.showBlock[4] = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        }
      );
    }
  }

  elementId: number
  itemId: number

  delInvoices(contractId: number, paymentId: number, item: any): void {
    this.elementId = item.id;
    this.contractsService.delPaymentsFactInvoices(contractId, paymentId, this.elementId).subscribe(
      (response: any[]) => {
        console.log('del', contractId, paymentId, this.elementId);
        console.log('Payment deleted successfully:', response);
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  editPrepayInvoices(facttId: any, paymentId: any, item: any) {
    if (this.itemId) {
      let updatedData = this.editPrepayForm.value;
      this.contractsService.editPaymentsFactPrepay(facttId, paymentId, this.itemId, updatedData).subscribe(
        (response: prepay_invoices[]) => {
          this.getPaymentsFactDetails(this.getId,this.getFactId)
          this.show.showBlock[5] = false
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          // Можно добавить обработку успешного сохранения здесь, если нужно
        }
      );
    }
  }

  delPrepayInvoices(contractId: number, paymentId: number, item: any): void {
    const id = item.id;
    this.contractsService.delPaymentsFactPrepay(contractId, paymentId, id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  addRowPlanItems() {
    this.newPlanItemVisible = true;
  }

  saveItems(contract_id: number, payment_id: number) {
    const ItemsForm = this.ItemsForm.value;
    this.newPlanItemVisible = false;
    this.getPaymentsFactDetails(contract_id, payment_id)
    this.contractsService.addPaymentsFactItems(contract_id, payment_id, ItemsForm).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
        this.getPaymentsFactDetails(contract_id, payment_id)
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
    );
  }

  selectedStatuses!: string
  getStatuses: any[] = []
  selectedAccounting!: string
  getAccounting: any[] = []

  editItem(item: any) {
    this.itemId = item.id; // Установим значение свойства element
    this.editPrepayForm.patchValue(item); // Загрузим данные в форму
    item.editing = true;
  }

  editElem(item: any) {
    this.elementId = item.id; // Установим значение свойства element
    this.editInvoicesForm.patchValue(item); // Загрузим данные в форму
    item.editing = true;
  } 

  editItems(facttId: any, paymentId: any, item: any) {
    const id = item.id;
    const updatedData = {
      item: item.item
    };
    this.contractsService.editPaymentsFactItems(facttId, paymentId, id, updatedData).subscribe(
      (response: items[]) => {
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        // Можно добавить обработку успешного сохранения здесь, если нужно
      }
    );
    item.editing = false
  }

  delItems(contractId: number, paymentId: number, item: any): void {
    const id = item.id;
    this.contractsService.delPaymentsFactItems(contractId, paymentId, id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        // Удаление элемента из списка
        this.plan_items = this.plan_items.filter(planItem => planItem.id !== id);
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  saveDocx(contract_id: number, payment_id: number) {
    const createDocx = this.createDocx.value;
    this.contractsService.addPaymentsFactDocx(contract_id, payment_id, createDocx).subscribe(
      (response: any) => {
        console.log('access', response);
        this.downloadFile(response.file_path);
        this.theme.info = 'Файл сохранен успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Error occurred:', error);
      }
    );
  }

  downloadFile(fileUrl: string) {
    const link = document.createElement('a');
    link.href = fileUrl;
    link.target = '_blank';
    link.download = 'Требование на оплату.docx';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  getDetails() {
    this.contractsService.СontractsList(this.getId).subscribe(
      (response: listContracts[]) => {
        if (response.length > 0) {
          this.contractDetails = response[0];
        } else {
          this.contractDetails = null;
        }
        console.log(response)
        // Выборка 
        //Pay schem
        // const query = "[*].pay_schemes[]";
        // const paySchemesArray = this.performJmesPathQuery(response, query);
        // this.paySchemes = paySchemesArray ?? null;
        // //Статус
        // const statusArray = Object.entries(response[1].statuses);
        // this.statusSets = statusArray;
        // this.statusSet = this.contractDetails?.status ?? null;
        // //Валюта
        // const currencyArray = Object.entries(response[1].currencies);
        // this.CurrencySets = currencyArray;
        // this.CurrencySet = this.contractDetails?.currency ?? null;
        // //НДС
        // const varArray = Object.entries(response[1].VARrate);
        // this.varRateSets = varArray;
        // this.varRateSet = this.contractDetails?.VAT_rate ?? null;
      },
      (error: any) => {
        console.error('Failed to retrieve payments data:', error);
      }
    );
  }

  getPaymentsFactDetails(facttId: number, paymentId: number): void {
    console.log(facttId, paymentId)
    this.contractsService.getPaymentsFactDetails(facttId, paymentId).subscribe(
      (response: factDetails[]) => {
        console.log(response)
        this.factDetails = response.slice(0, response.length - 1);
        this.factDetails = response.slice(0, response.length - 1);
        if (response.length > 0) {
          this.contractDetailsFact = response[0];
        } else {
          this.contractDetailsFact = null;
        }

        // const sta = "[*].statuses.[*][][]";
        // this.getStatuses = jmespath.search(response, sta);
        // const acc = "[*].accounting_groups.[*][][]";
        // this.getAccounting = jmespath.search(response, acc);

        const query = "[*].invoices[]";
        this.invoices = jmespath.search(response, query);

        const item = "[*].plan_items[]";
        this.plan_items = jmespath.search(response, item);

        const inv = "[*].prepay_invoices[]";
        this.prepay_invoices = jmespath.search(response, inv);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }

  edit(contractDetails: any) {
    contractDetails.editing = true;
    this.editForm.patchValue({
      status: contractDetails.status,
      date_pay: contractDetails.date_pay,
      stage: contractDetails.stage,
      accounting_group: contractDetails.accounting_group,
    });
  }

  saveReg(element: any): void {
    let editedBudget = this.editForm.value;
    this.contractsService.editPaymentsFactDetails(this.getId, this.getFactId, editedBudget).subscribe(
      (response: any[]) => {
        element.editing = false;
        this.getPaymentsFactDetails(this.getId,this.getFactId)
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }
    );
  }
}
