/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Fact.moreComponent } from './fact.more.component';

describe('Fact.moreComponent', () => {
  let component: Fact.moreComponent;
  let fixture: ComponentFixture<Fact.moreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fact.moreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fact.moreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
