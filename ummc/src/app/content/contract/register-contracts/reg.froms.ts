import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import * as moment from "moment";
import { ApicontentService } from "src/app/api.content.service";
import { RegService } from "../reg.service";

export class regForms {
    constructor(private formBuilder: FormBuilder,public contractsService: ApicontentService,public reg:RegService) {
        this.reg.currentSetId.subscribe((id:number) => {
          this.getId = id
        });
        this.dynamicForm = this.formBuilder.group({
            by_invoices: [this.by_invoices, Validators.required],
            by_prepay_invoices: this.by_prepay_invoices,
            planitems: this.planitems,
            stage: this.stage,
            date_pay: this.date_pay,
            accounting_groups: this.accounting_groups,
        });
        this.InvoicesForm = this.formBuilder.group({
            num_document: ['', Validators.required],
            date_document: ['', Validators.required],
            amount: ['', Validators.required],
            pay_procedure: ['', Validators.required],
        });
        this.PrepayForm = this.formBuilder.group({
            num_document: ['', Validators.required],
            date_document: ['', Validators.required],
            amount: ['', Validators.required],
            pay_procedure: ['', Validators.required],
        });
        this.ItemsForm = this.formBuilder.group({
            item: ['', Validators.required],
        });
        this.сreate = this.formBuilder.group({
            json_cod: [''],
        });
        this.by_invoices = this.formBuilder.array([]);
        this.by_prepay_invoices = this.formBuilder.array([]);
        this.planitems = this.formBuilder.array([]);
        this.stage = this.formBuilder.control('');
        this.accounting_groups = this.formBuilder.control('');
        this.date_pay = this.formBuilder.control('');
        this.dynamicForm = this.formBuilder.group({
            by_invoices: this.by_invoices,
            by_prepay_invoices: this.by_prepay_invoices,
            planitems: this.planitems,
            stage: this.stage,
            date_pay: this.date_pay,
            accounting_groups: this.accounting_groups,
        });

        this.paymentForm = this.formBuilder.group({
            type_pay: ['', Validators.required],
            stage: ['', Validators.required],
            date_pay: [''],
            budget_item: ['', Validators.required],
            amount: ['', Validators.required],
            amount_without_VAT: ['', Validators.required],
            amount_VAT: ['', Validators.required]
          });
        this.actsForm = this.formBuilder.group({
            description: ['', Validators.required],
            document_fact: ['', Validators.required],
            planned_date: ['', Validators.required],
            stage: ['', Validators.required],
            cost_item: ['', Validators.required],
            amount: ['', Validators.required],
          });
    }

    actsForm!: FormGroup;
    paymentForm!: FormGroup;

    accounting_groups: FormControl;
    date_pay: FormControl | any;
    stage: FormControl;
    dynamicForm: FormGroup;
    by_invoices!: FormArray;
    by_prepay_invoices!: FormArray;
    planitems!: FormArray;
    сreate: FormGroup;
    InvoicesForm!: FormGroup;
    ItemsForm!: FormGroup;
    PrepayForm!: FormGroup;
  
    addGroup1() {
        const num_document = this.dynamicForm.get('num_document')?.value;
        const date_document = this.dynamicForm.get('date_document')?.value;
        const amount = this.dynamicForm.get('amount')?.value;
        const pay_procedure = this.dynamicForm.get('pay_procedure')?.value;
    
        // Format the date as YYYY-MM-DD
        const formattedDate = moment(date_document).format('YYYY-MM-DD');
    
        // Update the form control with the formatted date
        this.dynamicForm.get('date_document')?.setValue(formattedDate);
    
        const group = this.formBuilder.group({
          num_document: [num_document],
          date_document: [formattedDate],
          amount: [amount],
          pay_procedure: [pay_procedure],
        });
    
        this.by_invoices.push(group);
      }
      addGroup2() {
        const num_document = this.dynamicForm.get('num_document')?.value;
        const date_document = this.dynamicForm.get('date_document')?.value;
        const amount = this.dynamicForm.get('amount')?.value;
        const pay_procedure = this.dynamicForm.get('pay_procedure')?.value;
    
        // Format the date as YYYY-MM-DD
        const formattedDate = moment(date_document).format('YYYY-MM-DD');
    
        // Update the form control with the formatted date
        this.dynamicForm.get('date_document')?.setValue(formattedDate);
    
        const group = this.formBuilder.group({
          num_document: [num_document],
          date_document: [formattedDate],
          amount: [amount],
          pay_procedure: [pay_procedure],
        });
        this.by_prepay_invoices.push(group);
      }
      addGroup3() {
        const group = this.formBuilder.group({
          plan_item: ['']
        });
        this.planitems.push(group);
      }
      removeGroup1(index: number) {
        this.by_invoices.removeAt(index);
      }
      removeGroup2(index: number) {
        this.by_prepay_invoices.removeAt(index);
      }
      removeGroup3(index: number) {
        this.planitems.removeAt(index);
      }
      
      getId: number

      addPaymentsFact(addContractsList: any): void {
        const response = this.dynamicForm.value
        console.log(response)
    
        this.сreate = this.formBuilder.group({
          json_cod: [response],
        });
        const json = this.сreate.value
        console.log(json)
        this.contractsService.addContractsList(addContractsList, json).subscribe(
          (serverResponse) => {
            // Обработка ответа от сервера, если необходимо
            console.log('Ответ от сервера:', serverResponse);
            this.dynamicForm.value
            // this.dynamicForm.reset();
            this.reg.getPaymentsFact(this.getId)
          },
          (error) => {
            // Обработка ошибки при отправке данных, если необходимо
            console.error('Ошибка отправки данных:', error);
            this.dynamicForm.reset();
          }
    
        );
      }

      get paymentForms(): FormGroup {
        return this.dynamicForm.get('by_invoices') as FormGroup;
      }
    
      get prepayForm(): FormGroup {
        return this.dynamicForm.get('by_prepay_invoices') as FormGroup;
      }
    
      get planItemsForm(): FormGroup {
        return this.dynamicForm.get('planitems') as FormGroup;
      }
}
