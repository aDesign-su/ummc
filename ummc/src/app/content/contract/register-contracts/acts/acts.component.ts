import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { WBSList, acts, listContracts, payments } from '../reg-contracts';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { RegService } from '../../reg.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-acts',
  templateUrl: './acts.component.html',
  styleUrls: ['./acts.component.css'],
  providers: [
    
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class ActsComponent implements OnInit {
  constructor(public theme: ThemeService, public contractsService: ApicontentService,public reg:RegService,public show:ShowService,private formBuilder: FormBuilder,private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.reg.currentSetId.subscribe((id:number) => {
      this.getId = id
      console.log(this.getId)
    });
    this.editAct = this.formBuilder.group({
      description: ['', Validators.required],
      document_fact: ['', Validators.required],
      planned_date: ['', Validators.required],
      stage: ['', Validators.required],
      cost_item: ['', Validators.required],
      amount: ['', Validators.required],
    });
    this.editWBSAct = this.formBuilder.group({
      amount: [''],
    });
   }

  ngOnInit() {
    this.reg.getActsList(this.getId)
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource = new MatTableDataSource<listContracts>([]);
  // getAct: acts[] = []
  getId: number
  
  editAct: FormGroup
  editWBSAct: FormGroup

  // Пагинация для вложенной таблицы Акты
  onPageActs(event: PageEvent): void {
    this.paginator.pageIndex = event.pageIndex;
    this.paginator.pageSize = event.pageSize;
  }

  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  // getActsList(actsId: number): void {
  //   this.contractsService.getActsList(actsId).subscribe(
  //     (response: acts[]) => {
  //       this.getAct = response.slice(0, response.length - 1)
  //       this.dataSource.paginator = this.paginator;
  //     },
  //     (error: any) => {
  //       console.error('Failed to update data:', error);
  //     }
  //   );
  // }

  acts: any
  amount: number
  // Кнопка редактирование таблицы Акты
  editActs(acts: any) {
    this.acts = acts.id;
    this.amount = acts.amount;
    console.log(acts)
    this.editWBSAct.patchValue(acts)
    // acts.editing = true;
  }

  // Изменить цену СДР АКТ 
  saveWBSActAmount(Id: number, wbs): void {
    console.log(Id, wbs)

    if (Id) {
      let editedObject = this.editWBSAct.value;
      this.contractsService.editWBSActAmount(this.getId, this.currentPaymentID, Id, editedObject.amount).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          // this.element.editing = false
          this.getWBSAct(this.currentPaymentID)
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  saveAct() {
    if (this.acts) {
      let editedBudget = this.editAct.value;
      console.log('set',editedBudget)

      const planned_date = moment(editedBudget.planned_date).format('YYYY-MM-DD');
      editedBudget.planned_date = planned_date;

      this.contractsService.editActsList(this.getId, this.acts, editedBudget).subscribe(
        (response: acts[]) => {
          console.log('Payment updated successfully:', response);
          this.reg.getActsList(this.getId);
          this.show.showBlock[6] = false;
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        },
        (error: any) => {
          console.error('Failed to update payment:', error);
        }
      );
    }
  }

  // Удалить данные в таблице Платежи
  deleteActs(contractId: number, actsId: number): void {
    this.contractsService.delActsList(contractId, actsId).subscribe(
      (response: acts[]) => {
        console.log('Payment deleted successfully:', response);
        this.reg.getActsList(contractId)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  WBSList: WBSList[] = []
  WBSListAct: WBSList[] = []
  currentPaymentID: number;
  
  getWBSAct(id: number) {
    this.currentPaymentID = id
    console.log(id)
    this.contractsService.getPaymentsActWBS(this.getId, id).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSListAct = response
        this.WBSList.forEach(item => {
          item.selected = false;
        });
        this.WBSList.forEach(item => {
          if (this.WBSListAct.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    )
  }

  addPaymentWBS(typePay: string) {
    const selectedIds = this.WBSList.filter(item => item.selected).map(item => {
      return { id: item.id };
    });
    const updatedData = { wbs: selectedIds };
    console.log(updatedData);
    this.contractsService.addWbsPayments(this.getId, this.currentPaymentID, typePay, updatedData).subscribe(
      (response) => {
        console.log('add successfully:', response);
        if (typePay == 'acts') {
          this.getWBSAct(this.currentPaymentID)
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add WBS:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  // Удалить данные СДР (АКТ)
  delWBSAct(id): void {
    this.contractsService.deliteWBSAct(this.getId, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        console.log('Payment deleted successfully:', response);
        this.getWBSAct(this.currentPaymentID)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  
  currentContractID: number;
  toppingsWBSList = new FormControl();
  toggleShowBlock: boolean
  selectedCheck: any
  selectedCheckboxes: any[] = [];

  toggleCheckbox(index: number) {
    this.selectedCheckboxes[index] = !this.selectedCheckboxes[index];
  }

  toggleShowWbs() {   
    this.toggleShowBlock = true;
    this.contractsService.getWBSList(this.getId).subscribe(
      (response: WBSList[]) => {
        this.WBSList = response.slice(0, response.length - 1);
  
        const initialToppings = this.WBSList.map(item => item.id);
        this.toppingsWBSList.setValue(initialToppings);
  
        const selectedIds = this.WBSListAct.map(item => {
          this.selectedCheck = item.id;
          return item.id; // Вернуть только id элемента
        });
        // Сохраним выбранные элементы в selectedCheckboxes
        this.selectedCheckboxes = selectedIds;

        this.WBSList.forEach(item => {
          item.selected = false;
          if (this.WBSListAct.some(planItem => planItem.code === item.code)) {
            item.selected = true;
          }
        });

        console.log('set', this.selectedCheckboxes);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  closedWbs() {this.toggleShowBlock = false}
}
