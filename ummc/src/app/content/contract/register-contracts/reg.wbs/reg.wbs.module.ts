import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegWbsComponent } from './reg.wbs.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [RegWbsComponent],
  exports: [RegWbsComponent]
})
export class RegWbsModule { }
