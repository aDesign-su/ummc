import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { RegService } from '../../reg.service';
import { WBSList, acts } from '../reg-contracts';
import { ThemeService } from 'src/app/theme.service';

@Component({
  selector: 'app-reg-wbs',
  templateUrl: './reg.wbs.component.html',
  styleUrls: ['./reg.wbs.component.css']
})
export class RegWbsComponent implements OnInit {
  constructor(public theme: ThemeService, public contractsService: ApicontentService,public reg:RegService,public show:ShowService,private formBuilder: FormBuilder) {
    this.reg.currentSetId.subscribe((id:number) => {
      this.getId = id
    });
    this.editWBS = this.formBuilder.group({
      amount: '', 
    });
   }

  ngOnInit() {
    this.reg.getWbsList(this.getId)
  }

  editWBS: FormGroup

  getId: number
  // currentContractID: number;

 
  
  element: any
  wbs: number
  editWbsList(element:WBSList) {
    this.element = element.id; // Установим значение свойства element
    this.editWBS.patchValue({
      amount: element.amount
    })
  }
  
  saveWbs() {
    if (this.element) {
      let editedBudget = this.editWBS.value;
      console.log('set',editedBudget)
      this.contractsService.editWBSamount(this.element, editedBudget.amount).subscribe(
        (response) => {
          console.log('Payment updated successfully:', response);
          this.reg.getWbsList(this.getId)
          this.show.showBlock[10] = false;
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
        },
        (error: any) => {
          console.error('Failed to update payment:', error);
        }
      );
    }
  }

  delWBS(id: number) { 
    this.contractsService.delWBS(id).subscribe(
      (response: acts[]) => {
        console.log('WBS deleted successfully:', response);
        this.reg.getWbsList(this.getId)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
}
