import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { WBSList, acts, listContracts, payments, paymentsFact } from '../reg-contracts';
import { RegService } from '../../reg.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import * as jmespath from 'jmespath';
import { ShowService } from 'src/app/helper/show.service';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { regForms } from '../reg.froms'

@Component({
  selector: 'app-reg.more',
  templateUrl: './reg.more.component.html',
  styleUrls: ['./reg.more.component.css'],
  providers: [
    
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})

export class RegMoreComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, private formBuilder: FormBuilder, public contractsService: ApicontentService,private router: Router, public reg:RegService,private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string,private cdr: ChangeDetectorRef) { 
    this.reg.currentSetId.subscribe((id:number) => {
      this.getId = id
      console.log(this.getId)
    });
    this.reg.currentFactId.subscribe((id:number) => {
      this.getFactId = id
      console.log(this.getFactId)
    });
    this.searchForm = this.formBuilder.group({
      searchText: ['']
    });
    this.editWBSFact = this.formBuilder.group({
      amount: [''],
    });
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      this.getChildren,
    );
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.contractsService.addSDR().subscribe(data => {
      this.dataSource.data = data;
      this.treeControl.expand(this.treeControl.dataNodes[0]);
      // После загрузки данных, установите состояние checkbox для выбранных элементов
      this.setSelectedCheckboxes();
      console.log(data);
    });

    // Инициализируем экземпляр regForms и передайте FormBuilder
    this.regFormsInstance = new regForms(this.formBuilder,this.contractsService,this.reg);
  }
    // Экземпляр класса regForms
  regFormsInstance: regForms;
    // Доступ к формам можно получить через regFormsInstance
  get paymentForm(): FormGroup {
    return this.regFormsInstance.paymentForm;
  }
  get actsForm(): FormGroup {
    return this.regFormsInstance.actsForm;
  }
  get dynamicForm(): FormGroup {
    return this.regFormsInstance.dynamicForm;
  }
  setSelectedCheckboxes() {
    const selectedIds = this.selectedItems.map(item => item.id);
    this.treeControl.dataNodes.forEach(node => {
      if (selectedIds.includes(node.id)) {
        this.checklistSelection.select(node);
      }
    });
  }

  // Функция для установки состояния checkbox для выбранных элементов
  // setSelectedCheckboxes() {
  //   // Предположим, что this.selectedItems - это массив выбранных элементов, например, ID элементов
  //   for (const item of this.selectedItems) {
  //     const node = this.findNodeById(this.dataSource.data, item);
  //     if (node) {
  //       node.checked = true; // Предполагается, что у вашего элемента есть свойство checked
  //     }
  //   }
  // }
  // Функция для поиска узла по ID в иерархическом дереве данных
  findNodeById(data: any[], id: number): any {
    for (const item of data) {
      if (item.id === id) {
        return item;
      }
      if (item.children) {
        const node = this.findNodeById(item.children, id);
        if (node) {
          return node;
        }
      }
    }
    return null;
  }
  ngOnInit() {
    this.getDetails()
    this.getPaymentsFact(this.getId)
    this.reg.getPaymentsFact(this.getId)
    this.reg.getPaymentsPlan(this.getId)
    this.getPaymentsList(this.getId)
    this.getWBSFacts(this.getId)

    this.filteredData = this.dataSource.data;
    
    this.contractsService.getContractsList().subscribe(
      (response: any[]) => {
        this.getContracts = response
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );

//Запустить проверку
    this.cdr.detectChanges();
  }

  filteredData: any[] = [];
  searchString = ''

  // search filter logic start
  filterLeafNode(node: TodoItemFlatNode): boolean {
    if (!this.searchString) {
      return false
    }
    return node.name.toLowerCase()
      .indexOf(this.searchString?.toLowerCase()) === -1
  }
  
  filterParentNode(node: TodoItemFlatNode): boolean {
    if (
      !this.searchString ||
      node.name.toLowerCase().indexOf(this.searchString?.toLowerCase()) !==
        -1
    ) {
      return false
    }
    const descendants = this.treeControl.getDescendants(node)
    if (
      descendants.some(
        (descendantNode) =>
          descendantNode.name
            .toLowerCase()
            .indexOf(this.searchString?.toLowerCase()) !== -1
      )
    ) {
      return false
    }
    return true
  }
  // applyFilters() {
  //   const filterValue = this.searchForm.value.searchText.toLowerCase();
  //   if (!filterValue) {
  //     // Если в поле поиска пустая строка, восстановите исходные данные
  //     // this.filteredData = this.dataSource.data;
  //     this.dataSource.data = this.filteredData;
  //     return;
  //   }
  //   this.filteredData = this.filterTreeNodes(this.dataSource.data, filterValue);
  //   this.treeControl.expandAll();
  //   this.dataSource.data = this.filteredData;
  // }
  
  // filterTreeNodes(nodes: any[], filterValue: string): any[] {
  //   return nodes.filter((node) => {
  //     const match = node.name.toLowerCase().includes(filterValue);
  //     const hasChildren = node.children && node.children.length > 0;
  //     if (hasChildren) {
  //       node.children = this.filterTreeNodes(node.children, filterValue);
  //     }
  //     return match || hasChildren;
  //   });
  // }

  getSelectedNodes(): TodoItemFlatNode[] {
    return this.checklistSelection.selected;
  }
  searchForm: FormGroup
  editWBSFact: FormGroup

  // Переменный для выборки
  payScheme: string[] | null = null;
  paySchemes!: any
  statusSet: string | null = null;
  statusSets: any
  CurrencySet: string | null = null;
  CurrencySets!: any
  varRateSet: string | null = null;
  varRateSets!: any
  selectedAccounting!: string
  typePay: any | null = null;
  typePays!: any

  ContractsSet: string | null = null;
  getContracts: any[] = []

  @ViewChild(MatPaginator) paginator1!: MatPaginator;
  @ViewChild(MatPaginator) paginator3!: MatPaginator;
  getAccounting: any[] = []

  elementsFact: paymentsFact[] = [];
  dataSourceFact = new MatTableDataSource<paymentsFact>(this.elementsFact);
  currentContractID: number;
  
  WBSList: WBSList[] = []
  WBSListFact: any[] = []
  WBSListFacts: any[] = []

  getAct: acts[] = []
  // payments: payments[] = []
  selectedItems: any = []
  // fact: paymentsFact[] = []
  contractDetails: listContracts | null = null;
  getId: number
  setFactId: number
  getFactId: number
  submitSDR(element:number) {
    // Получить все выбранные элементы из checklistSelection
    const selectedItems = this.checklistSelection.selected;
    // Обработать выбранные элементы и собрать их в объект JSON
    const wbsData = [];
    //объект со свойством id для каждого выбранного элемента
    for (const node of selectedItems) {
      console.log('get element:', node.name);
      this.selectedItems = node.id
      const wbsItem = {
        id: node.id,
      };
      wbsData.push(wbsItem);
    }
    const wbs = {
      wbs: wbsData,
    };

    this.contractsService.addWbsContracts(element, wbs).subscribe(
      (response: any) => {
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        console.log('access', this.reg.getWbsList(this.getId));
        this.reg.getWbsList(this.getId)
      },
      (error: any) => {
        console.error('Error occurred:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );

    // После сохранения, можно очистить выбор, если это необходимо
    this.checklistSelection.clear();
  }
  
  addGroup1() {
    this.regFormsInstance.addGroup1();
  }
  addGroup2() {
    this.regFormsInstance.addGroup2();
  }
  addGroup3() {
    this.regFormsInstance.addGroup3();
  }
  removeGroup1(index: number) {
    this.regFormsInstance.removeGroup1(index);
  }
  removeGroup2(index: number) {
    this.regFormsInstance.removeGroup2(index);
  }
  removeGroup3(index: number) {
    this.regFormsInstance.removeGroup3(index);
  }
  addPaymentsFact(addContractsList: any): void {
    // Вызов метода из regFormsInstance
    this.regFormsInstance.addPaymentsFact(addContractsList);
    const num_document = this.regFormsInstance.dynamicForm.get('num_document')?.value;
    this.show.showBlock[0] = false;
    this.theme.access = 'Данные добавлены успешно.'
    setTimeout(() => {
      this.theme.access = null;
    }, 5000);
  }

   // Получение данных для селекта Тип платежа
   getPaymentsList(contractId: number): void {
    this.contractsService.getPaymentsList(contractId).subscribe(
      (response: any[]) => {
        // Тип платежа
        const types = response[response.length - 1].types;
        this.typePays = Object.entries(types);
      },
    );
  }



  // Добавление данных в таблицу Платежи
  addPayment(contractId: number): void {
    const paymentData = this.paymentForm.value;
    // console.log('Payment added successfully:', this.getPaymentsPlan(this.getId));
    
    const formattedDate = moment(paymentData.date_pay).format('YYYY-MM-DD');
    paymentData.date_pay = formattedDate;
    
    this.contractsService.addPaymentsList(contractId, paymentData).subscribe(
      (response: payments[]) => {
        // console.log('Payment added successfully:', response);
        // this.payments = response;
        this.show.closedAdd(0)
        this.reg.getPaymentsPlan(contractId);
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add payment:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
    // Добавление данных в таблицу Акты
  addActs(contractId: number): void {
    const actsData = this.actsForm.value as acts;

    // Format the date as YYYY-MM-DD
    const formattedDate = moment(actsData.planned_date).format('YYYY-MM-DD');

    // Update the actsData object with the formatted date
    actsData.planned_date = formattedDate;

    this.contractsService.addActsList(contractId, actsData).subscribe(
      (response: acts[]) => {
        console.log('Payment added successfully:', response);
        this.getAct = response;
        // this.getActsList(contractId);
        // this.actsForm.reset();
        this.show.showBlock[1] = false
        this.reg.getActsList(contractId);
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add payment:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  
  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ДД-ММ-ГГГГ';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }
  // Используем библиотеку jsonformatter
  performJmesPathQuery(data: any, query: string): any {
    return jmespath.search(data, query);
  }
  getDetails() {
    this.contractsService.СontractsList(this.getId).subscribe(
      (response: listContracts[]) => {
        if (response.length > 0) {
          this.contractDetails = response[0];
        } else {
          this.contractDetails = null;
        }

        // Выборка 
        //Pay schem
        const query = "[*].pay_schemes[]";
        const paySchemesArray = this.performJmesPathQuery(response, query);
        this.paySchemes = paySchemesArray ?? null;
        //Статус
        const statusArray = Object.entries(response[1].statuses);
        this.statusSets = statusArray;
        this.statusSet = this.contractDetails?.status ?? null;
        //Валюта
        const currencyArray = Object.entries(response[1].currencies);
        this.CurrencySets = currencyArray;
        this.CurrencySet = this.contractDetails?.currency ?? null;
        //НДС
        const varArray = Object.entries(response[1].VARrate);
        this.varRateSets = varArray;
        this.varRateSet = this.contractDetails?.VAT_rate ?? null;
      },
      (error: any) => {
        console.error('Failed to retrieve payments data:', error);
      }
    );
  }

  // Получение входных данных в таблицу Платежи Факт
  getPaymentsFact(factId: number): void {
    this.contractsService.getPaymentsFact(factId).subscribe(
      (response: any[]) => {
        // this.fact = response.slice(0, response.length - 1)
        // this.dataSourceFact.paginator = this.paginator3;

        const acc = "[*].accounting_groups.[*][][]";
        this.getAccounting = jmespath.search(response, acc);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  // Удалить данные в таблице Платежи Факт
  deletePaymentFact(contractId: number, paymentId: number): void {
    this.contractsService.delPaymentsFact(contractId, paymentId).subscribe(
      (response: paymentsFact[]) => {
        console.log('Payment deleted successfully:', response);
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.reg.getPaymentsFact(contractId);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }


  // Кнопка редактирование блока "подробнее"
  edit(contractDetails: any) {
    contractDetails.editing = true;
    let selectedScheme = this.paySchemes.find((option: any) => option.name === contractDetails.pay_schem);
    if (selectedScheme) {
      this.payScheme = selectedScheme.id; // присваиваем id найденной схемы оплаты
    }
  }
  // Сохранить данные для таблицы Реестр (блок подробнее)
  saveReg(element: any): void {
    element.editing = false;

    // Преобразование даты в нужный формат
    const dateEnd = moment(element.date_end).format('YYYY-MM-DD');
    const startEnd = moment(element.date_start).format('YYYY-MM-DD');
    const date_contr_SAP = moment(element.date_contr_SAP).format('YYYY-MM-DD');

    const id = element.id;
    const updatedData = {
      code_counterparty: this.ContractsSet,
      code_contract: element.code_contract,
      legal_number: element.legal_number,
      subject: element.subject,
      VAT_rate: this.varRateSet,
      amount_with_VAT: element.amount_with_VAT,
      status: this.statusSet,
      currency: this.CurrencySet,
      contract_limit: parseFloat(element.contract_limit),
      fact_development: parseFloat(element.fact_development),
      fact_financing: parseFloat(element.fact_financing),
      date_start: startEnd,
      date_end: dateEnd,
      advance: parseFloat(element.advance),
      sdr_id: element.sdr_id,
      legal_code_SAP: element.legal_code_SAP,
      num_contr_SAP: element.num_contr_SAP,
      date_contr_SAP: date_contr_SAP
    };

    this.contractsService.updСontractsList(id, updatedData).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        element.editing = false;
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }

  showFactMore(id: number, elem:number) {
    this.getId = id
    this.setFactId = elem
    if(elem) {
      this.router.navigate(['register-contracts/more/fact/more']);
      this.reg.setFactId(this.setFactId);
    }
  }

  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();
  selectedParent: TodoItemFlatNode | null = null;
  newItemName = '';
  treeControl: FlatTreeControl<TodoItemFlatNode>;
  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;
  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  getLevel = (node: TodoItemFlatNode) => node.level;
  isExpandable = (node: TodoItemFlatNode) => node.expandable;
  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;
  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;
  hasNoContent = (_: string, _nodeData: TodoItemFlatNode) => _nodeData.name === '';

   /** Transformer to convert nested node to flat node. Record the nodes in maps for later use. */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name ? existingNode : new TodoItemFlatNode();
    flatNode.id = node.id;
    flatNode.name = node.name;
    flatNode.level = level;
    flatNode.expandable = !!node.children?.length;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  };

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every(child => {
        return this.checklistSelection.isSelected(child);
      });
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.forEach(child => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TodoItemFlatNode): void {
    let parent: TodoItemFlatNode 
    while (parent) {
      this.checkRootNodeSelection(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TodoItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every(child => {
        return this.checklistSelection.isSelected(child);
      });
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }
  closedWbs() {this.toggleShowBlock = false}
  
  addWBSListFact: WBSList[] = []
  toppingsWBSList = new FormControl();
  toggleShowBlock: boolean
  selectedCheck: any
  selectedCheckboxes: any[] = [];
  currentPaymentID: number;

  toggleShowWbs() {
    this.toggleShowBlock = true;
    this.contractsService.getWBSList(this.getId).subscribe(
      (response: WBSList[]) => {
        this.WBSList = response.slice(0, response.length - 1);
  
        const initialToppings = this.WBSList.map(item => item.id);
        this.toppingsWBSList.setValue(initialToppings);
  
        const selectedIds = this.WBSListFacts.map(item => {
          this.selectedCheck = item.id;
          return item.id; // Вернуть только id элемента
        });
        // Сохраним выбранные элементы в selectedCheckboxes
        this.selectedCheckboxes = selectedIds;

        this.WBSList.forEach(item => {
          item.selected = false;
          if (this.WBSListFact.some(planItem => planItem.code === item.code)) {
            item.selected = true;
          }
        });

        console.log('set0', this.selectedCheckboxes);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }

  // Удалить данные СДР (Платежи План)
  delWBSFact(id): void {
    // const currentPaymentID  = id
    this.contractsService.deliteWBSFact(this.getId, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        this.getWBSFacts(this.currentPaymentID)
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  
  addPaymentWBS(typePay: string) {
    const selectedIds = this.WBSList.filter(item => item.selected).map(item => {
      return { id: item.id };
    });
    const updatedData = { wbs: selectedIds };
    console.log(updatedData);
    this.contractsService.addWbsPayments(this.getId, this.currentPaymentID, typePay, updatedData).subscribe(
      (response) => {
        console.log('add successfully:', response);
        if (typePay == 'payments_fact') {
          this.getWBSFacts(this.currentPaymentID)
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      },
      (error: any) => {
        console.error('Failed to add WBS:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  
// Получить данные о СДР фАКТ
  getWBSFacts(id: number) {
    this.currentPaymentID = id;
    this.contractsService.getPaymentsFactWBS(this.getId, id).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSListFact = response
        this.WBSList.forEach(item => {
          if (this.WBSListFact.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    )
  }

  facts: any
  amount: number

  editFact(fact: any) {
    this.facts = fact.id;
    this.amount = fact.amount;
    console.log(fact)
    this.editWBSFact.patchValue(fact)
    // acts.editing = true;
  }

  // Изменить цену СДР АКТ 
  saveWBSFactAmount(Id: number, wbs): void {
    console.log(Id, wbs)
    if (Id) {
      let editedObject = this.editWBSFact.value;
      this.contractsService.editWBSFactAmount(this.getId, this.currentPaymentID, Id, editedObject.amount).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          // this.element.editing = false
          this.getWBSFacts(this.currentPaymentID)
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  /* Get the parent node of a node */
  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }
}

export class TodoItemNode {
  children: TodoItemNode[];
  name: string;
  id: number;
}
export class TodoItemFlatNode {
  id: number;
  name: string;
  level: number;
  expandable: boolean;
}