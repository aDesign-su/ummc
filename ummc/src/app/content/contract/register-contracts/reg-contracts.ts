export interface acts {
    id: number,
    description: string,
    document_fact: string,
    planned_date: string,
    stage: number,
    cost_item: string,
    VAT_rate: string,
    amount: number,
    amount_without_VAT: number,
    amount_VAT: number
    editing: boolean;
}
export interface ContractsCreates {
    code_counterparty: number,
    code_contract: number,
    legal_number: string,
    subject: string,
    VAT_rate: string,
    amount_with_VAT: number,
    currency: string,
    date_start: string,
    date_end: string,
    legal_code_SAP: number,
    num_contr_SAP: number,
    date_contr_SAP: string
}
export interface paymentsFact {
    id: number,
    contract_id: number,
    status: string,
    date_pay: string,
    stage: string,
    amount: number,
    amount_without_VAT: number,
    amount_VAT: number
    editing: boolean;
    facttId: number,
    paymentId: number,
}
export interface factDetails {
    id: any;
    payment_id: number,
    stage: number,
    status: string,
    date_pay: string,
    amount: number,
    invoices: invoices,
    prepay_invoices: prepay_invoices,
    accounting_group: string,
    plan_items: items,
    amount_without_VAT: number,
    amount_VAT: number,
    date_document: string,
    editing: boolean,
    file_path: any
}
export interface prepay_invoices {
    id: number,
    num_document: number,
    date_document: string,
    amount: number,
    pay_procedure: string
}
export interface invoices {
    id: number,
    num_document: number,
    date_document: string,
    amount: number,
    pay_procedure: string
}
export interface items {
    item: number
}
export interface plan_items {
    whom_company: string,
    whom_FCs: string,
    pfm_code: number,
    construction_manager: string,
    commercial_director: string,
    executor: string,
    num_tel: number
}
export interface payments {
    id: number,
    type_pay: types,
    date_pay: string,
    stage: number,
    budget_item: string,
    VAT_rate: VARrate | null,
    amount: number,
    amount_without_VAT: number,
    amount_VAT: number
    editing: boolean;
}
export interface types {
    Платёж: string,
    Аванс: string
}
export interface regContracts {
    id: number,
    code_counterparty: number;
    code_contract: number;
    legal_number: string;
    subject: string;
    VAT_rate: number | null;
    amount_without_VAT: number;
    amount_with_VAT: number;
    status: string;
    currency: string;
    contract_limit: number | null;
    fact_development: number | null;
    prcent_development: number | null;
    fact_financing: number | null;
    procent_financing: number | null;
    date_start: string;
    date_end: string;
    pay_schem: string | null;
    advance: number;
    sdr_id: number | null;
    editing: boolean;
}
export interface ContractGroup {
    code: number;
    name: string;
    contracts: listContracts[];
}
export interface listContracts {
    id: number,
    name: string,
    code_counterparty: number,
    code_contract: number,
    legal_number: string,
    subject: string,
    VAT_rate: string,
    amount_without_VAT: number,
    amount_with_VAT: number,
    status: string,
    currency: string,
    contract_limit: number,
    fact_development: number,
    prcent_development: number,
    fact_financing: number,
    procent_financing: number,
    date_start: string,
    date_end: string,
    pay_schem: string,
    advance: number,
    sdr_id: number,
    side: string,
    legal_code_SAP: number,
    num_contr_SAP: number,
    date_contr_SAP: string,
    statuses: directoryStatus,
    currencies: currencies,
    VARrate: VARrate,
    editing?: boolean
}
export interface directoryStatus {
    AT: string,
    ST: string,
    PD: string,
    RI: string,
    PT: string,
    PG: string,
    CD: string
}
export interface currencies {
    RUB: string,
    USD: string,
    EUR: string,
    CNY: string
}
export interface VARrate {
    0: string,
    10: string,
    20: string
}
export interface Document {
    number: string;
    date: string;
    sum: number;
    order: string;
}

export class ApiTreeData {
    id: number;
    name: string;
    code: string;
    level: number;
    children: ApiTreeData[];
    parent?: ApiTreeData;
    selected?: boolean;
    indeterminate?: boolean;
    disabled?: boolean;  

}
export interface TreeNode {
    id: number;
    name: string;
    code: string;
    level: number;
    children: TreeNode[];
}

export interface WBSList {
    id: number;
    name: string;
    code: string;
    amount?: number;
    editing: boolean;
    selected?:boolean;
}