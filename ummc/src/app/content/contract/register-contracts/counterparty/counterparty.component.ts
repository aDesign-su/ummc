import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { RegService } from '../../reg.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { listContracts } from '../reg-contracts';
import * as jmespath from 'jmespath';

@Component({
  selector: 'app-counterparty',
  templateUrl: './counterparty.component.html',
  styleUrls: ['./counterparty.component.css']
})
export class CounterpartyComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, private formBuilder: FormBuilder, public contractsService: ApicontentService,private router: Router, public reg:RegService) {
    this.reg.currentSetCode.subscribe((code:number) => {
      this.getCode = code
      console.log(this.getCode)
    });
    /** Иницилизация фильтров Реестры */
    this.searchForm = this.formBuilder.group({
      searchText: ''
    });
   }

  ngOnInit() {
    this.getWorkReg()
    this.generateYearsRange();
  }
  searchForm!: FormGroup;
  
  getCode: number
  contractDetails: listContracts | null = null;
  contractCode: listContracts | null = null;
  displayedColumns: string[] = [
    'serial_number',
    'side',
    'code_contract',
    'subject',
    'amount_without_VAT',
    'amount_with_VAT',
    'action'
  ];
  dataSource = new MatTableDataSource<listContracts>([]);
  filteredData: listContracts[] = []
  ContractGroups: listContracts[] = [];

  @ViewChild('table_2') paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getWorkReg(): void {
    this.contractsService.getWorkReg().subscribe(
      (data: listContracts[]) => {
        /** Вывод контрагента */
          this.contractDetails = data[0];

        /** Данные для фильтрации */
          this.ContractGroups = data
          this.statusOptions = jmespath.search(this.ContractGroups, '[].statuses.[*]|[]|[]');
          this.currencyOptions = jmespath.search(this.ContractGroups, '[].currencies.[*]|[]|[]');

        const cur = "[*].contracts[]";
        data = jmespath.search(data, cur);
        this.dataSource.data = data;

        this.reg.currentSetCode.subscribe((code: number) => {
          this.getCode = code;
          console.log(this.getCode);

        /** Вывод код контракта */
          this.filteredData = data.filter((item: any) => {
            this.contractCode = item.code_contract
          });

          console.log('test', this.contractCode);
        /** Теперь сравните полученный код с полем code_counterparty в каждом элементе данных */
          this.filteredData = data.filter(item => item.code_counterparty === this.getCode);
          
          this.dataSource.data = this.filteredData;
          this.applyFilters();

          console.log('Filtered Data', this.filteredData);
        });
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  // Данные для фильтра Статус
  yearsRange: number[] = [];
  selectedYear: number = new Date().getFullYear();
  statusOptions: any = [];
  currencyOptions: any[] = [];
  selectedStatus: string = 'All';
  selectedCurrency: string = 'All';

  filteredContracts: listContracts[] = [];

  newPlanItemVisible = false;

  // Создание даты по годам
  generateYearsRange(): void {
    const currentYear = new Date().getFullYear();
    const range = 23;
    for (let i = currentYear; i > currentYear - range; i--) {
      this.yearsRange.push(i);
    }
  }

  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    this.filteredContracts = this.dataSource.data.filter(contract => {
      const isStatusMatched = this.selectedStatus === 'All' ? true : contract.status === this.selectedStatus;
      const isCurrencyMatched = this.selectedCurrency === 'All' ? true : contract.currency === this.selectedCurrency;
      const isYearMatched = this.selectedYear === null ? true : new Date(contract.date_start).getFullYear() === this.selectedYear;

      return (
        isStatusMatched &&
        isCurrencyMatched &&
        isYearMatched &&
        this.matchesSearchText(contract, searchText)
      );
    });
  }

  matchesSearchText(contract: listContracts, searchText: string): boolean {
    return (
      (contract.code_counterparty?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.code_contract?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.legal_number?.toLowerCase() ?? '').includes(searchText) ||
      (contract.subject?.toLowerCase() ?? '').includes(searchText) ||
      (contract.VAT_rate?.toLowerCase() ?? '').includes(searchText) ||
      (contract.amount_without_VAT?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.amount_with_VAT?.toString().toLowerCase() ?? '').includes(searchText)
    );
  }

  setId: number
  showRegMore(elem:number) { 
    // console.log(elem)
    this.setId = elem 
    if(elem) { 
      this.router.navigate(['register-contracts/more']); 
      this.reg.setSetId(this.setId); 
    } 
  }
}
