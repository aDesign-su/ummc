import { Component, Inject, OnInit, ViewChild, Injectable, ElementRef } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { ContractGroup, VARrate, acts, listContracts, payments, regContracts, ContractsCreates, paymentsFact, Document, factDetails, prepay_invoices, invoices, items, ApiTreeData, WBSList } from './reg-contracts';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';
import * as jmespath from 'jmespath';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { TreeNode } from 'primeng/api';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { SelectionChange, SelectionModel } from '@angular/cdk/collections';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MatOption } from '@angular/material/core';
import { Router } from '@angular/router';
import { RegService } from '../reg.service';

@Component({
  selector: 'app-register-contracts',
  templateUrl: './register-contracts.component.html',
  styleUrls: ['./register-contracts.component.css'],
  providers: [

    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
        fullPickerInput: '',
      },
    },
  ],
})
export class RegisterContractsComponent implements OnInit {
  public treeControl = new NestedTreeControl<ApiTreeData>(node => node.children);
  public TreeDataSource = new MatTreeNestedDataSource<ApiTreeData>();

  constructor(public theme: ThemeService, public contractsService: ApicontentService, private formBuilder: FormBuilder, private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string, private router: Router, public reg:RegService) {
    this.paymentForm = this.formBuilder.group({
      type_pay: ['', Validators.required],
      date_pay: ['', Validators.required],
      stage: ['', Validators.required],
      budget_item: ['', Validators.required],
      amount: ['', Validators.required],
      amount_without_VAT: ['', Validators.required],
      amount_VAT: ['', Validators.required]
    });
    this.actsForm = this.formBuilder.group({
      description: ['', Validators.required],
      document_fact: ['', Validators.required],
      planned_date: ['', Validators.required],
      stage: ['', Validators.required],
      cost_item: ['', Validators.required],
      amount: ['', Validators.required],
    });
    this.contractsCreate = this.formBuilder.group({
      code_counterparty: ['', Validators.required],
      code_contract: ['', Validators.required],
      legal_number: ['', Validators.required],
      subject: ['', Validators.required],
      VAT_rate: ['', Validators.required],
      amount_with_VAT: ['', Validators.required],
      currency: ['', Validators.required],
      date_start: ['', Validators.required],
      date_end: ['', Validators.required],
      legal_code_SAP: ['', Validators.required],
      num_contr_SAP: ['', Validators.required],
      date_contr_SAP: ['', Validators.required],
    });
    this.createDocx = this.formBuilder.group({
      whom_company: ['АО Святогор', Validators.required],
      whom_FCs: ['Тропникову Д.Л.', Validators.required],
      pfm_code: ['13091000', Validators.required],
      construction_manager: ['Федоров А.И.', Validators.required],
      commercial_director: ['Барашев Р.Х.', Validators.required],
      executor: ['Соловьева О.Д.', Validators.required],
      num_tel: ['+71234567890', Validators.required],
    });
    this.InvoicesForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.PrepayForm = this.formBuilder.group({
      num_document: ['', Validators.required],
      date_document: ['', Validators.required],
      amount: ['', Validators.required],
      pay_procedure: ['', Validators.required],
    });
    this.ItemsForm = this.formBuilder.group({
      item: ['', Validators.required],
    });
    this.сreate = this.formBuilder.group({
      json_cod: [''],
    });
    this.by_invoices = this.formBuilder.array([]);
    this.by_prepay_invoices = this.formBuilder.array([]);
    this.planitems = this.formBuilder.array([]);
    this.stage = this.formBuilder.control('');
    this.accounting_groups = this.formBuilder.control('');
    this.dynamicForm = this.formBuilder.group({
      by_invoices: this.by_invoices,
      by_prepay_invoices: this.by_prepay_invoices,
      planitems: this.planitems,
      stage: this.stage,
      accounting_groups: this.accounting_groups,
    });

  }
  сreate: FormGroup;
  accounting_groups: FormControl;
  stage: FormControl;
  dynamicForm: FormGroup;
  by_invoices!: FormArray;
  by_prepay_invoices!: FormArray;
  planitems!: FormArray;
  paymentForm!: FormGroup;
  actsForm!: FormGroup;
  searchForm!: FormGroup;
  contractsCreate!: FormGroup;
  createDocx!: FormGroup;
  InvoicesForm!: FormGroup;
  ItemsForm!: FormGroup;
  PrepayForm!: FormGroup;
  private currentContractID: number;
  private currentPaymentID: number;
  ngOnInit() {
    // Перезагружаем страницу, чтобы не потерять токен (костыль)
    if (!localStorage.getItem('foo')) {
      localStorage.setItem('foo', 'no reload')
      location.reload()
    } else {
      localStorage.removeItem('foo')
    }
    // Иницилизация таблиц
    this.getWorkReg()
    this.generateYearsRange()

    // Иницилизация фильтров Реестры
    this.searchForm = this.formBuilder.group({
      searchText: ''
    });
    this.applyFilters();
    this.getContractsList()
  }

  addGroup1() {
    const num_document = this.dynamicForm.get('num_document')?.value;
    const date_document = this.dynamicForm.get('date_document')?.value;
    const amount = this.dynamicForm.get('amount')?.value;
    const pay_procedure = this.dynamicForm.get('pay_procedure')?.value;

    // Format the date as YYYY-MM-DD
    const formattedDate = moment(date_document).format('YYYY-MM-DD');

    // Update the form control with the formatted date
    this.dynamicForm.get('date_document')?.setValue(formattedDate);

    const group = this.formBuilder.group({
      num_document: [num_document],
      date_document: [formattedDate],
      amount: [amount],
      pay_procedure: [pay_procedure],
    });

    this.by_invoices.push(group);
  }
  addGroup2() {
    const num_document = this.dynamicForm.get('num_document')?.value;
    const date_document = this.dynamicForm.get('date_document')?.value;
    const amount = this.dynamicForm.get('amount')?.value;
    const pay_procedure = this.dynamicForm.get('pay_procedure')?.value;

    // Format the date as YYYY-MM-DD
    const formattedDate = moment(date_document).format('YYYY-MM-DD');

    // Update the form control with the formatted date
    this.dynamicForm.get('date_document')?.setValue(formattedDate);

    const group = this.formBuilder.group({
      num_document: [num_document],
      date_document: [formattedDate],
      amount: [amount],
      pay_procedure: [pay_procedure],
    });
    this.by_prepay_invoices.push(group);
  }
  addGroup3() {
    const group = this.formBuilder.group({
      plan_item: ['']
    });
    this.planitems.push(group);
  }
  removeGroup1(index: number) {
    this.by_invoices.removeAt(index);
  }
  removeGroup2(index: number) {
    this.by_prepay_invoices.removeAt(index);
  }
  removeGroup3(index: number) {
    this.planitems.removeAt(index);
  }
  addPaymentsFact(addContractsList: any): void {
    const response = this.dynamicForm.value
    console.log(response)

    this.сreate = this.formBuilder.group({
      json_cod: [response],
    });
    const json = this.сreate.value
    console.log(json)
    this.contractsService.addContractsList(addContractsList, json).subscribe(
      (serverResponse) => {
        // Обработка ответа от сервера, если необходимо
        console.log('Ответ от сервера:', serverResponse);
        this.dynamicForm.value
        this.dynamicForm.reset();
      },
      (error) => {
        // Обработка ошибки при отправке данных, если необходимо
        console.error('Ошибка отправки данных:', error);
        this.dynamicForm.reset();
      }

    );

    // const actsData = this.actsForm.value as acts;

    // // Format the date as YYYY-MM-DD
    // const formattedDate = moment(actsData.planned_date).format('YYYY-MM-DD');

    // // Update the actsData object with the formatted date
    // actsData.planned_date = formattedDate;

    // this.contractsService.addActsList(addContractsList, response).subscribe(
    //   (response: acts[]) => {
    //     console.log('Payment added successfully:', response);
    //     this.getAct = response;
    //     this.getActsList(contractId);
    //     this.actsForm.reset();
    //   },
    // );
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  // Изменения формата даты, для -ru языка
  getDateFormatString(): string {
    if (this._locale === 'ru-RU') {
      return 'ГГГГ-ММ-ДД';
    } else if (this._locale === 'fr') {
      return 'ДД-ММ-ГГГГ';
    }
    return '';
  }

  
  @ViewChild(MatPaginator) paginator1!: MatPaginator;
  @ViewChild(MatPaginator) paginator2!: MatPaginator;
  @ViewChild(MatPaginator) paginator3!: MatPaginator;
  @ViewChild('select') select;
  pageSize = 10;
  pageSizeOptions: number[] = [2, 10, 20];

  @ViewChild('table_1') paginator!: MatPaginator;
  ngAfterViewInit() {
    // this.dataSourceReg.paginator = this.paginatorReg;
    this.dataSource.paginator = this.paginator;
  }

  tabs = ['Этапы', 'Работы', 'Календарь платежей', 'План актирования'];
  displayedColumns: string[] = [
    'serial_number',
    'side',
    'code_contract',
    'action'
  ];
  displayedColumnsList: string[] = [
    'code_counterparty',
    'code_contract',
    'side',
    'subject',
    'currency',
    'contract_limit',
    'fact_development',
    'percent_development',
    'fact_financing',
    'procent_financing',
    'action'
  ];

  showBlock1 = false
  showBlock2 = false
  showBlock3 = false
  showBlock4 = false
  showBlock5 = false
  showBlock6 = false
  showBlock7 = false
  showBlock8 = false
  showBlock9 = false
  showBlock10 = false
  showBlock11 = false

  contracts() {
    this.showBlock4 = !this.showBlock4
  }

  // Используем библиотеку jsonformatter
  performJmesPathQuery(data: any, query: string): any {
    return jmespath.search(data, query);
  }
  // Переменный для выборки
  payScheme: string[] | null = null;
  paySchemes!: any
  statusSet: string | null = null;
  statusSets: any
  CurrencySet: string | null = null;
  CurrencySets!: any
  varRateSet: string | null = null;
  varRateSets!: any

  typePay: any | null = null;
  typePays!: any

  contractId: any
  paymentsEdit: any
  contractDetails: any | null = null;
  contractDetailsFact: any | null = null;

  // Переменные для получения списка в добавление договоров
  getCurrency: any[] = []
  selectedCur!: string;
  getVARrate: any[] = []
  selectedVAR!: string;
  getContracts: any[] = []
  selectedCon!: string;

  selectedStatuses!: string
  getStatuses: any[] = []
  selectedAccounting!: string
  getAccounting: any[] = []


  invoices: any[] = []
  plan_items: any[] = []
  prepay_invoices: any[] = []
  getAct: acts[] = []
  fact: paymentsFact[] = []
  payments: payments[] = []
  WBSList: WBSList[] = []
  WBSListFact: WBSList[] = []
  WBSListPlan: WBSList[] = []
  WBSListAct: WBSList[] = []

  factDetails: factDetails[] = []
  paymentsList: payments[] = []
  ContractsCreatesList: ContractsCreates[] = []

  selected = new FormControl(0);
  counterpartiesData: any[] = [];

  ContractGroups: ContractGroup[] = [];
  dataSource = new MatTableDataSource<ContractGroup>([]);

  elementsReg: regContracts[] = [];
  dataSourceReg = new MatTableDataSource<regContracts>(this.elementsReg);


  elementsFact: paymentsFact[] = [];
  dataSourceFact = new MatTableDataSource<paymentsFact>(this.elementsFact);

  // Данные для фильтра Статус
  yearsRange: number[] = [];
  selectedYear: number = new Date().getFullYear();
  statusOptions: any = [];
  currencyOptions: any[] = [];
  selectedStatus: string = 'All';
  selectedCurrency: string = 'All';

  filteredContracts: ContractGroup[] = [];

  newPlanItemVisible = false;


  addDocx() {
    this.showBlock6 = !this.showBlock6;
  }
  addInvoices() {
    this.showBlock8 = !this.showBlock8;
    this.showBlock7 = false;

    this.showBlock9 = false;

  }
  addItems() {
    this.showBlock9 = false;
    this.showBlock8 = false;
    this.showBlock7 = false;
  }
  addPrepay() {
    this.showBlock8 = false;
    this.showBlock7 = !this.showBlock7;
  }
  saveDocx(contract_id: number, payment_id: number) {
    const createDocx = this.createDocx.value;
    this.contractsService.addPaymentsFactDocx(contract_id, payment_id, createDocx).subscribe(
      (response: any) => {
        console.log('access', response);
        this.downloadFile(response.file_path);
      },
      (error: any) => {
        console.error('Error occurred:', error);
      }
    );
  }
  downloadFile(fileUrl: string) {
    const link = document.createElement('a');
    link.href = fileUrl;
    link.target = '_blank';
    link.download = 'Требование на оплату.docx';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  saveInvoices(contract_id: number, payment_id: number) {
    const Invoices = this.InvoicesForm.value as factDetails;

    console.log('id', this.invoices);
    // Format the date as YYYY-MM-DD
    const StartdDate = moment(Invoices.date_document).format('YYYY-MM-DD');

    // Update the Invoices object with the formatted date
    Invoices.date_document = StartdDate;

    this.contractsService.addPaymentsFactInvoices(contract_id, payment_id, Invoices).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
      },
    );
  }
  saveItems(contract_id: number, payment_id: number) {
    const ItemsForm = this.ItemsForm.value;
    this.newPlanItemVisible = false;
    this.getPaymentsFactDetails(contract_id, payment_id)
    this.contractsService.addPaymentsFactItems(contract_id, payment_id, ItemsForm).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
      },
    );
  }

  savePrepay(contract_id: number, payment_id: number) {
    const PrepayForm = this.PrepayForm.value as factDetails;

    // Format the date as YYYY-MM-DD
    const StartdDate = moment(PrepayForm.date_document).format('YYYY-MM-DD');

    // Update the PrepayForm object with the formatted date
    PrepayForm.date_document = StartdDate;

    this.contractsService.addPaymentsFactPrepay(contract_id, payment_id, PrepayForm).subscribe(
      (response: factDetails[]) => {
        console.log('access', response);
      },
    );
  }

  editItems(facttId: any, paymentId: any, item: any) {
    const id = item.id;
    const updatedData = {
      item: item.item
    };
    this.contractsService.editPaymentsFactItems(facttId, paymentId, id, updatedData).subscribe(
      (response: items[]) => {
        // Можно добавить обработку успешного сохранения здесь, если нужно
      }
    );
    item.editing = false
  }
  ediInvoices(facttId: any, paymentId: any, item: any) {
    const id = item.id;
    const updatedData = {
      num_document: item.num_document,
      date_document: item.date_document,
      amount: item.amount,
      pay_procedure: item.pay_procedure,
    };

    this.contractsService.editPaymentsFactInvoices(facttId, paymentId, id, updatedData).subscribe(
      (response: invoices[]) => {
        // Можно добавить обработку успешного сохранения здесь, если нужно
      }
    );
    item.editing = false
  }
  editPrepayInvoices(facttId: any, paymentId: any, item: any) {
    const id = item.id;
    const updatedData = {
      num_document: item.num_document,
      date_document: item.date_document,
      amount: item.amount,
      pay_procedure: item.pay_procedure,
    };
    this.contractsService.editPaymentsFactPrepay(facttId, paymentId, id, updatedData).subscribe(
      (response: prepay_invoices[]) => {
        // Можно добавить обработку успешного сохранения здесь, если нужно
      }
    );

    item.editing = false
  }

  addRowPlanItems() {
    this.newPlanItemVisible = true;
  }
  delPrepayInvoices(contractId: number, paymentId: number, item: any): void {
    const id = item.id;
    this.contractsService.delPaymentsFactPrepay(contractId, paymentId, id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsList(contractId);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  delItems(contractId: number, paymentId: number, item: any): void {
    const id = item.id;
    this.contractsService.delPaymentsFactItems(contractId, paymentId, id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        // Удаление элемента из списка
        this.plan_items = this.plan_items.filter(planItem => planItem.id !== id);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  delInvoices(contractId: number, paymentId: number, item: any): void {
    const id = item.id;
    this.contractsService.delPaymentsFactInvoices(contractId, paymentId, id).subscribe(
      (response: any[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsList(contractId);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  // Пагинация для вложенной таблицы Платежи
  onPagePayments(event: PageEvent): void {
    this.paginator1.pageIndex = event.pageIndex;
    this.paginator1.pageSize = event.pageSize;
  }
  // Пагинация для вложенной таблицы Акты
  onPageActs(event: PageEvent): void {
    this.paginator2.pageIndex = event.pageIndex;
    this.paginator2.pageSize = event.pageSize;
  }
  // Пагинация для вложенной таблицы Факт
  onPageFact(event: PageEvent): void {
    this.paginator3.pageIndex = event.pageIndex;
    this.paginator3.pageSize = event.pageSize;
  }

  // Создание даты по годам
  generateYearsRange(): void {
    const currentYear = new Date().getFullYear();
    const range = 23;
    for (let i = currentYear; i > currentYear - range; i--) {
      this.yearsRange.push(i);
    }
  }

  // Сортировка по полю в таблице Реестр
  applyFilters(): void {
    const searchText = this.searchForm.value.searchText.toLowerCase();
    // this.filteredContracts = this.dataSource.data.filter(contract => {
    //   const isStatusMatched = this.selectedStatus === 'All' ? true : contract.status === this.selectedStatus;
    //   const isCurrencyMatched = this.selectedCurrency === 'All' ? true : contract.currency === this.selectedCurrency;
    //   const isYearMatched = this.selectedYear === null ? true : new Date(contract.date_start).getFullYear() === this.selectedYear;

    //   return (
    //     isStatusMatched &&
    //     isCurrencyMatched &&
    //     isYearMatched &&
    //     this.matchesSearchText(contract, searchText)
    //   );
    // });
  }
  matchesSearchText(contract: listContracts, searchText: string): boolean {
    return (
      (contract.code_counterparty?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.code_contract?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.legal_number?.toLowerCase() ?? '').includes(searchText) ||
      (contract.subject?.toLowerCase() ?? '').includes(searchText) ||
      (contract.VAT_rate?.toLowerCase() ?? '').includes(searchText) ||
      (contract.amount_without_VAT?.toString().toLowerCase() ?? '').includes(searchText) ||
      (contract.amount_with_VAT?.toString().toLowerCase() ?? '').includes(searchText)
    );
  }
  // Получение входных данных в таблицу Реестр
  getWorkReg(): void {
    this.contractsService.getWorkReg().subscribe(
      (data: ContractGroup[]) => {
        this.ContractGroups = data;
        const contracts: listContracts[] = this.ContractGroups.flatMap(group => group.contracts);

        this.statusOptions = jmespath.search(this.ContractGroups, '[].statuses.[*]|[]|[]');
        this.currencyOptions = jmespath.search(this.ContractGroups, '[].currencies.[*]|[]|[]');

        // this.filteredContracts = contracts.map(contract => ({ ...contract, editing: false }));
        this.filteredContracts = data.slice(0, data.length - 1)

        this.dataSource.data = this.filteredContracts;
        console.log(this.filteredContracts)

        this.generateYearsRange();
        // Apply initial filtering
        this.applyFilters();

        // Выборка для добавление договора 
        //Валюта
        const cur = "[*].currencies.[*][][]";
        this.getCurrency = jmespath.search(data, cur);
        //Статус
        const sta = "[].VARrate.[*][][]";
        this.getVARrate = jmespath.search(data, sta);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  //Получение Код контрагента
  getContractsList(): void {
    this.contractsService.getContractsList().subscribe(
      (response: any[]) => {
        this.getContracts = response
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }

  // Получение данных для таблицы Реестр (блок подробнее)
  getDetails(contractId: number) {
    this.contractsService.СontractsList(contractId).subscribe(
      (response: listContracts[]) => {
        console.log('set:', response);
        if (response.length > 0) {
          this.contractDetails = response[0];
        } else {
          this.contractDetails = null;
        }

        // Выборка 
        //Pay schem
        const query = "[*].pay_schemes[]";
        const paySchemesArray = this.performJmesPathQuery(response, query);
        this.paySchemes = paySchemesArray ?? null;
        //Статус
        const statusArray = Object.entries(response[1].statuses);
        this.statusSets = statusArray;
        this.statusSet = this.contractDetails?.status ?? null;
        //Валюта
        const currencyArray = Object.entries(response[1].currencies);
        this.CurrencySets = currencyArray;
        this.CurrencySet = this.contractDetails?.currency ?? null;
        //НДС
        const varArray = Object.entries(response[1].VARrate);
        this.varRateSets = varArray;
        this.varRateSet = this.contractDetails?.VAT_rate ?? null;
      },
      (error: any) => {
        console.error('Failed to retrieve payments data:', error);
      }
    );
  }
  // Сохранить данные для таблицы Реестр (блок подробнее)
  saveReg(element: any): void {
    element.editing = false;

    // Преобразование даты в нужный формат
    const dateEnd = moment(element.date_end).format('YYYY-MM-DD');
    const startEnd = moment(element.date_start).format('YYYY-MM-DD');
    const date_contr_SAP = moment(element.date_contr_SAP).format('YYYY-MM-DD');

    const id = element.id;
    const updatedData = {
      code_counterparty: element.code_counterparty,
      code_contract: element.code_contract,
      legal_number: element.legal_number,
      subject: element.subject,
      VAT_rate: this.varRateSet,
      amount_without_VAT: element.amount_without_VAT,
      amount_with_VAT: element.amount_with_VAT,
      status: this.statusSet,
      currency: this.CurrencySet,
      contract_limit: parseFloat(element.contract_limit),
      fact_development: parseFloat(element.fact_development),
      prcent_development: element.prcent_development,
      fact_financing: parseFloat(element.fact_financing),
      procent_financing: element.procent_financing,
      date_start: startEnd,
      date_end: dateEnd,
      pay_schem: this.payScheme,
      advance: parseFloat(element.advance),
      sdr_id: element.sdr_id,
      side: element.side,
      legal_code_SAP: element.legal_code_SAP,
      num_contr_SAP: element.num_contr_SAP,
      date_contr_SAP: date_contr_SAP
    };

    this.contractsService.updСontractsList(id, updatedData).subscribe(
      (response: any) => {
        console.log('Data updated successfully:', response);
        element.editing = false;
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  // Получение входных данных в таблицу Платежи Факт
  getPaymentsFact(factId: number): void {
    this.contractsService.getPaymentsFact(factId).subscribe(
      (response: any[]) => {
        this.fact = response.slice(0, response.length - 1)
        this.dataSourceFact.paginator = this.paginator3;

        const acc = "[*].accounting_groups.[*][][]";
        this.getAccounting = jmespath.search(response, acc);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  editPaymentFact(facttId: any, item: any) {
    const id = item.id;
    const updatedData = {
      stage: item.stage,
      status: this.selectedStatuses,
      // amount: item.amount,
      accounting_group: this.selectedAccounting,
    };
    this.contractsService.editPaymentsFactDetails(facttId, id, updatedData).subscribe(
      (response: any[]) => {
        console.log(response);
        // Можно добавить обработку успешного сохранения здесь, если нужно
        item.editing = false;
        this.getPaymentsFactDetails(facttId, id);
      }
    );
  }
  // Просмотр продробной информации в таблице Платежи Факт
  getPaymentsFactDetails(facttId: number, paymentId: number): void {
    console.log(facttId, paymentId)
    this.contractsService.getPaymentsFactDetails(facttId, paymentId).subscribe(
      (response: factDetails[]) => {
        this.factDetails = response.slice(0, response.length - 1);
        if (response.length > 0) {
          this.contractDetailsFact = response[0];
        } else {
          this.contractDetailsFact = null;
        }

        const sta = "[*].statuses.[*][][]";
        this.getStatuses = jmespath.search(response, sta);
        const acc = "[*].accounting_groups.[*][][]";
        this.getAccounting = jmespath.search(response, acc);

        const query = "[*].invoices[]";
        this.invoices = jmespath.search(response, query);

        const item = "[*].plan_items[]";
        this.plan_items = jmespath.search(response, item);

        const inv = "[*].prepay_invoices[]";
        this.prepay_invoices = jmespath.search(response, inv);
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );

    this.contractsService.getPaymentsFactWBS(facttId, paymentId).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSListFact = response
        this.WBSList.forEach(item => {
          if (this.WBSListFact.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    )


  }

  // Удалить данные в таблице Платежи Факт
  deletePaymentFact(contractId: number, paymentId: number): void {
    this.contractsService.delPaymentsFact(contractId, paymentId).subscribe(
      (response: paymentsFact[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsFact(contractId);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  toppingsWBSList = new FormControl();
  // Получение входных данных в таблицу СДР
  getWbsList(contractId: number): void {
    this.TreeSDRData(contractId)
    this.contractsService.getWBSList(contractId).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSList = response.slice(0, response.length - 1);
        const initialToppings = this.WBSList.map(item => item.id);
        this.toppingsWBSList.setValue(initialToppings);
        console.log(this.WBSList);

      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }

  // Получение входных данных в таблицу Платежи
  getPaymentsList(contractId: number): void {
    this.contractsService.getPaymentsList(contractId).subscribe(
      (response: any[]) => {
        console.log('Payments data retrieved successfully:', response);
        this.payments = response.slice(0, response.length - 1);
        this.dataSource.paginator = this.paginator1;

        // Тип платежа
        const types = response[response.length - 1].types;
        const varRate = response[response.length - 1].VARrate;

        // Convert the types and VAT rates to an array of key-value pairs
        this.typePays = Object.entries(types);
        // this.varRateSets = Object.entries(varRate);

      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  // Добавление договора
  addContractsCreate(): void {
    const ContractsData = this.contractsCreate.value as ContractsCreates;

    // Format the date as YYYY-MM-DD
    const StartdDate = moment(ContractsData.date_start).format('YYYY-MM-DD');
    const EndDate = moment(ContractsData.date_end).format('YYYY-MM-DD');
    const SapDate = moment(ContractsData.date_contr_SAP).format('YYYY-MM-DD');

    // Update the ContractsData object with the formatted date
    ContractsData.date_start = StartdDate;
    ContractsData.date_end = EndDate;
    ContractsData.date_contr_SAP = SapDate;

    this.contractsService.addContractsCreate(ContractsData, ContractsData).subscribe(
      (response: ContractsCreates[]) => {
        console.log('Payment added successfully:', response);
        this.ContractsCreatesList = response;
        this.getWorkReg();
        this.contractsCreate.reset();
        this.theme.access = 'Данные добавлены успешно. См. детали контрагента'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.showBlock4=false
      },
      (error: any) => {
        console.error('Failed to add payment:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }
  // Добавление данных в таблицу Платежи
  addPayment(contractId: number): void {
    const paymentData = this.paymentForm.value as payments;
    // Format the date as YYYY-MM-DD
    const formattedDate = moment(paymentData.date_pay).format('YYYY-MM-DD');
    // Update the paymentData object with the formatted date
    paymentData.date_pay = formattedDate;
    this.contractsService.addPaymentsList(contractId, paymentData).subscribe(
      (response: payments[]) => {
        console.log('Payment added successfully:', response);
        this.payments = response;
        this.getPaymentsList(contractId);
        this.paymentForm.reset();
      },
      (error: any) => {
        console.error('Failed to add payment:', error);
      }
    );
  }
  // Обновить данные в таблице Платежи
  savePayment(contractId: number, payment: payments): void {
    payment.editing = false;
    // Преобразование даты в нужный формат
    const dateEnd = moment(payment.date_pay).format('YYYY-MM-DD');

    const updatedData = {
      type_pay: payment.type_pay,
      date_pay: dateEnd,
      stage: payment.stage,
      budget_item: payment.budget_item,
    };

    this.contractsService.editPaymentsList(contractId, payment.id, updatedData).subscribe(
      (response: payments[]) => {
        console.log('Payment updated successfully:', response);
        this.getPaymentsList(contractId);
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }

  // Удалить данные в таблице Платежи
  deletePayment(contractId: number, paymentId: number): void {
    this.contractsService.delPaymentsList(contractId, paymentId).subscribe(
      (response: payments[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsList(contractId);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  // Получение входных данных в таблицу Акты
  getActsList(actsId: number): void {
    this.contractsService.getActsList(actsId).subscribe(
      (response: acts[]) => {
        this.getAct = response.slice(0, response.length - 1)
        this.dataSource.paginator = this.paginator2;
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
  // Добавление данных в таблицу Акты
  addActs(contractId: number): void {
    const actsData = this.actsForm.value as acts;

    // Format the date as YYYY-MM-DD
    const formattedDate = moment(actsData.planned_date).format('YYYY-MM-DD');

    // Update the actsData object with the formatted date
    actsData.planned_date = formattedDate;

    this.contractsService.addActsList(contractId, actsData).subscribe(
      (response: acts[]) => {
        console.log('Payment added successfully:', response);
        this.getAct = response;
        this.getActsList(contractId);
        this.actsForm.reset();
      },
      (error: any) => {
        console.error('Failed to add payment:', error);
      }
    );
  }
  // Обновить данные в таблице Акты
  saveActs(contractId: number, acts: acts): void {
    acts.editing = false;
    // Преобразование даты в нужный формат
    const dateEnd = moment(acts.planned_date).format('YYYY-MM-DD');

    const updatedData = {
      description: acts.description,
      document_fact: acts.document_fact,
      planned_date: dateEnd,
      stage: acts.stage,
      // VAT_rate: this.varRateSet,
      cost_item: acts.cost_item,
      amount: acts.amount,
    };

    this.contractsService.editActsList(contractId, acts.id, updatedData).subscribe(
      (response: acts[]) => {
        console.log('Payment updated successfully:', response);
        this.getActsList(contractId);
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }

  // Изменить цену СДР ОБЩАЯ
  saveWBSamount(Id: number, wbs): void {

    console.log(Id, wbs)
    this.contractsService.editWBSamount(Id, wbs.amount).subscribe(
      (response) => {
        console.log('Payment updated successfully:', response);
        this.getWbsList(this.currentContractID)
        wbs.editing = false;
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }
  // Изменить цену СДР (План ФАКТ)
  saveWBSFactAmount(Id: number, wbs): void {
    console.log(Id, wbs.amount)
    this.contractsService.editWBSFactAmount(this.currentContractID, this.currentPaymentID, wbs.id, wbs.amount).subscribe(
      (response) => {
        console.log('Payment updated successfully:', response);
        this.getWbsList(this.currentContractID)
        wbs.editing = false;
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }
  // Удалить данные СДР (Платежи ФАКТ)
  delWBSFact(id): void {
    this.contractsService.deliteWBSFact(this.currentContractID, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        console.log('Payment deleted successfully:', response);
        this.getPaymentsFactDetails(this.currentContractID, this.currentPaymentID)
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  // Удалить данные в таблице Платежи
  deleteActs(contractId: number, actsId: number): void {
    this.contractsService.delActsList(contractId, actsId).subscribe(
      (response: acts[]) => {
        console.log('Payment deleted successfully:', response);
        this.getActsList(contractId)
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }



  addPaymentWBS(typePay: string) {
    const selectedIds = this.WBSList.filter(item => item.selected).map(item => {
      return { id: item.id };
    });
    const updatedData = { wbs: selectedIds };
    console.log(updatedData);
    this.contractsService.addWbsPayments(this.currentContractID, this.currentPaymentID, typePay, updatedData).subscribe(
      (response) => {
        console.log('add successfully:', response);
        if (typePay == 'payments_fact') {
          this.getPaymentsFactDetails(this.currentContractID, this.currentPaymentID)
        }
        if (typePay == 'payments_plan') {
          this.getWBSPlan(this.currentPaymentID)
        }
        if (typePay == 'acts') {
          this.getWBSAct(this.currentPaymentID)
        }
      },
      (error: any) => {
        console.error('Failed to add WBS:', error);
      }
    );
  }


  // Закрыть блок "Добавить"
  closedAdd() {
    this.showBlock2 = false
    this.showBlock3 = false
    this.showBlock4 = false
    this.showBlock5 = false
    this.showBlock6 = false
    this.showBlock7 = false
    this.showBlock8 = false
    this.showBlock9 = false
    this.showBlock10 = false
    this.showBlock11 = false
  }
  // Закрыть блок "подронее"
  closed() {
    this.showBlock1 = false
    this.showBlock2 = false
    this.showBlock3 = false
  }
  // Показать/Скрыть блок "добавить" Платежи
  addElementPay() {
    this.showBlock2 = !this.showBlock2
    this.showBlock3 = false
  }
  // Показать/Скрыть блок "добавить" Акты
  addElementActs() {
    this.showBlock3 = !this.showBlock3
    this.showBlock2 = false
  }
  // Кнопка редактирование блока "подробнее"
  edit(contractDetails: any) {
    contractDetails.editing = true;
    let selectedScheme = this.paySchemes.find((option: any) => option.name === contractDetails.pay_schem);
    if (selectedScheme) {
      this.payScheme = selectedScheme.id; // присваиваем id найденной схемы оплаты
    }
  }



  WindowTreeSDR() {
    this.showBlock9 = !this.showBlock9;
  }


  TreeSDRData(id: number) {

    this.contractsService.addSDR().subscribe(
      (serverResponse: ApiTreeData[]) => {
        console.log(serverResponse);

        // Подготовка функции для рекурсивной установки флага "selected"
        const markSelectedNodes = (node: ApiTreeData, selectedIds: number[]) => {
          if (selectedIds.includes(node.id)) {
            node.selected = true;
            node.disabled = false;  // Установка узла как неактивного
          }

          let allChildrenSelected = true;

          if (node.children) {
            node.children.forEach(childNode => {
              markSelectedNodes(childNode, selectedIds);
              if (!childNode.selected) {
                allChildrenSelected = false;
              }
            });
          }

          if (allChildrenSelected && node.children && node.children.length > 0) {
            node.selected = true;
            node.disabled = true;  // Установка узла как неактивного
          }
        };

        // Получение списка уже отмеченных ID
        this.getWbsTree(this.currentContractID).then(selectedIds => {
          serverResponse.forEach(node => markSelectedNodes(node, selectedIds));
        });

        // Устанавливаем родителей для всех узлов
        serverResponse.forEach(node => {
          this.setParent(node, null);
        });

        this.TreeDataSource.data = serverResponse;
        this.treeControl.dataNodes = this.TreeDataSource.data;
        // Раскрыть все узлы
        this.treeControl.expandAll();
      },
      (error) => {
        console.error('Ошибка отправки данных:', error);
      }
    );
  }

  getWbsTree(contractId: number): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.contractsService.getWBSTree(contractId).subscribe(
        (response: any[]) => {
          console.log("для дерева");
          console.log(response);
          const selectedIds = response.map(item => item.id);
          resolve(selectedIds);
        },
        (error: any) => {
          console.error('Failed to update data:', error);
          reject(error);
        }
      );
    });
  }

  delWBS(id: number) {
    this.contractsService.delWBS(id).subscribe(
      (response: acts[]) => {
        console.log('WBS deleted successfully:', response);
        this.getWbsList(this.currentContractID)
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }

  public hasChild = (_: number, node: ApiTreeData) =>
    !!node.children && node.children.length > 0;



  private checkAllParents(node: ApiTreeData) {
    if (node.parent) {
      const descendants = this.treeControl.getDescendants(node.parent);
      node.parent.selected =
        descendants.every(child => child.selected);
      node.parent.indeterminate =
        descendants.some(child => child.selected);
      this.checkAllParents(node.parent);
    }
  }
  private setParent(node: ApiTreeData, parent: ApiTreeData) {
    node.parent = parent;
    if (node.children) {
      node.children.forEach(childNode => {
        this.setParent(childNode, node);
      });
    }
  }

  itemToggle(checked: boolean, node: ApiTreeData) {
    node.selected = checked;
    if (node.children) {
      node.children.forEach(child => {
        this.itemToggle(checked, child);
      });
    }
    this.checkAllParents(node);
  }






  public submitSDR() {
    let result = this.TreeDataSource.data.reduce(
      (acc: { id: number }[], node: ApiTreeData) =>
        acc.concat(
          this.treeControl.getDescendants(node)
            .filter(descendant => descendant.selected)
            .map(descendant => ({ id: descendant.id }))
        ),
      [] as { id: number }[]
    );

    const output = {
      wbs: result
    };
    console.log(this.currentContractID)


    this.contractsService.addWbsContracts(this.currentContractID, output).subscribe(
      (response: any) => {
        console.log('access', response);
        this.getWbsList(this.currentContractID)
        this.showBlock9 = false
      },
      (error: any) => {
        console.error('Error occurred:', error);
      }
    );
  }

  // Кнопка редактирование таблицы Платежи
  editPayments(payment: any) {
    payment.editing = true;
  }
  // Кнопка редактирование таблицы Акты
  editActs(acts: any) {
    acts.editing = true;
  }


  // Кнопка в таблице "подробнее" (Вывод по id)
  showDetails(element: listContracts) {
    this.currentContractID = element.id
    console.log("вот выбранный ид контракта", this.currentContractID)
    this.showBlock1 = true
    if (element.id) {
      this.getWbsTree(element.id)
      this.TreeSDRData(element.id)


      this.getWbsList(element.id)
      this.getPaymentsList(element.id);
      this.getActsList(element.id);
      this.getPaymentsFact(element.id);
      this.getDetails(element.id);
    }
  }
  // Кнопка в таблице (Платежи, факт) "подробнее" (Вывод по id)
  showFactDetails(id: number, paymentId: number) {
    this.showBlock5 = true
    if (id) {
      this.currentPaymentID = paymentId
      console.log("выбранный плтаеж", this.currentPaymentID)
      this.getPaymentsFactDetails(id, paymentId);

    }
  }


  COLUMNS_SCHEMA = [
    {
      key: "docNumber",
      type: "text",
      label: "№ документа"
    },
    {
      key: "docDate",
      type: "date",
      label: "Дата документа "
    },
    {
      key: "sumWithTax",
      type: "text",
      label: "Сумма, с НДС"
    },
    {
      key: "calculationOrder",
      type: "text",
      label: "Порядок расчётов согласно условиям договора"
    },
    {
      key: "isEdit",
      type: "isEdit",
      label: ""
    }
  ]
  displayedColumnsDocuments: string[] = this.COLUMNS_SCHEMA.map(col => col.key);
  dataSourceDocuments = [
    { docNumber: '001', docDate: '2023-01-01', sumWithTax: '1000', calculationOrder: 'Сначала НДС' },
    { docNumber: '002', docDate: '2023-01-02', sumWithTax: '2000', calculationOrder: 'Сначала основная сумма' }
  ];

  columnsSchemaDocuments: any = this.COLUMNS_SCHEMA;
  addRowDocuments() {
    const newRow = { docNumber: "", docDate: "", sumWithTax: '', calculationOrder: "", isEdit: true };
    this.dataSourceDocuments = [...this.dataSourceDocuments, newRow];
  }


  COLUMNS_SCHEMA_Prepaid = [
    {
      key: "docNumber",
      type: "text",
      label: "№ документа"
    },
    {
      key: "docDate",
      type: "date",
      label: "Дата документа "
    },
    {
      key: "sumWithTax",
      type: "text",
      label: "Сумма, с НДС"
    },
    {
      key: "calculationOrder",
      type: "text",
      label: "Порядок расчётов поставки (выполнения работ) согласно условиям договора"
    },
    {
      key: "isEdit",
      type: "isEdit",
      label: ""
    }
  ]

  dataSourcePrepaid = [
    { docNumber: '001', docDate: '2023-01-01', sumWithTax: '1000', calculationOrder: 'Сначала НДС' },
    { docNumber: '002', docDate: '2023-01-02', sumWithTax: '2000', calculationOrder: 'Сначала основная сумма' }
  ];


  displayedColumnsPrepaid: string[] = this.COLUMNS_SCHEMA_Prepaid.map(col => col.key);
  columnsSchemaPrepaid: any = this.COLUMNS_SCHEMA_Prepaid;
  addRowPrepaid() {
    const newRow = { docNumber: "", docDate: "", sumWithTax: '', calculationOrder: "", isEdit: true };
    this.dataSourcePrepaid = [...this.dataSourceDocuments, newRow];
  }

  COLUMNS_SCHEMA_Planitems = [
    {
      key: "Planitems",
      type: "number",
      label: "Пункты плана"
    },
    {
      key: "isEdit",
      type: "isEdit",
      label: ""
    }
  ]


  displayedColumnsPlanitems: string[] = this.COLUMNS_SCHEMA_Planitems.map(col => col.key);
  columnsSchemaPlanitems: any = this.COLUMNS_SCHEMA_Planitems;


  editItem(item: any) {
    this.selectedStatuses = item.status;
    this.selectedAccounting = item.accounting_group;
    item.editing = true;
  }
  saveItem(item: any) {
    item.editing = false;
  }

  //Добавить счёт-фактуру
  addRowInvoices() {
    const newRow = {
      num_document: "",
      date_document: "",
      amount: '',
      pay_procedure: "",
      editing: true
    };
    this.invoices = [...this.invoices, newRow];
  }
  // Добавить счёт на предлоплату
  addRowPrepaymentInvoice() {
    const newRow = {
      num_document: "",
      date_document: "",
      amount: '',
      pay_procedure: "",
      editing: true
    };
    this.prepay_invoices = [...this.prepay_invoices, newRow];
  }

  panelOpenState = false;


  // Получить данные о СДР ПЛАН
  getWBSPlan(id: number) {
    this.currentPaymentID = id
    console.log("Платеж нгомер", this.currentPaymentID)
    this.showBlock10 = true
    this.contractsService.getPaymentsPlanWBS(this.currentContractID, id).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSListPlan = response
        this.WBSList.forEach(item => {
          item.selected = false;
        });
        this.WBSList.forEach(item => {
          if (this.WBSListPlan.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    )
  }

  saveWBSPlanAmount(Id: number, wbs): void {
    console.log(Id, wbs.amount)
    this.contractsService.editWBSPlanAmount(this.currentContractID, this.currentPaymentID, wbs.id, wbs.amount).subscribe(
      (response) => {
        console.log('Payment updated successfully:', response);
        this.getWbsList(this.currentContractID)
        wbs.editing = false;
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }


  // Удалить данные СДР (Платежи План)
  delWBSPlan(id): void {
    this.contractsService.deliteWBSPlan(this.currentContractID, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        console.log('Payment deleted successfully:', response);
        this.getWBSPlan(this.currentPaymentID)
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }






  // Получить данные о СДР АКТ
  getWBSAct(id: number) {
    this.currentPaymentID = id
    console.log("Платеж нгомер", this.currentPaymentID)
    this.showBlock11 = true
    this.contractsService.getPaymentsActWBS(this.currentContractID, id).subscribe(
      (response: WBSList[]) => {
        console.log(response);
        this.WBSListAct = response
        this.WBSList.forEach(item => {
          item.selected = false;
        });
        this.WBSList.forEach(item => {
          if (this.WBSListAct.some(fact => fact.code === item.code)) {
            item.selected = true;
          }
        });
      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    )
  }
// Изменить цену СДР АКТ 
  saveWBSActAmount(Id: number, wbs): void {
    console.log(Id, wbs.amount)
    this.contractsService.editWBSActAmount(this.currentContractID, this.currentPaymentID, wbs.id, wbs.amount).subscribe(
      (response) => {
        console.log('Payment updated successfully:', response);
        this.getWbsList(this.currentContractID)
        wbs.editing = false;
      },
      (error: any) => {
        console.error('Failed to update payment:', error);
      }
    );
  }


  // Удалить данные СДР (АКТ)
  delWBSAct(id): void {
    this.contractsService.deliteWBSAct(this.currentContractID, this.currentPaymentID, id).subscribe(
      (response: payments[]) => {
        console.log('Payment deleted successfully:', response);
        this.getWBSAct(this.currentPaymentID)
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    );
  }
  
  setId: number
  setCode: number
  showRegMore(elem:number) { 
    // console.log(elem)
    this.setCode = elem 
    if(elem) { 
      this.router.navigate(['register-contracts/code']); 
      this.reg.setSetCode(this.setCode); 
    } 
  }
}