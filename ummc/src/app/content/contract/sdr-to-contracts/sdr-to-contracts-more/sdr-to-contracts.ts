export interface SdrToContractsItemData {
    id: number
    budget_deviations: string
    dates_deviations: string
    status: string
    code_wbs: string
    name_wbs: string
    project_type: string
    head_of_project: string
    company: string
    date_start_plan: string
    date_end_fact: string
    date_start_prediction: string
    date_end_prediction: string
    budget_plan: string
    budget_prediction: string
    budget_saving: string
    npv: string
    irr: string
    pi: number
    Documents?: PortfolioReporDocument[]
}
export interface PortfolioReporDocument {
    id: number
    portfolio_report_id_id: number
    document: string
    files: string
    status_id_id: number
    status_title: string
}