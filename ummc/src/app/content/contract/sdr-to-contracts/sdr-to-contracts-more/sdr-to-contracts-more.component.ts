import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/content/budget/project-budget/common.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { ShowService } from 'src/app/helper/show.service';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SdrToContractsItemData } from './sdr-to-contracts';

declare var bootstrap: any;
@Component({
  selector: 'app-sdr-to-contracts-more',
  templateUrl: './sdr-to-contracts-more.component.html',
  styleUrls: ['./sdr-to-contracts-more.component.css']
})
export class SdrToContractsMoreComponent implements OnInit {
  getId: number
  SdrToContractsItemDataTable = this.theme.initTable<SdrToContractsItemData>();
  SdrToContractsItemData: SdrToContractsItemData[]
  dataSdrToContractsDocument = this.theme.initTable<SdrToContractsItemData>();

  constructor(public common: CommonService, private route: ActivatedRoute, public show: ShowService, public theme: ThemeService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe) {
    this.common.currentSetId.subscribe(id => {
      this.getId = id
      console.log(this.getId)
    });
  }

  ngOnInit() {
    console.log('id:', this.getId)
    this.getSdrToContractsItem(this.getId)
  }

  getSdrToContractsItem(requestId): void {
    this.params.getSdrToContractsItem(requestId).subscribe(
      (data: any) => {
        const mainData = data[0];
        this.SdrToContractsItemDataTable.data = [mainData];
        console.log('шапка',this.SdrToContractsItemDataTable.data);

        this.dataSdrToContractsDocument.data= data.slice(1);
        console.log('data',this.dataSdrToContractsDocument.data);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });


  }

}
