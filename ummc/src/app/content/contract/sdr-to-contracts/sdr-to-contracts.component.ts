import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { CommonService } from '../../budget/project-budget/common.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ObjectBudgetNode } from '../../budget/project-budget/registerBudget';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SDR_Contracts_Tree } from './sdr-to-contracts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sdr-to-contracts',
  templateUrl: './sdr-to-contracts.component.html',
  styleUrls: ['./sdr-to-contracts.component.css']
})
export class SdrToContractsComponent implements OnInit {
  setId: number
  treeControl = new FlatTreeControl<ObjectBudgetNode>(
    node => node.level,
    node => node.expandable
  );
  private _transformer = (node: SDR_Contracts_Tree, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      code_wbs: node.code_wbs,
      acts: node.acts,
      payments_plan: node.payments_plan,
      payments_fact: node.payments_fact,
      num_of_contracts: node.num_of_contracts,
      id_wbs: node.id_wbs,
      level: level,
    };
  };


  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  displayedColumns: string[] = ["serial_number", "name", "acts", "payments_plan", "payments_fact", 'actions'];
  dataSDRContractsTree = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public theme: ThemeService, public show: ShowService, public isTitle: CommonService, private params: ApicontentService,private router: Router,public common:CommonService, private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.getSDRContracts()
  }


  getSDRContracts() {
    this.params.getSDRContracts().subscribe(
      (data: any[]) => {
        this.dataSDRContractsTree.data = data
        console.log(this.dataSDRContractsTree.data)
      }
    );
  }

  showDetails(id: number) {
    console.log(id)
    this.setId = id
    if (id) {
      this.router.navigate(['sdr-contracts/more']);
      this.common.setSetId(this.setId);
    }
  }

}
