export interface SDR_Contracts_Tree {
    code_wbs: string
    name: string
    acts: number
    payments_plan: number
    payments_fact: number
    num_of_contracts: number
    id_wbs: number
    level: number
    children?: SDR_Contracts_Tree[];
}