import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApicontentService } from 'src/app/api.content.service';
import { WBSList, acts, payments, paymentsFact } from './register-contracts/reg-contracts';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RegService {
  private setCodeSource = new BehaviorSubject<number | null>(null);
  currentSetCode = this.setCodeSource.asObservable();
  private setCodeKey = 'setCode';

  private setIdSource = new BehaviorSubject<number | null>(null);
  currentSetId = this.setIdSource.asObservable();
  private setIdKey = 'setId';

  private factIdSource = new BehaviorSubject<number | null>(null);
  currentFactId = this.factIdSource.asObservable();
  private factIdKey = 'factId';

  constructor(public contractsService: ApicontentService) {
    this.loadFromLocalStorage(this.setCodeKey, this.setCodeSource);
    this.loadFromLocalStorage(this.setIdKey, this.setIdSource);
    this.loadFromLocalStorage(this.factIdKey, this.factIdSource);
  }

  private loadFromLocalStorage(key: string, source: BehaviorSubject<number | null>): void {
    const value = localStorage.getItem(key);
    const parsedValue = value ? +value : null;
    source.next(parsedValue);
  }

  private saveToLocalStorage(key: string, value: number): void {
    localStorage.setItem(key, value.toString());
  }
  setSetCode(id: number) {
    this.saveToLocalStorage(this.setCodeKey, id);
    this.setCodeSource.next(id);
  }

  setSetId(id: number) {
    this.saveToLocalStorage(this.setIdKey, id);
    this.setIdSource.next(id);
  }

  setFactId(id: number) {
    this.saveToLocalStorage(this.factIdKey, id);
    this.factIdSource.next(id);
  }

  clearSetCode() {
    localStorage.removeItem(this.setCodeKey);
    this.setCodeSource.next(null);
  }

  clearSetId() {
    localStorage.removeItem(this.setIdKey);
    this.setIdSource.next(null);
  }

  clearFactId() {
    localStorage.removeItem(this.factIdKey);
    this.factIdSource.next(null);
  }
  payments: payments[] = []
  fact: paymentsFact[] = []
  getAct: acts[] = []
  
    // Получение входных данных в таблицу Платежи
 getPaymentsPlan(contractId: number): void {
  this.contractsService.getPaymentsList(contractId).subscribe(
    (response: any[]) => {
      this.payments = response.slice(0, response.length - 1);
    });
  }
    // Получение входных данных в таблицу Платежи Факт
  getPaymentsFact(factId: number): void {
    this.contractsService.getPaymentsFact(factId).subscribe(
      (response: any[]) => {
        this.fact = response.slice(0, response.length - 1)
      },
    );
  }
  getActsList(actsId: number): void {
    this.contractsService.getActsList(actsId).subscribe(
      (response: acts[]) => {
        this.getAct = response.slice(0, response.length - 1)
        // this.dataSource.paginator = this.paginator;
      },
    );
  }
  
  toppingsWBSList = new FormControl();
  WBSList: WBSList[] = []

  // Получение входных данных в таблицу СДР
  getWbsList(contractId: number): void {
    this.contractsService.getWBSList(contractId).subscribe(
      (response: WBSList[]) => {
        this.WBSList = response.slice(0, response.length - 1);

        const initialToppings = this.WBSList.map(item => item.id);
        this.toppingsWBSList.setValue(initialToppings);

      },
      (error: any) => {
        console.error('Failed to update data:', error);
      }
    );
  }
}

