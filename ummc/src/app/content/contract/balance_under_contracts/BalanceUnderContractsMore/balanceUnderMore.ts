export type BalanceUnderContractBMore = BalanceUnderContractMoreData[]

export interface BalanceUnderContractMoreData {
    contract_id: number
    code_contract: number
    counterparty: string
    subject: string
    amount: number
    utilization_plan: number
    utilization_fact: number
    utilization_deviation: number
    utilization_remains: number
    financing_plan: number
    financing_fact: number
    financing_deviation: number
    financing_remains: number
    utilization_realization: number
    financing_realization: number
}
