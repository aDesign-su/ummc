import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from 'src/app/helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BalanceUnderContractBMore } from './balanceUnderMore';
import { ShowService } from 'src/app/helper/show.service';
import { CommonService } from 'src/app/content/budget/project-budget/common.service';


@Component({
  selector: 'app-BalanceUnderContractsMore',
  templateUrl: './BalanceUnderContractsMore.component.html',
  styleUrls: ['./BalanceUnderContractsMore.component.css']
})

export class BalanceUnderContractsMoreComponent implements OnInit {
  DataBalanceUnderContractItem= this.theme.initTable<BalanceUnderContractBMore>(); 
  isLoading:boolean;
BalanceUnderContractsMoreComponent: any;
  constructor(private route: ActivatedRoute,  public isTitle: CommonService, public theme: ThemeService,public show: ShowService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe) {
  
  }

  ngOnInit() {
    this.isLoading = true;
    const requestId = this.route.snapshot.paramMap.get('id');
    if (requestId) {
      
      console.log(requestId)
      this.getBalanceUnderContractItem(requestId)
    }
  }

  getBalanceUnderContractItem(id): void {
    this.isLoading = true;
    console.log(id)
    this.params.getBalanceUnderContractDetail(id).subscribe(
      (data: any) => {
        this.DataBalanceUnderContractItem.data=data
        this.isLoading = false;
        console.log(data)
        console.log(this.isLoading)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }



 
}
