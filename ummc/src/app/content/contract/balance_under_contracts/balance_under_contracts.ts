export type BalanceUnderContracts = BalanceUnderContractsData[]

export interface BalanceUnderContractsData {
  counterparty: string
  code_counterparty: number
  amount: number
  utilization_plan: number
  utilization_fact: number
  utilization_deviation: number
  utilization_remains: number
  financing_plan: number
  financing_fact: number
  financing_deviation: number
  financing_remains: number
  utilization_realization: number
  financing_realization: number
}
