import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { CommonService } from '../../budget/project-budget/common.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ObjectBudgetNode } from '../../budget/project-budget/registerBudget';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { BalanceUnderContractsData } from './balance_under_contracts';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-balance_under_contracts',
  templateUrl: './balance_under_contracts.component.html',
  styleUrls: ['./balance_under_contracts.component.css']
})
export class BalanceUnderContractsComponent implements OnInit {

  dataBalanceUnderContractsTableSource = this.theme.initTable<BalanceUnderContractsData>();
  @ViewChild('paginatorForBalanceUnderContracts') paginatorForBalanceUnderContracts: MatPaginator;
  isLoading = true;

  constructor(public theme: ThemeService, private router: Router, public isTitle: CommonService, private fb: FormBuilder, public params: ApicontentService, private datePipe: DatePipe,) {
  }

  ngOnInit() {
    this.getDataBalanceUnderContracts()
  }

  ngAfterViewInit() {
    this.dataBalanceUnderContractsTableSource.paginator = this.paginatorForBalanceUnderContracts;
  }

  getDataBalanceUnderContracts(): void {
    this.isLoading = true;
    this.params.getBalanceUnderContracts().subscribe(
      (data: any) => {
        console.log(data)
        this.dataBalanceUnderContractsTableSource.data = data;
        this.isLoading = false;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }





  // Остаток по договорам// подробнее
  moreBalanceUnderContract(requestId: number): void {
    this.router.navigate(['/balance-under-contracts', requestId]);
  }

}
