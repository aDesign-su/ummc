import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { BudgetObject } from '../../budget/registry/registry';
import { ApicontentService } from 'src/app/api.content.service';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {GpcsDocuments} from './gpcs-documents';
import _ from 'lodash';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-gpcs-documents',
  templateUrl: './gpcs-documents.component.html',
  styleUrls: ['./gpcs-documents.component.css']
})
export class GpcsDocumentsComponent implements OnInit {
  [x: string]: any;
  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder,private wbsService: ApicontentService,private _liveAnnouncer: LiveAnnouncer) {
    this.gpcsForm = formBuilder.group({
      name: ["", [Validators.required]],
      budget: [null, [Validators.required]],
      user_created: 36
    });
    this.gpfcsForm = formBuilder.group({
      name: ["", [Validators.required]],
      budget: [null, [Validators.required]],
      user_created: 36
    });
  }

  ngOnInit() {
    this.getGpcsDoc();
    this.getGpfcsDoc();
    this.getBudgets()
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }
  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
  getBudget: BudgetObject[] = [];
  selectedRegion: string;
  editedElement!: GpcsDocuments;

  loading: boolean = true;

  changeLoading() {
    this.loading = !this.loading;
  }

  getBudgets() {
    this.wbsService.getBudgetObjects().subscribe(
      (data: BudgetObject[]) => {
        this.getBudget = data;
      }
    )
  }

  displayedColumns: string[] = ['name', 'date', 'budget_name', 'action'];
  dataGpcs: GpcsDocuments[] = [];
  dataGpfcs: GpcsDocuments[] = [];

  gpcsForm!: FormGroup;
  gpfcsForm!: FormGroup;

  get _gpcsName() {
    return this.gpcsForm.get("name");
  }

  get _gpcsBudget() {
    return this.gpcsForm.get("budget");
  }

  get _gpfcsName() {
    return this.gpfcsForm.get("name");
  }

  get _gpfcsBudget() {
    return this.gpfcsForm.get("budget");
  }

  @ViewChild(MatSort) sort: MatSort;
  ngAfterViewInit() {
    // this.dataSourceGpcs.sort = this.sort;
    // this.dataSourceGpfcs.sort = this.sort;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  getGpcsDoc() {
    this.wbsService.getGpcsDocuments().subscribe(
      (data: GpcsDocuments[]) => {
        this.dataGpcs = data['data'];
        console.log("Success get " + this.dataGpcs);
        this.changeLoading();
      }, error => {
        console.log("Failed get " + error);
      }
    )
  }

  getGpfcsDoc() {
    this.wbsService.getGpfcsDocuments().subscribe(
      (data: GpcsDocuments[]) => {
        this.dataGpfcs = data['data'];
        console.log("Success get " + this.dataGpfcs);
      }, error => {
        console.log("Failed get " + error);
      }
    )
  }

  addGpcsDoc() {
    this.loading = true;
    const AddedElement = this.gpcsForm.value;
    this.wbsService.addGpcsDocument(AddedElement).subscribe(
      (data: any) => {
        console.log("Success added " + data);
        this.getGpcsDoc();
        this.closedAdd();
      }, error => {
        console.log("Failed add " + error);
      }
    )
  }

  addGpfcsDoc() {
    this.loading = true;
    const AddedElement = this.gpfcsForm.value;
    this.wbsService.addGpfcsDocument(AddedElement).subscribe(
      (data: any) => {
        console.log("Success added " + data);
        this.getGpfcsDoc();
        this.closedAdd();
        this.changeLoading();
      }, error => {
        console.log("Failed add " + error);
      }
    )
  }

  setEditing(element: any){
    this.dataGpcs.forEach(element => this.show.cancel(element))
    this.dataGpfcs.forEach(element => this.show.cancel(element))
    element.editing = true;
    this.editedElement = _.cloneDeep(element);
  }

  cancelEditing(element: any) {
    this.editedElement = null;
    this.show.cancel(element);
  }

  editGpcsDoc(UpdatedDocument: any) {
    this.loading = true;
    this.wbsService.editGpcsDocument(this.editedElement).subscribe(
      (data: any) => {
        console.log("Success edit " + data);
        this.getGpcsDoc();
        // this.cancelEditing(UpdatedDocument);
      }, error => {
        console.log("Failed edit" + error);
      }
    )
  }

  editGpfcsDoc(UpdatedDocument: any) {
    this.loading = true;
    this.wbsService.editGpfcsDocument(this.editedElement).subscribe(
      (data: any) => {
        console.log("Success edit " + data);
        this.getGpfcsDoc();
        this.changeLoading();
        // this.cancelEditing(UpdatedDocument);
      }, error => {
        console.log("Failed edit" + error);
      }
    )
  }

  deleteGpcsDoc(id: number) {
    this.loading = true;
    this.wbsService.deleteGpcsDocument(id).subscribe(
      (data: any) => {
        console.log("Success delete " + data);
        this.getGpcsDoc();
      }, error => {
        console.log("Failed delete " + error);
      }
    )
  }

  deleteGpfcsDoc(id: number) {
    this.loading = true;
    this.wbsService.deleteGpfcsDocument(id).subscribe(
      (data: any) => {
        console.log("Success delete " + data);
        this.getGpfcsDoc();
        this.changeLoading();
      }, error => {
        console.log("Failed delete " + error);
      }
    )
  }
}
