export interface GpcsDocuments {
  id: number,
  date: Date,
  name: string,
  user_created: number,
  budget: number,
  budget_name: string
}

export interface AddGpcsDocument {
  name: string,
  user_created: number,
  budget: number
}

export interface EditGpcsDocument {
  id: number,
  name: string,
  user_created: number,
  budget: number
}
