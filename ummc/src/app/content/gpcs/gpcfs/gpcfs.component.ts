import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { BudgetObject } from '../../budget/registry/registry';
import {GpcsDocuments} from '../gpcs-documents/gpcs-documents'
import {GpcsPrice} from '../gpcs/gpcs'
import {GpfcsPrice, GpfcsTableView, GpfcsForm, GpfcsFormMonths, Plan, Fact} from './gpfcs'
import _ from 'lodash';
import {NodeObject} from '../../budget/svz-documents/svz/svz'
import {ActivatedRoute} from '@angular/router'
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-gpcfs',
  templateUrl: './gpcfs.component.html',
  styleUrls: ['./gpcfs.component.css']
})
export class GpcfsComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder,private wbsService: ApicontentService,private _liveAnnouncer: LiveAnnouncer, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(data => {
      this.gpfcsFilter = data["id"];
    })

    this.addObject = formBuilder.group({
      id: null,
      date: [null, [Validators.required]],
      total_price: [null, [Validators.required]],
      smr_price: [null, [Validators.required]],
      pir_price: [null, [Validators.required]],
      oto_price: [null, [Validators.required]],
      other_price: [null, [Validators.required]],
      gpfcs: [null]
    })
    this.addSystem = formBuilder.group({
      id: null,
      date_start: [null, [Validators.required]],
      date_end: [null, [Validators.required]],
      january_price: [null, [Validators.required]],
      february_price: [null, [Validators.required]],
      march_price: [null, [Validators.required]],
      april_price: [null, [Validators.required]],
      may_price: [null, [Validators.required]],
      june_price: [null, [Validators.required]],
      july_price: [null, [Validators.required]],
      august_price: [null, [Validators.required]],
      september_price: [null, [Validators.required]],
      october_price: [null, [Validators.required]],
      november_price: [null, [Validators.required]],
      december_price: [null, [Validators.required]],
      document: [null, [Validators.required]],
      objects_wbs: [null, [Validators.required]]
    })
  }

  dataSource: MatTableDataSource<any>;

  async ngOnInit() {
    Promise.all([this.getDocuments(), this.getNodes(), this.getFinancingPlan(), this.getFinancingFact()]).then(() => {
      if (typeof this.gpfcsFilter !== 'undefined') {
        this.getGpfcsFiltered();
      } else {
        this.getGpfcs();
      }
      this.deactivloading();
    })
  }
  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }

  ngAfterViewInit() {
  }

  monthsShowing = false;

  changeMonthShowing() {
    this.monthsShowing = !this.monthsShowing;
  }

  loading: boolean = true;

  private activLoading() {
    this.loading = true;
  }

  private deactivloading() {
    this.loading = false;
  }

  formDisplayedColumns: string[] = ['WbsCode', 'Id', 'Name', 'StartDate', 'EndDate', 'Approved', 'Completed', 'Remains', 'TotalPrice', 'SmrPrice', 'PirPrice', 'OtoPrice', 'FinancingPlan', 'OnePg', 'TwoPg', 'Action'];
  formByMonthsDisplayedColumns: string[] = [
    'WbsCode', 'Id', 'Name', 'StartDate', 'EndDate', 'Approved', 'Completed', 'Remains', 'TotalPrice', 'SmrPrice', 'PirPrice', 'OtoPrice', 'OnePg', 'TwoPg', 'FinancingPlan', 
    'JanuaryPlan', 'JanuaryFact', 
    'FebruaryPlan', 'FebruaryFact', 
    'MarchPlan', 'MarchFact', 
    'AprilPlan', 'AprilFact', 
    'MayPlan', 'MayFact', 
    'JunePlan', 'JuneFact', 
    'JulyPlan', 'JulyFact', 
    'AugustPlan', 'AugustFact', 
    'SeptemberPlan', 'SeptemberFact', 
    'OctoberPlan', 'OctoberFact', 
    'NovemberPlan', 'NovemberFact', 
    'DecemberPlan', 'DecemberFact', 
    'Action'
  ];
  priceColumns: string[] = ['Date', 'TotalAmount', 'CIW', 'DSW', 'OM', 'Other', 'Actions']
  data: GpfcsTableView[] = [];

  gpfcsFilter!: number;
  documents: GpcsDocuments[] = [];
  price: GpfcsPrice[] = [];
  nodes: NodeObject[] = [];
  editedElement!: GpfcsTableView;
  editedPrice!: GpfcsPrice;

  planGeneral: Plan[] = [];
  factGeneral: Fact[] = [];

  priceUsedElement: GpfcsTableView;
  priceMenuName: string;

  private priceMenuVisible = false;
  private priceAddMenuVisible = false;

  setPriceMenuInvisible() {
    this.priceMenuVisible = false;
  }

  get getPriceMenuVisible() {
    return this.priceMenuVisible;
  }

  setPriceAddMenuInvisible() {
    this.priceAddMenuVisible = false;
  }

  get getPriceAddMenuVisible() {
    return this.priceAddMenuVisible;
  }

  setPriceMenuVisible(element: GpfcsTableView, monthName: string) {
    this.getMonthPrice(element.id);
    this.priceMenuName = monthName;
    this.priceUsedElement = element;
    this.priceMenuVisible = true;
  }

  setPriceAddMenuVisible(name: string) {
    //this.addedObjectName = name;
    this.priceAddMenuVisible = true;
  }

  checkField(value: any) {
    return (value || value == 0);
  }

  addObject!: FormGroup;
  addSystem!: FormGroup;

  _addObjectProperty(name: string) {
    return this.addObject.get(name);
  }

  _addSystemProperty(name: string) {
    return this.addSystem.get(name);
  }

  async getFinancingPlan(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getPlan().subscribe(
        (data: any) => {
          console.log("Plan")
          console.log(data)
          this.planGeneral = data;
          resolve();
        }
      )
    })
  }

  async getFinancingFact(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getFinancingFact().subscribe(
        (data: any) => {
          console.log(data)
          this.factGeneral = data;
          resolve();
        }
      )
    })
  }

  async getNodes(): Promise<void> {
    return new Promise((resolve, reject) => {
      const Filter = {level: 3}
      this.wbsService.getSvzWbsByLevelFilter(Filter).subscribe(
        (data: NodeObject[]) => {
          console.log("Nodes")
          console.log(data);
          this.nodes = data;
          resolve();
        }
      )
    });
  }

  async getGpfcs() {
    this.wbsService.getGpfcs().subscribe(
      (data: GpfcsTableView[]) => {
        console.log("Gpfcs")
        console.log(data)
        this.data = this.setDocumentName(data);
        this.getGpfcsForm()
      }
    )
  }

  async getGpfcsForm() {

    function fillWithMonths(nodes, gpfcsData, planGeneral, factGeneral) {
      let filledData: GpfcsFormMonths[] = [];
      
      gpfcsData.forEach(gpfcs => {
        nodes.forEach(node => {
          if (node.id == gpfcs.objects_wbs) {
            let formMonths = gpfcs;
            formMonths.code_wbs = node.code_wbs
            formMonths.code_assoi = node.code_assoi

            formMonths.info_object_approved = 0;
            formMonths.total_plan = 0;

            formMonths.january_fact_price = 0;
            formMonths.february_fact_price = 0;
            formMonths.march_fact_price = 0;
            formMonths.april_fact_price = 0;
            formMonths.may_fact_price = 0;
            formMonths.june_fact_price = 0;
            formMonths.july_fact_price = 0;
            formMonths.august_fact_price = 0;
            formMonths.september_fact_price = 0;
            formMonths.october_fact_price = 0;
            formMonths.november_fact_price = 0;
            formMonths.december_fact_price = 0;

            formMonths.january_plan_price = 0;
            formMonths.february_plan_price = 0;
            formMonths.march_plan_price = 0;
            formMonths.april_plan_price = 0;
            formMonths.may_plan_price = 0;
            formMonths.june_plan_price = 0;
            formMonths.july_plan_price = 0;
            formMonths.august_plan_price = 0;
            formMonths.september_plan_price = 0;
            formMonths.october_plan_price = 0;
            formMonths.november_plan_price = 0;
            formMonths.december_plan_price = 0;

            formMonths.one_pg = 0;
            formMonths.two_pg = 0;

            for (let i = 0; i < factGeneral.length - 1; i++) {
              if (factGeneral[i].code_wbs === gpfcs.code_wbs) {
                formMonths.info_object_approved = factGeneral.plan_fin[i].amount;
                break;
              }
            }

            for (let i = 0; i < planGeneral.length - 1; i++) {
              if (planGeneral[i].id === gpfcs.id) {
                for (let j = 0; j < planGeneral[i].plan_fin.length; j++) {
                  let counter = 0;
                  if (planGeneral[i].plan_util[j]['title'][-1] === 'у') {
                    if (counter <= 5) {
                      formMonths.one_pg += planGeneral[i].plan_fin[j]['amount'];
                    } else {
                      formMonths.two_pg += planGeneral[i].plan_fin[j]['amount'];
                    }

                    switch (counter) {
                      case 0:
                        formMonths.january_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 1:
                        formMonths.february_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 2:
                        formMonths.march_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 3:
                        formMonths.april_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 4:
                        formMonths.may_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 5:
                        formMonths.june_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 6:
                        formMonths.july_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 7:
                        formMonths.august_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 8:
                        formMonths.september_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 9:
                        formMonths.october_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 10:
                        formMonths.november_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 11:
                        formMonths.december_plan_price = planGeneral[i].plan_fin[j]['amount'];
                        break;

                    }
                    counter += 1;
                  }
                } 

                break;
              }
            }
            

            for (let i = 0; i < factGeneral.length - 1; i++) {
              if (factGeneral[i].id === gpfcs.id) {
                for (let j = 0; j < factGeneral[i].plan_util.length; j++) {
                  let counter = 0;
                  if (factGeneral[i].plan_util[j]['title'][0] !== '2') {
                    if (counter <= 5) {
                      formMonths.one_pg += factGeneral[i].plan_fin[j]['amount'];
                    } else {
                      formMonths.two_pg += factGeneral[i].plan_fin[j]['amount'];
                    }

                    switch (counter) {
                      case 0:
                        formMonths.january_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 1:
                        formMonths.february_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 2:
                        formMonths.march_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 3:
                        formMonths.april_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 4:
                        formMonths.may_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 5:
                        formMonths.june_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 6:
                        formMonths.july_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 7:
                        formMonths.august_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 8:
                        formMonths.september_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 9:
                        formMonths.october_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 10:
                        formMonths.november_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 11:
                        formMonths.december_fact_price = factGeneral[i].plan_fin[j]['amount'];
                        break;

                    }
                    counter += 1;
                  }
                } 

                break;
              }
            }

            for (let i = 0; i < planGeneral.length - 1; i++) {
              if (planGeneral[i].id === gpfcs.id) {
                formMonths.one_pg = calcByHalfYear();
                formMonths.two_pg = calcByHalfYear();
                break;
              } else {
                formMonths.one_pg = 0;
                formMonths.two_pg = 0;
              }
            }
    
            formMonths.january_plan_price = 0;
            formMonths.january_fact_price = 0;
            formMonths.february_plan_price = 0;
            formMonths.february_fact_price = 0;
            formMonths.march_plan_price = 0;
            formMonths.march_fact_price = 0;
            formMonths.april_plan_price = 0;
            formMonths.april_fact_price = 0;
            formMonths.may_plan_price = 0;
            formMonths.may_fact_price = 0;
            formMonths.june_plan_price = 0;
            formMonths.june_fact_price = 0;
            formMonths.july_plan_price = 0;
            formMonths.july_fact_price = 0;
            formMonths.august_plan_price = 0;
            formMonths.august_fact_price = 0;
            formMonths.september_plan_price = 0;
            formMonths.september_fact_price = 0;
            formMonths.october_plan_price = 0;
            formMonths.october_fact_price = 0;
            formMonths.november_plan_price = 0;
            formMonths.november_fact_price = 0;
            formMonths.december_plan_price = 0;
            formMonths.december_fact_price = 0;
            
            for (let i = 0; i < planGeneral.length - 1; i++) {
              if (planGeneral[i].code_wbs === gpfcs.code_wbs) {
                formMonths.total_plan = planGeneral.plan_fin[i].amount;
                break;
              }
            }

            filledData.push(formMonths);
          }
        })
      })

      return filledData;
    }

    function calcByHalfYear() {
      return 1;
    }

    this.dataSource = new MatTableDataSource(fillWithMonths(this.nodes, this.data, this.planGeneral, this.factGeneral));
  }

  async getGpfcsFiltered() {
    const Filter = {id: this.gpfcsFilter}
    this.wbsService.getGpfcsFiltered(Filter).subscribe(
      (data: GpfcsTableView[]) => {
        this.data = this.setDocumentName(data['data']);
      }
    )
  }

  setDocumentName(data: GpfcsTableView[]) {
    data.forEach(element => {
      this.documents.forEach(document => {
        if (element.document == document.id || element['document_id'] == document.id) {
          element.name = document.name;
        }
      })
    })

    return data;
  }

  addGpfcs() {
    this.loading = true;
    const AddedGpfcs = this.addSystem.value;
    this.wbsService.addGpfcs(AddedGpfcs).subscribe(
      (data: GpfcsTableView[]) => {
        console.log(data);
        this.closedAdd();
        this.getGpfcs();
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      }, error => {
        console.log(error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  setElementEditing(element: any) {
    this.data.forEach(element => this.show.cancel(element))
    element.editing = true;
    this.editedElement = _.cloneDeep(element);
  }

  cancelElementEditing(element: any) {
    this.editedElement = null;
    this.show.cancel(element);
  }

  saveGpfcs(element: any) {
    this.loading = true;
    this.wbsService.updateGpfcs(this.editedElement).subscribe(
      (data: any) => {
        console.log("Update success " + data);
        this.cancelElementEditing(element);
        this.getGpfcs();
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        console.log("Update error " + error);
        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }
  delGpfcs(id: number) {
    this.loading = true;
    this.wbsService.deleteGpfcs(id).subscribe(
      (data: any) => {
        console.log("Delete success " + data);
        this.getGpfcs();
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        console.log("Delete error " + error);
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  async getDocuments(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getGpfcsDocuments().subscribe(
        (data: GpcsDocuments[]) => {
          console.log("Документы")
          console.log(data);
          this.documents = data['data'];
          resolve();
        }
      )
    });
  }

  getMonthPrice(id: number) {
    const gpfcsFilter = {id: id};
    this.wbsService.getGpfcsPrice(gpfcsFilter).subscribe(
      (data: GpfcsPrice[]) => {
        console.log(data['data']);
        this.price = data['data'];
      }, error => {
        console.log(error)
      }
    )
  }

  setPriceEditing(element: any) {
    this.price.forEach(element => this.show.cancel(element))
    element.editing = true;
    this.editedPrice = _.cloneDeep(element);
  }

  cancelPriceEditing(element: any) {
    this.editedPrice = null;
    this.show.cancel(element);
  }

  addPriceObject(id: number) {
    let AddedObject = this.addObject.value;
    AddedObject.gpfcs = id;
    this.wbsService.addGpfcsPriceObject(AddedObject).subscribe(
      (data: GpfcsPrice[]) => {
        this.setPriceAddMenuInvisible();
        this.getMonthPrice(this.priceUsedElement.id);
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  editPriceObject(element: GpfcsPrice) {
    this.wbsService.updateGpfcsPriceObject(this.editedPrice).subscribe(
      (data: GpcsPrice) => {
        console.log("Success edit " + data);
        this.cancelPriceEditing(element);
        this.getMonthPrice(this.priceUsedElement.id);
        this.theme.info = 'Данные изменены успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  deletePriceObject(id: number) {
    this.wbsService.deleteGpfcsPriceObject(id).subscribe(
      (data: any) => {
        console.log("Success delete " + data);
        this.getMonthPrice(this.priceUsedElement.id);
        this.theme.info = 'Объект удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось удалить объект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }
}
