/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GpcfsComponent } from './gpcfs.component';

describe('GpcfsComponent', () => {
  let component: GpcfsComponent;
  let fixture: ComponentFixture<GpcfsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpcfsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpcfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
