import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { BudgetObject } from '../../budget/registry/registry';
import { ApicontentService } from 'src/app/api.content.service';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GpcsForm, GpcsFormMonths, GpcsPrice, GpcsTableView, Fact, Plan, MonthlyPayment } from './gpcs'
import { GpcsDocuments } from '../gpcs-documents/gpcs-documents'
import _ from 'lodash';
import { NodeObject } from '../../budget/svz-documents/svz/svz'
import { ActivatedRoute } from '@angular/router'
import { CommonService } from '../../budget/project-budget/common.service';


@Component({
  selector: 'app-gpcs',
  templateUrl: './gpcs.component.html',
  styleUrls: ['./gpcs.component.css']
})
export class GpcsComponent implements OnInit {
  constructor(public theme: ThemeService, public show:ShowService, public isTitle: CommonService, private formBuilder: FormBuilder,private wbsService: ApicontentService,private _liveAnnouncer: LiveAnnouncer, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(data => {
      this.gpcsFilter = data["id"];
    })
    this.generateGpcs = formBuilder.group({
      name: [null, [Validators.required]],
      comment: [null, [Validators.required]],
      type_gpcs: [null, [Validators.required]],
      user_created: 36
    })
    this.updateGpcsForm = formBuilder.group({
      id: [null, [Validators.required]],
      build_date_start: [null, [Validators.required]],
      build_date_end: [null, [Validators.required]],
      info_object_approved: [null, [Validators.required]],
      info_object_completed: [null, [Validators.required]],
      info_object_remains: [null, [Validators.required]],
      total_price: [null, [Validators.required]],
      smr_price: [null, [Validators.required]],
      pir_price: [null, [Validators.required]],
      oto_price: [null, [Validators.required]],
      other_price: [null, [Validators.required]],
      one_pg: [null, [Validators.required]],
      two_pg: [null, [Validators.required]],
      total_plan: [null, [Validators.required]],
      document: [null, [Validators.required]],
      gpcs: [null, [Validators.required]],
      common_budget: [null, [Validators.required]]
    })
  }

  
  formDisplayedColumns: string[] = ['WbsCode', 'Id', 'Name', 'StartDate', 'EndDate', 'Approved', 'Completed', 'Remains', 'TotalPrice', 'SmrPrice', 'PirPrice', 'OtoPrice', 'OtherPrice', 'FinancingPlan', 'OnePg', 'TwoPg', 'Action'];
  formByMonthsDisplayedColumns: string[] = [
    'WbsCode', 'Id', 'Name', 'StartDate', 'EndDate', 'Approved', 'Completed', 'Remains', 'TotalPrice', 'SmrPrice', 'PirPrice', 'OtoPrice', 'OtherPrice', 'OnePg', 'TwoPg', 'FinancingPlan', 
    'JanuaryPlan', 'JanuaryFact', 
    'FebruaryPlan', 'FebruaryFact', 
    'MarchPlan', 'MarchFact', 
    'AprilPlan', 'AprilFact', 
    'MayPlan', 'MayFact', 
    'JunePlan', 'JuneFact', 
    'JulyPlan', 'JulyFact', 
    'AugustPlan', 'AugustFact', 
    'SeptemberPlan', 'SeptemberFact', 
    'OctoberPlan', 'OctoberFact', 
    'NovemberPlan', 'NovemberFact', 
    'DecemberPlan', 'DecemberFact', 
    'Action'
  ];
  priceColumns: string[] = ['Date', 'TotalAmount', 'CIW', 'DSW', 'OM', 'Other', 'Actions'];
  types: string[] = ["Годовой", "Корректировка", "Отсутствует"];

  dataSource: MatTableDataSource<any>;

  data: GpcsTableView[] = []

  gpcsFilter!: number;
  documents: GpcsDocuments[] = [];
  price: GpcsPrice[] = [];
  nodes: NodeObject[] = [];

  planUtilizationGeneral: Plan[] = [];
  factUtilizationGeneral: Fact[] = [];
  planFinancing = [];
  factFinancing = [];
  planMonths;

  //! Temp
  svzData = [];

  totalCost;

  documentSelector = false;
  monthsShowing = false;

  async ngOnInit() {
    Promise.all([
      this.getDocuments(), 
      this.getNodes(), 
      this.getCurrentBudget(),
      this.getFinancingPlan(),
      this.getFinancingFact(), 
      this.getUtilizationPlan(), 
      this.getUtilizationFact(),
      this.getSvz()
    ]).then(() => {
      if (typeof this.gpcsFilter !== 'undefined') {
        this.getGpcsFiltered();
      } else {
        this.getGpcs();
      }
      this.deactivLoading();
    })
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  ngAfterViewInit() {
  }

  loading: boolean = true;

  private activLoading() {
    this.loading = true;
  }

  private deactivLoading() {
    this.loading = false;
  }

  changeMonthShowing() {
    this.monthsShowing = !this.monthsShowing;
  }

  checkField(value: any) {
    return (value || value == 0);
  }

  setSelectedTable(event) {
    this.documentSelector = event.index == 1;
  }

  prepareToEditGpcsForm(form: GpcsForm) {
    this.setUpdatedFields(form);
    this.show.toggleShowBlock(1);
  }

  setUpdatedFields(form: GpcsForm) {
    for (let field in form) {
      if (this.updateGpcsForm.get(field)) {
        this.updateGpcsForm.get(field).setValue(form[field]);
      }
    }
  }

  generateGpcs!: FormGroup;
  updateGpcsForm!: FormGroup;

  _addGenerateObjectProperty(name: string) {
    return this.generateGpcs.get(name);
  }

  _addGpcsFormProperty(name: string) {
    return this.updateGpcsForm.get(name);
  }

  async getNodes(): Promise<void> {
    return new Promise((resolve, reject) => {
      const Filter = {level: 3}
      this.wbsService.getSvzWbsByLevelFilter(Filter).subscribe(
        (data: NodeObject[]) => {
          this.nodes = data;
          resolve();
        }
      )
    })
  }

  addGenerateGpcs() {
    this.wbsService.getGenerateGpcs(this.generateGpcs.value).subscribe(
      (data: any) => {
        console.log(data);
        this.show.closedAdd(0);
        this.theme.info = 'Форма сгенерирована успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось сгенерировать форму. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  async getDocuments(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getGpcsDocuments().subscribe(
        (data: GpcsDocuments[]) => {
          this.documents = data['data'];
          resolve();
        }
      );
    })
  }

  async getGpcsFiltered() {
    const Filter = {id: this.gpcsFilter}
    this.wbsService.getGpcsFiltered(Filter).subscribe(
      (data: GpcsTableView[]) => {
        let processedData = data['data'];
        this.data = processedData;
      }
    )
  }

  async getGpcs() {
    this.wbsService.getGpcs().subscribe(
      (data: GpcsTableView[]) => {
        console.log(data)
        this.data = data;

        this.getGpcsForm(this.documents[0].id);
      }
    )
  }

  async getSvz() {
    this.wbsService.getSvzObjects().subscribe(
      (data: any[]) => {
        console.log(data)
        this.svzData = data

      }
    )
  }

  async getGpcsForm(id: number) {

    function fillWithMonths(data: GpcsFormMonths[], svzData, gpcsData, planGeneral, factGeneral, planFinancing, factFinancing, totalCost) {
      // let filledData: GpcsFormMonths[] = [];
      let filledData = [];
      
      // data.forEach(form => {
      planGeneral.forEach(object => {
        gpcsData.forEach(gpcs => {
          // if (form.gpcs == gpcs.id) {
            // let formMonths = form;
          if (object.id == gpcs.id) {
            // let formMonths = form;

            let formMonths = {};

            //! Fix
            formMonths['code_wbs'] = object.code_wbs;
            formMonths['code_assoi'] = "123";
            formMonths['name'] = object.name_wbs;
            formMonths['build_date_start'] = object.date_start;
            formMonths['build_date_end'] = object.date_end;
            formMonths['info_object_approved'] = svzData[svzData.length - 1].actual_price;
            formMonths['total_price'] = totalCost['total_price'];
            formMonths['other_price'] = totalCost['other_price'];
            formMonths['oto_price'] = totalCost['oto_price'];
            formMonths['pir_price'] = totalCost['pir_price'];
            formMonths['smr_price'] = totalCost['smr_price'];
            formMonths['info_object_completed'] = 0.4 * totalCost['total_price'];
            formMonths['info_object_remains'] = svzData[svzData.length - 1].total_price;
            formMonths['total_plan'] = 0;

            formMonths['january_fact_price'] = 0;
            formMonths['february_fact_price'] = 0;
            formMonths['march_fact_price'] = 0;
            formMonths['april_fact_price'] = 0;
            formMonths['may_fact_price'] = 0;
            formMonths['june_fact_price'] = 0;
            formMonths['july_fact_price'] = 0;
            formMonths['august_fact_price'] = 0;
            formMonths['september_fact_price'] = 0;
            formMonths['october_fact_price'] = 0;
            formMonths['november_fact_price'] = 0;
            formMonths['december_fact_price'] = 0;

            formMonths['january_plan_price'] = 0;
            formMonths['february_plan_price'] = 0;
            formMonths['march_plan_price'] = 0;
            formMonths['april_plan_price'] = 0;
            formMonths['may_plan_price'] = 0;
            formMonths['june_plan_price'] = 0;
            formMonths['july_plan_price'] = 0;
            formMonths['august_plan_price'] = 0;
            formMonths['september_plan_price'] = 0;
            formMonths['october_plan_price'] = 0;
            formMonths['november_plan_price'] = 0;
            formMonths['december_plan_price'] = 0;

            formMonths['one_pg'] = 0;
            formMonths['two_pg'] = 0;

            for (let i = 0; i < planGeneral.length - 1; i++) {
              if (object.code_wbs === gpcs.code_wbs) {
                formMonths['info_object_approved'] = planGeneral.plan_fin[i].amount;
                break;
              }
            }

            // for (let i = 0; i < planGeneral.length - 1; i++) {
            //   if (object.id === gpcs.id) {
                // for (let j = 0; j < object.plan_util.length; j++) {
                //   let counter = 0;
                //   if (object.plan_util[j]['title'][-1] === 'у') {
                for (let j = 0; j < object.plan_util.length; j++) {
                  let counter = 0;
                  if (object.plan_util[j]['title'][object.plan_util[j]['title'].length - 1] == "y") {
                    if (counter <= 5) {
                      formMonths['one_pg'] += object.plan_util[j]['amount'];
                    } else {
                      formMonths['two_pg'] += object.plan_util[j]['amount'];
                    }

                    switch (counter) {
                      case 0:
                        formMonths['january_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 1:
                        formMonths['february_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 2:
                        formMonths['march_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 3:
                        formMonths['april_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 4:
                        formMonths['may_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 5:
                        formMonths['june_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 6:
                        formMonths['july_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 7:
                        formMonths['august_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 8:
                        formMonths['september_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 9:
                        formMonths['october_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 10:
                        formMonths['november_plan_price'] = object.plan_util[j]['amount'];
                        break;
                      case 11:
                        formMonths['december_plan_price'] = object.plan_util[j]['amount'];
                        break;

                    }
                    counter += 1;
                  }
                } 

                // break;
            //   }
            // }
            

            for (let i = 0; i < factGeneral.length - 1; i++) {
              if (factGeneral[i].id === gpcs.id) {
                let counter = 0
                for (let j = 0; j < factGeneral[i].plan_util.length; j++) {
                  if (factGeneral[i].plan_util[j]['title'][0] !== '2') {
                    if (counter <= 5) {
                      formMonths['one_pg'] += factGeneral[i].plan_fin[j]['amount'];
                    } else {
                      formMonths['two_pg'] += factGeneral[i].plan_fin[j]['amount'];
                    }

                    switch (counter) {
                      case 0:
                        formMonths['january_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 1:
                        formMonths['february_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 2:
                        formMonths['march_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 3:
                        formMonths['april_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 4:
                        formMonths['may_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 5:
                        formMonths['june_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 6:
                        formMonths['july_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 7:
                        formMonths['august_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 8:
                        formMonths['september_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 9:
                        formMonths['october_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 10:
                        formMonths['november_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;
                      case 11:
                        formMonths['december_fact_price'] = factGeneral[i].plan_fin[j]['amount'];
                        break;

                    }
                    counter += 1;
                  }
                } 

                break;
              }
            }

            // for (let i = 0; i < planFinancing.length - 1; i++) {
            //   if (object.code_wbs === gpcs.code_wbs) {
            //     formMonths['total_plan'] = planGeneral.plan_fin[i].amount;
            //     break;
            //   }
            // }

            for (let i = 0; i < planFinancing.length - 1; i++) {
              if (object.id === gpcs.id) {
                formMonths['total_plan'] = planGeneral[i].amount;
                break;
              }
            }

            filledData.push(formMonths);
          }
        })
      })
  
      return filledData;
    }

    this.wbsService.getGpcsForm(id).subscribe(
      (data: GpcsForm[]) => {
        console.log(data);
        let processedData = data['data_form1'];

        this.dataSource = new MatTableDataSource(fillWithMonths(processedData, this.svzData, this.data, this.planUtilizationGeneral, this.factUtilizationGeneral, this.planFinancing, this.factFinancing, this.totalCost));
      }
    )
  }

  editGpcsForm() {
    this.activLoading();
    this.show.closedAdd(1);
    this.wbsService.editGpcsForm(this.updateGpcsForm.value).subscribe(
      (data: GpcsForm[]) => {
        console.log(data);
        this.getGpcs();
        this.theme.info = 'Форма изменена успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить форму. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  async getFinancingFact(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getFinancingFact(2023).subscribe(
        (data: any) => {
          this.factFinancing = data;
          console.log(data)
          resolve();
        }
      )
    })
  }

  async getFinancingPlan(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getPlan(2023).subscribe(
        (data: any) => {
          this.planFinancing = data;
          console.log(data)
          resolve();
        }
      )
    })
  }

  async getUtilizationPlan(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getUtilizationPlan(2024).subscribe(
        (data: Plan[]) => {
          console.log(data);
          this.planUtilizationGeneral = data;
          resolve();
        }
      )
    })
  }

  async getUtilizationFact(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.wbsService.getUtilizationFact(2024).subscribe(
        (data: Fact[]) => {
          console.log(data)
          this.factUtilizationGeneral = data;
          resolve();
        }
      )
    })
  }

  getTotalCost(id: number) {
    this.wbsService.getTotalCost(id).subscribe(
      (data: any) => {
        console.log(data);
      }
    )
  }

  getCurrentBudget() {
    return new Promise((resolve, reject) => {
      this.wbsService.getRegisterBudget().subscribe(
        (data: any) => {
          console.log(data['data']);
          (async () => {
            for (let object of data['data']) {
              if (object['is_current']) {
                await this.getTotalBudget(object['id']);
                break;
              }
            } 
          })();
          resolve("done");
        }
      )
    })
  }

  async getTotalBudget(id: number) {
    return new Promise((resolve, reject) => {
      this.wbsService.getTotalCost(id).subscribe(
        (data: any) => {
          this.totalCost = data['total_cost_object'];
          console.log(data['total_cost_object']);
          resolve("done");
        }
      )
    })
  }
}
