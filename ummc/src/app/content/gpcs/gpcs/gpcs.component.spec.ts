/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GpcsComponent } from './gpcs.component';

describe('GpcsComponent', () => {
  let component: GpcsComponent;
  let fixture: ComponentFixture<GpcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
