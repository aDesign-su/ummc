import { Component, OnInit } from '@angular/core';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { AnalogyService } from '../../references/analogy/analogy.service';
import { Ppo, getPpo } from './ppo';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { ApicontentService } from 'src/app/api.content.service';
import { ObjectBudget } from '../../budget/project-budget/registerBudget';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-ppo',
  templateUrl: './ppo.component.html',
  styleUrls: ['./ppo.component.css']
})
export class PpoComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private analogy:AnalogyService,private ppo:ApicontentService,private formBuilder: FormBuilder) { 
    this.select1 = this.formBuilder.group({
      parent_id: [''], 
    });
    this.select2 = this.formBuilder.group({
      level: [''], 
    });
    this.createPpoForm = this.formBuilder.group({
      quantity: [],
      cost_for_one: [],
      total_cost: [],
      build_plan: [],
      prepaid_previous_year: [],
      equipment_in_stock_start: [],
      prepaid_next_year: [],
      equipment_in_stock_end: [],
      financing: [],
      date_quarter: [''],
      wbs: []
    });
    this.createPpoMainForm = this.formBuilder.group({
      quantity: [],
      cost_for_one: [],
      total_cost: [],
      build_plan: [],
      prepaid_previous_year: [],
      equipment_in_stock_start: [],
      prepaid_next_year: [],
      equipment_in_stock_end: [],
      financing: [],
      date_quarter: [''],
      wbs: []
    });
    this.editPpoForm = this.formBuilder.group({
      quantity: [],
      cost_for_one: [],
      total_cost: [],
      build_plan: [],
      prepaid_previous_year: [],
      equipment_in_stock_start: [],
      prepaid_next_year: [],
      equipment_in_stock_end: [],
      financing: [],
      date_quarter: [''],
    });
  }

  ngOnInit() {
    this.getPpo()
  }

  editPpoForm: FormGroup
  createPpoForm: FormGroup
  createPpoMainForm: FormGroup
  select1: FormGroup
  select2: FormGroup

  getWbsLevel1: any[] = []
  wbsList1!: string;

  getWbsLevel2: any[] = []
  wbsList2!: string;

  element: number

  getPpo() {
    this.ppo.getPpo().subscribe(
      (data: any[]) => {
        this.dataSources.data = data
      }
    )
  }

  addMainPpo() {
    const ppo = this.createPpoMainForm.value;
    this.ppo.addPpo(ppo).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getPpo();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  addPpo() {
    const ppo = this.createPpoForm.value;
    this.ppo.addPpo(ppo).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getPpo();
        // this.closed()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  } 

  editPpo(element:getPpo) {
    this.element = element.id; 
    this.editPpoForm.patchValue({
      quantity: element.quantity,
      cost_for_one: element.cost_for_one,
      total_cost: element.total_cost,
      build_plan: element.build_plan,
      prepaid_previous_year: element.prepaid_previous_year,
      equipment_in_stock_start: element.equipment_in_stock_start,
      prepaid_next_year: element.prepaid_next_year,
      equipment_in_stock_end: element.equipment_in_stock_end,
      financing: element.financing,
      date_quarter: element.date_quarter,
    });
  }

  savePpo() {
    if (this.element) {
      let editedBudget = this.editPpoForm.value;
      this.ppo.updPpo(this.element, editedBudget).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getPpo();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  delPpo(element: any) {
    const id = element.id;
    this.ppo.delPpo(id).subscribe(
      (response: any[]) => {
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getPpo();
      },
    );
  }

  getSelect1() {
    const filter = this.select1.value;
    this.ppo.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        this.getWbsLevel1 = response
      },
    );
  }

  getSelect2() {
    const filter = this.select2.value;
    this.ppo.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        this.getWbsLevel2 = response
      },
    );
  }

  displayedColumnsTime: string[] = ['name', 'quantity', 'cost_for_one', 'total_cost', 'build_plan', 'prepaid_previous_year', 'equipment_in_stock_start', 'prepaid_next_year', 'equipment_in_stock_end', 'financing', 'date_quarter', 'actions'];
  dataSource = new MatTableDataSource<getPpo>([]);
  
  private _transformer = (node: getPpo, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      name: node.name,
      wbs_name: node.wbs_name,
      wbs_code: node.wbs_code,
      wbs: node.wbs,
      quantity: node.quantity,
      cost_for_one: node.cost_for_one,
      total_cost: node.total_cost,
      build_plan: node.build_plan,
      prepaid_previous_year: node.prepaid_previous_year,
      equipment_in_stock_start: node.equipment_in_stock_start,
      prepaid_next_year: node.prepaid_next_year,
      equipment_in_stock_end: node.equipment_in_stock_end,
      financing: node.financing,
      date_quarter: node.date_quarter,
      wbs_pk: node.wbs_pk,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Ppo>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Ppo) => node.expandable;

}
