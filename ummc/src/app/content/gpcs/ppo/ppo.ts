export interface Ppo {
    expandable: boolean;
    name: string;
    level: number;
}
  export interface getPpo {
    id: number,
    expandable: boolean,
    wbs: number,
    quantity: number,
    cost_for_one: number,
    total_cost: number,
    build_plan: number,
    prepaid_previous_year: number,
    equipment_in_stock_start: number,
    prepaid_next_year: number,
    equipment_in_stock_end: number,
    financing: number,
    date_quarter: "5",
    name: string,
    wbs_name: string,
    wbs_code: string,
    parent_id: number,
    level: number,
    wbs_pk: number,
    children?: getPpo[];
}