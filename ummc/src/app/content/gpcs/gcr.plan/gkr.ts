export interface Gkr {
    expandable: boolean;
    name: string;
    level: number;
}
  export interface getGkr {
    id: number,
    expandable: boolean,
    p_m: number,
    cubic_meter: number,
    cost: number,
    cost_total: number,
    name: string,
    code_assoi: string,
    code_wbs: string,
    parent_id: number,
    level: number,
    wbs_pk: number,
    wbs: string,
    children?: getGkr[];
}