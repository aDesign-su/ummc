import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { AnalogyService } from '../../references/analogy/analogy.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { ObjectBudget } from '../../budget/project-budget/registerBudget';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { Gkr, getGkr } from './gkr';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-gcr.plan',
  templateUrl: './gcr.plan.component.html',
  styleUrls: ['./gcr.plan.component.css']
})
export class GcrPlanComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private analogy:AnalogyService,private gkr:ApicontentService,private formBuilder: FormBuilder) {
    this.select1 = this.formBuilder.group({
      parent_id: [''], 
    });
    this.select2 = this.formBuilder.group({
      level: [], 
    });
    this.createGkrForm = this.formBuilder.group({
      p_m: [],
      cubic_meter: [],
      cost: [],
      cost_total: [''],
      wbs: []
    });
    this.createGkrMainForm = this.formBuilder.group({
      p_m: [],
      cubic_meter: [],
      cost: [],
      cost_total: [''],
      wbs: []
    });
    this.editGkrForm = this.formBuilder.group({
      p_m: [],
      cubic_meter: [],
      cost: [],
      cost_total: [''],
    });
    this.totalCost = this.formBuilder.group({
      level: [3], 
    });
   }

  ngOnInit() {
    this.getGkr()
    // this.addTotalCoast()
  }

  getGkr() {
    this.gkr.getGkr().subscribe(
      (data: any[]) => {
        this.orginals.data = data
        this.dataSources.data = data
        const codeWbsValues = data.map(item => item.code_wbs);
        this.codeWbs = codeWbsValues
        this.applyFilters();
        
      }
    )
  }

  codeWbs: any = []
  selectedStatus: any = 'All';
  filteredContracts: any[] = [];

  applyFilters(): void {
    this.filteredContracts = this.dataSources.data.filter(contract => {
      const isStatusMatched = this.selectedStatus === 'All' ? true : contract.code_wbs === this.selectedStatus;
      return (
        isStatusMatched 
      );
    });
  }

  addMainGkr() {
    const gkr = this.createGkrMainForm.value;
    this.gkr.addGkr(gkr).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getGkr();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  total: number
  addTotalCoast() {
    const gkr = this.totalCost.value;
    // console.log(gkr)
    this.gkr.addTotalGkr(gkr).subscribe(
      (response: any) => {
        this.total = response;
        console.log(this.total)
      },
    );
  }

  addGkr() {
    const gkr = this.createGkrForm.value;
    this.gkr.addGkr(gkr).subscribe(
      (response: any[]) => {
        for (let i = 0; i < this.show.showBlock.length; i++) {
          this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
        }
        this.createGkrForm.reset()
        this.theme.access = 'Данные добавлены успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
        this.getGkr();
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.theme.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    );
  }

  editGkr(element:getGkr) {
    this.element = element.id; 
    this.editGkrForm.patchValue({
      p_m: element.p_m,
      cubic_meter: element.cubic_meter,
      cost: element.cost,
      cost_total: element.cost_total,
    });
  }

  saveGkr() {
    if (this.element) {
      let editedBudget = this.editGkrForm.value;
      this.gkr.updGkr(this.element, editedBudget).subscribe(
        (data: any) => {
          for (let i = 0; i < this.show.showBlock.length; i++) {
            this.show.showBlock[i] = false; // Закроем все блоки, устанавливая значение в false
          }
          this.theme.info = 'Данные изменены успешно.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.getGkr();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }
  
  delGkr(element: any) {
    const id = element.id;
    this.gkr.delGkr(id).subscribe(
      (response: any[]) => {
        this.theme.info = 'Элемент удален.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
        this.getGkr();
      },
    );
  }

  // create() {
  //   this.getSelect1()
  // }

  setId: number
  element: number
  select1: FormGroup
  select2: FormGroup
  createGkrMainForm: FormGroup
  createGkrForm: FormGroup
  editGkrForm: FormGroup
  totalCost: FormGroup

  getCurrencyList: any[] = []
  CurrencyList!: string;

  getWbsLevel1: any[] = []
  wbsList1: string
  getWbsLevel2: any[] = []
  wbsList2: string

  getSelect1() {
    const filter = this.select1.value;
    this.gkr.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        this.getWbsLevel1 = response
      },
    );
  }

  getSelect2() {
    const filter = this.select2.value;
    this.gkr.addFilter(filter).subscribe(
      (response: ObjectBudget[]) => {
        this.getWbsLevel2 = response
        console.log(response)
      },
    );
  }

  displayedColumnsTime: string[] = ['serial_number', 'code_wbs', 'code_assoi', 'name', 'p_m', 'cubic_meter', 'cost', 'cost_total', 'actions'];
  displayedColumns: string[] = ['item', '1','2','3','4','5', 'total'];
  dataSource = new MatTableDataSource<getGkr>([]);
  
  private _transformer = (node: getGkr, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      name: node.name,
      code_assoi: node.code_assoi,
      code_wbs: node.code_wbs,
      wbs: node.wbs,
      p_m: node.p_m,
      cubic_meter: node.cubic_meter,
      cost: node.cost,
      cost_total: node.cost_total,
      parent_id: node.parent_id,
      wbs_pk: node.wbs_pk,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<Gkr>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );

  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  orginals = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: Gkr) => node.expandable;

  
}
