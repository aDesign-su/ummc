import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { ProjectRegister } from './project-register';
import {ShowService} from '../../helper/show.service'
import {loadCompilerCliMigrationsModule} from '@angular/core/schematics/utils/load_esm'
import { CommonService } from '../budget/project-budget/common.service';

import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-project-register',
  templateUrl: './project-register.component.html',
  styleUrls: ['./project-register.component.css']
})
export class ProjectRegisterComponent implements OnInit {

  dataProjectsTableSource: MatTableDataSource<ProjectRegister> = new MatTableDataSource<ProjectRegister>();
  displayedColumnsProjects: string[] = ['id', 'code_wbs', 'name', 'action'];

  AddedProject!: FormGroup;
  UpdatedProject!: FormGroup;

  constructor(public theme: ThemeService, public show: ShowService, private formBuilder: FormBuilder, public isTitle: CommonService, public params: ApicontentService, private _snackBar: MatSnackBar) {
    this.AddedProject = formBuilder.group({
      // id: null,
      code_wbs: [null, [Validators.required]],
      name: [null, [Validators.required]],
      user_created: 36
    });
    this.UpdatedProject = formBuilder.group({
      id: null,
      code_wbs: [null, [Validators.required]],
      name: [null, [Validators.required]],
      user_created: 36
    });
  }

  ngOnInit() {
    this.getDataProjects()
  }

  loading: boolean = true;
  project: string

  getAddedFormControl(name: string) {
    return this.AddedProject.get(name);
  }

  getUpdatedFormControl(name: string) {
    return this.UpdatedProject.get(name);
  }

  setWbs(wbs: any) {
    if(wbs) {
      this.project = wbs.name
      setTimeout(() => {
        this.project = null;
      }, 3000);
      let projectId = wbs.id
      projectId = localStorage.setItem('project_id', projectId);
    }
  }

  setUpdatedFields(element: ProjectRegister) {
    for (let field in element) {
      if (field == "code_wbs") {
        this.UpdatedProject.get(field).setValue(element[field].slice(1));
      } else {
        this.UpdatedProject.get(field).setValue(element[field]);
      }
    }
  }

  getDataProjects(): void {
    this.loading=true
    this.params.getProjects().subscribe(
      (data) => {
        console.log(data)
        this.dataProjectsTableSource.data = data;
        this.loading=false
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  addProject(): void {
    this.AddedProject.value.code_wbs = "P" + this.AddedProject.value.code_wbs
    this.params.addProject(this.AddedProject.value).subscribe(
      (data) => {
        console.log(data);
        this.closedAdd();
        this.getDataProjects();
        this.theme.openSnackBar('Проект добавлен');
        this.theme.access = 'Проект добавлен успешно.'
        setTimeout(() => {
          this.theme.access = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось добавить проект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  updateProject(): void {
    this.UpdatedProject.value.code_wbs = "P" + this.UpdatedProject.value.code_wbs
    this.params.editProject(this.UpdatedProject.value).subscribe(
      (data) => {
        console.log(data);
        this.closedAdd();
        this.getDataProjects();
        this.theme.openSnackBar('Проект обновлен');
        this.theme.info = 'Проект изменен успешно.'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось обновить проект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  removeProject(id: number): void {
    this.params.removeProject(id).subscribe(
      (data) => {
        console.log(data);
        this.getDataProjects();
        this.theme.openSnackBar('Проект удален');
        this.theme.info = 'Проект удален'
        setTimeout(() => {
          this.theme.info = null;
        }, 5000);
      }, error => {
        this.theme.error = 'Не удалось удалить проект. Ошибка #400 Bad Request.'
        setTimeout(() => {
          this.theme.error = null;
        }, 5000);
      }
    )
  }

  closedAdd() {
    for (let i = 0; i < this.show.showBlock.length; i++) {
      this.show.showBlock[i] = false; // Закрываем все блоки, устанавливая значение в false
    }
  }
}
