import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { Project, KeyProjectValue } from './params';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import * as jmespath from 'jmespath';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { factDetails } from '../../contract/register-contracts/reg-contracts';
import { SortEvent } from 'primeng/api';
import { Observable, combineLatest, concat, defer, map, of } from 'rxjs';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
declare var bootstrap: any;


@Component({
  selector: 'app-params-project',
  templateUrl: './params-project.component.html',
  styleUrls: ['./params-project.component.css',],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ParamsProjectComponent implements OnInit {
  expandedElements: any[] = [];
  dataProjectsParamsTable: MatTableDataSource<Project> = new MatTableDataSource<Project>();
  columnsToDisplay = ['name', "project_manager", "created", "activ", "phase", "level"];//Главная таблица
  columnsToDisplayWithExpand = ['expand', ...this.columnsToDisplay, 'action'];//Главная таблица/Стрелка для раскрытия



  nestedDisplayedColumns: string[] = ['select', 'serial_number', 'title', 'budget', 'priority', 'category'];

  displayedColumns: string[] = ['title', 'budget', 'IRR', 'WACC', 'NPV', 'priority', 'category'];
  tableSelectionKeyProjectValues = new MatTableDataSource<any>([]);
  selectionKeyProjectValues = new SelectionModel<any>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  projectValueCreate: FormGroup
  selectedProjectDetails: any;
  ProjectList: any[] = []
  setProject: Project[] = []
  NameProj = ''
  @ViewChild(MatPaginator) paginator: MatPaginator;

  editPortfolioForm: FormGroup;
  portfolioId: number | null = null;

  constructor(public theme: ThemeService, public params: ApicontentService, private formBuilder: FormBuilder, private _adapter: DateAdapter<any>, @Inject(MAT_DATE_LOCALE) private _locale: string) {
    this.projectValueCreate = this.formBuilder.group({
      project: null,
      title: "",
      budget: null,
      comment_budget: "",
      IRR: null,
      comment_IRR: "",
      WACC: null,
      effective_plan_date: null,
      comment_effective_plan_date: "",
      production_volume: null,
      comment_production_volume: "",
      NPV: null,
      comment_NPV: "",
      priority: "",
      comment_priority: "",
      category: "",
      comment_category: ""
    });
  }

  ngAfterViewInit() {
    // Применение сортировки к источнику данных после инициализации компонента
    this.tableSelectionKeyProjectValues.sort = this.sort;
  }

  ngOnInit() {
    this.getParamsProj()
    this.initializeForm();
  }

  private initializeForm(): void {
    this.editPortfolioForm = this.formBuilder.group({
      name: [''],
      activ: [''],
      phase: [''],
      level: [''],
      project_manager: [''],
      effective_plan: [''],
      created: [''],
    });
  }
  editPortfolio(data: Project) {
    console.log(data)
    this.portfolioId = data.id;
    this.editPortfolioForm.patchValue({
      name: data.name,
      activ: data.activ,
      phase: data.phase,
      level: data.level,
      project_manager: data.project_manager,
      effective_plan: data.effective_plan,
      created: new Date(data.created)
    });
  }

  
  private closeForm() {
    const offcanvasElement = document.getElementById('editPortfolio');
    const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    if (offcanvasInstance) {
      offcanvasInstance.hide();
    }
  }

  updatePortfolioProject(): void {
    if (this.editPortfolioForm.valid) {
      const formData = this.editPortfolioForm.value;
      this.params.updatePortfolioProject(this.portfolioId,formData).subscribe(
        (data: Project[]) => {
          console.log(data)
          this.getParamsProj()
          this.closeForm()
          this.theme.openSnackBar('Портфель обновлен');
          this.selectionKeyProjectValues.clear();
        },
        (error: any) => {
          console.error('Error fetching data:', error);
          this.theme.openSnackBar('Ошибка сервера');
        }
      );
    } else {
      console.log('Form is invalid!');
    }
  }



  getParamsProj(): void {
    this.params.getDataParamProj().subscribe(
      (data: Project[]) => {
        console.log(data)
        this.dataProjectsParamsTable.data = data;
        const name = "[*].{id: id, name: name}";
        this.ProjectList = jmespath.search(data, name);
        console.log(this.ProjectList)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }


  //*Раскрывается табличка с табличкой
  toggleExpand(element: any): void {
    const index = this.expandedElements.indexOf(element);
    if (index === -1) {
      this.expandedElements.push(element);
    } else {
      this.expandedElements.splice(index, 1);
    }
  }

  // Кнопка в таблице "подробнее" (Вывод по id)
  showProjDetails(id: number): void {
    this.params.getDataParamProj().subscribe(
      (data: Project[]) => {

        const project = data.find(proj => proj.id === id);
        console.log('proj', project)
        this.NameProj = project.name
        if (project) {
          this.selectedProjectDetails = project.keyprojectvalues;
          console.log('Key Project Values:', this.selectedProjectDetails);
        } else {
          console.log('Project not found');
        }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }



  // Обновление
  updateProj(proj: KeyProjectValue) {
    // Создаем новый объект на основе proj, но с преобразованием даты в строку
    const updatedProj = {
      ...proj,
      effective_plan_date: moment(proj.effective_plan_date).format('YYYY-MM-DD')
    }

    this.params.updateKeyProjectList(updatedProj.id, updatedProj).subscribe(
      (response) => {
        console.log('Proj update :', response);
        this.getParamsProj();
        // addScrollingLabel
      },
      (error: any) => {
        console.error('Failed to update project:', error);
      }
    )
  }
  // Удаление
  deleteProj(id: number) {
    console.log("Удалил " + id)
    this.params.deleteKeyProjectList(id).subscribe(
      (response) => {
        console.log('Proj deleted :', response);
        this.getParamsProj();
        // Фильтрация selectedProjectDetails для удаления карточки с заданным id
        this.selectedProjectDetails = this.selectedProjectDetails.filter(proj => proj.id !== id);
      },
      (error: any) => {
        console.error('Failed to delete payment:', error);
      }
    )
  }


  addKeyProjectList(): void {
    const addProject = this.projectValueCreate.value as any;
    // Format the date as YYYY-MM-DD
    const date = moment(addProject.effective_plan_date).format('YYYY-MM-DD');
    // Update the addProject object with the formatted date
    addProject.effective_plan_date = date;
    this.params.addKeyProjectList(addProject).subscribe(
      (data: Project[]) => {
        this.setProject = data;
        this.projectValueCreate.reset();

        const offcanvasElement = document.getElementById('addScrolling');
        const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
        if (offcanvasInstance) {
          offcanvasInstance.hide();
        }
        this.theme.openSnackBar('Карточка проекта добавлена');
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  toggleSelection(row: any) {
    this.selectionKeyProjectValues.toggle(row);
    console.log('Selected cards:', this.selectionKeyProjectValues.selected);
    this.tableSelectionKeyProjectValues.data = this.selectionKeyProjectValues.selected;
  }
  checkboxLabel(row?: any): string {
    return `${this.selectionKeyProjectValues.isSelected(row) ? 'deselect' : 'select'} row`;
  }
}
