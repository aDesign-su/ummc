export interface Project {
  active:boolean
  id: number;
  name: string;
  activ: string;
  phase: string;
  level: string;
  project_manager: string;
  effective_plan: number;
  created: string;
  keyprojectvalues: KeyProjectValue[];
}

export interface KeyProjectValue {
  id: number;
  title: string;
  budget: number;
  comment_budget: string | null;
  IRR: number;
  comment_IRR: string;
  WACC: number;
  effective_plan_date: Date;
  comment_effective_plan_date: string;
  production_volume: number;
  comment_production_volume: string;
  production_volume_year: number;
  NPV: number;
  comment_NPV: string;
  priority: string;
  comment_priority: string | null;
  category: string;
  comment_category: string | null;
  created: string;
  editing:boolean;
}