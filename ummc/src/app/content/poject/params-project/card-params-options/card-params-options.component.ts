import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Project, KeyProjectValue } from '../params';

@Component({
  selector: 'app-card-params-options',
  templateUrl: './card-params-options.component.html',
  styleUrls: ['./card-params-options.component.css']
})
export class CardParamsOptionsComponent implements OnInit {

  @Input() details: KeyProjectValue;
  @Output() deleteEvent = new EventEmitter<number>();
  @Output() saveEvent = new EventEmitter<KeyProjectValue>();

  deleteCard() {
    this.deleteEvent.emit(this.details.id);
    console.log(this.details)
  }

  editCardPackage(card: any) {
    card.editing = true;
  }

  saveCardPackage(card: KeyProjectValue) {
    card.editing = false;
    this.saveEvent.emit(card);
  }



  constructor() {

  }

  ngOnInit(): void {
    console.log(this.details)
  }

}
