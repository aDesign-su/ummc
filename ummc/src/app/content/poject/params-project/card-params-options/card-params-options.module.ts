import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardParamsOptionsComponent } from './card-params-options.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  imports: [
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule, FormsModule, MatInputModule, MatDatepickerModule,
    MatNativeDateModule,

  ],
  declarations: [CardParamsOptionsComponent],
  exports: [
    CardParamsOptionsComponent
  ]
})
export class CardParamsOptionsModule { }
