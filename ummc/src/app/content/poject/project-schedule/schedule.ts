export interface GanttObject {
  pID: number,
  pName: string,
  pStart: string,
  pEnd: string,
  pClass: string,
  pLink: string,
  pMile: number,
  pRes: string,
  pComp: number,
  pGroup: number,
  pParent: number,
  pOpen: number,
  pDepend: string,
  pCaption: string,
  pNotes: string,
  level: number,
  code_wbs: string,
  childrens: ObjectBudget[]
}
export interface ObjectBudget {
  id: number,
  name: string,
  parent_id: number,
  delete_flg: boolean,
  node_parent: number,
  code_wbs: string,
  part_code_wbs: string,
  ancestor_code_wbs: string,
  code_assoi: string,
  code_sap: string,
  category_id: number,
  common_node_id: number,
  level: number,
  wbs_id: number,
  budget_source_id: number,
  register_budget_id: number,
  user_created_id: number,
  date_start: string,
  date_end: string,
  comment: string,
  total_price: number,
  smr_price: number,
  pir_price: number,
  oto_price: number,
  other_price: number,
  problem: string,
  auto: boolean,
  common_budget_id: number,
  budget_source_name: string,
  children: ObjectBudget[]
}
export interface ProjectRegister {
  id: number;
  code_wbs: string;
  name: string;
  user_created: number;
}
export interface Budget {
  id: number,
  user_created_id: number,
  name: string,
  is_base: boolean,
  is_current: boolean,
  comment: string,
  delete_flg: boolean
}