import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexTitleSubtitle } from "ng-apexcharts";
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ObjectBudget, GanttObject, ProjectRegister, Budget } from './schedule';
import * as Highcharts from 'highcharts';
import HighchartsGantt from 'highcharts/modules/gantt';
import { DateRange } from '@angular/material/datepicker';
import {GanttEditorComponent, GanttEditorOptions} from 'ng-gantt';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { filter } from 'rxjs';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  public editorOptions: GanttEditorOptions;

  budgetsSource: Budget[] = [];
  baseBudgets: number[] = [];
  currentBudgets: number[] = [];

  projectsSource: ProjectRegister[] = [];
  directionsSource = [];

  dataSourceRaw: ObjectBudget[] = [];
  dataSource = new Map();
  dataSourceArray = [];

  selectedProjects = new FormControl('');
  selectedDirections = new FormControl('');

  financingFactData: any = [];
  financingPlanData: any = [];
  utilizationFactData: any = [];
  utilizationPlanData: any = [];

  selectedData = {
    plan: this.financingPlanData,
    fact: this.financingFactData
  }

  @ViewChild("editor") editor: GanttEditorComponent;

  loading: boolean = true;

  loadingActiv() {
    this.loading = true;
  }

  loadingDeactiv() {
    this.loading = false;
  }

  scheduleLevels = ['Проект', 'Направление', 'Объект', 'Система', 'Пакет работ или смета'];
  scheduleColors = {0: "ggroupblack", 1: "gtaskblue", 2: "gtaskred", 3: "gtaskyellow", 4: "gtaskgreen"};

  constructor(public theme: ThemeService, public show: ShowService, private contentService: ApicontentService, private ganttService: ApicontentService) {
  }

  async ngOnInit() {
    Promise.all([
      this.getBudget(), 
      this.getProjectsList(),
      this.getUtilizationPlan(),
      this.getUtilizationFact(),
      this.getFinancingFact(),
      this.getFinancingPlan()
    ]).then(done => {
      this.getBudgetProjects();
    });
  }

  async startLoadingPromise() {
    let promise = new Promise((resolve, reject) => {
      this.loadingActiv();
      this.show.closedAdd(0);
      setTimeout(() => {
        resolve("result");
      }, 1000);
    })

    await promise;
  }

  getBudgetProjects(budget: number[] = this.currentBudgets): void {
    budget.forEach(budgetId => {
      this.ganttService.getObjectBudgetGeneral(budgetId,false,false).subscribe(
        (data: any[]) => {
          console.log(data)
          this.dataSourceRaw = data['data'];
          this.dataSource = this.convertDataToGanttObject(this.dataSourceRaw);
          this.projectsSource = this.handleProjectFilters(this.projectsSource);
          this.dataSourceArray = this.convertDataSourceToArray(this.dataSource.values());
          this.setGanttStyle();
          this.loadingDeactiv();
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    })
  }

  private handleProjectFilters(filters) {
    let existFilters = [];

    filters.forEach(filter => {
      if (this.dataSource.has(filter.code_wbs)) {
        existFilters.push(filter);
      }
    })

    return existFilters;
  }

  private convertDataSourceToArray(data: any) {
    return Array.from(data);
  }

  setGanttStyle() {
    this.editorOptions = {
      vFormat: 'month',
      vEditable: false,
      vLang: 'ru',
    };
  }

  //! Для ускорения поиска используется Map с ключом - сдр кодом, 
  //! но в списке проектов они повторяются, что ломает логику метода.
  //! Уточнить, будут ли повторяться коды сдр и будет ли синхронизация между проектами 
  //! из get_projects и budget/get_common_object
  convertDataToGanttObject(data: ObjectBudget[], planData = this.financingPlanData, factData = this.financingFactData, toggleLevel: number = 1) {
    let parentObject = new Map();
    let filteredProjectSource = [];

    data.forEach(wbs => {
      parentObject.set(wbs.code_wbs ,{
        pID: wbs.id,
        pName: wbs.code_wbs + " " + wbs.code_assoi + " " + wbs.name,
        pStart: wbs.date_start,
        pEnd: wbs.date_end,
        pClass: this.scheduleColors[wbs.level],
        pLink: "",
        pMile: 0,
        pRes: "",
        pComp: this.calculatePercent(wbs, planData, factData),
        pGroup: wbs.children.length > 0 ? 1 : 0,
        pParent: wbs.level == 0 ? 0 : wbs.parent_id,
        pOpen: wbs.level < toggleLevel ? 1 : 0,
        pDepend: "",
        pCaption: "",
        pNotes: "",
        level: wbs.level,
        code_wbs: wbs.code_wbs,
        childrens: wbs.children
      });

      parentObject = new Map([...parentObject].concat([...this.convertDataToGanttObject(wbs.children)]));
    })

    return parentObject;
  }

  calculatePercent(wbs: ObjectBudget, planData = this.financingPlanData, factData = this.financingFactData) {
    const factDataAmount = this.getAmount(factData, wbs);
    const planDataAmount = this.getAmount(planData, wbs);

    if (factDataAmount !== 0 && planDataAmount !== 0) {
      return factDataAmount / planDataAmount * 100;
    } else {
      return 0;
    }
  }

  getAmount(data: any[], wbs: ObjectBudget) {
    for (const element of data) {
      if (wbs.code_wbs == element['code_wbs']) {
        return element['amount'];
      }
    }
    return 0;
  }

  distributeBudget() {
    this.budgetsSource.forEach(budget => {
      if (budget.is_base) {
        this.baseBudgets.push(budget.id);
      }
      if (budget.is_current) {
        this.currentBudgets.push(budget.id);
      }
    });
  }

  changeBudget(budget: number[]) {
    this.startLoadingPromise().then(result => {
      this.getBudgetProjects(budget);
    });
  }

  async getBudget() {
    return new Promise((resolve, reject) => {
      this.contentService.getRegisterBudget().subscribe(
        (data: any) => {
          this.budgetsSource = data['data'];
          this.distributeBudget();
          console.log(data);
          resolve("done");
        }, error => {
          console.log(error);
        }
      )
    })
  }

  async getProjectsList() {
    return new Promise((resolve, reject) => {
    this.contentService.getProjects().subscribe(
      (data: any) => {
        console.log(data);
        this.projectsSource = data;
        resolve("done");
      }, error => {
        console.log(error)
      }
    )
    })
  }

  async getFinancingPlan() {
    return new Promise((resolve, reject) => {
    this.contentService.getPlan().subscribe(
      (data: any) => {
        console.log(data)
        this.financingPlanData = data;
        resolve("done")
      }, error => {
        console.log(error)
      }
    )})
  }

  async getFinancingFact() {
    return new Promise((resolve, reject) => {
    this.contentService.getFinancingFact().subscribe(
      (data: any) => {
        console.log(data)
        this.financingFactData = data;
        resolve("done");
      }, error => {
        console.log(error)
      }
    )})
  }

  async getUtilizationPlan() {
    return new Promise((resolve, reject) => {
    this.contentService.getUtilizationPlan().subscribe(
      (data: any) => {
        console.log(data)
        this.utilizationPlanData = data;
        resolve("done");
      }, error => {
        console.log(error)
      }
    )})
  }

  async getUtilizationFact() {
    return new Promise((resolve, reject) => {
    this.contentService.getUtilizationFact().subscribe(
      (data: any) => {
        console.log(data)
        this.utilizationFactData = data;
        resolve("done");
      }, error => {
        console.log(error)
      }
    )})
  }

  setWbsCodeFilters() {
    this.startLoadingPromise().then(result => {
      let iterateOverChildren = (object: GanttObject) => {
        for (let child of object.childrens) {
          let findedChild = this.dataSource.get(child.code_wbs);
          if (findedChild.level !== 1 || findedChild.level === 1 && includedDirections.includes(findedChild.code_wbs)) {
            filtered.set(findedChild.code_wbs, findedChild);
            iterateOverChildren(findedChild);
          }
        }
      }
  
      let filtered = new Map();
      let includedDirections: any[];

      if (this.selectedDirections.value.length === 0) {
        const directionsCodesWbs = this.directionsSource.map(direction => direction.code_wbs);
        this.collectDirectionsBySelectedProjects(directionsCodesWbs);
        includedDirections = directionsCodesWbs;
      } else {
        includedDirections = Array.from(this.selectedDirections.value);
      }
      
  
      for (let code of this.selectedProjects.value) {
        filtered.set(code, this.dataSource.get(code));
        iterateOverChildren(this.dataSource.get(code));
      }
      
      this.dataSourceArray = this.convertDataSourceToArray(filtered.values());
      this.setGanttStyle();
      this.loadingDeactiv();
    })
  }

  resetWbsCodeFilters() {
    this.startLoadingPromise().then(result => {
      this.dataSourceArray = this.convertDataSourceToArray(this.dataSource.values());
      this.setGanttStyle();
      this.loadingDeactiv();
      this.selectedProjects.reset();
      this.selectedDirections.reset();
    });
  }

  collectDirectionsBySelectedProjects(projectsCodes: any) {
    this.directionsSource = [];
    const projectsCodesArray = Array.from(projectsCodes);

    for (const projectCode of projectsCodesArray) {
      const projectObject = this.dataSource.get(projectCode);
      projectObject.childrens.forEach(child => this.directionsSource.push(child));
    }
  }

  toggleToLevel(level) {
    this.startLoadingPromise().then(result => {
      this.dataSourceArray = this.changeGanttObjectsToggle(this.dataSourceArray, this.selectedData.plan, this.selectedData.fact, level);
      this.loading = false;
    });
  }

  changeGanttObjectsToggle(data, planData = this.financingPlanData, factData = this.financingFactData, toggleLevel: number = 1) {
    let parentObject = [];
    data.forEach(object => {
      parentObject.push({
        pID: object.pID,
        pName: object.pName,
        pStart: object.pStart,
        pEnd: object.pEnd,
        pClass: object.pClass,
        pLink: object.pLink,
        pMile: object.pMile,
        pRes: object.pRes,
        pComp: object.pComp,
        pGroup: object.pGroup,
        pParent: object.pParent,
        pOpen: object.level < toggleLevel ? 1 : 0,
        pDepend: object.pDepend,
        pCaption: object.pCaption,
        pNotes: object.pNotes,
        level: object.level,
        code_wbs: object.code_wbs,
        childrens: object.childrens
      });
    })
    return parentObject;
  }

  setSelectedData(planData, factData) {
    this.startLoadingPromise().then(result => {
      this.selectedData.plan = planData;
      this.selectedData.fact = factData;
      this.convertDataToGanttObject(this.dataSourceRaw, planData, factData);
      this.loadingDeactiv();
    })
  }

}
