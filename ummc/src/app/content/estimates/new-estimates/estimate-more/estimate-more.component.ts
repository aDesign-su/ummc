import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, map, startWith } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ShowService } from 'src/app/helper/show.service';
import { CommonService } from 'src/app/content/budget/project-budget/common.service';
import { EstimateMore } from './estimate-more';

@Component({
  selector: 'app-estimate-more',
  templateUrl: './estimate-more.component.html',
  styleUrls: ['./estimate-more.component.css']
})
export class EstimateMoreComponent implements OnInit {
  isLoading: boolean;
  DataEstimateMoreItemDocument= this.theme.initTable<EstimateMore>(); 
  DataEstimateMoreItemProperties= this.theme.initTable<EstimateMore>(); 
  DataEstimateMoreItemVariables = this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemGsDocSignatures = this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemRegionInfo = this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemIndexespo= this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemAddzatrat= this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemVidrabCatalog= this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemVidrabGroup= this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemVidRab=this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemMat=this.theme.initTable<EstimateMore>();
  DataEstimateMoreItemMch=this.theme.initTable<EstimateMore>();
  constructor(private activeteRoute: ActivatedRoute, public isTitle: CommonService, public theme: ThemeService, public show: ShowService, private router: Router, private fb: FormBuilder, public params: ApicontentService, private dialog: MatDialog, private _snackBar: MatSnackBar, private datePipe: DatePipe) {
    this.requestId = this.activeteRoute.snapshot.params['id'];
    this.documentGsfxForm = this.fb.group({
      wbs_system: [''],
      wbs_work_package: [''],
      document_gsfx: [''],
      title: ['', Validators.required],
    })
    this.getNodeForm = this.fb.group({
      level: [0]
    })
  }
  getNodeForm!: FormGroup
  documentGsfxForm!: FormGroup

  getNodeFlat: any[] = []

  directionOptionId: any;
  objectOptionId: any;
  systemOptionId: any;
  workPackagesOptionId: any;
  documentGsfx

  getNode(gsfx: any) {
    this.documentGsfx = gsfx.document_gsfx
    const object = this.getNodeForm.value
    this.params.getSvzWbsByLevelFilter(object).subscribe(
      (response: any[]) => {
        this.getNodeFlat = response
      },
    )}
    directionOptionSelected(event: any) {
      const object = event.value
      this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
        (response: any[]) => {
          this.directionOptionId = response
      },
    )}
    objectOptionSelected(event: any) {
      const object = event.value
      this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
        (response: any[]) => {
          this.objectOptionId = response
      },
    )}
    systemOptionSelected(event: any) {
      const object = event.value
      this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
        (response: any[]) => {
          this.systemOptionId = response
      },
    )}
    workPackagesOptionSelected(event: any) {
      const object = event.value
      this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
        (response: any[]) => {
          this.workPackagesOptionId = response
      },
    )}
    addDocumentGsfx() {
      const object = this.documentGsfxForm.value
      this.params.addDocumentGsfx(object).subscribe(
        (response: any[]) => {
          this.show.showBlock[1] = false
          console.log(response)
      },
    )}

  requestId
  getID: any
  header: any[] = []
  data: any[] = []
  resource: any[] = []

  ngOnInit() {
    this.getDataSouce()
    // this.isLoading = true;
    // if (requestId) {
    //   console.log(requestId)
    //   this.getBDataEstimateMoreItemItem(requestId)
    // }
  }
  isCollapseArray: boolean[] = [];
  // isCollapse: boolean = false
  onShowSource(id: number) {
    if (id) {
      // console.log(id);
      // Получить индекс элемента в массиве по его id
      const index = this.data.findIndex(item => item.id === id);
      if (index !== -1) {
        // Изменить состояние isCollapse для соответствующего элемента
        this.isCollapseArray[index] = !this.isCollapseArray[index];
      }
    }
  }
  getDataSouce() {
    this.params.getGsfxData(this.requestId).subscribe(
      (data: any) => {
        console.log(data)
        this.header = data.header_chapters
        this.data = data.chapters
        this.resource = this.data.map((chapter: any) => chapter.resources);
        console.log('resources', this.resource);
        // this.resource = data.chapters.resources

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getBDataEstimateMoreItemItem(id): void {
    this.isLoading = true;
    console.log(id)
    this.params.getDataEstimateMoreItem(id).subscribe(
      (data: any) => {
        
        this.DataEstimateMoreItemDocument.data = Object.values(data.document)
        this.DataEstimateMoreItemProperties.data = Object.values(data.properties)
        this.DataEstimateMoreItemVariables.data = Object.values(data.variables);
        this.DataEstimateMoreItemGsDocSignatures.data = Object.values(data.gsdocsignatures);
        this.DataEstimateMoreItemRegionInfo.data = Object.values(data.regioninfo);
        this.DataEstimateMoreItemIndexespo.data = Object.values(data.indexespos);
        this.DataEstimateMoreItemAddzatrat.data= Object.values(data.addzatrats);
        this.DataEstimateMoreItemVidrabCatalog.data= Object.values(data.vidrab_catalog);
        this.DataEstimateMoreItemVidrabGroup.data= Object.values(data.vidrab_group);
        this.DataEstimateMoreItemVidRab.data= Object.values(data.vid_rab);
        this.DataEstimateMoreItemMat.data= Object.values(data.mat);
        this.DataEstimateMoreItemMch.data= Object.values(data.mch);
        this.isLoading = false;
        // console.log(this.DataEstimateMoreItem.data)
        console.log(this.isLoading)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

}
