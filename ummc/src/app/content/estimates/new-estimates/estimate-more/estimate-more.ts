export interface EstimateMore {
    document: Document[]
    properties: Property[]
    variables: any[]
    gsdocsignatures: Gsdocsignature[]
    regioninfo: Regioninfo[]
    indexespos: Indexespo[]
    addzatrats: Addzatrat[]
    vidrab_catalog: VidrabCatalog[]
    vidrab_group: VidrabGroup[]
    vid_rab: VidRab[]
    chapters: any[]
    workslist: any[]
    resources: any[]
    mat: Mat[]
    mch: Mch[]
  }
  
  export interface Document {
    id: number
    guid: string
  }
  
  export interface Property {
    id: number
    document_id: number
    locnum: string
    constr: string
    stage: string
    description: string
  }
  
  export interface Gsdocsignature {
    id: number
    document_id: number
    caption: string
    value: string
  }
  
  export interface Regioninfo {
    id: number
    document_id: number
    region_name: string
  }
  
  export interface Indexespo {
    id: number
    document_id: number
    caption: string
    code: string
    oz: string
    em: string
    zm: string
    mt: string
  }
  
  export interface Addzatrat {
    id: number
    document_id: number
    glava: string
    caption: string
    value: string
    options: string
  }
  
  export interface VidrabCatalog {
    id: number
    document_id: number
    type: string
    cat_file: string
  }
  
  export interface VidrabGroup {
    id: number
    vidrab_catalog_id: number
    caption: string
  }
  
  export interface VidRab {
    id: number
    vidrab_group_id: number
    caption: string
    os_column: string
    category: string
    nr_code: string
    sp_code: string
    nacl: string
    plan: string
    nacl_curr: string
    plan_curr: string
    nacl_mask: string
    plan_mask: string
  }
  
  export interface Mat {
    id: number
    resources_id: number
    caption: string
    code: string
    units: string
    quantity: string
    value: string
  }
  
  export interface Mch {
    id: number
    resources_id: number
    caption: string
    code: string
    units: string
    quantity: string
  }
  