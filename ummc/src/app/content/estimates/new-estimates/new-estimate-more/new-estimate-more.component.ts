import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ApicontentService } from 'src/app/api.content.service';
import { CommonService } from 'src/app/content/budget/project-budget/common.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { GRANDEstimate } from '../estimate';
import { map, of } from 'rxjs';
interface MetaTitles {
  [key: string]: string;
}

interface EstimateWorkType {
  id: number;
  estimate_document_id: number;
  delete_flag: boolean;
  value: number;
  parent_id: number | null;
  estimate_name_cost_id: number;
  work_type_id: number;
  lft: number;
  rght: number;
  tree_id: number;
  level: number;
  estimate_name_cost_title: string;
  work_type_title: string;
}

interface WorkTypeCost {
  [key: string]: {
    cost: string;
    value: number;
  };
}

interface VidRab {
  [key: string]: {
    work_type_title: string;
    costs: WorkTypeCost;
  };
}

interface ApiResponse {
  _links: any; // Определите более конкретный тип если необходимо
  data: EstimateWorkType[];
  vid_rab: VidRab;
  _meta: {
    titles: {
      [key: string]: string;
    };
  };
}

@Component({
  selector: 'app-new-estimate-more',
  templateUrl: './new-estimate-more.component.html',
  styleUrls: ['./new-estimate-more.component.css']
})


export class NewEstimateMoreComponent implements OnInit {
  dataRegisterEstimatesGSFX: MatTableDataSource<GRANDEstimate> = new MatTableDataSource<GRANDEstimate>();
  // dataSource:MatTableDataSource<any> = new MatTableDataSource<any>();
  // displayedColumnsGSFX: string[] = ['index', 'constr', 'stage', 'description', 'commonHeader', 'caption', 'value', 'value2', 'value3', 'value4', 'value5', 'value6', 'action'];
  public displayedColumns: string[] = ['index', 'work_type_title']; // Начальные столбцы
  public dataSource = new MatTableDataSource<any>();
  public metaTitles: any; // Для хранения заголовков столбцов
  public totals: any = {}; // Для хранения итоговых значений



  constructor(public theme: ThemeService, private fb: FormBuilder, private route: ActivatedRoute, public show: ShowService, public isTitle: CommonService, private router: Router, public params: ApicontentService, private datePipe: DatePipe, private snackBar: MatSnackBar) {

  }
  requestId: any

  ngOnInit() {
    this.requestId = this.route.snapshot.paramMap.get('id');
    if (this.requestId) {
      console.log(this.requestId)
      this.getDataEstimateWorkTypesItem(this.requestId)
    }
    this.getDataSouce()
  }

  isCollapseArray: boolean[] = [];
  header: any[] = []
  data: any[] = []
  resource: any[] = []
  worklist: any[] = []

  onShowSource(id: number) {
    if (id) {
      // console.log(id);
      // Получить индекс элемента в массиве по его id
      const index = this.data.findIndex(item => item.id === id);
      if (index !== -1) {
        // Изменить состояние isCollapse для соответствующего элемента
        this.isCollapseArray[index] = !this.isCollapseArray[index];
      }
    }
  }
  getDataSouce() {
    this.params.getGsfxData(this.requestId).subscribe(
      (data: any) => {
        console.log(data)
        this.header = data.header_chapters
        this.data = data.chapters
        this.resource = this.data.map((chapter: any) => chapter.resources);
        this.worklist = this.data.map((chapter: any) => chapter.worklist);
        console.log('worklist', this.worklist);
        // this.resource = data.chapters.resources

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  getDataEstimateWorkTypesItem(id): void {
    this.params.getDataEstimateWorkTypesItem(id).subscribe(
      (data: any) => {
        
        this.metaTitles = data._meta.titles;

        // Добавляем столбцы для каждого типа затрат
        this.displayedColumns = this.displayedColumns.concat(Object.keys(this.metaTitles).map(key => `cost_${key}`));

        // Формируем данные для каждой строки
        const rows = Object.values(data.vid_rab).map((workType: any, index) => {
          const row: any = {
            index: index + 1,
            work_type_title: workType.work_type_title
          };

          // Добавляем стоимости для каждого типа затрат
          Object.keys(workType.costs).forEach(costKey => {
            row[`cost_${costKey}`] = workType.costs[costKey].value;
          });

          return row;
        });
        this.calculateTotals(rows);
        this.dataSource.data = rows;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }



  private calculateTotals(rows: any[]): void {
    // Инициализируем итоговые значения нулями
    this.displayedColumns.slice(2).forEach(column => this.totals[column] = 0);

    // Суммируем значения по каждому столбцу
    rows.forEach(row => {
      this.displayedColumns.slice(2).forEach(column => {
        this.totals[column] += row[column] || 0;
      });
    });
    this.dataSource.data.push({
      work_type_title: 'Итого',
      ...this.totals
    });
  }




}
