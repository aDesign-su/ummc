import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../budget/project-budget/common.service';
import { GRANDEstimate } from './estimate';
import { HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { RegisterEstimates } from '../register-estimates/register-estimates';
import { ShowService } from 'src/app/helper/show.service';
declare var bootstrap: any;

@Component({
  selector: 'app-new-estimates',
  templateUrl: './new-estimates.component.html',
  styleUrls: ['./new-estimates.component.css']
})
export class NewEstimatesComponent implements OnInit {
  dataRegisterEstimatesGSFX: MatTableDataSource<GRANDEstimate> = new MatTableDataSource<GRANDEstimate>();
  displayedColumnsGSFX: string[] = ['index', 'file_name', 'date_created', 'action'];

  editMode: boolean
  selectedtEstimat: number;

  FormAddEditEstimates: FormGroup;
  wbsData: any[] = [];
  PackageWBSData: any[] = [];
  StatusEstimateData: any[] = [];
  TypeEstimateData: any[] = [];
  CodeEstimateData: any[] = [];
  UserEstimateData: any[] = [];
  ReasonEstimateData: any[] = [];
  LevelEstimateData: any[] = [];


  files: File[] = [];
  uploadedFiles: File[] = [];
  progress: number = 0;
  uploading: boolean = false;
  uploadedFilesCount: number = 0;

  constructor(public theme: ThemeService, private fb: FormBuilder, public show: ShowService, public isTitle: CommonService, private router: Router, public params: ApicontentService, private datePipe: DatePipe, private snackBar: MatSnackBar) {
    this.documentGsfxForm = this.fb.group({
      wbs_system: [''],
      wbs_work_package: [''],
      document_gsfx: [''],
      title: ['', Validators.required],
    })
    this.estimateForm = this.fb.group({
      parrent: [],
      cypher: ['', Validators.required],
      title: ['', Validators.required],
    })
    this.getNodeForm = this.fb.group({
      level: [3],
      project_id: [localStorage.getItem('project_id')]
    })
    this.getParentIdForm = this.fb.group({
      parent_id: []
    })

    this.FormAddEditEstimates = this.fb.group({
      cypher: [''],
      title: [''],
      date_created: [''],
      wbs_system: new FormControl(''),
      wbs_work_package: [''],
      status: [''],
      type_estimate: [''],
      code: [''],
      contractor: [''],
      developer: [''],
      estimate_reason: [''],
      estimate_level: [''],

    });
  }

  ngOnInit() {
    this.getDataGSFXEstimate()
    this.registerEstimate()
    this.getWbs()
    this.getStatusEstimate()
    this.getTypeEstimate()
    this.getCodeEstimate()
    this.getUserEstimate()
    this.getReasonEstimate()
    this.getLevelEstimate()
    this.registerEstimateIsBufer()
  }
  estimateForm!: FormGroup
  documentGsfxForm!: FormGroup
  getNodeForm!: FormGroup
  getParentIdForm!: FormGroup

  getRegisterEstimate: any[] = []
  getNodeFlat: any[] = []
  directionOptionId: any;
  objectOptionId: any;
  systemOptionId: any;
  workPackagesOptionId: any;
  documentGsfx
  section: number
  setEstimateId: number
  disableSelect = new FormControl(false)

  private transformDate(date: any): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  getNode(gsfx: any) {
    this.section = 1
    this.documentGsfx = gsfx.id
    const object = this.getNodeForm.value
    this.params.getSvzWbsByLevelFilter(object).subscribe(
      (response: any[]) => {
        this.getNodeFlat = response
      },
    )
  }
  directionOptionSelected(event: any) {
    const object = event.value
    this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
      (response: any[]) => {
        this.directionOptionId = response
      },
    )
  }
  objectOptionSelected(event: any) {
    const object = event.value
    this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
      (response: any[]) => {
        this.objectOptionId = response
      },
    )
  }
  systemOptionSelected(event: any) {
    const object = event.value
    this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
      (response: any[]) => {
        this.systemOptionId = response
      },
    )
  }
  workPackagesOptionSelected(event: any) {
    const object = event.value
    this.params.getSvzWbsByLevelFilter({ parent_id: object }).subscribe(
      (response: any[]) => {
        this.workPackagesOptionId = response
      },
    )
  }
  addDocumentGsfx() {
    const object = this.documentGsfxForm.value
    this.params.addDocumentGsfx(object).subscribe(
      (response: any[]) => {
        this.show.showBlock[1] = false
        console.log(response)
    },
  )}
  addEstimateGsfx() {
    const object = this.estimateForm.value
    this.params.addEstimate(object).subscribe(
      (response: any[]) => {
        this.show.showBlock[1] = false
        console.log(response)
    },
  )}

  displayedColumns: string[] = ['serial_number', 'system_code_wbs', 'system_name', 'number_package_code_wbs', 'name_work_package', 'cypher', "replacement_code", "change_code", "design_working_documentation", "section_code", "estimate_name", "estimate_developer", "estimate_status", "estimate_cost_2001", "estimate_cost_current_prices", 'action'];
  displayedColumns2: string[] = ['serial_number', 'system_code_wbs', 'system_name', 'number_package_code_wbs', 'name_work_package', 'cypher', "replacement_code", "change_code", "design_working_documentation", "section_code", "estimate_name", "estimate_developer", "estimate_status", "estimate_cost_2001", "estimate_cost_current_prices", 'action'];
  dataSource = new MatTableDataSource<any>([]);
  dataSource2 = new MatTableDataSource<any>([]);
  registerEstimateIsBufer() {
    this.params.getRegisterEstimatesIsBufer().subscribe(
      (response: any) => {
        // Assuming your data is inside the 'data' property
        this.dataSource2.data = response.data;
        // console.log(this.dataSource2.data);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  registerEstimate() {
    this.params.getRegisterEstimates().subscribe(
      (response: any) => {
        // Assuming your data is inside the 'data' property
        this.dataSource.data = response.data;
        console.log(this.dataSource.data);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  approve(element) {
    let docId = element.id
    if(docId) {
      let isBufer = 
        {
          title: element.title,
          id: docId,
          is_buffer: false
        }
      
      this.params.updateRegisterEstimatesIsBufer(docId, isBufer).subscribe(
        (data: any) => {
          this.theme.info = 'Утверждено.'
          setTimeout(() => {
            this.theme.info = null;
          }, 5000);
          this.registerEstimate();
          this.registerEstimateIsBufer();
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
      console.log(isBufer)
    }
  }

  //Получить данные
  getDataGSFXEstimate(): void {
    this.params.getDataGSFXEstimate().subscribe(
      (data: any) => {
        this.dataRegisterEstimatesGSFX.data = Object.values(data.data);
        console.log(this.dataRegisterEstimatesGSFX.data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file, file.name);
      this.params.uploadRegisterEstimatesGSFX(formData).subscribe(
        (data) => {
          this.theme.openSnackBar('Смета загружена');
          // this.getDataRegisterEstimatesGSFX()

        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }

  onFilesSelected(fileInput: any): void {
    this.files = Array.from(fileInput.files || []);
  }

  uploadFiles(): void {
    this.uploading = true;
    this.uploadedFilesCount = 0;

    const totalFiles = this.files.length;

    this.files.forEach(file => {
      this.params.uploadFilesGSFXEstimate(file)
        .subscribe(
          event => {
            if (event.type === 'progress') {
              this.progress = Math.round((this.uploadedFilesCount + event.value) / totalFiles);
            } else if (event.type === 'complete') {
              this.uploadedFilesCount++;
              this.uploadedFiles.push(file);
              console.log(`Загружено файлов: ${this.uploadedFilesCount} из ${totalFiles}`);
              if (this.uploadedFilesCount === totalFiles) {
                this.uploading = false;
                this.progress = 100;
                // Дополнительные действия после завершения загрузки, если необходимо
              }
            }
          },
          error => {
            console.error('Ошибка при загрузке файлов:', error);
            this.uploading = false;
          }
        );
    });
  }

  // Остаток по договорам// подробнее
  moreRegisterEstimatesGSFX(element: number): void {
    // this.router.navigate(['/new-estimated-item', requestId]);
    const setId = element
    if (element) {
      this.router.navigate(['/new-estimated-item', setId]);
    }
  }

  showDetails(element: any) {
    const setId = element
    if (element) {
      // console.log(setId)
      // this.estimated.setSetId(this.estId);
      this.router.navigate(['new-estimated/more', setId]);
    }
  }
  getSections2(element: number) {
    this.setEstimateId = element
    this.section = 2
  }


  getWbs(): void {
    const filter = { "name": "", "level": 3, "parent_id": null, "type_node": "" };
    this.params.getSvzWbsByLevelFilter(filter).subscribe(
      (data) => {
        console.log(data)
        this.wbsData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getPackageWBS(selectedId: any) {
    console.log(selectedId);
    const filter = { "name": "", "level": '', "parent_id": selectedId, "type_node": "" };
    this.params.getSvzWbsByLevelFilter(filter).subscribe(
      (data) => {
        console.log('Пакет', data)
        this.PackageWBSData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getStatusEstimate() {
    this.params.getPostEstimateStatus().subscribe(
      (data) => {
        console.log('Статусы', data['data'])
        this.StatusEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getTypeEstimate() {
    this.params.getPostEstimateType().subscribe(
      (data) => {
        console.log('Типы', data['data'])
        this.TypeEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getCodeEstimate() {
    this.params.getCodeEstimate().subscribe(
      (data) => {
        console.log('Коды', data['data'])
        this.CodeEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getUserEstimate() {
    this.params.getUserEstimate().subscribe(
      (data) => {
        console.log('Поставщик и разработчик ', data['data'])
        this.UserEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getReasonEstimate() {
    this.params.getPostEstimateReason().subscribe(
      (data) => {
        console.log('Основание ', data['data'])
        this.ReasonEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  getLevelEstimate() {
    this.params.getPostEstimateLevel().subscribe(
      (data) => {
        console.log('Уровни ', data['data'])
        this.LevelEstimateData = data['data'];
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }


  updateEstimates(data) {
    console.log(data)
    if (this.FormAddEditEstimates.valid) {
      this.params.updateEstimates(this.selectedtEstimat, data).subscribe(
        (data: any) => {

          // Находим индекс обновленного элемента
          const updatedItemIndex = this.dataSource.data.findIndex(item => item.id === data.id);
          console.log(data.id)
          // Обновляем этот элемент в массиве
          if (updatedItemIndex > -1) {
            this.dataSource.data[updatedItemIndex] = data;
          }
          // Обновляем таблицу с новым массивом данных
          this.dataSource.data = [...this.dataSource.data];


          const offcanvasElement = document.getElementById('EditAddScrolling');
          const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
          if (offcanvasInstance) {
            offcanvasInstance.hide();
          }
          this.theme.openSnackBar('Смета обновлена');
          this.editMode = false;
          this.selectedtEstimat = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  approvedToggleValue: boolean

  // Изменить смету
  editEstimates(element): void {
    this.FormAddEditEstimates.reset()
    this.editMode = true;
    this.selectedtEstimat = element.id;
    this.getPackageWBS(element.wbs_system_id)
    // Заполняем форму редактирования данными из элемента
    this.FormAddEditEstimates.patchValue(element);
    console.log(this.FormAddEditEstimates.value)
  }
}
