export interface GRANDEstimate {

  id: number
  guid: string
  document: number
  locnum: string
  constr: string
  stage: string
  description: string
  caption: string
  value: string

}


export interface RegisterEstimates {
  id: number
  cypher: any
  title: string
  date_created: string
  date_estimate: any
  base_cost: any
  current_cost: any
  delete_flag: boolean
  lft: number
  rght: number
  tree_id: number
  level: number
  parent: any
  wbs_system: number
  wbs_work_package: number
  status: number
  type_estimate: number
  code: number
  contractor: any
  developer: any
  estimate_reason: number
  estimate_level: number
  document_gsfx: number
  _links: Links
}

export interface Links {
  self: Self
  costs: Costs
}

export interface Self {
  href: string
}

export interface Costs {
  href: string
}
