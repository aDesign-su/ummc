import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApicontentService } from 'src/app/api.content.service';
import { ShowService } from 'src/app/helper/show.service';
import { ThemeService } from 'src/app/theme.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { PeriodicElement } from '../../references/analogy/analogy.component';
import { Estimates, EstimatesNode } from './estimates';
import * as jmespath from 'jmespath';
import { CommonService } from '../../budget/project-budget/common.service';

@Component({
  selector: 'app-estimated-positions',
  templateUrl: './estimated-positions.component.html',
  styleUrls: ['./estimated-positions.component.css']
})
export class EstimatedPositionsComponent implements OnInit {
  constructor(public theme: ThemeService,public show:ShowService, public isTitle: CommonService, private estim: ApicontentService, private formBuilder: FormBuilder) {
    this.getForm = this.formBuilder.group({
      _content: ''
      });
   }

  ngOnInit() {
    this.getSourceObjectBudget()
  }

  source: any[] = []
  getSourceObjectBudget() {
    const estimate = this.getForm.value;
    this.estim.getConsolidationEstimates(estimate).subscribe(
      (data: any[]) => {
        const est = "@.data[]";
        this.source = jmespath.search(data, est);
        this.dataSources.data = this.source;
        console.log(data)
      }
    );
  }

  onStatusSelected() {
    const selectedStatus = { estimate_status: this.selectStatus };
    console.log(selectedStatus);
    this.estim.getConsolidationEstimates(selectedStatus).subscribe(
      (data: any[]) => {
        const est = "@.data[]";
        this.source = jmespath.search(data, est);
        this.dataSources.data = this.source;
        console.log(data)
      }
    );
  }
  getForm: FormGroup;
  pkAll = ''
  selectStatus: string
  success: string = 'готово'
  inProcess: string = 'В процессе'
  inDevelopment: string = 'В разработке'
  
  displayedColumns: string[] = ['serial_number', 'name', "estimate_status", "estimate_cost_2001", "estimate_cost_current_prices"];
  dataSource = new MatTableDataSource<Estimates>([]);

  private _transformer = (node: Estimates, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      code_wbs: node.code_wbs,
      estimate_status: node.estimate_status,
      estimate_cost_2001: node.estimate_cost_2001,
      estimate_cost_current_prices: node.estimate_cost_current_prices,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<EstimatesNode>(
    node => node.level,
    node => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children
  );
  
  dataSources = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: EstimatesNode) => node.expandable;
}
