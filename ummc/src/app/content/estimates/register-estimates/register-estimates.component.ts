import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ApicontentService } from 'src/app/api.content.service';
import { ThemeService } from 'src/app/theme.service';
import { RegisterEstimates, RegisterEstimatesGSFX } from './register-estimates';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../budget/project-budget/common.service';
declare var bootstrap: any;

@Component({
  selector: 'app-register-estimates',
  templateUrl: './register-estimates.component.html',
  styleUrls: ['./register-estimates.component.css']
})
export class RegisterEstimatesComponent implements OnInit {
  dataRegisterEstimatesTable: MatTableDataSource<RegisterEstimates> = new MatTableDataSource<RegisterEstimates>();
  dataRegisterEstimatesGSFX:MatTableDataSource<RegisterEstimatesGSFX> = new MatTableDataSource<RegisterEstimatesGSFX>();
  displayedColumns: string[] = ['serial_number', 'system_code_wbs', 'system_name', 'number_package_code_wbs', 'name_work_package', 'code_number_estimate_total', "replacement_code", "change_code", "design_working_documentation", "section_code", "estimate_name", "estimate_developer", "estimate_status", "estimate_cost_2001", "estimate_cost_current_prices", 'action'];
  displayedColumnsGSFX: string[] = ['serial_number', 'name', 'number', 'cost_pz', 'caption'];
  FormAddEditEstimates: FormGroup;
  editMode: boolean
  selectedtEstimat: number;
  wbsData: any[] = [];
  PackageWBSData: any[] = [];
  constructor(public theme: ThemeService, private fb: FormBuilder, public isTitle: CommonService, public params: ApicontentService, private datePipe: DatePipe,) {

    this.FormAddEditEstimates = this.fb.group({
      wbs_system: new FormControl(''),
      // system_name: [''],
      wbs_work_package: [''],
      // name_work_package: [''],
      code_number_estimate_total: [''],
      replacement_code: [''],
      change_code: [''],
      design_working_documentation: [''],
      section_code: [''],
      estimate_name: [''],
      name_local_estimate_calculation: [''],
      estimate_developer: [''],
      estimate_status: [''],
      estimate_cost_2001: [''],
      labor_costs_main_wokers: [''],
      labor_costs_machine_operators: [''],
      smr_2001: [''],
      base_salary_amount: [''],
      costs_operation_machines_mechanisms: [''],
      overhead_amount: [''],
      estimated_profit_amount: [''],
      materials: [''],
      contractor_materials: [''],
      controlled_materials: [''],
      uncontrolled_materials: [''],
      customer_materials: [''],
      equipment: [''],
      contractor_equipment: [''],
      customer_equipment: [''],
      other_expenses_2001: [''],
      estimate_cost_current_prices: [''],
      smr_current_price: [''],
      base_salary_amount_current_price: [''],
      costs_operation_machines_mechanisms_current_price: [''],
      overhead_amount_current_price: [''],
      estimated_profit_amount_current_price: [''],
      materials_current_price: [''],
      contractor_materials_current_price: [''],
      controlled_materials_current_price: [''],
      uncontrolled_materials_current_price: [''],
      customer_materials_current_price: [''],
      equipment_current_price: [''],
      contractor_equipment_current_price: [''],
      customer_equipment_current_price: [''],
      costs_chapters_8_9_12: [''],
      temporary_buildings_structures: [''],
      winter_rise_prices: [''],
      snow_removal: [''],
      mechanisms_relocation: [''],
      costs_shift_work: [''],
      costs_fight_against_gnat: [''],
      electricity_cost_compensation: [''],
      commissioning_works: [''],
      travel_costs: [''],
      author_supervision: [''],
      object_insurance: [''],
      unexpected_expenses: [''],
      total_with_contractual_coefficient: [''],
      contractual_coefficient: [''],
      contractor_name: [''],
      no_contract_contractor: [''],
      receipt_date_doc_designer: [''],
      date_submission_comments: [''],
      no_letter: [''],
      reason_change: [''],
      no_date_letter: [''],
      designer: ['']
    });
  }


  ngOnInit() {
    this.getDataRegisterEstimates()
    this.getDataRegisterEstimatesGSFX()
    this.getWbs()
  }


  getWbs(): void {
    const filter = { "name": "", "level": 3, "parent_id": null, "type_node": "" };
    this.params.getSvzWbsByLevelFilter(filter).subscribe(
      (data) => {
        console.log(data)
        this.wbsData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  getPackageWBS(selectedId: any) {
    console.log(selectedId);
    const filter = { "name": "", "level": '', "parent_id": selectedId, "type_node": "" };
    this.params.getSvzWbsByLevelFilter(filter).subscribe(
      (data) => {
        console.log('Пакет', data)
        this.PackageWBSData = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }




  add() {
    this.editMode = false
    this.FormAddEditEstimates.reset()
  }

  private closeForm() {
    const offcanvasElement = document.getElementById('EditAddScrolling');
    const offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    if (offcanvasInstance) {
      offcanvasInstance.hide();
    }
  }
  // Полуить данные Реестр смет 
  getDataRegisterEstimates() {
    this.params.getRegisterEstimates().subscribe(
      (data) => {
        console.log(data)
        this.dataRegisterEstimatesTable.data = data;

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  // Полуить данные Реестр смет (GSFX)
  getDataRegisterEstimatesGSFX() {
    this.params.getRegisterEstimatesGSFX().subscribe(
      (data) => {
        console.log(data)
        this.dataRegisterEstimatesGSFX.data = data;

      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }

  private transformDate(date: any): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  editEstimates(element): void {
    this.editMode = true;
    this.selectedtEstimat = element.id;
    this.getPackageWBS(element.wbs_system)
    // Заполняем форму редактирования данными из элемента
    this.FormAddEditEstimates.setValue({
      wbs_system: element.wbs_system,
      // system_name: [''],
      wbs_work_package: element.wbs_work_package,
      // name_work_package: element.name_work_package,
      code_number_estimate_total: element.code_number_estimate_total,
      replacement_code: element.replacement_code,
      change_code: element.change_code,
      design_working_documentation: element.design_working_documentation,
      section_code: element.section_code,
      estimate_name: element.estimate_name,
      name_local_estimate_calculation: element.name_local_estimate_calculation,
      estimate_developer: element.estimate_developer,
      estimate_status: element.estimate_status,
      estimate_cost_2001: element.estimate_cost_2001,
      labor_costs_main_wokers: element.labor_costs_main_wokers,
      labor_costs_machine_operators: element.labor_costs_machine_operators,
      smr_2001: element.smr_2001,
      base_salary_amount: element.base_salary_amount,
      costs_operation_machines_mechanisms: element.costs_operation_machines_mechanisms,
      overhead_amount: element.overhead_amount,
      estimated_profit_amount: element.estimated_profit_amount,
      materials: element.materials,
      contractor_materials: element.contractor_materials,
      controlled_materials: element.controlled_materials,
      uncontrolled_materials: element.uncontrolled_materials,
      customer_materials: element.customer_materials,
      equipment: element.equipment,
      contractor_equipment: element.contractor_equipment,
      customer_equipment: element.customer_equipment,
      other_expenses_2001: element.other_expenses_2001,
      estimate_cost_current_prices: element.estimate_cost_current_prices,
      smr_current_price: element.smr_current_price,
      base_salary_amount_current_price: element.base_salary_amount_current_price,
      costs_operation_machines_mechanisms_current_price: element.costs_operation_machines_mechanisms_current_price,
      overhead_amount_current_price: element.overhead_amount_current_price,
      estimated_profit_amount_current_price: element.estimated_profit_amount_current_price,
      materials_current_price: element.materials_current_price,
      contractor_materials_current_price: element.contractor_materials_current_price,
      controlled_materials_current_price: element.controlled_materials_current_price,
      uncontrolled_materials_current_price: element.uncontrolled_materials_current_price,
      customer_materials_current_price: element.customer_materials_current_price,
      equipment_current_price: element.equipment_current_price,
      contractor_equipment_current_price: element.contractor_equipment_current_price,
      customer_equipment_current_price: element.customer_equipment_current_price,
      costs_chapters_8_9_12: element.costs_chapters_8_9_12,
      temporary_buildings_structures: element.temporary_buildings_structures,
      winter_rise_prices: element.winter_rise_prices,
      snow_removal: element.snow_removal,
      mechanisms_relocation: element.mechanisms_relocation,
      costs_shift_work: element.costs_shift_work,
      costs_fight_against_gnat: element.costs_fight_against_gnat,
      electricity_cost_compensation: element.electricity_cost_compensation,
      commissioning_works: element.commissioning_works,
      travel_costs: element.travel_costs,
      author_supervision: element.author_supervision,
      object_insurance: element.object_insurance,
      unexpected_expenses: element.unexpected_expenses,
      total_with_contractual_coefficient: element.total_with_contractual_coefficient,
      contractual_coefficient: element.contractual_coefficient,
      contractor_name: element.contractor_name,
      no_contract_contractor: element.no_contract_contractor,
      receipt_date_doc_designer: element.receipt_date_doc_designer ? new Date(element.receipt_date_doc_designer) : null,
      date_submission_comments: element.date_submission_comments ? new Date(element.date_submission_comments) : null,
      no_letter: element.no_letter,
      reason_change: element.reason_change,
      no_date_letter: element.no_date_letter,
      designer: element.designer
    });
    console.log(this.FormAddEditEstimates.value)
  }

  updateRegisterEstimates(id, data) {
    console.log('обновление', id)
    if (this.FormAddEditEstimates.valid) {
      //   console.log('Upadte to add:', formData);
      this.params.updateRegisterEstimates(id, data).subscribe(
        (data: any) => {
          this.closeForm()
          this.theme.openSnackBar('Смета обновлена');
          this.getDataRegisterEstimates()
          this.editMode = false;
          this.selectedtEstimat = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  createEstimate(data): void {
    console.log('Создали', data)
    if (this.FormAddEditEstimates.valid) {
      const formData: RegisterEstimates = data
      console.log('Data to add:', formData);
      this.params.addRegisterEstimates(formData).subscribe(
        (data: RegisterEstimates[]) => {
          this.closeForm()
          this.theme.openSnackBar('Смета добавлена');
          this.getDataRegisterEstimates()
          this.editMode = false;
          this.selectedtEstimat = null;
        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }
  createOrUpdateEstimate(): void {
    if (this.FormAddEditEstimates.valid) {
      const formData = { ...this.FormAddEditEstimates.value };
      if (formData.receipt_date_doc_designer) {
        formData.receipt_date_doc_designer = this.transformDate(formData.receipt_date_doc_designer);
      }
      if (formData.date_submission_comments) {
        formData.date_submission_comments = this.transformDate(formData.date_submission_comments);
      }

      if (this.editMode) {
        this.updateRegisterEstimates(this.selectedtEstimat, formData);
      }
      else {
        this.createEstimate(formData);
      }

    }
  }

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file, file.name);
      this.params.uploadRegisterEstimatesGSFX(formData).subscribe(
        (data) => {
          this.theme.openSnackBar('Смета загружена');
          this.getDataRegisterEstimatesGSFX()

        },
        (error: any) => {
          console.error('Error fetching data:', error);
        }
      );
    }
  }

}
