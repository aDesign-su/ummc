import {Component, OnInit, ViewChild} from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { GanttEditorComponent, GanttEditorOptions } from 'ng-gantt'
import {ApicontentService} from '../../api.content.service'
import {Wbs, GanttObject} from './home'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public editorOptions: GanttEditorOptions;
  rawData: Wbs[];
  budgets = [];
  data: any;
  financingFactData: any = [];
  financingPlanData: any = [];
  utilizationFactData: any = [];
  utilizationPlanData: any = [];

  selectedData = {
    plan: this.financingPlanData,
    fact: this.financingFactData
  }

  @ViewChild("editor") editor: GanttEditorComponent;

  loading: boolean = false;
  scheduleLevels = ['Проект', 'Направление', 'Объект', 'Система', 'Пакет работ или смета'];
  colors = {0: "ggroupblack", 1: "gtaskblue", 2: "gtaskred", 3: "gtaskyellow", 4: "gtaskgreen"};

  constructor(public theme: ThemeService, private contentService: ApicontentService, private ganttService: ApicontentService) {
  }

  ngOnInit() {
    // this.getWbs();
    // this.getBudget();
    // this.getFinancingPlan();
    // this.getFinancingFact();
    // this.getUtilizationPlan();
    // this.getUtilizationFact();
  }

  // getWbs(): void {
  //   this.ganttService.getWbs().subscribe(
  //     (data: Wbs[]) => {
  //       console.log(data)
  //       this.rawData = data
  //       this.data = this.convertToGanttObject(data);
  //       //Стили не применяются
  //       this.editorOptions = {
  //         vFormat: 'month',
  //         vEditable: true,
  //         vLang: 'ru',
  //       };
  //       // this.changeLoadingStatus();
  //     },
  //     (error: any) => {
  //       console.error('Error fetching data:', error);
  //     }
  //   );
  // }

  convertToGanttObject(data: Wbs[], planData = this.financingPlanData, factData = this.financingFactData, toggleLevel: number = 1) {
    let objects = [];
    this.data = [];
    data.forEach(wbs => {
      objects.push({
        pID: wbs.id,
        pName: wbs.code_wbs + " " + wbs.name,
        pStart: wbs.start_date,
        pEnd: wbs.end_date,
        pClass: this.colors[wbs.level],
        pLink: "",
        pMile: 0,
        pRes: "",
        pComp: this.calculatePercent(wbs, planData, factData),
        pGroup: wbs.children ? 1 : 0,
        pParent: wbs.level == 0 ? 0 : wbs.parent_id,
        pOpen: wbs.level < toggleLevel ? 1 : 0,
        pDepend: "",
        pCaption: "",
        pNotes: "",
        level: wbs.level
      });
      objects.push(...this.convertToGanttObject(wbs.children));
    })
    return objects;
  }

  calculatePercent(wbs: Wbs, planData = this.financingPlanData, factData = this.financingFactData) {
    const factDataAmount = this.getAmount(factData, wbs);
    const planDataAmount = this.getAmount(planData, wbs);

    if (factDataAmount == 0 || planDataAmount == 0) {
      return factDataAmount / planDataAmount * 100;
    } else {
      return 0;
    }
  }

  getAmount(data: any[], wbs: Wbs) {
    for (const element of data) {
      if (wbs.name == element['name']) {
        return element['amount'];
      }
    }
    return 0;
  }

  changeLoadingStatus() {
    this.loading = !this.loading;
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');

  }

  toggleToLevel(level: number = 0) {
    this.data = this.convertToGanttObject(this.rawData, this.selectedData.plan, this.selectedData.fact, level);
  }

  setSelectedData(planData, factData) {
    this.selectedData.plan = planData;
    this.selectedData.fact = factData;
    this.convertToGanttObject(this.rawData, this.selectedData.plan, this.selectedData.fact);
  }

  getBudget() {
    this.contentService.getRegisterBudget().subscribe(
      (data: any) => {
        this.budgets = data['data']
        console.log(data);
      }, error => {
        console.log(error);
      }
    )
  }

  getFinancingPlan() {
    this.contentService.getPlan().subscribe(
      (data: any) => {
        console.log(data)
        this.financingPlanData = data;
      }, error => {
        console.log(error)
      }
    )
  }

  getFinancingFact() {
    this.contentService.getFinancingFact().subscribe(
      (data: any) => {
        console.log(data)
        this.financingFactData = data;
      }, error => {
        console.log(error)
      }
    )
  }

  getUtilizationPlan() {
    this.contentService.getUtilizationPlan().subscribe(
      (data: any) => {
        console.log(data)
        this.utilizationPlanData = data;
      }, error => {
        console.log(error)
      }
    )
  }

  getUtilizationFact() {
    this.contentService.getUtilizationFact().subscribe(
      (data: any) => {
        console.log(data)
        this.utilizationFactData = data;
      }, error => {
        console.log(error)
      }
    )
  }

}
