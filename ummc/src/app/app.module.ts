//===== Angular =======
  import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
  import { LOCALE_ID, NgModule } from '@angular/core';
  import { BrowserModule } from '@angular/platform-browser';
  import { RouterModule, Routes } from '@angular/router';
  import { CommonModule, DatePipe } from '@angular/common';
  import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//===== Main Page =======
  import { AppComponent } from './app.component';
  import { MenuComponent } from './menu/menu.component';
  import { LoginComponent } from './authentication/login/login/login.component';
  import { SignupComponent } from './authentication/login/signup/signup.component';
  import { RestoreComponent } from './authentication/login/restore/restore.component';
  import { ProfileComponent } from './authentication/login/profile/profile.component';

//======== Content =========
  //======== Главная =========
  import { HomeComponent } from './content/home/home.component';

  //======== Портфель проектов =========
  import { ProjectRegisterComponent } from './content/project-register/project-register.component';
  import { ProjectPortfolioComponent } from './content/project-portfolio/project-portfolio.component';
    //======== Доп.страницы (подробнее) =========
    import { InnerTableComponent } from './content/project-portfolio/inner-table/inner-table.component';

  //======== Синхронизация =========
  import { PrimaveraComponent } from './content/primavera/primavera.component';
      //======== Доп.страницы (подробнее) =========
     import { SynchPrimaveraComponent } from './content/primavera/synch.primavera/synch.primavera.component';
  //======== Проект =========
  import { WbsComponent } from './content/budget/wbs/wbs.component';
  import { ParamsProjectComponent } from './content/poject/params-project/params-project.component';
    //======== Доп.страницы (подробнее) =========
    import { CardParamsOptionsModule } from "./content/poject/params-project/card-params-options/card-params-options.module";

  import { ScheduleComponent } from './content/poject/project-schedule/schedule.component';
  import { ProjectTeamHandbookComponent } from './content/references/project-team-handbook/project-team-handbook.component';

  //======== Бюджет =========
  import { IntangibleComponent } from './content/budget/intangible/intangible.component';
  import { ProjectBudgetComponent } from './content/budget/project-budget/project-budget.component';
    //======== Доп.страницы (подробнее) =========
    import { ObjectBudgetComponent } from './content/budget/project-budget/object-budget/object-budget.component';
    import { TransposedBudgetComponent } from './content/budget/project-budget/transposed-budget/transposed-budget.component';
    import { TotalCostsComponent } from './content/budget/project-budget/total-costs/total-costs.component';
    import { InflationBudgetComponent } from './content/budget/project-budget/inflation/inflation.component';
    import { NmaComponent } from './content/budget/project-budget/nma/nma.component';
    import { CompareBudgetComponent } from './content/budget/project-budget/compare-budget/compare-budget.component';

  import { CardObjComponent } from './content/budget/card-obj/card-obj.component';
    //======== Доп.страницы (подробнее) =========
    import { MoreCardObjComponent } from './content/budget/card-obj/more.card-obj/more.card-obj.component';
    import { MoreCardObjModule } from './content/budget/card-obj/more.card-obj/more.card-obj.module';

  import { SvzDocumentsComponent } from './content/budget/svz-documents/svz-documents.component';
    //======== Доп.страницы (подробнее) =========
    import { SvzComponent } from './content/budget/svz-documents/svz/svz.component';

  import { TotalProjectCostsComponent} from './content/budget/total-project-costs/total-project-costs.component';
  import { Operating_costsComponent } from './content/budget/operating_costs/operating_costs.component';
  import { RiskComponent } from './content/budget/risk/risk.component';
  import { EstimatedComponent } from './content/budget/estimated/estimated.component';
    //======== Доп.страницы (подробнее) =========
    import { MoreEstComponent } from './content/budget/estimated/moreEst/moreEst.component';
    import { EquipmentEstComponent } from './content/budget/estimated/equipmentEst/equipmentEst.component';
    import { PackageEstComponent } from './content/budget/estimated/packageEst/packageEst.component';
    import { ERatioComponent } from './content/budget/estimated/e-ratio/e-ratio.component';

  import { RegistryComponent } from './content/budget/registry/registry.component';
  import { ReserveManagementComponent } from './content/budget/reserve-management/reserve-management.component';
  import { ReserveComponent } from './content/budget/reserve-management/reserve/reserve.component';

  //======== Финансирование =========
  import { PlanComponent } from './content/financing/plan/plan.component';
  import { FinancingFactComponent } from './content/financing/fact/financing-fact.component';

  //======== Освоение =========
  import { UtilizationFactComponent } from './content/utilization/fact/utilization-fact.component';

  //======== Отчётность =========
  import { MasteringComponent } from './content/reporting/mastering/mastering.component';
  import { MasteringCauseComponent } from './content/reporting/mastering-cause/mastering-cause.component';
  import { ReportingFinancingComponent } from './content/reporting/reporting-financing/reporting-financing.component';
  import { ReportingFinancingCauseComponent } from './content/reporting/reporting-financing-cause/reporting-financing-cause.component';
  import { FactorAnalysisComponent } from './content/reporting/factor-analysis/factor-analysis.component';
    //======== Доп.страницы (подробнее) =========
    import { InnerTableFactorAnalysisComponent } from './content/reporting/factor-analysis/inner-table-factor-analysis/inner-table-factor-analysis.component';
    import { WaterfallChartFactorAnalysisComponent } from './content/reporting/factor-analysis/waterfall-chart-factor-analysis/waterfall-chart-factor-analysis.component';
  import { PortfolioReportComponent } from './content/reporting/portfolio-report/portfolio-report.component';
  import { StausProjectComponent } from './content/reporting/portfolio-report/staus-project/staus-project.component';
      //======== Доп.страницы (подробнее) =========
    import { PortfolioReportMoreComponent } from './content/reporting/portfolio-report/portfolio-report-more/portfolio-report-more.component';


  //======== Сметы =========
  import { RegisterEstimatesComponent } from './content/estimates/register-estimates/register-estimates.component';
  import { EstimatedPositionsComponent } from './content/estimates/estimated-positions/estimated-positions.component';
  import{NewEstimatesComponent} from './content/estimates/new-estimates/new-estimates.component'
  import { EstimateMoreComponent } from './content/estimates/new-estimates/estimate-more/estimate-more.component';
  import { NewEstimateMoreComponent } from './content/estimates/new-estimates/new-estimate-more/new-estimate-more.component';

  //======== Контрактация =========
    //======== Разделительная ведомость =========
    import{ SeparationSheetComponent} from './content/contracting/separation-sheet/separation-sheet.component'
      //======== Доп.страницы (подробнее) =========
      import { SeparationSheetItemComponent} from './content/contracting/separation-sheet/separation-sheet-item/separation-sheet-item.component';

    //======== Контрактные пакеты =========
    import { ContractPackagesComponent } from './content/contracting/contract-packages/contract-packages.component';
    import { ContractPackagesItemComponent } from './content/contracting/contract-packages/contract-packages-item/contract-packages-item.component';
    import { DirectoryContractPackagesComponent } from './content/contracting/contract-packages/directory-contract-packages/directory-contract-packages.component';

  //======== Управление договорами =========
  import { RegisterContractsComponent } from './content/contract/register-contracts/register-contracts.component';
    //======== Доп.страницы (подробнее) =========
    import { CounterpartyComponent } from './content/contract/register-contracts/counterparty/counterparty.component';
    import { RegMoreComponent } from './content/contract/register-contracts/reg.more/reg.more.component';
    import { FactMoreComponent } from './content/contract/register-contracts/fact.more/fact.more.component';
    import { PaymentsModule } from './content/contract/register-contracts/payments/payments.module';
    import { ActsModule } from './content/contract/register-contracts/acts/acts.module';
    import { RegWbsModule } from './content/contract/register-contracts/reg.wbs/reg.wbs.module';

  import {SdrToContractsComponent} from './content/contract/sdr-to-contracts/sdr-to-contracts.component'
    //======== Доп.страницы (подробнее) =========
    import { SdrToContractsMoreComponent } from './content/contract/sdr-to-contracts/sdr-to-contracts-more/sdr-to-contracts-more.component';

  import { BalanceUnderContractsComponent } from './content/contract/balance_under_contracts/balance_under_contracts.component';
  import {BalanceUnderContractsMoreComponent} from './content/contract/balance_under_contracts/BalanceUnderContractsMore/BalanceUnderContractsMore.component'
  //======== ГПКС / ГПФКС =========
  import { GpcsDocumentsComponent } from './content/gpcs/gpcs-documents/gpcs-documents.component';
  import { GpcsComponent } from './content/gpcs/gpcs/gpcs.component';
  import { GpcfsComponent } from './content/gpcs/gpcfs/gpcfs.component';
  import { GcrPlanComponent } from './content/gpcs/gcr.plan/gcr.plan.component';
  import { PpoComponent } from './content/gpcs/ppo/ppo.component';

  //======== Справочники =========
  import { AnalogyComponent } from './content/references/analogy/analogy.component';
    //======== Доп.страницы (подробнее) =========
    import { MoreComponent } from './content/references/analogy/more/more.component';
    import { EquipmentModule } from './content/references/analogy/equipment/equipment.module';
    import { PackageModule } from './content/references/analogy/package/package.module';

  import { RatioComponent } from './content/references/ratio/ratio.component';
  import { CategoryComponent } from './content/references/category/category.component';
    //======== Доп.страницы (подробнее) =========
    import { ObjModule } from './content/references/category/obj/obj.module';
    import { EquipModule } from './content/references/category/equip/equip.module';
    import { PackModule } from './content/references/category/pack/pack.module';

  import { CurrencyComponent } from './content/references/currency/currency.component';
  import { RegionsComponent } from './content/references/regions/regions.component';
  import { MacroComponent } from './content/references/macro/macro.component';
    //======== Доп.страницы (подробнее) =========
    import { CurrencyModule } from './content/references/macro/currency/currency.module';
    import { MetalsModule } from './content/references/macro/metals/metals.module';
    import { InflationModule } from './content/references/macro/inflation-icp/inflation.module';
    import { InflationIpcModule } from './content/references/macro/inflation-ipc/inflation-ipc.module';
    import { ParamModule } from './content/references/macro/param/param.module';
  import { RoleReferenceComponent } from './content/references/role-reference/role-reference.component';
  import { IntangibleAssetsComponent } from './content/references/intangible-assets/intangible-assets.component';
  import { GeneralProjectCostsComponent } from './content/references/general-project-costs/general-project-costs.component';
  import { SourceValueComponent } from './content/references/source-value/source-value.component';
  import { DirectoryOfUnitPricesComponent } from './content/references/directory-of-unit-prices/directory-of-unit-prices.component';
  import { DirectoryOfUnitsOfMeasurementComponent } from './content/references/directory-of-units-of-measurement/directory-of-units-of-measurement.component';


//===== Service Page =======
import { ThemeService } from './theme.service'
import { AuthenticationService } from './authentication/login/login/authentication.service'
import { AuthGuard } from './authentication/login/helpers/auht.guard';
import { AuthInterceptor } from './authentication/login/helpers/auth.interceptor';

//======== Module page =========
import { HeaderComponent } from './header/header.component';

//======== help component =========
import { DeleteDialogComponent } from './helper/DeleteDialogComponent/DeleteDialogComponent.component';
import { CloseBlockDirective } from './helper/closeBlock.directive';
import { CustomPaginatorIntl } from './helper/custom-paginator-intl';
import { ExpandableTextComponent } from './helper/expandable-text/expandable-text.component';
import { CustomNumberPipe } from './helper/customNumber/customNumber.pipe';
import { DateFilterDirective } from './helper/dateFilter.directive';

//======== Page 404 =========
import { NotFoundComponent } from 'src/404/not-found/not-found.component';
import { ServerErrorComponent } from 'src/500/server-error/server-error.component';

//======== Plfgins =========
import { IConfig, NgxMaskModule } from 'ngx-mask'
import { NgApexchartsModule } from 'ng-apexcharts';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgGanttEditorModule } from 'ng-gantt';
// import { TextMaskModule } from 'angular2-text-mask';

//==== Primeng Design ====
import { TreeTableModule } from 'primeng/treetable';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { ContextMenuModule } from 'primeng/contextmenu';
import { TableModule } from 'primeng/table';
import { MenuModule } from 'primeng/menu';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TreeModule } from 'primeng/tree';
import { ScrollerModule } from 'primeng/scroller';

//==== Material Design ====
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import {MatButtonToggleGroup, MatButtonToggleModule} from '@angular/material/button-toggle';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatChipsModule } from '@angular/material/chips';
import { MatRadioModule } from '@angular/material/radio';
import { MatBadgeModule } from '@angular/material/badge';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectFilterModule } from 'mat-select-filter';
import { PlanUtilizationComponent } from './content/utilization/plan/utilization-plan.component';
import { AlertComponent } from './helper/alert/alert.component';
import { ResourceDirectoryComponent } from './content/references/resource-directory/resource-directory.component';
import { TypicalAssetsComponent } from './content/references/typical-assets/typical-assets.component';
import { ZimonoProjectComponent } from './content/references/zimono-project/zimono-project.component';







const appRoutes: Routes = [
  //======== Auth page =========
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'restore', component: RestoreComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },

  //======== Content =========
      //======== Главная =========
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },

      //======== Портфель проектов =========
      { path: 'project-register', component: ProjectRegisterComponent, canActivate: [AuthGuard] },
      { path: 'project-portfolio', component: ProjectPortfolioComponent,canActivate: [AuthGuard] },

      //======== Синхронизация =========
      { path: 'primavera', component: PrimaveraComponent },

      //======== Проект =========
      { path: 'wbs', component: WbsComponent },
      { path: 'params-project', component: ParamsProjectComponent },
      { path: 'schedule', component: ScheduleComponent },
      { path: 'team-handbook', component: ProjectTeamHandbookComponent },

      //======== Бюджет =========
      { path: 'nma', component: IntangibleComponent },
      { path: 'project-budget', component: ProjectBudgetComponent },
          //======== Доп.страницы (подробнее) =========
          { path: 'project-budget/more', component: ObjectBudgetComponent },
          { path: 'project-budget/transposed-budget', component: TransposedBudgetComponent },
          { path: 'compare-budget', component: CompareBudgetComponent },

      { path: 'card-obj', component: CardObjComponent, canActivate: [AuthGuard] },
          //======== Доп.страницы (подробнее) =========
          { path: 'registry-obj/more', component: MoreCardObjComponent },

      { path: 'svz-documents', component: SvzDocumentsComponent },
          //======== Доп.страницы (подробнее) =========
          { path: 'svz', component: SvzComponent },

      { path: 'total-project-costs', component: TotalProjectCostsComponent,},
      { path: 'operating-costs', component: Operating_costsComponent , canActivate: [AuthGuard]},
      { path: 'risk', component: RiskComponent, canActivate: [AuthGuard]  },
      { path: 'estimated', component: EstimatedComponent },
          //======== Доп.страницы (подробнее) =========
          { path: 'estimated/more', component: MoreEstComponent },

      { path: 'registry', component: RegistryComponent },


      { path: 'reserve-management', component: ReserveManagementComponent, canActivate: [AuthGuard] },
      //======== Доп.страницы (подробнее) =========
            { path: 'reserve-management/reserve', component: ReserveComponent, canActivate: [AuthGuard] },


      //======== Финансирование =========
      { path: 'plan', component: PlanComponent}, // ???
      { path: 'financing-plan', component: PlanComponent},
      { path: 'financing-fact', component: FinancingFactComponent},

      //======== Освоение =========
      { path: 'utilization-fact', component: UtilizationFactComponent},
      { path: 'utilization-plan', component: PlanUtilizationComponent},
      //======== Отчётность =========
      { path: 'mastering', component: MasteringComponent },
      { path: 'mastering-cause', component: MasteringCauseComponent },
      { path: 'reporting-financing', component: ReportingFinancingComponent,},
      { path: 'reporting-financing-cause', component: ReportingFinancingCauseComponent,},
      { path: 'factor-analysis', component: FactorAnalysisComponent,canActivate: [AuthGuard]},
      { path: 'portfolio-report', component: PortfolioReportComponent,canActivate: [AuthGuard]},
            //======== Доп.страницы (подробнее) =========
            { path: 'portfolio-report/:id', component: PortfolioReportMoreComponent, canActivate: [AuthGuard]},
            { path: 'status-project', component: StausProjectComponent,canActivate: [AuthGuard]},


      //======== Сметы =========
      { path: 'register-estimates', component: RegisterEstimatesComponent , canActivate: [AuthGuard]},
      { path: 'estimated-positions', component: EstimatedPositionsComponent,},
      { path: 'new-estimated',component:NewEstimatesComponent, canActivate: [AuthGuard]},
      //======== Доп.страницы (подробнее) =========
              { path: 'new-estimated-item/:id', component: EstimateMoreComponent , canActivate: [AuthGuard]},
              { path: 'new-estimated/more/:id', component: NewEstimateMoreComponent, canActivate: [AuthGuard]},


      //======== Контрактация =========
      //======== Разделительная ведомость =========
      { path: 'separation-sheet', component:SeparationSheetComponent , canActivate: [AuthGuard]},
           //======== Доп.страницы (подробнее) =========
           { path: 'separation-sheet-item/:id', component: SeparationSheetItemComponent },


      //======== Контрактные пакеты =========
      { path: 'contract-packages', component:ContractPackagesComponent , canActivate: [AuthGuard]},
            //======== Справочник =========
            { path: 'directory-contract-packages', component:DirectoryContractPackagesComponent , canActivate: [AuthGuard]},
            //======== Доп.страницы (подробнее) =========
            { path: 'contract-packages-item/:id', component: ContractPackagesItemComponent, canActivate: [AuthGuard] },



      //======== Управление договорами =========
      { path: 'register-contracts', component: RegisterContractsComponent, canActivate: [AuthGuard] },
          //======== Доп.страницы (подробнее) =========
          { path: 'register-contracts/code', component: CounterpartyComponent },
          { path: 'register-contracts/more', component: RegMoreComponent },
          { path: 'register-contracts/more/fact/more', component: FactMoreComponent },
      { path: 'sdr-contracts', component: SdrToContractsComponent, canActivate: [AuthGuard] },
      //======== Доп.страницы (подробнее) =========
          { path: 'sdr-contracts/more', component: SdrToContractsMoreComponent },

      { path: 'balance-under-contracts', component: BalanceUnderContractsComponent, canActivate: [AuthGuard] },
      //======== Доп.страницы (подробнее) =========
          { path: 'balance-under-contracts/:id', component: BalanceUnderContractsMoreComponent , canActivate: [AuthGuard]},


      //======== ГПКС / ГПФКС =========
      { path: 'gpcs-documents', component: GpcsDocumentsComponent },
      { path: 'gpcs', component: GpcsComponent },
      { path: 'gpfcs', component: GpcfsComponent },
      { path: 'gkr', component: GcrPlanComponent },
      { path: 'ppo', component: PpoComponent },

      //======== Справочники =========
      { path: 'analogy', component: AnalogyComponent },
          //======== Доп.страницы (подробнее) =========
          { path: 'analogy/more', component: MoreComponent },

      { path: 'difficulty-factor', component: RatioComponent },
      { path: 'category', component: CategoryComponent,},
      { path: 'currency', component: CurrencyComponent,},
      { path: 'regions', component: RegionsComponent,},
      { path: 'macro', component: MacroComponent },
      { path: 'role-reference', component: RoleReferenceComponent },
      { path: 'intangible-assets', component: IntangibleAssetsComponent, canActivate: [AuthGuard] },
      { path: 'typical-assets', component: TypicalAssetsComponent, canActivate: [AuthGuard] },
      { path: 'general-project-costs', component: GeneralProjectCostsComponent },
      { path: 'source-value', component: SourceValueComponent },
      { path: 'directory-resources', component: ResourceDirectoryComponent, canActivate: [AuthGuard] },
      { path: 'directory-unit-prices', component: DirectoryOfUnitPricesComponent, canActivate: [AuthGuard] },
      { path: 'units', component: DirectoryOfUnitsOfMeasurementComponent, canActivate: [AuthGuard] },
      { path: 'zimono', component: ZimonoProjectComponent },

  //======== Page 404 / 500 =========
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
  { path: '500', component: ServerErrorComponent }
];
const maskConfig: Partial<IConfig> = {};
@NgModule({
    declarations: [
      //======== main component =========
      AppComponent,
      //======== help component =========
      CustomNumberPipe,
      CloseBlockDirective,
      DateFilterDirective,
      DeleteDialogComponent,
      AlertComponent,
      //======== Home page =========
        HomeComponent,
      //======== Auth page =========
        LoginComponent,
        SignupComponent,
        RestoreComponent,
        ProfileComponent,
      //======== Module page =========
        MenuComponent,
        HeaderComponent,
      //======== Content =========
        //======== Синхронизация =========
          PrimaveraComponent,
          //======== Доп.страницы (подробнее) =========
            SynchPrimaveraComponent,

        //======== Портфель проектов =========
          ProjectRegisterComponent,
          ProjectPortfolioComponent,
          //======== Доп.страницы (подробнее) =========
            InnerTableComponent,

        //======== Проект =========
          WbsComponent,
          ParamsProjectComponent,
          ScheduleComponent,
          ProjectTeamHandbookComponent,
        //======== Бюджет =========
          IntangibleComponent,
          ProjectBudgetComponent,
          //======== Доп.страницы (подробнее) =========
            ObjectBudgetComponent,
            TransposedBudgetComponent,
            TotalCostsComponent,
            InflationBudgetComponent,
            NmaComponent,
            CompareBudgetComponent,
            
          CardObjComponent,
          //======== Доп.страницы (подробнее) =========
            MoreCardObjComponent,

          SvzDocumentsComponent,
          //======== Доп.страницы (подробнее) =========
            SvzComponent,

          TotalProjectCostsComponent,
          Operating_costsComponent,
          RiskComponent,
          EstimatedComponent,
          //======== Доп.страницы (подробнее) =========
            MoreEstComponent,
            EquipmentEstComponent,
            PackageEstComponent,
            ERatioComponent,

          RegistryComponent,

          //Управление резервами
          ReserveManagementComponent,
          //======== Доп.страницы (подробнее) =========
            ReserveComponent,
        //======== Финансирование =========
          PlanComponent,
          FinancingFactComponent,
        //======== Освоение =========
          UtilizationFactComponent,
          PlanUtilizationComponent,
        //======== Отчётность =========
          MasteringComponent,
          MasteringCauseComponent,
          ReportingFinancingComponent,
          ReportingFinancingCauseComponent,
          //======== Доп.страницы (подробнее) =========
            PortfolioReportMoreComponent,
          FactorAnalysisComponent,
          //======== Доп.страницы (подробнее) =========
            InnerTableFactorAnalysisComponent,
            WaterfallChartFactorAnalysisComponent,
          PortfolioReportComponent,
          StausProjectComponent,
      //======== Сметы =========
          RegisterEstimatesComponent,
          EstimatedPositionsComponent,
          NewEstimatesComponent,
          EstimateMoreComponent,
          NewEstimateMoreComponent,
      //======== Контрактация =========
          SeparationSheetComponent,
          SeparationSheetItemComponent,
          ContractPackagesComponent,
          DirectoryContractPackagesComponent,
          ContractPackagesItemComponent,
      //======== Управление договорами =========
        RegisterContractsComponent,
        //======== Доп.страницы (подробнее) =========
          CounterpartyComponent,
          RegMoreComponent,
          FactMoreComponent,
        SdrToContractsComponent,
        //======== Доп.страницы (подробнее) =========
          SdrToContractsMoreComponent,
        BalanceUnderContractsComponent,
        //======== Доп.страницы (подробнее) =========
        BalanceUnderContractsMoreComponent,
      //======== ГПКС / ГПФКС =========
        GpcsDocumentsComponent,
        GpcsComponent,
        GpcfsComponent,
        PpoComponent,
        GcrPlanComponent,
      //======== Справочники =========
        AnalogyComponent,
        //======== Доп.страницы (подробнее) =========
          MoreComponent,

        RatioComponent,
        CategoryComponent,
        CurrencyComponent,
        RegionsComponent,
        MacroComponent,
        RoleReferenceComponent,
        IntangibleAssetsComponent,
        GeneralProjectCostsComponent,
        ExpandableTextComponent,
        SourceValueComponent,
        ResourceDirectoryComponent,
        TypicalAssetsComponent,
        DirectoryOfUnitPricesComponent,
        DirectoryOfUnitsOfMeasurementComponent,
        ZimonoProjectComponent
      ],
  imports: [
    NgxMaskModule.forRoot(maskConfig),
    // TextMaskModule,
    NgApexchartsModule,
    HighchartsChartModule,
    MatFormFieldModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    CommonModule, MatButtonToggleModule,
    TreeTableModule, ToastModule, DialogModule, ButtonModule, MultiSelectModule, InputTextModule, ContextMenuModule, PaymentsModule, ActsModule, RegWbsModule, ScrollerModule, MoreCardObjModule, ObjModule, EquipModule, PackModule,
    PackageModule, EquipmentModule,
    MatSelectFilterModule, MatInputModule, MatExpansionModule, MatIconModule, MatSidenavModule, MatSelectModule, MatButtonModule, MatSlideToggleModule, MatStepperModule, MatCardModule, MatDividerModule, MatTooltipModule, MatMenuModule, MatPaginatorModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatTabsModule, MatCheckboxModule, MatListModule, MatTreeModule, MatProgressSpinnerModule, MatToolbarModule, MatProgressBarModule, MatSortModule, MatChipsModule,
    CardParamsOptionsModule, CurrencyModule, MetalsModule, InflationModule, InflationIpcModule, TableModule, ButtonModule, MenuModule, CheckboxModule, InputSwitchModule, MatSnackBarModule, TreeModule, MatAutocompleteModule, MatRadioModule, MatDialogModule, MatBadgeModule, ParamModule, NgGanttEditorModule,
    ],
  exports: [MenuComponent],
  providers: [
    ThemeService,
    AuthenticationService,
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide:MAT_DATE_LOCALE,useValue:'ru-RU' },
    { provide: MatPaginatorIntl, useClass: CustomPaginatorIntl }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
