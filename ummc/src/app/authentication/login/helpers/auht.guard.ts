import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(): boolean {
    const token = localStorage.getItem('access');
    if (token) {
      return true;
    } else {
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 2000);
      return false;
    }
  }
}
