import { Component, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ThemeService } from 'src/app/theme.service';
import { AuthService } from './../../auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userName!: string
  hide = true;
  isCard = false
  isCard_rights = false

  changePasswordForm!: FormGroup;
  error!: string;
  successMessage!: string;

  security() {
    this.isCard = !this.isCard
  }
  rights() {
    this.isCard_rights = !this.isCard_rights
  }

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }


  constructor(private formBuilder: FormBuilder,public theme: ThemeService,public AuthService: AuthService) {
    this.changePasswordForm = this.formBuilder.group({
      old_password: ['', Validators.required],
      password: ['', Validators.required, this.characterLengthValidator(8, 20)],
      confirmed_password: ['', Validators.required]
    },{ 
      asyncValidators: this.passwordMatchValidator 
    })
   }

   characterLengthValidator(minLength: number, maxLength: number): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      const value = control.value;
      const length = value ? value.length : 0;
  
      if (length >= minLength && length <= maxLength) {
        return of(null); // Возвращаем Observable с null, если длина символов находится в заданных пределах
      } else {
        return of({ invalidLength: true }); // Возвращаем Observable с объектом ошибки, если длина символов не соответствует требованиям
      }
    };
  }

  passwordMatchValidator: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    const password = control.get('password');
    const confirmed_password = control.get('confirmed_password');
  
    if (password && confirmed_password && password.value === confirmed_password.value) {
      return of(null); // Пароли совпадают, возвращаем null
    } else {
      return of({ passwordMismatch: true }); // Пароли не совпадают, возвращаем ошибку
    }
  };

  onSubmit() {
    const old_password = this.changePasswordForm.value.old_password;
    const password = this.changePasswordForm.value.password;
    const confirmed_password = this.changePasswordForm.value.confirmed_password;
    
    this.AuthService.changePassword(old_password, password, confirmed_password).subscribe(
      () => {
        this.successMessage = 'Пароль был успешно изменён!';
        this.changePasswordForm.reset();
        setTimeout(() => {
          this.successMessage = ''
        }, 5000);
      },
      error => {
        this.error = error;
        setTimeout(() => {
          this.error = ''
        }, 5000);
      }
    );
  }

  ngOnInit() {
    let token = this.getTokenFromLocalStorage()
    const token_parts = token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.userName = token_decoded.user_id;
  }
  getTokenFromLocalStorage(): any {
    return localStorage.getItem('access');
  }
}
