import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';
import { signup } from './signup';
import { ApicontentService } from 'src/app/api.content.service';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  constructor(private http: HttpClient,private api:ApicontentService) { }
  
  // baseUrl = 'http://darkdes-django-z38bs.tw1.ru/api/v1'; // internet
  // baseUrl = 'https://asbudget.ugmk.com/api/api/v1'; // local

  Register(user: signup) {
    return this.http.post(`${this.api.baseAuthUrl}/registration/`, user).pipe(
      catchError((error) => {
        if (error.status === 400) {
          return throwError('Пользователь с таким именем пользователя уже существует.');
        } else {
          return throwError('Не удалось зарегистрироваться. Попробуйте еще раз.');
        }
      })
    );
  }
}
