import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, map, Observable, throwError } from 'rxjs';
import { ApicontentService } from '../api.content.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private httpOptions: any;

  // текущий JWT токен
  public token: any;

  // время окончания жизни токена
  public token_expires: any;

  // логин пользователя
  public username: any;

  // сообщения об ошибках авторизации
  public errors: any = [];

  // currentUserValue: any;

  constructor(private http: HttpClient, private router: Router, private api: ApicontentService) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access')
      })
    };
  }

  // baseUrl = 'http://darkdes-django-z38bs.tw1.ru/api/v1'  // internet
  // baseUrl = 'https://asbudget.ugmk.com/api/api/v1'  // local

  // Вход
  slogin(username: string, password: string): Observable<any> {
    const loginData = { username, password };
    return this.http.post<any>(`${this.api.baseAuthUrl}/token/`, loginData)
      .pipe(
        map(response => { 
          localStorage.setItem('access', response.access);
          localStorage.setItem('refresh', response.refresh);
          this.updateData(response['refresh']);
          if (response.access) {
            this.router.navigate(['/home']);
          } else {
            this.router.navigate(['/login']);
          }
          return response;
        }),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 401) {
            return throwError('Invalid credentials');
          } else {
            return throwError('Something went wrong');
          }
        })
      );
  }

  // Восстановление пароля
  resetPassword(email: string): Observable<any> {
    return this.http.post(`${this.api.baseAuthUrl}/reset-password/send-mail/`, { email });
  }
  restorePassword(token: string, password: string, confirmed_password: string): Observable<any> {
    return this.http.post(`${this.api.baseAuthUrl}/reset-password/verify-code/`, { token, password, confirmed_password });
  }
  // Изменение пароля
  changePassword(old_password: string, password: string, confirmed_password: string): Observable<any> {
    const payload = { old_password, password, confirmed_password };
    return this.http.put(`${this.api.baseAuthUrl}/change-password/`, payload, this.httpOptions);
  }
  // обновление JWT токена
  public refreshToken() {
    this.http.post(`${this.api.baseAuthUrl}/token/refresh/`, JSON.stringify({ token: this.token }), this.httpOptions.headers).subscribe(
      (data: any) => {
        this.updateData(data['refresh']);
      },
      err => {
        this.errors = err['error'];
      }
    );
  }

  public logout() {
    this.token = null;
    this.token_expires = null;
    this.username = null;
  }

  private updateData(access: any) {
    this.token = access;
    this.errors = [];

    // декодирование токена для получения логина и времени жизни токена
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
  }
}
