import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../theme.service';
import { AuthService } from '../authentication/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  toggle:any = true;
  status = 'Enable'; 
  openSetting: boolean = false
  interval: any;
  
  constructor(public themeService: ThemeService,public theme: ThemeService, public AuthService: AuthService, private router: Router) { }

  ngOnInit() {
    this.toggle = localStorage.getItem('theme');
    this.themeService.theme = localStorage.getItem('dark')

    const isCollapsedState = this.themeService.getState('isCollapsed');
    this.themeService.isCollapsed = isCollapsedState === true;

    const isCollapsedStateDrop = this.themeService.getState('drop-up');
    this.themeService.isDropUp = isCollapsedStateDrop === true;

    const isCollapsedStateFull = this.themeService.getState('full-menu-min');
    this.themeService.isFull = isCollapsedStateFull === true;

    const isCollapsedStateBtn = this.themeService.getState('disabled');
    this.themeService.isButtonDisabled = isCollapsedStateBtn

    const isMenu = this.themeService.getState('isMenuCollapsed');
    this.themeService.isMenu = isMenu

    const isButton = this.themeService.getState('set-wrapper-min');
    this.themeService.isButton = isButton

    this.themeService.loadAccordionStates();
  }
  startTime() {
    this.interval = setTimeout(() => {
      window.location.reload();
    }, 0.005);
  }
  get dark() {
    return this.themeService.theme === 'dark';
  }

  setting() {
    this.openSetting = !this.openSetting
  }

  set dark(enabled: boolean) {
    
    if (this.themeService.theme = 'dark') {
      this.themeService.theme = enabled ? 'dark' : null;
      this.themeService.theme = localStorage.setItem('dark', this.themeService.theme);
    }
    if (this.themeService.theme = null) {
      this.themeService.theme = enabled ? 'dark' : null;
      this.themeService.theme = localStorage.removeItem('dark');
    }
    // this.themeService.theme = !this.themeService.theme;
    // this.themeService.theme = localStorage.setItem('dark', this.themeService.theme);
    this.themeService.theme = enabled ? 'dark' : null;
    console.log(this.themeService.theme)
  }

  data() {
    if (this.toggle = true) {
      this.toggle = localStorage.setItem('theme', this.toggle);
    }
    this.toggle = !this.toggle;
    this.status = this.toggle ? 'Enable' : 'Disable';
   }
   test() {
    this.toggle = localStorage.removeItem('theme');
   }
   logout() {
    this.AuthService.logout();
    localStorage.removeItem('access');
    this.router.navigate(['/login'])
   }
}
