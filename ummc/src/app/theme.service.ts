import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Injectable()
export class ThemeService {

constructor(private _snackBar: MatSnackBar) { }
accordionStates: { [key: string]: boolean } = {};
private style: string = 'btn-brand-none';
private menu: string = 'menu';
private drop: string = 'drop-up';
private full: string = 'full-menu-min';
private button: string = 'set-wrapper-min'
public isAccordionExpanded = true;
isButtonDisabled = false;
isCollapsed = false;
isDropUp = false;
isFull = false;
isMenu = false;
isButton = false;

access: string = ''
error: string = ''
info: string = ''

  getButton(): string {return this.button;}
  setButton(button: string) {this.button = button;}

  getStyle(): string {return this.style;}
  setStyle(style: string) {this.style = style;}

  getMenu(): string {return this.menu;}
  setMenu(menu: string) {this.menu = menu;}

  getDrop(): string {return this.drop;}
  setDrop(drop: string) {this.drop = drop;}

  getFull(): string {return this.full;}
  setFull(full: string) {this.full = full;}

  setAccordionExpanded(expanded: boolean) {
    this.isAccordionExpanded = expanded;
  }

  get theme(): any {
    return document.documentElement.getAttribute('theme');
  }

  set theme(name: any) {
    document.documentElement.setAttribute('theme', name);
  }

  saveState(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  saveDropUp(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  saveFull(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  saveBtn(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  saveMenu(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  saveButton(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  saveAccordionState(key: string, expanded: boolean): void {
    this.accordionStates[key] = expanded;
    localStorage.setItem('accordionStates', JSON.stringify(this.accordionStates));
  }
  loadAccordionStates(): void {
    const states = localStorage.getItem('accordionStates');
    this.accordionStates = states ? JSON.parse(states) : {};
  }
  onAccordionOpened(key: string): void {
    this.saveAccordionState(key, true);
  }
  onAccordionClosed(key: string): void {
    this.saveAccordionState(key, false);
  }
  getState(key: string): any {
    const value = localStorage.getItem(key);
    return value ? JSON.parse(value) : null;
  }

  removeState(key: string): void {
    localStorage.removeItem(key);
  }
  toggleCollapse(saveState = true,isDropUp = true,saveBtn = true, saveMenu = true, saveButton = true): void {
    this.isButtonDisabled = !this.isButtonDisabled;
    this.isCollapsed = !this.isCollapsed;
    this.isDropUp = !this.isDropUp;
    this.isMenu = !this.isMenu;
    this.isButton = !this.isButton;

    this.accordionStates = {}; // Очистить объект состояний

    Object.keys(this.accordionStates).forEach((key) => {
      this.accordionStates[key] = false;
    });
    
    if (saveState || isDropUp || saveBtn || saveMenu || saveButton) {
      this.saveState('isCollapsed', this.isCollapsed);
      this.saveDropUp('drop-up', this.isDropUp);
      this.saveBtn('disabled', this.isButtonDisabled);
      this.saveBtn('isMenuCollapsed', this.isMenu);
      this.saveButton('set-wrapper-min', this.isButton);
    
    } else {
      this.removeState('isCollapsed');
      this.removeState('drop-up');
      this.removeState('disabled');
      this.removeState('isMenuCollapsed');
      this.removeState('set-wrapper-min');
    }
  }
  toggleCollapseFull(saveFull = true): void {
    this.isFull = !this.isFull;

    if (saveFull) {
      this.saveFull('full-menu-min', this.isFull);
      this.saveBtn('disabled', this.isButtonDisabled);
    } else {
      this.removeState('full-menu-min');
      this.removeState('disabled');
    }
  }
  setWidthMin () {
    this.toggleCollapse()
    this.setDrop('drop-dwn');
    this.setMenu('menu-min');
    this.setButton('set-wrapper-min');
    this.setStyle('btn-brand-close');
  }

  openSnackBar(msg: string) {
    this._snackBar.open(msg, 'Закрыть', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
      duration: 5000 // время отображения в миллисекундах
    });
  }

  initTable<T>(): MatTableDataSource<T> {
    return new MatTableDataSource<T>([]);
  }
  
  position: string = 'right';
  isMaximized: boolean = false;
  toggleMaximize() {
    this.isMaximized = !this.isMaximized;
  }
}
