import { HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerErrorService {
  constructor(private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 500) {
          // Перенаправляем на страницу ошибки 500
          this.router.navigate(['/500']);
          console.error('Internal Server Error: ', error);
          // Можно также добавить вывод сообщения пользователю
          // this.toastr.error('Internal Server Error. Please try again later.', 'Error');
        }
        return throwError(error);
      })
    );
  }
  
}
