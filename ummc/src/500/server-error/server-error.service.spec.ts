/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ServerErrorService } from './server-error.service';

describe('Service: ServerError', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerErrorService]
    });
  });

  it('should ...', inject([ServerErrorService], (service: ServerErrorService) => {
    expect(service).toBeTruthy();
  }));
});
