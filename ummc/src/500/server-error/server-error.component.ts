import { Component, OnInit } from '@angular/core';
import { ServerErrorService } from './server-error.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.css']
})
export class ServerErrorComponent implements OnInit {
  constructor(private errorService:ServerErrorService, private router:Router, private location: Location) { }

  ngOnInit() {
  }
  goBack() {
    this.location.back();
  }
  goHome() {
    this.router.navigate(['/home'])
  }
}
